grammar AgentsGrammar;

// Lexer
// Condition
When: 'When' | 'when';
ISee: 'I see' | 'i see' | 'seeing';
IFind: 'I find' | 'i find' | 'finding';
LessStrength: 'less strength';
MoreStrength: 'more strength';
IHave: 'i have' | 'I have' | 'having';
CurrentWeaponHigherStrength: 'current weapon has higher strength';
CurrentWeaponLowerStrength: 'current weapon has lower strength';

// Subjects
Enemy: 'an enemy' | 'enemy';
Monster: 'a monster' | 'monster';
Attribute: 'an attribute' | 'attribute';
Weapon: 'a weapon' | 'weapon';

// Actions
Run: 'run' | 'Run';
Fight: 'fight' | 'attack' | 'Fight' | 'Attack';
Flee: 'flee' | 'Flee';
Pickup: 'pick it up' | 'pick up' | 'Pick it up' | 'Pick up';
Replace: 'replace' | 'Replace';

// If
If: 'if';
Otherwise: 'otherwise';

// Priority
Priority: 'priorities' | 'priority' | 'prio';
PriorityDestroyFlag: 'destroy flag';
PriorityDefendFlag: 'defend flag';
PriorityEnemies: 'kill enemies' | 'kill enemy';
PriorityMonsters: 'kill monsters' | 'kill monster';
PriorityExplore: 'explore';
Set: 'Set' | 'set';
Main: 'main';
Inventory: 'inventory';
Armor: 'armor';

// Simple rules
DropWeaponIfNotUsed: 'Drop weapon if not used' | 'drop weapon if not used';
WeaponAsDefault: 'weapon as default';
Best: 'best';
Worst: 'worst';

// Food
Food: 'food';
Apple: 'apple' | 'Apple';
Cookie: 'cookie' | 'Cookie';
Soup: 'soup' | 'Soup';

// Rest
Strength: 'strength' | 'Strength';
Comma: ',';
And: 'and';
Use: 'use' | 'Use';
LessThan: 'less than' | 'under' | '<';
GreaterThan: 'more than' | 'greater than' | 'over' | '>';
Dot: '.';
WS: [ \t\r\n]+ -> skip;
Number: [0-9]+;

// Parser
agentScript: ((conditionRule | priorityRule | (simpleFoodRule | simpleWeaponDefaultRule)) Dot)+;

conditionRule: When condition subject Comma? action ifClause? | action When condition subject Comma? ifClause? Dot;
condition: ISee | IFind;
subject: Enemy | Monster | Attribute | Weapon;
action: Run | Fight | Pickup | Flee | Replace;
ifClause: If ifCondition (Comma? Otherwise Comma? action)?;
ifCondition:
    IHave? (LessStrength | MoreStrength) |
    CurrentWeaponLowerStrength | CurrentWeaponHigherStrength |
    subject? strengthComparison;

priorityRule: Set (mainPriority | inventoryPriority) ifClause?;
mainPriority: Main Priority (Comma? mainPriorityOption)+;
mainPriorityOption: PriorityDestroyFlag | PriorityDefendFlag | PriorityEnemies | PriorityMonsters | PriorityExplore;
inventoryPriority: Inventory Priority ((Comma | And)? inventoryPriorityOption)+ ;
inventoryPriorityOption: Number (Food | Armor | Weapon);

simpleFoodRule: Use food If strengthComparison;
simpleWeaponDefaultRule: Use (Best | Worst) WeaponAsDefault;

food: Food | Apple | Cookie | Soup;
strengthComparison: Strength (LessThan | GreaterThan) Number;
