package nl.aimsites.nzbvuq.game.inputhandler.actions.plugins;

/**
 * Contains movement directions used in the MoveAction
 *
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public enum MoveDirection {
  NORTH,
  EAST,
  SOUTH,
  WEST,
  INIT
}
