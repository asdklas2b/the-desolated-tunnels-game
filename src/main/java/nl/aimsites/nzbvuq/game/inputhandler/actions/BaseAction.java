package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import nl.aimsites.nzbvuq.game.state.GameState;

import java.util.ArrayList;
import java.util.List;

/**
 * Base class for action logic
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
public abstract class BaseAction extends IAction {

  private List<String> targetOptions;

  public void setTargetOptions(List<String> targetOptions) {
    this.targetOptions = targetOptions;
  }

  public List<String> getTargetOptions() {
    return targetOptions;
  }

  /**
   * Responsible for registering all possible targets of an action
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  public BaseAction() {
    this.registerTargets();
  }

  /**
   * Checks if the target is a valid option for the action
   *
   * @param target A target for which to execute the action on/for
   * @return If the target is valid for the action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  public Boolean isValidTarget(String target) {
    if (targetOptions == null) {
      return true;
    }

    return this.targetOptions.contains(target) || target.contains(",");
  }

  /**
   * If needed, this method sends the action to all other players in the game
   *
   * @param target    The target that should be executed
   * @param character Character that executed the target
   * @return True if the action was send successfully, otherwise false
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  protected boolean sendActionToOtherPlayers(String target, Character character) {
    if (GameState.getInstance()
      .getPlayer()
      .getIdentificationKey()
      .equals(character.getIdentificationKey())) {
      var action =
        new Action(
          this.getClass(),
          target,
          character.getIdentificationKey(),
          character.getCurrentPosition().getCoordinates());

      return GameState.getInstance().sendAction(action);
    }

    return true;
  }

  /**
   * Returns a boolean that determines if the target can be null.
   *
   * @return default false because nothing is nullable with exception of action that overwrite this
   * function.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public boolean isTargetNullable() {
    return false;
  }

  /**
   * Registers all possible targets of an action Setting the possible targets to null allows all
   * target input
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  public abstract void registerTargets();
}
