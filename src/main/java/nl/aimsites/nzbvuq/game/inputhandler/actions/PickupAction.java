package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.Entity;
import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.entities.items.Armor;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Storage;
import nl.aimsites.nzbvuq.game.entities.items.attributes.special.Flag;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.inputhandler.ActionResponse;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.Command;
import nl.aimsites.nzbvuq.game.state.GameState;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Contains the game logic for the pickup action
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
@Command({"pickup", "get", "take", "retrieve"})
public class PickupAction extends BaseAction {

  /**
   * Executes the game logic bound to the action/target combination
   *
   * @param target    The target for/on which to apply the action
   * @param character The character for which the action need to be performed.
   * @return A string response for feedback as to what happened with the action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public String execute(String target, Character character) {
    var playerTile = character.getCurrentPosition();

    Entity item = findEntity(target, character, playerTile, true);

    if (!sendActionToOtherPlayers(target, character)) {
      return "You did not pick up the item, network error.";
    }

    if (!(item instanceof BaseItem)) {
      return "Your current position does not have a " + target + " available.";
    }

    if (item instanceof Flag) {
      if (((Flag) item).getTeamNumber() == ((Player) character).getTeamNumber()) {
        return "You have picked up your team it's flag";
      }
      return new StringBuilder("You picked up the other team it's flag, you have won!\n").append(" __    __  ____  ____  \n").append("|  |__|  ||    ||    \\ \n").append("|  |  |  | |  | |  _  |\n").append("|  |  |  | |  | |  |  |\n").append("|  `  '  | |  | |  |  |\n").append(" \\      /  |  | |  |  |\n").append("  \\_/\\_/  |____||__|__|").toString();
    }

    character.getInventory().addItem((BaseItem) item);
    character.getCurrentPosition().removeEntity(item);

    StringBuilder response = new StringBuilder(((BaseItem) item).pickUp());
    if (item instanceof Armor) {
      response.append(" The ").append(item.getName());
      response
        .append(" increased both your strength and max strength with ")
        .append(((Armor) item).getMaxStrengthAddition())
        .append(". You now have a max strength of ")
        .append(character.getMaxStrength())
        .append(".");
      character.addStrength(((Armor) item).getStrengthAddition());
    }

    return response.toString();
  }

  private Entity findEntity(String target, Character character, ReachableTile playerTile, boolean remove) {
    var item =
      playerTile.getOccupyingEntities().stream()
        .filter(x -> target.equals(x.getName()))
        .findFirst()
        .orElse(null);

    boolean tileContainStorage =
      playerTile.getOccupyingEntities().stream().anyMatch(entity -> entity instanceof Storage);

    if (item == null && tileContainStorage) {
      item = getItemFromStorage(target, playerTile, remove);
    } else if (item instanceof BaseItem) {
      if (remove) {
        character.getCurrentPosition().removeEntity(item);
      }
    }
    return item;
  }

  @Override
  public ActionResponse executeResponsibly(String target, Character character) {
    Entity entity = findEntity(target, character, character.getCurrentPosition(), false);

    if (entity instanceof Flag) {
      if (((Player) character).getTeamNumber() != ((Flag) entity).getTeamNumber()) {
        if (((Flag) entity).getTeamNumber() == GameState.getInstance().getPlayer().getTeamNumber()) {
          return new ActionResponse(
            execute(target, character),
            new StringBuilder("Your team (" + character.getName() + ") has picked up the flag, you have won!\n").append(" __    __  ____  ____  \n").append("|  |__|  ||    ||    \\ \n").append("|  |  |  | |  | |  _  |\n").append("|  |  |  | |  | |  |  |\n").append("|  `  '  | |  | |  |  |\n").append("  \\      /  |  | |  |  |\n").append("   \\_/\\_/  |____||__|__|").toString()
          );
        }
        return new ActionResponse(
          execute(target, character),
          new StringBuilder("The opposing team (" + character.getName() + ") picked up your flag, you have lost!\n").append(" _       ___   _____ ______ \n").append("| |     /   \\ / ___/|      |\n").append("| |    |     (   \\_ |      |\n").append("| |___ |  O  |\\__  ||_|  |_|\n").append("|     ||     |/  \\ |  |  |  \n").append("|     ||     |\\    |  |  |  \n").append("|_____| \\___/  \\___|  |__| ").toString()
        );
      }
      return new ActionResponse(
        execute(target, character),
        "Our flag has been picked up by " + character.getName()
      );
    }
    return new ActionResponse(
      execute(target, character),
      ""
    );
  }

  /**
   * The method gets an item from storages if it exists
   *
   * @param target     The target for/on which to apply the action
   * @param playerTile The tile where the player is standing
   * @return A Entity object or null
   * @author Azez Salami (A.salami@student.han.nl)
   */
  private Entity getItemFromStorage(String target, ReachableTile playerTile, boolean remove) {
    Entity item = null;
    List<Entity> storages =
      playerTile.getOccupyingEntities().stream()
        .filter(entity -> entity instanceof Storage)
        .collect(Collectors.toList());

    for (Entity storage : storages) {
      Storage storageOfTypeStorage = (Storage) storage;
      item =
        storageOfTypeStorage.getItems().stream()
          .filter(x -> x.getName().equals(target))
          .findFirst()
          .orElse(null);

      if (item != null && remove) {
        storageOfTypeStorage.removeItem((BaseItem) item);
        break;
      }
    }
    return item;
  }

  /**
   * Registers all possible targets of an action
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  @Override
  public void registerTargets() {
    this.setTargetOptions(null);
  }
}
