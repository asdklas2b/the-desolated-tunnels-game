package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.Command;

/**
 * Contains the game logic for the help action
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
@Command({"help", "h"})
public class HelpAction extends BaseAction {
  /**
   * Registers all possible targets of an action
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  @Override
  public void registerTargets() {}

  /**
   * Returns a boolean that determines if the target can be null. In this case it is true.
   *
   * @return true because when look is called without target the players looks around at the current
   *     tile.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  @Override
  public boolean isTargetNullable() {
    return true;
  }

  /**
   * Returns all possible commands and their representative description
   *
   * @param target The target for/on which to apply the action
   * @param character The character for which the action need to be performed. doesnt matter for this action
   * @return String response of the action result
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  @Override
  public String execute(String target, Character character) {
    return new StringBuilder("Possible commands:")
        .append(System.lineSeparator())
        .append("--------------------")
        .append(System.lineSeparator())
        .append("'attack' \t 'target' \t\t\t Attacks the target with the equipped weapon")
        .append(System.lineSeparator())
        .append("'consume' \t 'consumable' \t\t Consumes a consumable from the inventory")
        .append(System.lineSeparator())
        .append("'drop' \t\t 'item' \t\t\t Drops an item from the inventory")
        .append(System.lineSeparator())
        .append("'equip' \t 'wearable' \t\t Equips a wearable item from the inventory")
        .append(System.lineSeparator())
        .append("'look' \t\t \t\t\t\t\t Look around your current surroundings")
        .append(System.lineSeparator())
        .append("'move' \t\t 'direction' \t\t Tries to move to the direction, possible directions:")
        .append(System.lineSeparator())
        .append(
            "\t\t\t\t\t\t\t\t (north, east, south, west, up, down, left, right, forward, backward)")
        .append(System.lineSeparator())
        .append(
            "'open' \t\t 'openable entity' \t Opens the entity, possible entities: (door, doorway, inventory, backpack, items)")
        .append(System.lineSeparator())
        .append("'pickup' \t 'item' \t\t\t Picks up the specified item from the floor")
        .append(System.lineSeparator())
        .append("'use' \t\t 'item' \t\t\t Uses the item from inventory or equipment")
        .append(System.lineSeparator())
        .append("'Strength' \t\t \t\t\t\t\t Get you current amount of Strength")
        .toString();
  }
}
