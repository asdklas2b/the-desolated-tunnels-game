package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.Entity;
import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import nl.aimsites.nzbvuq.game.graph.tiles.UnreachableTile;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.Command;
import nl.aimsites.nzbvuq.game.state.GameState;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Contains the game logic for the look action
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
@Command({"look", "watch", "inspect", "l"})
public class LookAction extends BaseAction {

  private Graph world;

  /**
   * Executes the game logic bound to the action/target combination
   *
   * @param target The target for/on which to apply the action
   * @param character The character for which the action need to be performed.
   * @return A string response for feedback as to what happened with the action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  @Override
  public String execute(String target, Character character) {
    GameState instance = GameState.getInstance();
    world = instance.getCurrentWorld();

    if (target == null) {
      return lookAtPlayerSurroundings(character);
    }

    BaseItem item = character.getInventory().searchForItem(target);
    if (item != null) {
      return item.look();
    }

    if (Arrays.asList(StrengthAction.class.getAnnotation(Command.class).value()).contains(target)) {
      return (new StrengthAction()).execute(null, character);
    }

    Entity entity =
        character.getCurrentPosition().getOccupyingEntities().stream()
            .filter(x -> target.equals(x.getName()))
            .findFirst()
            .orElse(null);

    if (entity != null) {
      if (entity instanceof BaseItem) {
        return ((BaseItem) entity).look();
      }
      // TODO: 4-1-2021 implement response when the entity is not of type BaseItem. For instance a
      // monster or a storage place.
    }
    // TODO: 4-1-2021 implement action for when the item ask for does not exist. Not even in the
    // json files.

    return "You look in your inventory and around you but you cannot seem to find a"
        + (target.matches("^[aeiouAEIOU]\\w+") ? "n " : " ")
        + target
        + ".";
  }

  /**
   * Returns a string with the details about the surroundings of the player
   *
   * @param character Player to check the surroundings for
   * @return The string with details about the surroundings
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  private String lookAtPlayerSurroundings(Character character) {
    String tileName = character.getCurrentPosition().getClass().getSimpleName().toLowerCase();
    StringBuilder response =
        new StringBuilder("You are currently standing in a ")
            .append(tileName)
            .append(". ")
            .append(System.lineSeparator());


    List<Entity> surroundingEntities =
        character.getCurrentPosition().getOccupyingEntities().stream()
            .filter(entity -> !entity.equals(character))
            .collect(Collectors.toList());


    if (surroundingEntities.size() > 0) {
      response
          .append("You see")
          .append(surroundingEntities.size() > 1 ? " the following:" : " a(n) :");

      for (Entity item : surroundingEntities) {
        response
            .append(System.lineSeparator())
            .append("- ")
            .append(item.getName().substring(0, 1).toUpperCase())
            .append(item.getName().substring(1));
      }
    } else {
      response
          .append("There don't appear to be any items around you.")
          .append(System.lineSeparator());
    }

    response
        .append(System.lineSeparator())
        .append(getSurroundingTileInformation(character));

    return response.toString();
  }

  /**
   * Gets the description of the surrounding tiles
   *
   * @param character current player from the game instance
   * @return Description of the surrounding tiles
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  private String getSurroundingTileInformation(Character character) {
    StringBuilder surroundingsResponse = new StringBuilder();
    ReachableTile currentPosition = character.getCurrentPosition();

    surroundingsResponse
        .append("You look around your current position (" + currentPosition.getX() + "," + currentPosition.getY() +  ") and see the following: ")
        .append(System.lineSeparator());

    String northTileDescription =
        getTileInformation(currentPosition.getX(), currentPosition.getY() + 1, "north");
    String eastTileDescription =
        getTileInformation(currentPosition.getX() + 1, currentPosition.getY(), "east");
    String southTileDescription =
        getTileInformation(currentPosition.getX(), currentPosition.getY() - 1, "south");
    String westTileDescription =
        getTileInformation(currentPosition.getX() - 1, currentPosition.getY(), "west");

    surroundingsResponse
        .append(northTileDescription)
        .append(eastTileDescription)
        .append(southTileDescription)
        .append(westTileDescription);

    return surroundingsResponse.toString();
  }

  /**
   * Gets information of the specified tile
   *
   * @param x x coordinate of where to look
   * @param y y coordinate of where to look
   * @param directionName Name for the direction you are looking in
   * @return General information of the tile
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  private String getTileInformation(int x, int y, String directionName) {
    if (!world.tileExists(x, y)) {
      return "- You can't see whats "
          + directionName
          + System.lineSeparator()
          + "  You sense the void consumed this part of the world"
          + System.lineSeparator();
    }

    Optional<Tile> possibleTileExists;
    try {
      possibleTileExists = world.findTile(x, y);
    } catch (ArrayIndexOutOfBoundsException e) {
      possibleTileExists = Optional.empty();
    }

    if (possibleTileExists.isEmpty()) {
      return "- You can't see whats "
          + directionName
          + System.lineSeparator()
          + "  You sense the void consumed this part of the world"
          + System.lineSeparator();
    }

    Tile northTile = possibleTileExists.get();

    if (northTile instanceof ReachableTile) {
      ReachableTile reachableTile = (ReachableTile) northTile;

      return "- You can reach the tile to the "
          + directionName
          + " its a "
          + reachableTile.getClass().getSimpleName()
          + System.lineSeparator();
    } else if (northTile instanceof UnreachableTile) {
      UnreachableTile unreachableTile = (UnreachableTile) northTile;

      return "- You can't reach the tile to the "
          + directionName
          + " its a "
          + unreachableTile.getClass().getSimpleName()
          + System.lineSeparator();
    } else {
      return "- You can't see whats "
          + directionName
          + System.lineSeparator()
          + "  You sense the void consumed this part of the world"
          + System.lineSeparator();
    }
  }

  /**
   * Returns a boolean that determines if the target can be null. In this case it is true.
   *
   * @return true because when look is called without target the players looks around at the current
   *     tile.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  @Override
  public boolean isTargetNullable() {
    return true;
  }

  /**
   * Registers all possible targets of an action
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  @Override
  public void registerTargets() {}
}
