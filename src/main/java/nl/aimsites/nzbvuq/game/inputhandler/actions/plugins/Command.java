package nl.aimsites.nzbvuq.game.inputhandler.actions.plugins;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Command annotation for matching command to IAction based classes
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Command {
  String[] value();
}
