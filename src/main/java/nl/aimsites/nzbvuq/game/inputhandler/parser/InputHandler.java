package nl.aimsites.nzbvuq.game.inputhandler.parser;

import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.inputhandler.PrintType;
import nl.aimsites.nzbvuq.game.inputhandler.actions.*;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.Command;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import nl.aimsites.nzbvuq.game.state.GameState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Implementation responsible for handling user input
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
public class InputHandler implements IInputHandler {

  private List<IAction> actions = null;

  /**
   * InputHandler constructor
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  public InputHandler() {
    registerActions();
  }

  /**
   * Parses a Scanner to to an action
   *
   * @param scanner a Scanner input which is used to derive a command and target to which to match
   *                an action.
   * @return returns the matched action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  public void handleInput(Scanner scanner) {
    if (!scanner.hasNext()) {
      return;
    }

    String command = scanner.next();

//    if (GameState.getInstance().getPlayer().getStrength() < 0) {
//      GameState.getInstance()
//        .getPrinter()
//        .println(
//          "You died, rest in peace",
//          PrintType.DEFAULT
//        );
//
//      return;
//    }

    for (IAction action : actions) {
      if (this.commandMatchesAction(action, command)) {
        String target = scanner.hasNext() ? scanner.next() : null;

        directExecute(command, target);

        return;
      }
    }
    GameState.getInstance()
      .getPrinter()
      .println(
        "Unknown command, type `help` for more information",
        PrintType.DEFAULT
      );
  }

  public void directExecute(String command, String target) {
    for (IAction action : actions) {
      if (this.commandMatchesAction(action, command)) {
        GameState.getInstance()
          .getPrinter()
          .println(
            command + " " + (target != null ? target : ""),
            PrintType.INPUT
          );

        if (target == null && !action.isTargetNullable()) {
          GameState.getInstance()
            .getPrinter()
            .println(
              "The "
                + action.getClass().getAnnotation(Command.class).value()[0]
                + " action can not be used without param. Type 'help' for more information about possible actions.",
              PrintType.DEFAULT
            );

          return;
        } else {
          GameState.getInstance()
            .getPrinter()
            .println(
              action.execute(
                target == null ? null : target.toLowerCase(),
                GameState.getInstance().getPlayer()
              ),
              PrintType.DEFAULT
            );
          return;
        }
      }
    }
  }


  /**
   * External actions can be executed with this function.
   *
   * @param action The action that needs to be executed
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public void handleAction(Action action) {
    BaseAction baseAction =
      (BaseAction)
        actions.stream()
          .filter(
            iAction ->
              iAction.getClass().getSimpleName().equals(action.getSimpleClassName()))
          .findFirst()
          .orElse(null);

    var player = GameState.getInstance().getPlayer(action.getCharacterId());
    Graph world = GameState.getInstance().getCurrentWorld();
    var tile = world.findTile(action.getPosX(), action.getPosY());

    tile.ifPresent(value -> {
      if (tile.get() instanceof ReachableTile) {
        removePlayerAtCurrentPosition(player);

        player.setCurrentPosition((ReachableTile) value);
        GameState.getInstance().spawnEntity(player, action.getPosX(), action.getPosY());
      }
    });

    System.out.println("Other player stands on point: x." + player.getCurrentPosition().getX() + " y." + player.getCurrentPosition().getY());
    if (baseAction != null) {
      var other = baseAction.executeResponsibly(action.getTarget(), player).getOther();
      if (other.length() > 0) {
        GameState.getInstance()
          .getPrinter()
          .println(
            "<- " + other,
            PrintType.OUTPUT
          );
      }
    }
  }

  /**
   * Removes the player at the current position if the player is currently on a tile
   *
   * @param player Player to remove at the current location
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  void removePlayerAtCurrentPosition(Player player) {
    if (player.getCurrentPosition() != null) {
      var currentPlayerPos = player.getCurrentPosition().getCoordinates();

      var currentPlayerTile =
        GameState.getInstance()
          .getCurrentWorld()
          .findTile(currentPlayerPos.getX(), currentPlayerPos.getY());

      currentPlayerTile.ifPresent(tile1 -> ((ReachableTile) tile1).removeEntity(player));
    }
  }

  /**
   * Registers all action classes
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  private void registerActions() {
    this.actions = new ArrayList<>();

    this.actions.add(new AttackAction());
    this.actions.add(new ConsumeAction());
    this.actions.add(new DropAction());
    this.actions.add(new EquipAction());
    this.actions.add(new LookAction());
    this.actions.add(new MoveAction());
    this.actions.add(new OpenAction());
    this.actions.add(new PickupAction());
    this.actions.add(new UseAction());
    this.actions.add(new HelpAction());
    this.actions.add(new StrengthAction());
  }

  /**
   * Looks up the class annotation to check if the command matches the action class
   *
   * @param action  The executable action which is checked against the command
   * @param command The string command with which to match the annotation
   * @return Returns if the command matches the action via its annotation
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  private boolean commandMatchesAction(IAction action, String command) {
    String[] classAnnotations = action.getClass().getAnnotation(Command.class).value();

    return Arrays.asList(classAnnotations).contains(command);
  }

  public List<IAction> getActions() {
    return actions;
  }
}
