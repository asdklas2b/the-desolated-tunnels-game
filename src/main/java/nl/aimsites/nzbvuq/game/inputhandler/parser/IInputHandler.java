package nl.aimsites.nzbvuq.game.inputhandler.parser;

import java.util.Scanner;

/**
 * Describes the command to action process
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
public interface IInputHandler {

  /**
   * Parses a Scanner to to an action
   *
   * @param input a scanner input which is used to derive a command and target to which to match an
   *     action.
   * @return returns the matched action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  void handleInput(Scanner input);
}
