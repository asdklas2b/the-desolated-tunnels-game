package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Strength;
import nl.aimsites.nzbvuq.game.entities.items.Weapon;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.Command;
import nl.aimsites.nzbvuq.game.state.GameState;

/**
 * Contains the game logic for the use action
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
@Command({"use"})
public class UseAction extends BaseAction {

  /**
   * Executes the game logic bound to the action/target combination
   *
   * @param target The target for/on which to apply the action
   * @param character The character for which the action need to be performed.
   * @return A string response for feedback as to what happened with the action
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  @Override
  public String execute(String target, Character character) {
    GameState game = GameState.getInstance();
    BaseItem item = character.getInventory().searchForItem(target);
    if (item != null) {
      if (item instanceof Strength) {
        return (new ConsumeAction()).execute(target, character);
      } else if (item instanceof Weapon) {
        if (character.getEquippedWeapon() != null && character.getEquippedWeapon().equals(item)) {
          return (new AttackAction()).execute(target, character);
        }
        return (new EquipAction()).execute(target, character);
      } else {
        // TODO: 17-12-2020 Implement logic if not predictable what player means with use.
        item.use();
      }
    } else if (game.getItemFactory().getItemFromString(target) != null) {
      return "You don't have this item in your inventory. So you cannot use it.";
    }
    return "The word " + target + " is not recognised. It is not an item.";
  }

  /**
   * Registers all possible targets of an action
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  @Override
  public void registerTargets() {
    this.setTargetOptions(null);
  }
}
