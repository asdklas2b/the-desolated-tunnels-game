package nl.aimsites.nzbvuq.game.inputhandler;

/**
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public interface Printer {
  void println(String line, PrintType type);
}
