package nl.aimsites.nzbvuq.game.inputhandler;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.Action;

/**
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public class ActionResponse {

  private final String own;
  private final String other;

  public ActionResponse(String own, String other) {
    this.own = own;
    this.other = other;
  }

  public String getOwn() {
    return own;
  }

  public String getOther() {
    return other;
  }
}
