package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Weapon;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.Command;

/**
 * Contains the game logic for the attack action
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
@Command({"equip", "wear"})
public class EquipAction extends BaseAction {

  /**
   * Executes the game logic bound to the action/target combination
   *
   * @param target The target for/on which to apply the action
   * @param character The character for which the action need to be performed.
   * @return A string response for feedback as to what happened with the action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  @Override
  public String execute(String target, Character character) {
    var item = character.getInventory().searchForItem(target);
    String firstLetterOfNextWordIsVowel = "^[aeiouAEIOU]\\w+";

    if (item != null) {
      if (item instanceof Weapon) {
        StringBuilder response = new StringBuilder(item.equip());
        Weapon oldWeapon = character.getEquippedWeapon();

        character.setEquippedWeapon((Weapon) item);
        response.append(getCustomResponseFromNewValues(item, oldWeapon));
        response
            .append(" You now have a max strength of ")
            .append(character.getMaxStrength())
            .append(".");

        if (!sendActionToOtherPlayers(target, character)) {
          return "You did not equip the sword, network error.";
        }

        return response.toString();
      }
      return "You can not equip a"
          + (target.matches(firstLetterOfNextWordIsVowel) ? "n " : " ")
          + item.getName()
          + " as it can not be used as a weapon.";
    }

    return "You don't have an item named " + target + ".";
  }

  /**
   * @param item the item that needs to be equipped
   * @param oldWeapon the weapon that gets replaced by the new equipped weapon.
   * @return String with the response of the change of strength
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  private String getCustomResponseFromNewValues(BaseItem item, Weapon oldWeapon) {
    StringBuilder response = new StringBuilder();
    if (oldWeapon == null) {
      response
          .append(" The ")
          .append(item.getName())
          .append(" increased your max strength with ")
          .append(((Weapon) item).getMaxStrengthAddition())
          .append(".");
    } else {
      int differenceInStrength =
          ((Weapon) item).getMaxStrengthAddition() - oldWeapon.getMaxStrengthAddition();
      boolean newWeaponIsStronger = differenceInStrength > 0;
      response
          .append(" The ")
          .append(item.getName())
          .append(" is ")
          .append(newWeaponIsStronger ? "stronger" : "weaker")
          .append(" than the ")
          .append(oldWeapon.getName())
          .append(". Your max strength ")
          .append(newWeaponIsStronger ? "increased" : "decreased")
          .append(" by ")
          .append(newWeaponIsStronger ? differenceInStrength : differenceInStrength * -1)
          .append(".");
    }
    return response.toString();
  }

  /**
   * Registers all possible targets of an action
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  @Override
  public void registerTargets() {
    this.setTargetOptions(null);
  }
}
