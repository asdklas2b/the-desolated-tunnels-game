package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.entities.items.Strength;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.Command;

/**
 * Contains the game logic for the consumable action
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
@Command({"consume", "eat", "drink"})
public class ConsumeAction extends BaseAction {

  /**
   * Executes the game logic bound to the action/target combination
   *
   * @param target The target for/on which to apply the action
   * @param character The character for which the action need to be performed.
   * @return A string response for feedback as to what happened with the action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  @Override
  public String execute(String target, Character character) {
    var consumable = character.getInventory().searchForItem(target);

    if (consumable != null) {
      if (consumable instanceof Strength) {
        character.getInventory().removeItem(consumable);
        character.addStrength(((Strength) consumable).getStrengthAddition());
      }

      if (!sendActionToOtherPlayers(target, character)) {
        return "You did not eat the item, network error.";
      }

      return consumable.consume();
    }

    return "You don't have an " + target + " in your inventory.";
  }

  /**
   * Registers all possible targets of an action
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  @Override
  public void registerTargets() {
    this.setTargetOptions(null);
  }
}
