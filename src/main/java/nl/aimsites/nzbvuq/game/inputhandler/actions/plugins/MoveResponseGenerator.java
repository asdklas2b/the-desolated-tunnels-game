package nl.aimsites.nzbvuq.game.inputhandler.actions.plugins;

import nl.aimsites.nzbvuq.game.graph.tiles.Corridor;
import nl.aimsites.nzbvuq.game.graph.tiles.Field;
import nl.aimsites.nzbvuq.game.graph.tiles.Room;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;

import java.util.HashMap;

/**
 * A generator made for generating text that can be given to a player to get insight in the
 * consequences of the action.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 */
public class MoveResponseGenerator {
  private Tile oldTile;
  private Tile newTile;
  private static final HashMap<String, String> moveResponses = new HashMap<>();

  /**
   * Creates an instance of MoveResponseGenerator
   *
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public MoveResponseGenerator() {
    String fieldName = Field.class.getSimpleName();
    String corridorName = Corridor.class.getSimpleName();
    String roomName = Room.class.getSimpleName();

    moveResponses.put(
        fieldName + corridorName,
        "You enter a corridor. Inside you can hear water dripping onto the ground.");
    moveResponses.put(
        fieldName + roomName,
        "As you step inside a room, you can feel the warm air flowing over you.");
    moveResponses.put(
        fieldName + fieldName, "As you walk through the fields, you hear a bird chirping.");
    moveResponses.put(
        corridorName + fieldName,
        "You step out of the corridor into a field. You feel the fresh air blow through your hair.");
    moveResponses.put(corridorName + roomName, "You step up some stairs and enter a room.");
    moveResponses.put(corridorName + corridorName, "you stay in the corridor.");
    moveResponses.put(
        roomName + corridorName,
        "As you enter the corridor you hear the cold air rush out of the tunnels into the room.");
    moveResponses.put(
        roomName + fieldName, "You leave the room and go outside. It is a bit colder out here.");
    moveResponses.put(roomName + roomName, "you stepped into another room.");
  }

  public void setOldTile(Tile oldTile) {
    this.oldTile = oldTile;
  }

  public void setNewTile(Tile newTile) {
    this.newTile = newTile;
  }

  @Override
  public String toString() {
    return moveResponses.get(
        oldTile.getClass().getSimpleName() + newTile.getClass().getSimpleName());
  }
}
