package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.inputhandler.ActionResponse;

/**
 * Resembles the base blueprint of an action
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
public abstract class IAction {

  /**
   * Executes the game logic bound to the action/target combination
   *
   * @param target The target for/on which to apply the action
   * @param character The character for which the action need to be performed.
   * @return A string response for feedback as to what happened with the action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  public abstract String execute(String target, Character character);

  public ActionResponse executeResponsibly(String target, Character character) {
    return new ActionResponse(
      execute(
        target,
        character
      ),
      ""
    );
  }

  /**
   * Checks if the action has a nullable target
   *
   * @return If the action has a nullable target
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public abstract boolean isTargetNullable();
}
