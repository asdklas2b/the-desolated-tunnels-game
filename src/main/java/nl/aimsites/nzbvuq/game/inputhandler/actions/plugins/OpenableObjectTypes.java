package nl.aimsites.nzbvuq.game.inputhandler.actions.plugins;

/**
 * Contains all openable object types
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 */
public enum OpenableObjectTypes {
  INVENTORY,
  DOORWAY
}
