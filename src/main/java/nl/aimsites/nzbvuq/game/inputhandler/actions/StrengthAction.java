package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.Command;

/**
 * Contains the game logic for the player to have insight in the strength
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 */
@Command({"strength", "health", "power", "energy", "s"})
public class StrengthAction extends BaseAction {
  private static final int SIZE_OF_STRENGTH_BAR = 40;

  /**
   * Executes the game logic bound to the action/target combination
   *
   * @param target The target for/on which to apply the action
   * @param character The character for which the action need to be performed.
   * @return A string response for feedback as to what happened with the action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  @Override
  public String execute(String target, Character character) {
    StringBuilder response =
      new StringBuilder();

    if (target != null && !character.getName().equals(target)) {
      var other = character.getCurrentPosition()
        .getOccupyingEntities()
        .stream()
        .filter(e -> e.getName().equalsIgnoreCase(target))
        .findFirst();

      if (!other.isPresent()) {
        return "Cannot find " + target;
      }

      if (!(other.get() instanceof Character)) {
        return target + " cannot have a strength";
      }

      character = (Character) other.get();

      response.append(target).append(" strength is ");
    } else {
      response.append("Your current strength is ");
    }
    response.append(character.getStrength())
      .append(".")
      .append(System.lineSeparator());

    response.append("0 |");
    int amountOfStrength =
        (int)
            ((character.getStrength() / (float) character.getMaxStrength()) * SIZE_OF_STRENGTH_BAR);

    response.append("#".repeat(Math.max(0, amountOfStrength)));
    response.append("|").append(character.getStrength());

    int amountOfLeftOver =
        SIZE_OF_STRENGTH_BAR
            - amountOfStrength
            - 1
            - String.valueOf(character.getStrength()).length();
    response.append("-".repeat(Math.max(0, amountOfLeftOver)));
    response.append("| ").append(character.getMaxStrength());

    response
        .append(System.lineSeparator())
        .append("Travel cost is ")
        .append(character.getTravelCostFromItems())
        .append(" per move.");

    return response.toString();
  }

  @Override
  public boolean isTargetNullable() {
    return true;
  }

  /**
   * Registers all possible targets of an action
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  @Override
  public void registerTargets() {
    this.setTargetOptions(null);
  }
}
