package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.generators.Direction;
import nl.aimsites.nzbvuq.game.graph.Edge;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import nl.aimsites.nzbvuq.game.inputhandler.ActionResponse;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.Command;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.MoveDirection;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.MoveResponseGenerator;
import nl.aimsites.nzbvuq.game.state.GameState;
import nl.aimsites.nzbvuq.game.state.WorldHandler;

import java.util.*;
import java.util.logging.Logger;

/**
 * Contains the game logic for the movement action
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 * @author Mark Jansen (MJ.Jansen4@student.han.nl)
 */
@Command({"move", "walk", "run", "go", "m"})
public class MoveAction extends BaseAction {

  private static final Logger LOGGER = Logger.getLogger(MoveAction.class.getName());

  private HashMap<String, MoveDirection> possibleDirections;
  private Graph world;
  private WorldHandler worldHandler;
  private MoveResponseGenerator responseGenerator;

  /**
   * MoveAction constructor
   *
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public MoveAction() {
    super();
  }

  /**
   * Executes the game logic bound to the action/target combination
   *
   * @param target The target for/on which to apply the action
   * @param character The character for which the action need to be performed.
   * @return A string response for feedback as to what happened with the action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  @Override
  public String execute(String target, Character character) {
    target = target.toLowerCase();
    if (!isValidTarget(target)) {
      return "Your only move options are: " + this.getTargetOptions().toString();
    }

    GameState instance = GameState.getInstance();
    world = instance.getCurrentWorld();
    worldHandler = instance.getWorldHandler();

    responseGenerator = new MoveResponseGenerator();
    responseGenerator.setOldTile(character.getCurrentPosition());

    Optional<Tile> destinationTile;
    boolean teleport = false;
    MoveDirection direction = MoveDirection.INIT;
    if (!target.contains(",")) {
       direction = possibleDirections.get(target);

       destinationTile =
        worldHandler.getDestinationTile(character.getCurrentPosition(), direction);
    } else {
      var cords = target.split(",");

      teleport = true;

      destinationTile = worldHandler.getTitle(
        new Point(Integer.parseInt(cords[0]), Integer.parseInt(cords[1]))
      );

      LOGGER.info("Teleported to -> " + destinationTile.get());
    }

    if (destinationTile.isPresent()) {
      Tile tile = destinationTile.get();

      if (!sendActionToOtherPlayers(target, character)) {
        return "You did not move, network error.";
      }

      var playerMoved = actionMove(character, tile, teleport);
      if (playerMoved != null) {
        return playerMoved;
      }

      character.addNewMoveToHistory((ReachableTile) tile, direction);
      return getHandleTileMovementCost(character, destinationTile.orElseThrow());
    }

    return responseGenerator.toString();
  }

  @Override
  public ActionResponse executeResponsibly(String target, Character character) {
    var response = execute(target, character);
    // "Player " + character.getName() + " " + character.getCurrentPosition().getCoordinates().getX() + "," + character.getCurrentPosition().getCoordinates().getY()
    if (GameState.getInstance().getPlayer().getCurrentPosition().getCoordinates()
      .equals(character.getCurrentPosition().getCoordinates())) {
      return new ActionResponse(
        response,
        "Player " + character.getName() + ", is now standing in your neighborhood"
      );
    }
    if (character.getHistory().size() > 1) {
      return new ActionResponse(
        response,
        character.getHistory()
          .get(character.getHistory().size() - 2)
          .getReachableTile()
          .getCoordinates()
          .equals(GameState.getInstance().getPlayer().getCurrentPosition().getCoordinates())
          ? "Player " + character.getName() + ", is no longer in your neighborhood"
          : ""
      );
    }
    return new ActionResponse(
      response,
      ""
    );
  }

  /**
   * removes travel cost from the players strength
   *
   * @param character The player that is performing the action
   * @param tile the tile that the played moved to
   * @return String with the response of the move action
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  private String getHandleTileMovementCost(
      Character character, Tile tile) {
    int travelCost = 0;

    if (tile instanceof ReachableTile) {
      var reachableTile = (ReachableTile) tile;
      travelCost = reachableTile.getCostOfTraversal();
      travelCost += character.getTravelCostFromItems();
      character.removeStrength(travelCost);
      character.setCurrentPosition(reachableTile);
      responseGenerator.setNewTile(character.getCurrentPosition());
    }

    if (worldHandler.getLastGeneratedChunk() != null) {
      GameState.getInstance().setCurrentWorld(worldHandler.getLastGeneratedChunk());
    }

    return responseGenerator.toString();
  }

  /**
   * Moves a character to a tile. Removes from old tile, add to new tile.
   *
   * @param character you want to move
   * @param newPosition new position that the players moves to
   * @return if character was able to be moved
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  private String actionMove(Character character, Tile newPosition, boolean teleport) {
    System.out.println("x:"+character.getCurrentPosition().getX()+ " y:"+ character.getCurrentPosition().getY());
    var opening =
        containsClosedOpening(
            character.getCurrentPosition().getAdjacentTiles(), newPosition.getCoordinates());

    if (opening != null) {
      return "You cannot move through a closed " + opening;
    }

    if (newPosition instanceof ReachableTile) {
      final boolean allowedToMoveToNewPosition =
          character.getCurrentPosition().getAdjacentTiles().stream()
              .anyMatch(edge -> edge.getDestinationTile().equals(newPosition));

      if (allowedToMoveToNewPosition || teleport) {
        ((ReachableTile) newPosition).addEntity(character);
        character.getCurrentPosition().removeEntity(character);

        character.setCurrentPosition((ReachableTile) newPosition);
        System.out.println("x:"+character.getCurrentPosition().getX()+ " y:"+ character.getCurrentPosition().getY());
        return null;
      }
    }

    return "You did not move, tile Unreachable";
  }

  /**
   * Checks if the list of edges contains a closed opening
   *
   * @param edges Edges to check
   * @param positionToMoveTo The position the player is moving to
   * @return String if the edges contains a closed opening otherwise NULL
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  private String containsClosedOpening(List<Edge> edges, Point positionToMoveTo) {
    for (var edge : edges) {
      if (edge.getOpeningEntity() == null || edge.getOpeningEntity().isOpened()) {
        continue;
      }

      if (edge.getDestinationTile().getCoordinates().equals(positionToMoveTo)) {
        return edge.getOpeningEntity().getName().toLowerCase();
      }
    }

    return null;
  }

  /**
   * Registers all possible targets of an action
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  @Override
  public void registerTargets() {
    possibleDirections = new HashMap<>();

    possibleDirections.put("up", MoveDirection.NORTH);
    possibleDirections.put("down", MoveDirection.SOUTH);
    possibleDirections.put("left", MoveDirection.WEST);
    possibleDirections.put("right", MoveDirection.EAST);
    possibleDirections.put("forward", MoveDirection.NORTH);
    possibleDirections.put("backward", MoveDirection.SOUTH);
    possibleDirections.put("north", MoveDirection.NORTH);
    possibleDirections.put("east", MoveDirection.EAST);
    possibleDirections.put("south", MoveDirection.SOUTH);
    possibleDirections.put("west", MoveDirection.WEST);
    possibleDirections.put("n", MoveDirection.NORTH);
    possibleDirections.put("e", MoveDirection.EAST);
    possibleDirections.put("s", MoveDirection.SOUTH);
    possibleDirections.put("w", MoveDirection.WEST);
    possibleDirections.put("init", MoveDirection.INIT);

    var list = new ArrayList<>(possibleDirections.keySet());

    this.setTargetOptions(list);
  }
}
