package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.entities.items.Armor;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.Command;

/**
 * Contains the game logic for the drop action
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
@Command({"drop", "leave"})
public class DropAction extends BaseAction {

  /**
   * Executes the game logic bound to the action/target combination
   *
   * @param target The target for/on which to apply the action
   * @param character The character for which the action need to be performed.
   * @return A string response for feedback as to what happened with the action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  @Override
  public String execute(String target, Character character) {
    var item = character.getInventory().searchForItem(target);

    if (item != null) {
      character.getInventory().removeItem(item);

      var currentPlayerTile = character.getCurrentPosition();
      currentPlayerTile.addEntity(item);

      StringBuilder response = new StringBuilder(item.drop());
      if (item instanceof Armor) {
        response.append(getCustomResponseFromNewStrengthValues(item, character));
      }

      if (!sendActionToOtherPlayers(target, character)) {
        return "You did not drop the item, network error.";
      }

      return response.toString();
    }

    return "You don't have " + target + " in your inventory";
  }

  /**
   * Generating a response for the new values for the dropped item
   *
   * @param item the item dropped by the player
   * @param character The character for which the action need to be performed.
   * @return String with the response of the new values
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  private String getCustomResponseFromNewStrengthValues(BaseItem item, Character character) {
    StringBuilder response = new StringBuilder();

    response.append(" Because you dropped this ").append(item.getName());
    if (character.getStrength() - ((Armor) item).getStrengthAddition() <= 0) {
      response
          .append(" your strength decreased by ")
          .append(character.getStrength() - 1)
          .append(" and your max strength by ");
      character.removeStrength(character.getStrength() - 1);
    } else {
      response.append(" your strength and max strength decreased with ");
      character.removeStrength(((Armor) item).getMaxStrengthAddition());
    }

    return response
        .append(((Armor) item).getStrengthAddition())
        .append(". You now have a max strength of ")
        .append(character.getMaxStrength())
        .append(".")
        .toString();
  }

  /**
   * Registers all possible targets of an action
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  @Override
  public void registerTargets() {
    this.setTargetOptions(null);
  }
}
