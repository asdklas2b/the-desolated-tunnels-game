package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.Entity;
import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Storage;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.Command;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.OpenableObjectTypes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Contains the game logic for the opening action
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 */
@Command({"open"})
public class OpenAction extends BaseAction {

  private HashMap<String, OpenableObjectTypes> openableObjectTypes;

  /**
   * Executes the game logic bound to the action/target combination
   *
   * @param target The target for/on which to apply the action
   * @param character The character for which the action need to be performed.
   * @return A string response for feedback as to what happened with the action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   * @author Azez Salami (a.salami@student.han.nl)
   */
  @Override
  public String execute(String target, Character character) {
    Entity openableEntity = getOpenableEntity(target, character);

    if (openableEntity instanceof Storage) {
      List<BaseItem> items = ((Storage) openableEntity).getItems();

      return ((Storage) openableEntity).open() + listOfItemsToString(items);
    }

    if (!isValidTarget(target)) {
      return "You can't open a " + target + " here.";
    }

    OpenableObjectTypes itemToBeOpened = openableObjectTypes.get(target);

    if (!sendActionToOtherPlayers(target, character)) {
      return "You could not open, network error.";
    }

    if (itemToBeOpened == OpenableObjectTypes.DOORWAY) {
      return openOpening(target, character);
    }

    if (itemToBeOpened == OpenableObjectTypes.INVENTORY) {
      List<BaseItem> items = character.getInventory().getItems();
      if (items.size() == 0) {
        return "There is nothing to see in your inventory because it's empty.";
      }

      StringBuilder response = new StringBuilder("Your inventory contains the following items:");
      response.append(listOfItemsToString(items));
      return response.toString();
    } else {
      // Todo: actual game logic
      return "You have opened the " + target;
    }
  }

  /**
   * Tries to open the opening that the player is standing next to
   *
   * @param character The character for which the action need to be performed.
   * @return A message describing the consequences of the action
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  private String openOpening(String target, Character character) {
    var currentLocation = character.getCurrentPosition();

    var surroundingTiles = currentLocation.getAdjacentTiles();
    String message = "No valid " + target + " found to open";

    if (target.equals("door")) {
      target = "wooden door";
    }

    for (var tile : surroundingTiles) {
      if (tile.getDestinationTile() instanceof ReachableTile) {
        var opening = tile.getOpeningEntity();

        if (opening == null || !target.equalsIgnoreCase(opening.getName())) {
          continue;
        }

        if (opening.isOpened()) {
          return "The " + target + " is already open";
        }

        opening.setOpened(true);

        message = opening.open();
      }
    }

    return message;
  }

  /**
   * Registers all possible targets of an action
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  @Override
  public void registerTargets() {
    openableObjectTypes = new HashMap<>();

    openableObjectTypes.put("door", OpenableObjectTypes.DOORWAY);
    openableObjectTypes.put("doorway", OpenableObjectTypes.DOORWAY);
    openableObjectTypes.put("window", OpenableObjectTypes.DOORWAY);
    openableObjectTypes.put("d", OpenableObjectTypes.DOORWAY);
    openableObjectTypes.put("inventory", OpenableObjectTypes.INVENTORY);
    openableObjectTypes.put("storage", OpenableObjectTypes.INVENTORY);
    openableObjectTypes.put("backpack", OpenableObjectTypes.INVENTORY);
    openableObjectTypes.put("items", OpenableObjectTypes.INVENTORY);
    openableObjectTypes.put("i", OpenableObjectTypes.INVENTORY);

    var list = new ArrayList<>(openableObjectTypes.keySet());

    this.setTargetOptions(list);
  }

  /**
   * listOfItemsToString makes StringBuilder of the list of the items
   *
   * @param items is a list of BaseItem objects
   * @return A StringBuilder of the list of the items
   * @author Azez Salami (a.salami@student.han.nl)
   */
  private StringBuilder listOfItemsToString(List<BaseItem> items) {
    StringBuilder listOfItems = new StringBuilder();
    items.forEach(
        item ->
            listOfItems
                .append(System.lineSeparator())
                .append("- ")
                .append(item.getName().substring(0, 1).toUpperCase())
                .append(item.getName().substring(1)));
    return listOfItems;
  }

  /**
   * Method gets an entity
   *
   * @param target The target is a name of the entity
   * @param character The character for which the action need to be performed.
   * @return A OpenableEntity
   * @author Azez Salami (a.salami@student.han.nl)
   */
  private Entity getOpenableEntity(String target, Character character) {
    return character.getCurrentPosition().getOccupyingEntities().stream()
        .filter(entity -> target.equals(entity.getName()))
        .findFirst()
        .orElse(null);
  }
}
