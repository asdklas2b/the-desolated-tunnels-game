package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.inputhandler.ActionResponse;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.Command;
import nl.aimsites.nzbvuq.game.state.GameState;

/**
 * Contains the game logic for the attack action
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
@Command({"attack", "hit"})
public class AttackAction extends BaseAction {

  private static final int FIST_DAMAGE = 10;

  /**
   * Executes the game logic bound to the action/target combination
   *
   * @param target The target for/on which to apply the action
   * @param character The character for which the action need to be performed.
   * @return A string response for feedback as to what happened with the action
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  @Override
  public String execute(String target, Character character) {
    if (!sendActionToOtherPlayers(target, character)) {
      return "You did not attack, network error.";
    }

    var opposing = character.getCurrentPosition()
      .getOccupyingEntities()
      .stream()
      .filter(e -> e.getName().equalsIgnoreCase(target))
      .filter(e -> !e.getName().equals(character.getName()))
      .findFirst();

    if (opposing.isEmpty()) {
      return "You did not attack, " + target + " not present around you";
    }

    if (!(opposing.get() instanceof Character)) {
      return "You cannot attack, " + target;
    }

    if (((Character)opposing.get()).getStrength() < 1) {
      return target + " is dead";
    }

    int damage = FIST_DAMAGE,
      strength = character.getTravelCostFromItems() < 1 ? 4 : character.getTravelCostFromItems();

    String weapon = "your fists";

    boolean youDied = false,
      opposingDied = false;

    if (character.getEquippedWeapon() != null) {
      damage = character.getEquippedWeapon().getDamage();
      weapon = character.getEquippedWeapon().getName();

      character.removeStrength(character.getTravelCostFromItems());
    }

    ((Character) opposing.get()).removeStrength(damage);
    character.removeStrength(strength);

    return "You attacked " + target + " with " + weapon + ", you did " + damage + " damage, " + (opposingDied ? "he died, ": "") + (youDied ? "you died, ": "") + "costing you " + strength + " strength";
  }

  @Override
  public ActionResponse executeResponsibly(String target, Character character) {
    var opposing = character.getCurrentPosition()
      .getOccupyingEntities()
      .stream()
      .filter(e -> e.getName().equalsIgnoreCase(target))
      .filter(e -> !e.getName().equals(character.getName()))
      .findFirst();

    if (!opposing.isPresent()) {
      return new ActionResponse(
        execute(target, character),
        ""
      );
    }

    if (!(opposing.get() instanceof Character)) {
      return new ActionResponse(
        execute(target, character),
        ""
      );
    }

    if (target.equalsIgnoreCase(GameState.getInstance().getPlayer().getName())) {
      return new ActionResponse(
        execute(target, character),
        "You have been attacked by " + character.getName() + ", " + ( GameState.getInstance().getPlayer().getStrength() < 1 ? "you died" : "you have " + GameState.getInstance().getPlayer().getStrength() + " strength remaining")
      );
    }

    return new ActionResponse(
      execute(target, character),
      character.getCurrentPosition().getCoordinates().equals(GameState.getInstance().getPlayer().getCurrentPosition().getCoordinates())
        ? character.getName() + " attacked " + target + (((Character) opposing.get()).getStrength() < 1 ? ", he died" : "")
        : ""
    );
  }

  /**
   * Registers all possible targets of an action
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  @Override
  public void registerTargets() {
    this.setTargetOptions(null);
  }
}
