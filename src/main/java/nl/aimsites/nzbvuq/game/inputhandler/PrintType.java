package nl.aimsites.nzbvuq.game.inputhandler;

/**
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public enum PrintType {
  INPUT,
  OUTPUT,
  DEFAULT
}
