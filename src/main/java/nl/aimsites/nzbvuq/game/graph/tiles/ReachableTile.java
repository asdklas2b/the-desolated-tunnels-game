package nl.aimsites.nzbvuq.game.graph.tiles;

import nl.aimsites.nzbvuq.game.entities.Entity;
import nl.aimsites.nzbvuq.game.graph.Edge;
import nl.aimsites.nzbvuq.game.graph.Point;

import java.util.ArrayList;
import java.util.List;

/**
 * A tile that is reachable by an entity
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public abstract class ReachableTile extends Tile {
  private final List<Edge> adjacentTiles;
  private List<Entity> occupyingEntities;
  private int costOfTraversal;

  /**
   * Constructs an instance of this class
   *
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public ReachableTile() {
    this.adjacentTiles = new ArrayList<>();
    this.occupyingEntities = new ArrayList<>();
  }

  /**
   * Constructs an instance of this class
   *
   * @param coordinates coordinate of the tile
   * @param costOfTraversal cost needed to walk on the tile
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  protected ReachableTile(Point coordinates, int costOfTraversal) {
    super(coordinates);
    this.costOfTraversal = costOfTraversal;
    this.adjacentTiles = new ArrayList<>();
    this.occupyingEntities = new ArrayList<>();
  }

  /**
   * Adds an edge to the list of edges
   *
   * @param edge edge to be added
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public void addAdjacentTile(Edge edge) {
    if (edge.getDestinationTile() == null) {
      throw new IllegalArgumentException("You can not add an edge that has no destination tile!");
    }
    if (this.equals(edge.getDestinationTile())) {
      throw new IllegalArgumentException("You can not connect a tile to itself!");
    }

    if (this.adjacentTiles.contains(edge)) {
      throw new IllegalArgumentException("Duplicate edge!");
    }

    this.adjacentTiles.add(edge);
  }

  /**
   * Adds an entity
   *
   * @param entity entity to be added
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public void addEntity(Entity entity) {
    if (entity == null) {
      throw new IllegalArgumentException("You can not add an entity that is null!");
    }

    if (this.occupyingEntities.contains(entity)) {
      throw new IllegalArgumentException("Duplicate entity!");
    }

    this.occupyingEntities.add(entity);
  }

  public List<Edge> getAdjacentTiles() {
    return List.copyOf(adjacentTiles);
  }

  public List<Entity> getOccupyingEntities() {
    return List.copyOf(occupyingEntities);
  }

  public void setOccupyingEntities(List<Entity> occupyingEntities) {
    this.occupyingEntities = occupyingEntities;
  }

  public int getCostOfTraversal() {
    return costOfTraversal;
  }

  /**
   * Removes an entity
   *
   * @param entity entity to be removed
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public void removeEntity(Entity entity) {
    if (entity == null) {
      throw new IllegalArgumentException("You can not remove an entity that is null!");
    }
    this.occupyingEntities.remove(entity);
  }
}
