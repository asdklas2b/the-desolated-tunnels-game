package nl.aimsites.nzbvuq.game.graph.tiles;

import nl.aimsites.nzbvuq.game.graph.Point;

/**
 * A room in the world
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class Room extends ReachableTile {
  public RoomType type;

  /**
   * Constructs an instance of this class
   *
   * @param coordinates coordinate of the room
   * @param costOfTraversal the cost needed to walk on the tile
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public Room(Point coordinates, int costOfTraversal) {
    super(coordinates, costOfTraversal);
  }
  /**
   * @param coordinates coordinate of the room
   * @param costOfTraversal the cost needed to walk on the tile
   * @param type type of the room
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public Room(Point coordinates, int costOfTraversal, RoomType type) {
    super(coordinates, costOfTraversal);
    this.type = type;
  }

  public RoomType getType() {
    return type;
  }

  public void setType(RoomType type) {
    this.type = type;
  }

  @Override
  public String toString() {
    if (type == null) {
      return "room";
    }

    return switch (type) {
      case CASTLE -> "castle";
      case HOUSE -> "house";
    };
  }
}
