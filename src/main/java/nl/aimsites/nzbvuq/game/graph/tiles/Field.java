package nl.aimsites.nzbvuq.game.graph.tiles;

import nl.aimsites.nzbvuq.game.graph.Point;

/**
 * A field in the world
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class Field extends ReachableTile {

  /**
   * Constructs an instance of the class
   *
   * @param coordinates coordinate of the tile
   * @param costOfTraversal cost needed to walk on the tile
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public Field(Point coordinates, int costOfTraversal) {
    super(coordinates, costOfTraversal);
  }

  @Override
  public String toString() {
    return "field";
  }
}
