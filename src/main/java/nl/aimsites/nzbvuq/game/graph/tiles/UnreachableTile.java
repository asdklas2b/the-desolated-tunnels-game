package nl.aimsites.nzbvuq.game.graph.tiles;

import nl.aimsites.nzbvuq.game.graph.Point;

/**
 * A tile that is unreachable by an entity
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public abstract class UnreachableTile extends Tile {

  public UnreachableTile(Point coordinates) {
    super(coordinates);
  }
}
