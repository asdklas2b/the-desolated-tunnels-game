package nl.aimsites.nzbvuq.game.graph.tiles;

import nl.aimsites.nzbvuq.game.graph.Point;

/**
 * A corridor in a castle
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class Corridor extends ReachableTile {

  /**
   * Constructs an instance of the class
   *
   * @param coordinates coordinates of the corridor
   * @param costOfTraversal cost needed to walk on corridor
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public Corridor(Point coordinates, int costOfTraversal) {
    super(coordinates, costOfTraversal);
  }

  @Override
  public String toString() {
    return "corridor";
  }
}
