package nl.aimsites.nzbvuq.game.graph.tiles;

/**
 * Room can have only one roomtype based on the vision document. CASTLE or HOUSE
 *
 * @author Feida Wei (FD.Wei@student.han.nl)
 */
public enum RoomType {
  CASTLE,
  HOUSE
}
