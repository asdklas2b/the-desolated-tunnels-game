package nl.aimsites.nzbvuq.game.graph;

import nl.aimsites.nzbvuq.game.entities.openings.Opening;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;

import java.util.Objects;

/**
 * This class is the "connection" between two tiles.
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class Edge {
  private Tile destinationTile;
  private Opening openingEntity;

  public Edge(Tile destinationTile) {
    setDestinationTile(destinationTile);
  }

  public Tile getDestinationTile() {
    return destinationTile;
  }

  public void setDestinationTile(Tile destinationTile) {
    if (destinationTile == null) {
      throw new IllegalArgumentException("Destination Tile can not be null!");
    }

    this.destinationTile = destinationTile;
  }

  public Opening getOpeningEntity() {
    return this.openingEntity;
  }

  public void setOpeningEntity(Opening openingEntity) {
    this.openingEntity = openingEntity;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Edge edge = (Edge) o;
    return destinationTile.equals(edge.destinationTile);
  }

  @Override
  public int hashCode() {
    return Objects.hash(destinationTile);
  }
}
