package nl.aimsites.nzbvuq.game.graph.tiles;

import nl.aimsites.nzbvuq.game.graph.Point;

import java.util.Objects;

/**
 * A tile in the world
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public abstract class Tile {
  private Point coordinates;

  public Tile() {}

  public Tile(Point coordinates) {
    this.coordinates = coordinates;
  }

  public Point getCoordinates() {
    return coordinates;
  }

  public int getX() {
    return coordinates.getX();
  }

  public int getY() {
    return coordinates.getY();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Tile tile = (Tile) o;
    return coordinates.equals(tile.coordinates);
  }

  @Override
  public int hashCode() {
    return Objects.hash(coordinates);
  }
}
