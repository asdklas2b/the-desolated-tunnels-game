package nl.aimsites.nzbvuq.game.graph.tiles;

import nl.aimsites.nzbvuq.game.graph.Point;

/**
 * A forest in the world
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class Forest extends UnreachableTile {

  /**
   * Constructs an instance of the class
   *
   * @param coordinates the coordinates of the tile
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public Forest(Point coordinates) {
    super(coordinates);
  }
}
