package nl.aimsites.nzbvuq.game.graph;

import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;

import java.util.Objects;
import java.util.Optional;
import java.util.Random;

/**
 * This is a chunk of the world, it contains a 2D array of tiles where entities can reside in.
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class Graph {
  private Tile[][] tiles;
  private int startX;
  private int startY;
  private int size;
  private long seed;
  private Random random;

  /**
   * Constructs an instance of the class
   *
   * @param startX starting X position of the chunk
   * @param startY starting Y position of the chunk
   * @param size size of the graph
   * @param seed the seed that was used to generate the graph
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public Graph(int startX, int startY, int size, long seed) {
    this.startX = startX;
    this.startY = startY;
    this.size = size;
    tiles = new Tile[size][size];
    this.seed = seed;
    random = new Random(seed);
  }

  /**
   * Constructs an empty instance of the class (used for data storage)
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public Graph() {}

  /**
   * Adds a tile in the graph on the position of their coordinate
   *
   * @param tile the tile to be added
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public void addTile(Tile tile) {
    if (tile == null) {
      throw new IllegalArgumentException("No tile");
    }

    tiles[tile.getY() - startY][tile.getX() - startX] = tile;
  }

  /**
   * Gets a random reachable Tile in the world
   *
   * @return A random reachable Tile
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  public ReachableTile getRandomReachableTile() {
    int randomY = getRandomNumberInRange(0, tiles[0].length - 1);
    int randomX = getRandomNumberInRange(0, tiles.length - 1);

    Tile randomTile = tiles[randomY][randomX];

    if (randomTile instanceof ReachableTile) {
      return (ReachableTile) randomTile;
    } else {
      return getRandomReachableTile();
    }
  }

  /**
   * Gets a random number in between a range
   *
   * @param min Range start
   * @param max Range end
   * @return A random number
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  private static int getRandomNumberInRange(int min, int max) {
    if (min >= max) {
      throw new IllegalArgumentException("max must be greater than min");
    }

    return new Random().nextInt((max - min) + 1) + min;
  }

  public Tile[][] getTiles() {
    return tiles;
  }

  public Optional<Tile> findTile(int x, int y) {
    final boolean pointExistsInsideGraph =
        (x >= startX && x <= startX + size - 1) && (y >= startY && y <= startY + size - 1);

    if (!pointExistsInsideGraph) {
      return Optional.empty();
    }

    return Optional.ofNullable(tiles[y - startY][x - startX]);
  }

  /**
   * Checks if a tile exists on the given coordinates
   *
   * @param x x coordinate of tile to check
   * @param y y coordinate of tile to check
   * @return if a tile exists on the given coordinates
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  public boolean tileExists(int x, int y) {
    final boolean pointExistsInsideGraph =
        (x >= startX && x <= startX + size - 1) && (y >= startY && y <= startY + size - 1);

    if (!pointExistsInsideGraph) {
      return false;
    }

    return y >= 0 && y <= tiles[x].length;
  }

  public void setTiles(Tile[][] tiles) {
    this.tiles = tiles;
  }

  public int getStartX() {
    return startX;
  }

  public void setStartX(int startX) {
    this.startX = startX;
  }

  public int getStartY() {
    return startY;
  }

  public void setStartY(int startY) {
    this.startY = startY;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public long getSeed() {
    return seed;
  }

  public void setSeed(long seed) {
    this.seed = seed;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Graph graph = (Graph) o;
    return startX == graph.startX && startY == graph.startY && size == graph.size;
  }

  @Override
  public int hashCode() {
    return Objects.hash(startX, startY, size);
  }
}
