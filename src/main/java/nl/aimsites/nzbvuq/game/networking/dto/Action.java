package nl.aimsites.nzbvuq.game.networking.dto;

import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.inputhandler.actions.BaseAction;

/**
 * This is an action object used by networking. An action a player does gets send to other peers
 * with this object. Currently only support for movement action, support for more actions will be
 * added in the future.
 *
 * @author Mark Jansen (MJ.Jansen4@student.han.nl)
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class Action {
  private String simpleClassName;
  private String target;
  private String characterId;
  private int posX;
  private int posY;

  /**
   * Sets the information of an action that the local player did.
   *
   * @param actionClass
   * @param target
   * @param characterId id of player that does the action
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public <T extends BaseAction> Action(Class<T> actionClass, String target, String characterId, Point characterPosition) {
    this.simpleClassName = actionClass.getSimpleName();
    this.target = target;
    this.characterId = characterId;
    this.posX = characterPosition.getX();
    this.posY = characterPosition.getY();
  }

  public Action(String simpleClassName, String target, String characterId, int posX, int posY) {
    this.simpleClassName = simpleClassName;
    this.target = target;
    this.characterId = characterId;
    this.posX = posX;
    this.posY = posY;
  }

  public String getSimpleClassName() {
    return simpleClassName;
  }

  public void setSimpleClassName(String simpleClassName) {
    this.simpleClassName = simpleClassName;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public String getCharacterId() {
    return characterId;
  }

  public void setCharacterId(String characterId) {
    this.characterId = characterId;
  }

  public int getPosX() {
    return posX;
  }

  public void setPosX(int posX) {
    this.posX = posX;
  }

  public int getPosY() {
    return posY;
  }

  public void setPosY(int posY) {
    this.posY = posY;
  }
}
