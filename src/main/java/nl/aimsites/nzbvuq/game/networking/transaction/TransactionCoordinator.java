package nl.aimsites.nzbvuq.game.networking.transaction;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.networking.TransactionHandler;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import nl.aimsites.nzbvuq.rpc.message.RPCPayloadType;
import nl.aimsites.nzbvuq.rpc.message.Request;
import nl.aimsites.nzbvuq.rpc.message.Response;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Transaction coordinator without Three phase commit algorithm used for direct communication and choose the leader algorithm.
 *
 * @author Dave Quentin (DM.Quentin@student.han.nl)
 * @author Mark Jansen (MJ.Jansen4@student.han.nl)
 */
public class TransactionCoordinator {
  private static final Logger LOGGER = Logger.getLogger(TransactionCoordinator.class.getName());

  private final TransactionHandler transactionHandler;
  private final Gson gson = new Gson();

  /**
   * transaction coordinator constructor
   *
   * @param transactionHandler the instance of the transactionHandler
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  public TransactionCoordinator(TransactionHandler transactionHandler) {
    this.transactionHandler = transactionHandler;
  }

  /**
   * Three-phase commit transaction coordinator constructor
   *
   * @param action the incoming object
   * @return List of strings
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  public boolean manageTransaction(Action action) {
    return Objects.requireNonNull(this.sendActionToLeader(action)).get(0).equals(ProtocolMessage.STATUS_OK);
  }

  /**
   * Send action to leader, which will validate the action
   *
   * @param action the incoming action
   * @return List of strings
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private List<String> sendActionToLeader(Action action) {
    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add(ProtocolMessage.ACTION);
    parameters.add(gson.toJson(action));

    Response response = sendToPlayer(transactionHandler.getLeader(), parameters);

    if (response != null && response.getType() == RPCPayloadType.EXCEPTION) {
      chooseTheLeaderAlgorithm(ProtocolMessage.LEADER_ELECTION);
    }
    if (response != null) {
      return response.getParameters();
    }
    return null;
  }

  /**
   * Start of choose the leader algorithm
   *
   * @param status the incoming action
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  public void chooseTheLeaderAlgorithm(String status) {
    switch (status) {
      case ProtocolMessage.LEADER_ELECTION -> {
        findLeader();
      }
      case ProtocolMessage.CHECK_LEADER -> {
        checkLeader();
      }
      default -> {
        return;
      }
    }
  }

  /**
   * Finds and chooses a candidate leader by asking peers on the network to participate in the election.
   *
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  private void findLeader() {
    if (transactionHandler.getElectionFlag()) {
      return;
    }

    chooseLeaderFromParticipants(findParticipantsOnNetwork());
  }

  /**
   * Send every known peer a message asking if the peer is eligible to become the leader
   *
   * @return peers that want to participate in the leader election algorithm
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  private ArrayList<String> findParticipantsOnNetwork() {
    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);
    parameters.add(ProtocolMessage.START_ELECTION);
    parameters.add(transactionHandler.getPlayer());

    ArrayList<String> participants = new ArrayList<>();

    transactionHandler.getPlayers().forEach(player -> {
      Response response = sendToPlayer(player, parameters);

      if (response != null && response.getType() != RPCPayloadType.EXCEPTION) {
        List<String> replyParameters = response.getParameters();
        if (replyParameters != null && !replyParameters.isEmpty() && replyParameters.get(0).equals(ProtocolMessage.PARTICIPATE_ELECTION)) {
          participants.add(player);
        }
      }
    });
    return participants;
  }

  /**
   * Chooses the right peer to become the next leader and notifies that peer.
   *
   * @param participants list of participants of the leader election algorithm
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  private void chooseLeaderFromParticipants(ArrayList<String> participants) {

    if (participants.isEmpty()) {
      checkLeader();
    } else {
      notifyLeader(getParticipantWithHighestID(participants));
    }
  }

  /**
   * goes through the list of players and return the player with the highest ID
   *
   * @param participants list of participants
   * @return player with the highest id
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  public String getParticipantWithHighestID(ArrayList<String> participants) {
    return participants.stream().max(Comparator.comparing(String::valueOf)).orElse(participants.get(0));
  }

  /**
   * Notifies the peer that won the leader election
   *
   * @param player that won the election
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  private void notifyLeader(String player) {
    var parameters = new ArrayList<String>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);
    parameters.add(ProtocolMessage.NOTIFY_LEADER);

    Response response = sendToPlayer(player, parameters);

    if (response != null && response.getType() == RPCPayloadType.EXCEPTION) {
      chooseTheLeaderAlgorithm(ProtocolMessage.LEADER_ELECTION);
    }
  }

  /**
   * Checking if the leader is still alive and if player is
   * the leader or the old leader is still not responding then
   * the leader will start to notify all the peers that it has won the election
   *
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  private void checkLeader() {
    String leader = transactionHandler.getLeader();
    String player = transactionHandler.getPlayer();

    if (leader == null || leader.equals(player)) {
      notifyLeaderToAllNodes(player);
      return;
    }

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);
    parameters.add(ProtocolMessage.NOTIFY_LEADER);

    Response response = sendToPlayer(leader, parameters);

    if (response == null || response.getType() == RPCPayloadType.EXCEPTION) {
      notifyLeaderToAllNodes(player);
      setLeader(player);
      transactionHandler.removeFromPlayersList(leader);
    }
  }

  /**
   * Notify all known peers that it has won the election
   *
   * @param leader winner of the election
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  private void notifyLeaderToAllNodes(String leader) {
    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);
    parameters.add(ProtocolMessage.NEW_LEADER);
    parameters.add(leader);

    transactionHandler.getPlayers().forEach(player -> {
      Response response = sendToPlayer(player, parameters);

      if (response != null && response.getType() == RPCPayloadType.RESPONSE) {
        LOGGER.log(Level.INFO, response.getParameters().get(0));
      }
    });
    transactionHandler.setElectionFlag(false);
  }

  /**
   * Sets the leader of the peer
   *
   * @param player the leader
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  private void setLeader(String player) {
    transactionHandler.setLeader(player);
  }

  /**
   * Using the RPC mechanism it can send a message to a certain player/peer
   *
   * @param player     peer to receive the message
   * @param parameters given to the message
   * @return response of the peer/player
   */
  private Response sendToPlayer(String player, List<String> parameters) {
    try {
      String method = "reply";
      Request request = new Request(TransactionHandler.class.getName(), method, parameters);
      CompletableFuture<Response> completableFutureResponse = transactionHandler.getRpc().sendRequest(player, request);
      return completableFutureResponse.get();
    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
      return null;
    }
  }
}
