package nl.aimsites.nzbvuq.game.networking.transaction;

/**
 * ENUMS for three-phase commit algorithm, actions and choose the leader algorithm
 *
 * @author Dave Quentin (DM.Quentin@student.han.nl)
 */
public class ProtocolMessage {

  //Status Codes
  public static final String STATUS_OK = "STATUS: OK";
  public static final String ALREADY_EXEC = "STATUS: ALREADY_EXECUTED";

  //Three Phase Commit
  public static final String THREE_PHASE = "THREE_PHASE";
  public static final String ACTION = "ACTION";
  public static final String GLOBAL_COMMIT = "GLOBAL_COMMIT";
  public static final String COMMITTED = "COMMITTED";
  public static final String PREPARE_TO_COMMIT = "PREPARE_TO_COMMIT";
  public static final String READY_TO_COMMIT = "READY_TO_COMMIT";
  public static final String PREPARE = "PREPARE";
  public static final String VOTE_COMMIT = "VOTE_COMMIT";
  public static final String GLOBAL_ABORT = "GLOBAL_ABORT";
  public static final String ABORTED = "ABORTED";
  public static final String VOTE_ABORT = "VOTE_ABORT";
  public static final String PHASE_ONE_COMPLETED = "PHASE_ONE_COMPLETED";
  public static final String PHASE_TWO_COMPLETED = "PHASE_TWO_COMPLETED";
  public static final String PHASE_THREE_COMPLETED = "PHASE_THREE_COMPLETED";


  // choose leader
  public static final String LEADER_ELECTION = "LEADER_ELECTION";
  public static final String START_ELECTION = "START_ELECTION";
  public static final String PARTICIPATE_ELECTION = "PARTICIPATE_ELECTION";
  public static final String NOTIFY_LEADER = "NOTIFY_LEADER";
  public static final String CHECK_LEADER = "CHECK_LEADER";
  public static final String NEW_LEADER = "NEW_LEADER";

}