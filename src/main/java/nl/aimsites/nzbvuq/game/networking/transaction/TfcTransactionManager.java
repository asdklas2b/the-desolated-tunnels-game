package nl.aimsites.nzbvuq.game.networking.transaction;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.game.networking.TransactionHandler;
import nl.aimsites.nzbvuq.game.networking.dto.Action;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Contains the three-phase commit logic for the incoming actions
 *
 * @author Dave Quentin (DM.Quentin@student.han.nl)
 */
public class TfcTransactionManager {
  private final static int ABORT_DELAY = 5000;
  private final static int COMMIT_DELAY = 5000;

  private final TransactionHandler transactionHandler;
  private final Gson gson = new Gson();

  //Three phase commit variables
  private final Timer timer = new Timer();
  private TimerTask task;
  private boolean aborted;
  private boolean committed;

  /**
   * transaction manager constructor
   *
   * @param transactionHandler the instance of the transactionHandler
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  public TfcTransactionManager(TransactionHandler transactionHandler) {
    this.transactionHandler = transactionHandler;
  }

  /**
   * Three-phase commit reply for the leader and normal players
   *
   * @param arguments the incoming arguments (execute, status, object)
   * @return List of strings specified from functions inside the switch
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  public List<String> reply(List<String> arguments) {
    if (task != null) {
      task.cancel();
    }

    switch (arguments.get(1)) {
      case ProtocolMessage.ACTION -> {
        return this.action(gson.fromJson(arguments.get(2), Action.class));
      }
      case ProtocolMessage.PREPARE -> {
        return this.prepareAction();
      }
      case ProtocolMessage.PREPARE_TO_COMMIT -> {
        return this.prepareToCommitAction();
      }
      case ProtocolMessage.GLOBAL_COMMIT -> {
        return this.confirmAction(gson.fromJson(arguments.get(2), Action.class));
      }
      case ProtocolMessage.GLOBAL_ABORT -> {
        return this.abortAction();
      }
      default -> {
        return this.voteAbort();
      }
    }
  }

  /**
   * Incoming action from a player to the leader
   *
   * @param action the incoming action that needs to be verified and executed
   * @return List of strings with the parameter "STATUS: OK" or "STATUS: ALREADY_EXECUTED"
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private List<String> action(Action action) {
    ArrayList<String> response = new ArrayList<>();
    if (this.transactionHandler.canExecuteAction(action)) {
      this.startThreePhase(action);
      response.add(ProtocolMessage.STATUS_OK);
    } else {
      response.add(ProtocolMessage.ALREADY_EXEC);
    }
    return response;
  }

  /**
   * Start a new thread for the three-phase commit, this function will be executed by the leader.
   *
   * @param action the incoming action that needs to be executed
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private void startThreePhase(Action action) {
    Thread t = new Thread() {
      @Override
      public void run() {
        transactionHandler.sendAction(action);
      }
    };
    t.start();
  }

  /**
   * Player gives a response to the leader in the first phase of the three-phase commit
   *
   * @return List of strings
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private List<String> prepareAction() {
    ArrayList<String> response = new ArrayList<>();

    committed = false;
    aborted = false;
    task = new TimerTask() {
      @Override
      public void run() {
        abort();
      }
    };
    scheduleTask(task, ABORT_DELAY);
    response.add(ProtocolMessage.VOTE_COMMIT);

    return response;
  }

  /**
   * Player gives a response to the leader in the second phase of the three-phase commit
   *
   * @return List of strings
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private List<String> prepareToCommitAction() {
    ArrayList<String> response = new ArrayList<>();

    task = new TimerTask() {
      @Override
      public void run() {
        abort();
      }
    };
    scheduleTask(task, COMMIT_DELAY);
    response.add(ProtocolMessage.READY_TO_COMMIT);

    return response;
  }

  /**
   * Player gives a response to the leader in the last phase of the three-phase commit and process the action inside the gamestate
   *
   * @return List of strings
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private List<String> confirmAction(Action action) {
    ArrayList<String> response = new ArrayList<>();
    this.transactionHandler.receiveAction(action);
    committed = true;
    response.add(ProtocolMessage.COMMITTED);
    return response;
  }

  /**
   * Player aborts a action
   *
   * @return returns a string "ABORTED"
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private List<String> abortAction() {
    ArrayList<String> response = new ArrayList<>();

    if (!aborted && !committed) {
      abort();
      response.add(ProtocolMessage.ABORTED);
    }

    return response;
  }

  /**
   * Player votes to abort a action
   *
   * @return returns a string "VOTE_ABORT"
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private List<String> voteAbort() {
    ArrayList<String> response = new ArrayList<>();
    response.add(ProtocolMessage.VOTE_ABORT);
    return response;
  }

  /**
   * Set aborted on true
   *
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private void abort() {
    aborted = true;
  }

  /**
   * Creates a schedule, to check if the leader response within a specific time range
   *
   * @param task  the timer task
   * @param delay the time frame within a leader must respond
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private void scheduleTask(TimerTask task, int delay) {
    timer.schedule(task, delay);
  }
}
