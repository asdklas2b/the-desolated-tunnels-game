package nl.aimsites.nzbvuq.game.networking;

import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import nl.aimsites.nzbvuq.game.networking.transaction.*;
import nl.aimsites.nzbvuq.game.state.GameState;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyListenerContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyManagerContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyUserContract;
import nl.aimsites.nzbvuq.rpc.ConnectionRequestHandler;
import nl.aimsites.nzbvuq.rpc.RPC;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Contains the TransactionHandler logic This is where the request comes in from the RPC and returns
 * a set of strings
 *
 * @author Dave Quentin (DM.Quentin@student.han.nl)
 */
public class TransactionHandler implements ITransactionHandler, LobbyListenerContract {
  private static final Logger LOGGER = Logger.getLogger(TransactionHandler.class.getName());

  private final String player;
  private final ArrayList<String> players;
  private final RPC rpc;

  private TfcTransactionManager tfcTransactionManager;
  private TransactionManager transactionManager;
  private TfcTransactionCoordinator tfcTransactionCoordinator;
  private TransactionCoordinator transactionCoordinator;

  private String leader;
  private boolean eventFlag;
  private boolean electionFlag;
  private boolean gameStarted;
  private LinkedList<Action> eventList;
  private List<IReceiveAction> listeners;

  /**
   * Instantiates a new TransactionHandler with the normal manager and coordinator but also with the
   * three-phase commit coordinator and manager.
   *
   * @param rpc     the rpc
   * @param manager the lobbyManagerContract
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  public TransactionHandler(RPC rpc, LobbyManagerContract manager) {
    this.players = new ArrayList<>();
    this.listeners = new ArrayList<>();
    this.eventFlag = false;
    this.electionFlag = false;
    this.gameStarted = false;
    this.rpc = rpc;
    this.player = rpc.getIdentifier();
    this.eventList = new LinkedList<>();

    setTransactionManagersAndCoordinators(
      new TfcTransactionManager(this),
      new TransactionManager(this),
      new TfcTransactionCoordinator(this),
      new TransactionCoordinator(this));
    ConnectionRequestHandler.getInstance().addHandler(this);
    manager.listen(this);
  }

  /**
   * Overrides the reply methode of the Interface IReceiveAction
   *
   * @param arguments the incoming arguments from the request
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  @Override
  public List<String> reply(List<String> arguments) {
    if (arguments.get(0).equals(ProtocolMessage.THREE_PHASE)) {
      return tfcTransactionManager.reply(arguments);
    }
    if (arguments.get(0).equals(ProtocolMessage.LEADER_ELECTION)) {
      return transactionManager.reply(arguments);
    }
    return null;
  }

  /**
   * Sends the transaction with the right transaction manager based on the leader or a normal player
   *
   * @param action the object that gets send
   * @return boolean if the action can be executed locally
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  @Override
  public boolean sendAction(Action action) {
    if (player.equals(this.leader)) {
      if (action.getCharacterId().equals(leader)) {
        return executeActionFromLeader(action);
      } else {
        this.addEvent(action);
        this.executeEventsInList();
      }
    } else {
      return transactionCoordinator.manageTransaction(action);
    }
    return true;
  }

  /**
   * Check if action from the leader can be executed
   *
   * @param action the object that gets send
   * @return boolean if the action can be executed locally
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private Boolean executeActionFromLeader(Action action) {
    if (canExecuteAction(action)) {
      Thread t =
        new Thread() {
          @Override
          public void run() {
            addEvent(action);
            executeEventsInList();
          }
        };
      t.start();
      return true;
    }
    return false;
  }

  /**
   * Recursive function that executes all events inside the EventsList one after another First in
   * First out
   *
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private void executeEventsInList() {
    if (!eventList.isEmpty() && !this.eventFlag) {
      this.eventFlag = true;
      executeEvent(eventList.pop());
    }
  }

  /**
   * Recursive function that executes given event
   *
   * @param action start three-phase commit for incoming event
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private void executeEvent(Action action) {
    tfcTransactionCoordinator.manageTransaction(action);
    this.eventFlag = false;
    executeEventsInList();
  }

  /**
   * Add update to listeners
   *
   * @param action the executed action
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  @Override
  public void receiveAction(Action action) {
    listeners.forEach(listener -> listener.onUpdate(action));
    LOGGER.log(
      Level.INFO,
      () ->
        MessageFormat.format(
          "Player: {0} Did action: {1} with target: {2}",
          action.getCharacterId(), action.getSimpleClassName(), action.getTarget()));
  }

  /**
   * Add receiveAction listener
   *
   * @param listener new listener
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  @Override
  public void addListener(IReceiveAction listener) {
    this.listeners.add(listener);
  }

  /**
   * Start the choose the leader algorithm
   *
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  @Override
  public void startLeaderElection() {
    transactionCoordinator.chooseTheLeaderAlgorithm(ProtocolMessage.LEADER_ELECTION);
  }

  /**
   * Execute the choose the leader algorithm
   *
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  public void chooseTheLeaderAlgorithm(String status) {
    this.transactionCoordinator.chooseTheLeaderAlgorithm(status);
  }

  /**
   * Add Players from a String of Identifiers
   *
   * @param newPlayers List of incoming identifiers
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  public void addPlayers(List<LobbyUserContract> newPlayers) {
    newPlayers.forEach(
      newPlayer -> {
        if (!this.player.equals(newPlayer.getIdentifier())) {
          this.players.add(newPlayer.getIdentifier());
        }
      });
  }

  /**
   * Set transaction managers, coordinators for unit-testing
   *
   * @param tfcTransactionManager     set Transaction manager for Three-phase commit
   * @param transactionManager        set Transaction manager for normal requests
   * @param tfcTransactionCoordinator set Transaction coordinator for Three-phase commit
   * @param transactionCoordinator    set Transaction coordinator for normal requests
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  public void setTransactionManagersAndCoordinators(
    TfcTransactionManager tfcTransactionManager,
    TransactionManager transactionManager,
    TfcTransactionCoordinator tfcTransactionCoordinator,
    TransactionCoordinator transactionCoordinator) {
    this.tfcTransactionManager = tfcTransactionManager;
    this.transactionManager = transactionManager;
    this.tfcTransactionCoordinator = tfcTransactionCoordinator;
    this.transactionCoordinator = transactionCoordinator;
  }

  /**
   * Removes a player from the list of players used in the choose the leader algorithm
   *
   * @param oldLeader player that used to be the leader and is no longer responding
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   * @author Dave Quentin (Dm.Quentin@student.han.nl)
   */
  public void removeFromPlayersList(String oldLeader) {
    this.players.removeIf(
      listPlayer -> listPlayer.equals(oldLeader));
  }

  public void addEvent(Action action) {
    eventList.add(action);
  }

  public boolean canExecuteAction(Action action) {
    // TODO: check if attribute or something else isn't already being executed
    return true;
  }

  /**
   * Add Players onStartgame
   *
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  @Override
  public void onStartGame(LobbyContract lobby) {
    if(!gameStarted){
      gameStarted = true;
      addPlayers(lobby.getPlayers());
      leader = lobby.getIdentifier();
      LOGGER.log(
        Level.INFO,
        () ->
          MessageFormat.format(
            "Current player: {0}  Leader: {1}",
            player, leader));
    }
  }

  @Override
  public void onAdd(LobbyContract lobby) throws UnsupportedOperationException {
    // TODO: Add player to list
  }

  @Override
  public void onUpdate(LobbyContract lobby) throws UnsupportedOperationException {
    // TODO: Update player list
  }

  @Override
  public void onRemove(LobbyContract lobby) throws UnsupportedOperationException {
    // TODO: Delete player
  }

  @Override
  public void onJoin(LobbyContract lobby) throws UnsupportedOperationException {
  }

  public String getLeader() {
    return this.leader;
  }

  public boolean getElectionFlag() {
    return this.electionFlag;
  }

  public void setElectionFlag(boolean electionFlag) {
    this.electionFlag = electionFlag;
  }

  public RPC getRpc() {
    return this.rpc;
  }

  public String getPlayer() {
    return this.player;
  }

  public void setLeader(String leader) {
    this.leader = leader;
  }

  public ArrayList<String> getPlayers() {
    return new ArrayList<>(this.players);
  }

  @Override
  public String getCurrentPlayer() {
    return this.player;
  }
}
