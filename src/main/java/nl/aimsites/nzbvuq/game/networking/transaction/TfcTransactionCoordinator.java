package nl.aimsites.nzbvuq.game.networking.transaction;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.networking.TransactionHandler;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import nl.aimsites.nzbvuq.rpc.message.RPCPayloadType;
import nl.aimsites.nzbvuq.rpc.message.Request;
import nl.aimsites.nzbvuq.rpc.message.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Contains the three-phase commit logic for the outgoing actions
 *
 * @author Dave Quentin (DM.Quentin@student.han.nl)
 */
public class TfcTransactionCoordinator {
  private static final Logger LOGGER = Logger.getLogger(TfcTransactionCoordinator.class.getName());

  private final TransactionHandler transactionHandler;
  private final Gson gson = new Gson();

  //Three-phase-commit
  private boolean abort = false;
  private final Timer timer = new Timer();
  private TimerTask task;
  private static final int DELAY = 5000;

  /**
   * Three-phase commit transaction coordinator constructor
   *
   * @param transactionHandler the instance of the transactionHandler
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  public TfcTransactionCoordinator(TransactionHandler transactionHandler) {
    this.transactionHandler = transactionHandler;
  }

  /**
   * Three-phase commit transaction coordinator constructor
   *
   * @param action the incoming object
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  public void manageTransaction(Action action) {
    this.startThreePhase(action);
  }

  /**
   * Player gives a response to the leader in the first phase of the three-phase commit
   *
   * @param action the action that needs to be executed
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private void startThreePhase(Action action) {
    abort = false;
    this.phaseOne(action);
    this.phaseTwo(action);
    this.phaseThree(action);

    if (!action.getCharacterId().equals(transactionHandler.getLeader())) {
      this.transactionHandler.receiveAction(action);
    }
  }

  /**
   * Phase one of the three-phase commit
   *
   * @param action the action to be send
   * @return "PHASE_ONE_COMPLETED" when phase one is complete
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  public String phaseOne(Action action) {
    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add(ProtocolMessage.PREPARE);

    this.transactionHandler.getPlayers().forEach(player -> {
      if (!player.equals(action.getCharacterId())) {
        task = new TimerTask() {
          @Override
          public void run() {
            abort = true;
          }
        };
        timer.schedule(task, DELAY);
        sendActionToPlayer(player, parameters);
        task.cancel();
      }
    });

    return ProtocolMessage.PHASE_ONE_COMPLETED;
  }

  /**
   * Phase two of the three-phase commit
   *
   * @param action the action to be send
   * @return "PHASE_TWO_COMPLETED" when phase two is complete
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  public String phaseTwo(Action action) {
    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add(ProtocolMessage.PREPARE_TO_COMMIT);

    this.transactionHandler.getPlayers().forEach(player -> {
      if (!player.equals(action.getCharacterId())) {
        sendActionToPlayer(player, parameters);
      }
    });

    return ProtocolMessage.PHASE_TWO_COMPLETED;
  }

  /**
   * Phase three of the three-phase commit
   *
   * @param action the action to be send
   * @return "PHASE_THREE_COMPLETED" when phase three is complete
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  public String phaseThree(Action action) {
    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add(ProtocolMessage.GLOBAL_COMMIT);
    parameters.add(gson.toJson(action));

    this.transactionHandler.getPlayers().forEach(player -> {
      if (!player.equals(action.getCharacterId())) {
        sendActionToPlayer(player, parameters);
      }
    });

    return ProtocolMessage.PHASE_THREE_COMPLETED;
  }

  /**
   * Send action to a player with custom parameters
   *
   * @param player     the receiver
   * @param parameters the included parameters for the request
   * @return returns a string response from the receiver
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  private String sendActionToPlayer(String player, List<String> parameters) {
    try {
      String method = "reply";

      Request request = new Request(TransactionHandler.class.getName(), method, parameters);

      CompletableFuture<Response> completableFutureResponse = transactionHandler.getRpc().sendRequest(player, request);
      Response response = completableFutureResponse.get();

      if (response.getType() == RPCPayloadType.EXCEPTION) {
        return "Cannot connect with player, delete player and start the agent";
      } else {
        return response.getParameters().get(0);
      }

    } catch (Exception e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
      return null;
    }
  }
}