package nl.aimsites.nzbvuq.game.networking;

import nl.aimsites.nzbvuq.game.networking.dto.Action;

/**
 * IReceiveAction Listener, to listen to incoming actions
 *
 * @author Dave Quentin (DM.Quentin@student.han.nl)
 */
public interface IReceiveAction {

  /**
   * Do something on incoming action
   *
   * @param action a action
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  void onUpdate(Action action);

}
