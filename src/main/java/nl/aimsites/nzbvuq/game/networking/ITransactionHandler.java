package nl.aimsites.nzbvuq.game.networking;

import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.networking.dto.Action;

import java.io.IOException;
import java.util.List;

/**
 * ITransactionHandler interface for sending and receiving actions from other peers that need to be in sync
 *
 * @author Mark Jansen (MJ.Jansen4@student.han.nl)
 * @author Dave Quentin (DM.Quentin@student.han.nl)
 */
public interface ITransactionHandler {

  /**
   * Send object to other peers
   *
   * @param action gets send
   * @return status of transmission
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  boolean sendAction(Action action);

  /**
   * Overrides the reply methode of the Interface IReceiveAction
   *
   * @param arguments the incoming arguments from the request
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  List<String> reply(List<String> arguments) throws IOException;

  /**
   * Execute the chooseTheLeaderAlgorithm, test
   *
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  void startLeaderElection();

  /**
   * Add update to listeners
   *
   * @param action the executed action
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  void receiveAction(Action action);

  /**
   * Add receiveAction listener
   *
   * @param listener new listener
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  void addListener(IReceiveAction listener);

  List<String> getPlayers();

  //Bandaid fix for code that's even more bandaid fixy
  String getCurrentPlayer();
}
