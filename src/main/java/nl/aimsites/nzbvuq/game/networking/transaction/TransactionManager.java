package nl.aimsites.nzbvuq.game.networking.transaction;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.networking.TransactionHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * A transaction manager for the choose the leader algorithm.
 *
 * @author Dave Quentin (DM.Quentin@student.han.nl)
 * @author Mark Jansen (MJ.Jansen4@student.han.nl)
 */
public class TransactionManager {
  private final TransactionHandler transactionHandler;


  /**
   * Constructor of TransactionManager which gets the TransactionsHandler
   *
   * @param transactionHandler needed for algorithm
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  public TransactionManager(TransactionHandler transactionHandler) {
    this.transactionHandler = transactionHandler;
  }

  /**
   * Leader election receiver
   *
   * @param arguments the incoming arguments (execute, status, object)
   * @return a list with response
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  public List<String> reply(List<String> arguments) {
    switch (arguments.get(1)) {
      case ProtocolMessage.START_ELECTION -> {
        return handleStartElection(arguments);
      }
      case ProtocolMessage.NOTIFY_LEADER -> {
        return handleNotifyLeader();
      }
      case ProtocolMessage.NEW_LEADER -> {
        return handleNewLeader(arguments);
      }
      default -> {
        return null;
      }
    }
  }

  /**
   * Handler when a message of the START_ELECTION flag comes in.
   * Determines if the peer is eligible to participate in the election
   * by comparing the identifier of the sending peer and itself
   * if the identifier has a high value it wil return PARTICIPATE_ELECTION as response
   *
   * @param arguments from the message
   * @return response to the incoming message
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  private ArrayList<String> handleStartElection(List<String> arguments) {
    ArrayList<String> responseParameters = new ArrayList<>();

    transactionHandler.setElectionFlag(true);
    String electionStarter = arguments.get(2);

    if (electionStarter.compareTo(transactionHandler.getPlayer()) < 0) {
      responseParameters.add(ProtocolMessage.PARTICIPATE_ELECTION);
    } else {
      responseParameters.add(ProtocolMessage.STATUS_OK);
    }

    return responseParameters;
  }

  /**
   * Handler for the NOTIFY_LEADER flag
   * when this message arrives it means that this peer has won the election
   * Before notifying all other peers that it has won the election
   * It first checks with the current leader to see if it has come back online
   *
   * @return response to the message
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  private ArrayList<String> handleNotifyLeader() {
    ArrayList<String> responseParameters = new ArrayList<>();
    checkLeader();
    responseParameters.add(ProtocolMessage.STATUS_OK);
    return responseParameters;
  }

  /**
   * Handler for the NEW_LEADER election flag.
   * The new leader notifying this peer that he is the new leader
   * Sets new leader and removes old leader from the list of player due to being offline
   *
   * @param arguments from the message
   * @return Response to the message
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  private ArrayList<String> handleNewLeader(List<String> arguments) {
    ArrayList<String> responseParameters = new ArrayList<>();

    var newLeader = arguments.get(2);
    String oldLeader = transactionHandler.getLeader();

    if (!oldLeader.equals(newLeader)) {
      transactionHandler.removeFromPlayersList(oldLeader);
    }

    transactionHandler.setLeader(newLeader);
    transactionHandler.setElectionFlag(false);

    responseParameters.add(transactionHandler.getPlayer() + ": received new leader " + newLeader);

    return responseParameters;
  }

  /**
   * Start a new thread to handle choose the leader algorithm while also being able to reply to messages
   *
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  private void checkLeader() {
    Thread t = new Thread(() -> transactionHandler.chooseTheLeaderAlgorithm(ProtocolMessage.CHECK_LEADER));
    t.start();
  }
}
