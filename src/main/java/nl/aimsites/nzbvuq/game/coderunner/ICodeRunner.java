package nl.aimsites.nzbvuq.game.coderunner;

import java.util.List;

/**
 * ICodeRunner interface. This interfaces enables the user to run dynamic Java code.
 *
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public interface ICodeRunner {
  /**
   * Runs the given code on the current thread
   *
   * @param code Code to run
   * @param types Class types that are used within the method
   * @param arguments Arguments to pass to the method
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  void run(String code, List<Class<?>> types, Object... arguments);

  /**
   * Runs the given code on the current thread
   *
   * @param code Code to run
   * @param arguments Arguments to pass to the method
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  void run(String code, Object... arguments);

  /**
   * Runs the given code on a new thread
   *
   * @param code Code to run
   * @param types Class types that are used within the method
   * @param arguments Arguments to pass to the method
   * @return The newly created (and started) thread
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  Thread runAsync(String code, List<Class<?>> types, Object... arguments);

  /**
   * Runs the given code on a new thread
   *
   * @param code Code to run
   * @param arguments Arguments to pass to the method
   * @return The newly created (and started) thread
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  Thread runAsync(String code, Object... arguments);
}
