package nl.aimsites.nzbvuq.game.coderunner;

import org.burningwave.core.assembler.ComponentContainer;
import org.burningwave.core.classes.ExecuteConfig;

import java.util.ArrayList;
import java.util.List;

import static org.burningwave.core.assembler.StaticComponentContainer.ManagedLoggersRepository;

/**
 * CodeRunner class. This ICodeRunner implementation uses Burningwave Core.
 *
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class CodeRunner implements ICodeRunner {
  /**
   * CodeRunner constructor
   *
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public CodeRunner() {
    ManagedLoggersRepository.disableLogging();
  }

  /**
   * Runs the given code on the current thread
   *
   * @param code Code to run
   * @param types Class types that are used within the method
   * @param arguments Arguments to pass to the method
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  @Override
  public void run(String code, List<Class<?>> types, Object... arguments) {
    var generator = ExecuteConfig.forBodySourceGenerator();

    generator.addCodeLine(code);

    types.forEach(generator::useType);

    generator.withParameter(arguments);

    execute(generator);
  }

  /**
   * Runs the given code on the current thread
   *
   * @param code Code to run
   * @param arguments Arguments to pass to the method
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  @Override
  public void run(String code, Object... arguments) {
    run(code, new ArrayList<>(), arguments);
  }

  /**
   * Runs the given code on a new thread
   *
   * @param code Code to run
   * @param types Class types that are used within the method
   * @param arguments Arguments to pass to the method
   * @return The newly created (and started) thread
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  @Override
  public Thread runAsync(String code, List<Class<?>> types, Object... arguments) {
    var thread = new Thread(() -> run(code, types, arguments));

    thread.start();

    return thread;
  }

  /**
   * Runs the given code on a new thread
   *
   * @param code Code to run
   * @param arguments Arguments to pass to the method
   * @return The newly created (and started) thread
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  @Override
  public Thread runAsync(String code, Object... arguments) {
    return runAsync(code, new ArrayList<>(), arguments);
  }

  /**
   * Executes the source generator config
   *
   * @param config Config object to execute
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  private void execute(ExecuteConfig.ForBodySourceGenerator config) {
    ComponentContainer.getInstance().getCodeExecutor().execute(config);
  }
}
