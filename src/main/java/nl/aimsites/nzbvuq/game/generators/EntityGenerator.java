package nl.aimsites.nzbvuq.game.generators;

import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.entities.MonsterFactory;
import nl.aimsites.nzbvuq.game.entities.OpeningEntityFactory;
import nl.aimsites.nzbvuq.game.entities.StorageEntityFactory;
import nl.aimsites.nzbvuq.game.entities.character.monsters.Monster;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Storage;
import nl.aimsites.nzbvuq.game.entities.items.Strength;
import nl.aimsites.nzbvuq.game.entities.openings.Opening;
import nl.aimsites.nzbvuq.game.graph.Edge;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.tiles.Field;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Room;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import nl.aimsites.nzbvuq.game.state.GameMode;

import java.util.List;
import java.util.Random;

/**
 * This class generates entities that can be found in the game world
 *
 * @author Vick Top (v.top@student.han.nl)
 */
public class EntityGenerator {
  private Random random;
  private ItemEntityFactory itemFactory;
  private StorageEntityFactory storageFactory;
  private OpeningEntityFactory openingFactory;
  private MonsterFactory monsterFactory;
  private FlagHandler flagHandler;
  private GameMode gameMode;

  /**
   * This constructor makes a new instance of an EntityGenerator object
   *
   * @author Vick Top (v.top@student.han.nl)
   */
  public EntityGenerator() {
    random = new Random();
  }

  /**
   * This constructor makes a new instance of EntityGenerator for testing purposes. A fixed random
   * is given to check if a entity will be placed
   *
   * @param random This fixed param is used to check if a certain entity will be placed.
   * @author Vick Top (v.top@student.han.nl)
   */
  public EntityGenerator(Random random) {
    this.random = random;
  }

  public void setRandom(final Random random) {
    this.random = random;
  }

  public void setItemFactory(ItemEntityFactory itemFactory) {
    this.itemFactory = itemFactory;
  }

  public void setStorageFactory(StorageEntityFactory storageFactory) {
    this.storageFactory = storageFactory;
  }

  public void setOpeningFactory(OpeningEntityFactory openingFactory) {
    this.openingFactory = openingFactory;
  }

  public void setFlagHandler(FlagHandler flagHandler) {
    this.flagHandler = flagHandler;
  }

  public void setGameMode(GameMode gameMode) {
    this.gameMode = gameMode;
  }

  public void setMonsterFactory(MonsterFactory monsterFactory) {
    this.monsterFactory = monsterFactory;
  }

  /**
   * This method generates new entities that can be found in the gameworld
   *
   * @param graph This is the graph where the enities will be placed on
   * @author Vick Top (v.top@student.han.nl)
   */
  public void generateEntities(Graph graph) {
    Tile[][] listOfTiles = graph.getTiles();

    for (Tile[] tileArray : listOfTiles) {
      for (Tile tile : tileArray) {
        if (tile instanceof ReachableTile) {
          placeStorage((ReachableTile) tile);
          placeStrength((ReachableTile) tile);
          placeBlockages((ReachableTile) tile);
          placeMonsters((ReachableTile) tile);
        }
      }
    }

    if (gameMode == GameMode.CAPTURE_THE_FLAG) {
      flagHandler.generateFlagsIfConditionsMet(graph);
    }
  }

  /**
   * This method decides if a chest will be place in the game world based on a chance It also places
   * a possible item inside this chest
   *
   * @param tile This is the tile the chest will be placed on
   * @author Vick Top (v.top@student.han.nl)
   */
  public void placeStorage(ReachableTile tile) {
    final List<BaseItem> storageList = storageFactory.getItemsFromType(Storage.class);
    List<BaseItem> availableItems = itemFactory.getAvailableItems();

    for (BaseItem storage : storageList) {
      final Storage clonedStorage = (Storage) storageFactory.getCloneFromItem(storage);
      double chance = random.nextDouble();

      if (chance < clonedStorage.getDropChance()) {
        tile.addEntity(clonedStorage);

        for (BaseItem item : availableItems) {
          if (chance < item.getDropChance()
              && clonedStorage.getItems().size() < clonedStorage.getCapacity()) {
            clonedStorage.addItem(item);
          }
        }
      }
    }
  }

  /**
   * Places strength items on a {@link ReachableTile} depending on its drop chance
   *
   * @param tile the tile the strength has to be placed on
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  private void placeStrength(ReachableTile tile) {
    final List<BaseItem> strengthList = itemFactory.getItemsFromType(Strength.class);

    for (BaseItem strength : strengthList) {
      final Strength clonedStrength = (Strength) itemFactory.getCloneFromItem(strength);
      double chance = random.nextDouble();

      if (chance < clonedStrength.getDropChance()) {
        tile.addEntity(clonedStrength);
      }
    }
  }

  /**
   * Places a {@link Opening} between two {@link ReachableTile}
   *
   * @param tile a reachable tile
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  private void placeBlockages(ReachableTile tile) {
    if (tile instanceof Room) {
      List<Opening> blockageEntities = openingFactory.getAvailableOpenings();
      List<Edge> edges = tile.getAdjacentTiles();

      int index = 0;

      for (Edge edge : edges) {
        final boolean edgeHasNoBlockageEntityAndDestinationTileIsField =
            edge.getOpeningEntity() == null && edge.getDestinationTile() instanceof Field;

        if (edgeHasNoBlockageEntityAndDestinationTileIsField) {
          if (index >= blockageEntities.size()) {
            return;
          }

          edge.setOpeningEntity(blockageEntities.get(index++).clone());
        }
      }
    }
  }

  /**
   * Places cloned monsters from the monster factory on a reachable tile Uses the chance of the json
   * file to determine if it gets added to a tile - Witch on Fields - Giant on Fields - Troll in
   * Castles - Ogre on any reachable tile
   *
   * @param tile a reachable tile
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  public void placeMonsters(ReachableTile tile) {
    final List<Monster> monsterList = monsterFactory.getMonstersFromType(Monster.class);

    for (Monster monster : monsterList) {
      final Double spawnChance = monster.getLocations().get(tile.toString());
      if (spawnChance != null && random.nextDouble() < spawnChance) {
        tile.addEntity(monster.clone());
      }
    }
  }
}
