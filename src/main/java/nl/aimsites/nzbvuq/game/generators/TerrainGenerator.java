package nl.aimsites.nzbvuq.game.generators;

import com.github.czyzby.noise4j.map.Grid;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.*;

import java.util.Random;

/**
 * This class generates a {@link Graph} based on the passed in {@link Grid} with terrain
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 * @author Feida Wei (FD.Wei@student.han.nl)
 */
public class TerrainGenerator {
  private final int castleSize;
  private final Random random;

  /**
   * This constructor constructs an instance of this class
   *
   * @param castleSize max castle size
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  public TerrainGenerator(int castleSize) {
    this.castleSize = castleSize;
    random = new Random();
  }

  /**
   * This constructor construct an instance of this class
   *
   * @param castleSize max castle size
   * @param random     random number generator
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  public TerrainGenerator(int castleSize, final Random random) {
    this.castleSize = castleSize;
    this.random = random;
  }

  /**
   * Generates terrain by assigning terrain to tiles
   *
   * @param grid     the grid that needs to be used for the world.
   * @param startX   starting X
   * @param startY   starting Y
   * @param gridSize size of the grid
   * @param seed     unique randomly generated numbers to generate a world
   * @return graph with all tiles connected and terrain assigned to Tiles
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public Graph generateTerrain(Grid grid, int gridSize, int startX, int startY, long seed) {
    Graph graph = new Graph(startX, startY, gridSize, seed);
    Tile[][] array = graph.getTiles();
    assignTerrain(grid, startX, startY, array, graph);
    return graph;
  }

  /**
   * Assigns terrain to tiles
   *
   * @param grid   the grid that needs to be used for the world.
   * @param startX starting X
   * @param startY starting Y
   * @param array  tile array that is generated from the grid
   * @param graph  graph that will get tiles from the grid
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  private void assignTerrain(Grid grid, int startX, int startY, Tile[][] array, Graph graph) {
    final float fieldValue = 1.0f;
    final float roomValue = 0.5f;
    final float corridorValue = 0.0f;
    int fieldGeneratedCount = 0;

    for (int y = 0; y < grid.getHeight(); y++) {
      for (int x = 0; x < grid.getWidth(); x++) {
        final float cell = grid.get(x, y);
        Tile tile = null;

        if (cell == fieldValue) {
          if (fieldGeneratedCount++ % 5 == 0) {
            tile = new Forest(new Point(startX + x, startY + y));
          } else {
            tile = new Field(new Point(startX + x, startY + y), 1);
          }
        } else if (cell == roomValue) {
          tile = new Room(new Point(startX + x, startY + y), 1, RoomType.HOUSE);
        } else if (cell == corridorValue) {
          tile = new Corridor(new Point(startX + x, startY + y), 1);
        }
        if (tile != null) {
          graph.addTile(tile);
        }
      }
    }
    checkTiles(array);
  }

  /**
   * Loops through all the tiles in the tile array, and checks every tile
   *
   * @param tiles tile array that was generated from grid
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  public void checkTiles(Tile[][] tiles) {
    for (int x = 0; x < tiles.length; x++) {
      for (int y = 0; y < tiles[x].length; y++) {
        checkTile(tiles, tiles[x][y], x, y);
      }
    }
  }

  /**
   * Checks if the tile is an instance of Room, if that is true then find a castle. Creates castle if there is a castle.
   *
   * @param tiles tile array
   * @param tile  current tile
   * @param x     tile x coordinate
   * @param y     tile y coordinate
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  private void checkTile(Tile[][] tiles, Tile tile, int x, int y) {
    if (tile instanceof Room) {
      if (isCastleFound(tiles, x, y)) {
        createCastle(tiles, x, y);
      }
    }
  }

  /**
   * Creates castle by setting the type to castle if there is a 3x3 room in the grid
   *
   * @param tiles tile array
   * @param x     tile x coordinate
   * @param y     tile y coordinate
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  private void createCastle(Tile[][] tiles, int x, int y) {
    for (int i = 0; i < castleSize; i++) {
      for (int j = 0; j < castleSize; j++) {
        if ((x + i <= tiles.length) && (y + j <= tiles[y].length)) {
          if (tiles[x][y] instanceof Room) {
            ((Room) tiles[x + i][y + j]).setType(RoomType.CASTLE);
          }
        }
      }
    }
  }

  /**
   * Checks every tile if there is a 3x3 Room
   *
   * @param tiles tile array
   * @param x     tile x coordinate
   * @param y     tile y coordinate
   * @return if a 3x3 room is found return true
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  private boolean isCastleFound(Tile[][] tiles, int x, int y) {
    int counter = 0;
    for (int i = 0; i < castleSize - 1; i++) {
      if ((x + i <= tiles.length) && (y + i <= tiles[y].length)) {
        if (tiles[x + i][y] instanceof Room && tiles[x][y + i] instanceof Room) {
          if (((Room) tiles[x + i][y]).getType() == RoomType.HOUSE) {
            counter++;
          }
          if (((Room) tiles[x][y + i]).getType() == RoomType.HOUSE) {
            counter++;
          }
        }
      }
    }
    return counter > castleSize;
  }
}
