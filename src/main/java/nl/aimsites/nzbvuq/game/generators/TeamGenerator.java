package nl.aimsites.nzbvuq.game.generators;

import nl.aimsites.nzbvuq.game.entities.character.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Generates 2 teams with the list of players from the lobby
 *
 * @author Tim Weening (T.Weening@student.han.nl)
 */
public class TeamGenerator {
  private List<Player> team1;
  private List<Player> team2;

  /**
   * Constructor that initializes the teams
   *
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public TeamGenerator() {
    team1 = new ArrayList<>();
    team2 = new ArrayList<>();
  }

  /**
   * Shuffles and splits the players in teams.
   *
   * @param allPlayersInLobby List of all the players that need to be put in a team.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public void generateTeams(List<Player> allPlayersInLobby) {
    Collections.shuffle(allPlayersInLobby);
    splitPlayersInTeams(allPlayersInLobby);
    setPlayerTeamNumber(team1, 1);
    setPlayerTeamNumber(team2, 2);
  }

  /**
   * Splits the full list of players in two pieces. The first half of the list ends up in team 1 the other half
   * in team 2. When teams are unequel team 1 will always be the one team with one player less as the ints are
   * rounded down after dividing.
   *
   * @param players List of all the players in the lobby that want to participate in the game session.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  private void splitPlayersInTeams(List<Player> players) {
    int amountOfPlayers = players.size();
    team1 = players.subList(0, amountOfPlayers/2);
    team2 = players.subList(amountOfPlayers/2, amountOfPlayers);
  }

  /**
   * Gives all players from a team the corresponding team number
   *
   * @param teamList The list of players that needs the team number connected to them.
   * @param teamNumber The number of the team.
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  private void setPlayerTeamNumber(List<Player> teamList, int teamNumber) {
    teamList.forEach(player -> player.setTeamNumber(teamNumber));
  }


  /**
   * Gets all corresponding players in team 1
   *
   * @return list of players
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public List<Player> getTeam1() {
    return team1;
  }


  /**
   * Gets all corresponding players in team 2
   *
   * @return list of players
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public List<Player> getTeam2() {
    return team2;
  }

  /**
   * Sets players in team 1
   *
   * @param team1 list of players
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public void setTeam1(List<Player> team1) {
    this.team1 = team1;
  }

  /**
   * Sets players in team 2
   *
   * @param team2 list of players
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public void setTeam2(List<Player> team2) {
    this.team2 = team2;
  }
}
