package nl.aimsites.nzbvuq.game.generators;

import com.github.czyzby.noise4j.map.Grid;

/**
 * Helper class that renders the generated Grid
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class Noise4JView {
  private final Grid grid;

  /**
   * Constructs an instance of this class
   *
   * @param grid the generated grid
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public Noise4JView(Grid grid) {
    this.grid = grid;
  }

  /**
   * Display the grid in the console
   *
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public void display() {
    final float wallValue = 1.0f;
    final float roomValue = 0.5f;
    final float corridorValue = 0.0f;

    for (int y = 0; y < this.grid.getHeight(); y++) {
      for (int x = 0; x < this.grid.getWidth(); x++) {
        final float cell = this.grid.get(x, y);

        if (cell == wallValue) {
          System.out.print("   ");
        } else if (cell == roomValue) {
          System.out.print("###");
        } else if (cell == corridorValue) {
          System.out.print("|||");
        }
      }

      System.out.println();
    }
  }
}
