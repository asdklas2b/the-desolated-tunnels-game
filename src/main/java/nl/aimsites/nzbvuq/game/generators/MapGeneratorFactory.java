package nl.aimsites.nzbvuq.game.generators;

import com.github.czyzby.noise4j.map.generator.room.dungeon.DungeonGenerator;
import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.state.GameMode;

/**
 * Factory class that serves different MapGenerator implementations
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class MapGeneratorFactory {

  /**
   * Serves a Noise4J implementation of the MapGenerator
   *
   * @param gridSize size of the grid
   * @param gameMode the current game mode
   * @return Noise4JMapGenerator
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public static MapGenerator getNoise4JMapGenerator(
      int gridSize, GameMode gameMode, ItemEntityFactory itemFactory) throws Exception {
    final int minRoomSize = 1;
    final int maxRoomSize = 3;
    DungeonGenerator dungeonGenerator = new DungeonGenerator();

    dungeonGenerator.setMinRoomSize(minRoomSize);
    dungeonGenerator.setMaxRoomSize(maxRoomSize);
    return new Noise4JMapGenerator(
        gridSize,
        dungeonGenerator,
        new TerrainGenerator(maxRoomSize),
        new EntityGenerator(),
        new ItemEntityFactory(),
        gameMode);
  }
}
