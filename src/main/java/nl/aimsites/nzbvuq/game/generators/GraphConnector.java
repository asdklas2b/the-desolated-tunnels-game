package nl.aimsites.nzbvuq.game.generators;

import nl.aimsites.nzbvuq.game.graph.Edge;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.IntStream;

/**
 * This class connects two graphs with each other
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class GraphConnector {
  /**
   * Connects two graphs with each other
   *
   * @param currentGraph the graphh where the player is standing on
   * @param secondGraph the graph that has to be connected
   * @param direction the direction in which the map is generating
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public static void connectTwoGraph(Graph currentGraph, Graph secondGraph, Direction direction) {
    if (direction == Direction.UP) {
      Tile[] tileArray = currentGraph.getTiles()[0];

      connect(tileArray, secondGraph, 0, -1);
    } else if (direction == Direction.DOWN) {
      Tile[] tileArray = currentGraph.getTiles()[currentGraph.getTiles().length - 1];

      connect(tileArray, secondGraph, 0, 1);
    } else if (direction == Direction.LEFT) {
      Tile[] tileArray = new Tile[currentGraph.getTiles().length];
      Tile[][] graphArray = currentGraph.getTiles();

      IntStream.range(0, graphArray.length).forEach(y -> tileArray[y] = graphArray[y][0]);

      connect(tileArray, secondGraph, -1, 0);
    } else if (direction == Direction.RIGHT) {
      Tile[] tileArray = new Tile[currentGraph.getTiles().length];
      Tile[][] graphArray = currentGraph.getTiles();

      IntStream.range(0, graphArray.length)
          .forEach(y -> tileArray[y] = graphArray[y][graphArray[y].length - 1]);

      connect(tileArray, secondGraph, 1, 0);
    }
  }

  /**
   * Connect current {@link Graph} with newly generate {@link Graph}
   *
   * @param tileArray the tile array from the current graph
   * @param secondGraph the newly generated graph
   * @param xChange the change in x
   * @param yChange the change in y
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  private static void connect(
      Tile[] tileArray, Graph secondGraph, final int xChange, final int yChange) {
    Arrays.stream(tileArray)
        .filter(tile -> tile instanceof ReachableTile)
        .forEach(
            tile -> {
              Optional<Tile> destinationTile =
                  secondGraph.findTile(tile.getX() + xChange, tile.getY() + yChange);

              destinationTile.ifPresent(
                  dest -> {
                    if (!(dest instanceof ReachableTile)) {
                      return;
                    }

                    ((ReachableTile) dest).addAdjacentTile(new Edge(tile));
                    ((ReachableTile) tile).addAdjacentTile(new Edge(dest));
                  });
            });
  }
}
