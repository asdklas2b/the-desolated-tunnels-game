package nl.aimsites.nzbvuq.game.generators;

import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.entities.items.attributes.special.Flag;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import nl.aimsites.nzbvuq.game.graph.tiles.UnreachableTile;

import java.util.*;

/**
 * This class handles the state of all flags and generates whenever necessary.
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class FlagHandler {
  private Map<Point, Boolean> generatedPoints;
  private Random random;
  private ItemEntityFactory itemFactory;
  private List<Point> toBeRemovedPoints;
  private List<Point> toBeAddedPoints;
  private int maxFlags;
  private int minSeparationDistanceBetweenFlags;
  private int maxBoundary;
  private int teamNumber = 1;

  /**
   * Constructs an instance of FlagHandler
   *
   * @param itemFactory the item factory
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public FlagHandler(ItemEntityFactory itemFactory) {
    this(itemFactory, 2, 5, 5);
  }

  /**
   * Constructs an instance of FlagHandler allowing setting configuration
   *
   * @param itemFactory                       the item factory
   * @param maxFlags                          amount of flags to be generated
   * @param minSeparationDistanceBetweenFlags minimum distance between flags
   * @param maxBoundary                       maximum range to be generated
   */
  public FlagHandler(ItemEntityFactory itemFactory, final int maxFlags, final int minSeparationDistanceBetweenFlags, final int maxBoundary) {
    if (minSeparationDistanceBetweenFlags > maxBoundary) {
      throw new IllegalStateException("Minimum distance between flags is bigger than max boundary. This is not allowed.");
    }

    this.itemFactory = itemFactory;
    this.maxFlags = maxFlags;
    this.minSeparationDistanceBetweenFlags = minSeparationDistanceBetweenFlags;
    this.maxBoundary = maxBoundary;
    random = new Random();

    generateNewFlagPoints();
  }

  /**
   * Generate flags on the graph if the conditions are met. For instance if the generated {@link Flag} coordinate is within the current {@link Graph}
   *
   * @param graph the current graph
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   * @see Flag
   */
  public void generateFlagsIfConditionsMet(Graph graph) {
    final int startX = graph.getStartX();
    final int startY = graph.getStartY();
    final int graphSize = graph.getSize();
    toBeRemovedPoints = new ArrayList<>();
    toBeAddedPoints = new ArrayList<>();

    generatedPoints.entrySet().stream()
      .filter(v -> !v.getValue())
      .forEach(entry -> {
          Point point = entry.getKey();
          final boolean pointExistInsideCurrentGraph = (point.getX() >= startX && point.getX() <= startX + graphSize - 1)
            && (point.getY() >= startY && point.getY() <= startY + graphSize - 1);

          if (pointExistInsideCurrentGraph) {
            tryPlaceFlag(graph, entry, point);
          }
        }
      );

    updateGeneratedPointsList();
  }

  /**
   * Updates the generated points list with the to be added and removed points.
   *
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   * @see #tryPlaceFlag(Graph, Map.Entry, Point)
   */
  private void updateGeneratedPointsList() {
    toBeRemovedPoints.forEach(point -> generatedPoints.remove(point));
    toBeRemovedPoints.clear();

    toBeAddedPoints.forEach(point -> generatedPoints.put(point, false));
    toBeAddedPoints.clear();
  }

  /**
   * Generate a certain amount of new flag points
   *
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  private void generateNewFlagPoints() {
    teamNumber = 1;
    generatedPoints = new HashMap<>();

    for (int i = 0; i < maxFlags; i++) {
      generatedPoints.put(generateRandomPoint(), false);
    }
  }


  /**
   * Generate a random point within the max boundary
   *
   * @return a random generated point
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  private Point generateRandomPoint() {
    Point generatedPoint;
    do {
      final int x = random.nextInt(maxBoundary + maxBoundary) - maxBoundary;
      final int y = random.nextInt(maxBoundary + maxBoundary) - maxBoundary;

      generatedPoint = new Point(x, y);
    } while (generatedPoints.containsKey(generatedPoint) && !isInProximityOfOtherFlags(generatedPoint));

    return generatedPoint;
  }

  /**
   * Checks if the current generated {@link Point} is in proximity of other generated {@link Point}
   *
   * @param generatedPoint the generated point
   * @return a boolean whether it is in proximity or not, false means it is not in the proximity of other points
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  private boolean isInProximityOfOtherFlags(Point generatedPoint) {
    for (Point point : generatedPoints.keySet()) {
      if ((point.getX() - generatedPoint.getX() < minSeparationDistanceBetweenFlags)
        && (point.getY() - generatedPoint.getY() < minSeparationDistanceBetweenFlags)
      ) {
        return false;
      }
    }

    return true;
  }

  /**
   * Tries to place a {@link Flag} on the {@link Graph} on the passed in {@link Point}.
   * If it can not find a {@link ReachableTile} in the current {@link Graph} it will generate a new {@link Point} outside the graph for later evaluation.
   *
   * @param graph the current graph
   * @param entry a map entry containing the point
   * @param point a coordinate point
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  private void tryPlaceFlag(Graph graph, Map.Entry<Point, Boolean> entry, Point point) {
    graph.findTile(point.getX(), point.getY())
      .ifPresent(
        tile -> {
          Flag flag = (Flag) itemFactory.getItemsFromType(Flag.class).get(0).clone();
          if (tile instanceof UnreachableTile) {
            crawlForReachableTileAndTryToPlaceFlag(graph, entry, point, flag);
          } else {
            ((ReachableTile) tile).addEntity(flag);
            flag.setTeamNumber(teamNumber++);
            entry.setValue(true);
          }
        }
      );
  }

  /**
   * Crawls for a {@link ReachableTile} and tries to place a {@link Flag} when found.
   *
   * @param graph the current graph
   * @param entry the map entry of the point
   * @param point the generated point
   * @param flag  the flag to be placed
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  private void crawlForReachableTileAndTryToPlaceFlag(Graph graph, Map.Entry<Point, Boolean> entry, Point point, Flag flag) {
    crawlGraphUntilReachableTileIsFound(graph)
      .ifPresentOrElse(reachableTile -> {
          reachableTile.addEntity(flag);
          flag.setTeamNumber(teamNumber++);
          entry.setValue(true);
        },
        () -> { //generates a new point and remove the old one because there are no reachable tiles in the current graph (very unlikely, virtually impossible!)
          Point newPoint = generateRandomPoint();
          this.toBeRemovedPoints.add(point);
          this.toBeAddedPoints.add(newPoint);
        }
      );
  }

  /**
   * Crawls the {@link Graph} for a {@link ReachableTile} and returns the first one found.
   *
   * @param graph the graph to be crawled
   * @return an optional containing a reachable tile
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   * @see ReachableTile
   */
  private Optional<ReachableTile> crawlGraphUntilReachableTileIsFound(Graph graph) {
    Tile[][] tiles = graph.getTiles();
    for (Tile[] tileArray : tiles) {
      for (Tile tile : tileArray) {
        if (tile instanceof ReachableTile) {
          return Optional.of((ReachableTile) tile);
        }
      }
    }

    return Optional.empty();
  }
}
