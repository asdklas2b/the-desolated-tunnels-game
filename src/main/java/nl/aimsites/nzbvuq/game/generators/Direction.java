package nl.aimsites.nzbvuq.game.generators;

/**
 * This enum class is used to let the {@link MapGenerator} know in which direction it has to expand
 * the {@link nl.aimsites.nzbvuq.game.graph.Graph}
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public enum Direction {
  UP,
  DOWN,
  LEFT,
  RIGHT
}
