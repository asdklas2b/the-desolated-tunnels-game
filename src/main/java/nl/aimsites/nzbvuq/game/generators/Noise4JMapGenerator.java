package nl.aimsites.nzbvuq.game.generators;

import com.github.czyzby.noise4j.map.Grid;
import com.github.czyzby.noise4j.map.generator.room.dungeon.DungeonGenerator;
import com.github.czyzby.noise4j.map.generator.util.Generators;
import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.entities.MonsterFactory;
import nl.aimsites.nzbvuq.game.entities.OpeningEntityFactory;
import nl.aimsites.nzbvuq.game.entities.StorageEntityFactory;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import nl.aimsites.nzbvuq.game.state.GameMode;

import java.util.Random;

import static java.lang.Math.abs;

/**
 * Noise4J implementation of the MapGenerator interface class.
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class Noise4JMapGenerator implements MapGenerator {
  private final Grid grid;
  private final int gridSize;
  private final DungeonGenerator dungeonGenerator;
  private final TerrainGenerator terrainGenerator;
  private final EntityGenerator entityGenerator;
  private final Random rnd = new Random();
  private final TileConnector connector = new TileConnector();
  private long seed = 0;

  /**
   * Constructs an instance of {@link Noise4JMapGenerator}.
   *
   * @param gridSize             size of the grid
   * @param dungeonGenerator     the generator used to generate the map
   * @param gridToGraphConverter the class that converts the grid to a graph
   * @param entityGenerator      the generator used to generate entities
   * @param itemFactory          the factory used to generate items
   * @param gameMode             the current game mode
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public Noise4JMapGenerator(
    int gridSize,
    DungeonGenerator dungeonGenerator,
    TerrainGenerator gridToGraphConverter,
    EntityGenerator entityGenerator,
    ItemEntityFactory itemFactory,
    GameMode gameMode) throws Exception {
    this.gridSize = gridSize;
    this.grid = new Grid(gridSize);
    this.dungeonGenerator = dungeonGenerator;
    this.terrainGenerator = gridToGraphConverter;
    this.entityGenerator = entityGenerator;
    this.entityGenerator.setItemFactory(itemFactory);
    this.entityGenerator.setStorageFactory(new StorageEntityFactory());
    this.entityGenerator.setOpeningFactory(new OpeningEntityFactory());
    this.entityGenerator.setMonsterFactory(new MonsterFactory());
    this.entityGenerator.setStorageFactory(new StorageEntityFactory());
    this.entityGenerator.setOpeningFactory(new OpeningEntityFactory());
    this.entityGenerator.setFlagHandler(new FlagHandler(itemFactory));
    this.entityGenerator.setGameMode(gameMode);
  }

  @Override
  public Graph generateMap(int startX, int startY) {
    // do not remove "" because it's a string concat to generate a unique seed
    final long seed =
      Long.parseLong(
        "" + startX + (startY < 0 ? abs(startY) : startY) + gridSize + this.seed);

    return generateMapSeed(startX, startY, seed);
  }

  @Override
  public Graph generateMap(int startX, int startY, long seed) {
    this.seed = seed;
    return generateMapSeed(startX, startY, seed);
  }

  @Override
  public Graph expandMap(Graph fromGraph,Direction generationDirection) {
    Point coordinates = getStartingTileBasedOnDirection(fromGraph.getTiles(), generationDirection);

    Graph graph = generateMap(coordinates.getX(), coordinates.getY());
    GraphConnector.connectTwoGraph(fromGraph, graph, generationDirection);

    return graph;
  }

  @Override
  public Graph expandMap(Graph fromGraph, long seed, Direction generationDirection) {
    Point coordinates = getStartingTileBasedOnDirection(fromGraph.getTiles(), generationDirection);

    Graph graph = generateMap(coordinates.getX(), coordinates.getY(), seed);
    GraphConnector.connectTwoGraph(fromGraph, graph, generationDirection);

    return graph;
  }

  /**
   * Generate coordinate based on the passed in direction
   *
   * @param tiles               the tilemap it has to generate from
   * @param generationDirection the direction it has to expand the tilemap
   * @return the generated coordinate
   * @author Jiakai Zheng (J.Zheng1@student.han.nl)
   */
  private Point getStartingTileBasedOnDirection(Tile[][] tiles, Direction generationDirection) {
    switch (generationDirection) {
      case RIGHT -> {
        Tile tempTile = tiles[0][tiles.length - 1];
        return new Point(tempTile.getX() + 1, tempTile.getY());
      }
      case LEFT -> {
        Tile tempTile = tiles[0][0];
        return new Point(tempTile.getX() - gridSize, tempTile.getY());
      }
      case DOWN -> {
        Tile tempTile = tiles[tiles.length - 1][0];
        return new Point(tempTile.getX(), tempTile.getY() + 1);
      }
      case UP -> {
        Tile tempTile = tiles[0][0];
        return new Point(tempTile.getX(), tempTile.getY() - gridSize);
      }
    }

    throw new IllegalArgumentException(
      "Something has gone wrong. It may be possible that a wrong direction has been passed in.");
  }

  /**
   * Generate a map based on the seed
   *
   * @param startX the starting X point of the map
   * @param startY the starting Y point of the map
   * @param seed   the seed it the map has to generate from
   * @return the generated map in graph form
   * @author Jiakai Zheng (J.Zheng1@student.han.nl)
   */
  private Graph generateMapSeed(int startX, int startY, final long seed) {
    Generators.setRandom(new Random(seed));
    this.entityGenerator.setRandom(Generators.getRandom());

    dungeonGenerator.generate(grid);

    Graph map = terrainGenerator.generateTerrain(grid, gridSize, startX, startY, seed);

    connector.connectTiles(map.getTiles());

    entityGenerator.generateEntities(map);

    return map;
  }
}
