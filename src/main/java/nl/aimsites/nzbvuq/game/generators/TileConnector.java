package nl.aimsites.nzbvuq.game.generators;

import nl.aimsites.nzbvuq.game.graph.Edge;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;

/**
 * Connects all tiles with the given graph
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 **/
public class TileConnector {

  /**
   * Connects all adjacent tiles with each other
   *
   * @param tiles all tiles from the graph in a 2D array
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public void connectTiles(Tile[][] tiles) {
    for (int x = 0; x < tiles.length; x++) {
      for (int y = 0; y < tiles[x].length; y++) {
        connectTile(tiles, tiles[x][y], x, y);
      }
    }
  }

  /**
   * Connect all tiles that are adjacent to the current {@link Tile}
   *
   * @param tiles tile array to look for nearby tiles to connect to
   * @param tile  current tile
   * @param x     tile x coordinate
   * @param y     tile y coordinate
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  private void connectTile(Tile[][] tiles, Tile tile, int x, int y) {
    if (!(tile instanceof ReachableTile)) {
      return;
    }

    if (x - 1 >= 0 && tiles[x - 1][y] instanceof ReachableTile) {
      ((ReachableTile) tile).addAdjacentTile(new Edge(tiles[x - 1][y]));
    }
    if (y - 1 >= 0 && tiles[x][y - 1] instanceof ReachableTile) {
      ((ReachableTile) tile).addAdjacentTile(new Edge(tiles[x][y - 1]));
    }
    if (x + 1 < tiles.length && tiles[x + 1][y] instanceof ReachableTile) {
      ((ReachableTile) tile).addAdjacentTile(new Edge(tiles[x + 1][y]));
    }
    if (y + 1 < tiles[y].length && tiles[x][y + 1] instanceof ReachableTile) {
      ((ReachableTile) tile).addAdjacentTile(new Edge(tiles[x][y + 1]));
    }
  }
}
