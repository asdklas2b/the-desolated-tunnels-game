package nl.aimsites.nzbvuq.game.generators;

import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;

/**
 * The interface generates a graph which is the world where the player resides in.
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public interface MapGenerator {
  /**
   * Generates a graph based with the startX and startY as starting point
   *
   * @param startX starting X
   * @param startY starting Y
   * @return a graph containing all the tiles in the world
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  Graph generateMap(int startX, int startY);

  /**
   * Generates a graph based on the seed with the startX and startY as starting point
   *
   * @param startX starting X
   * @param startY starting Y
   * @param seed generates the map that belongs to the seed
   * @return a graph containing all the tiles in the world
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  Graph generateMap(int startX, int startY, long seed);

  /**
   * Generates a new graph (chunk) and connects it to the current residing graph
   *
   * @param fromGraph the graph (chunk) where it has to be expand from
   * @param generationDirection the direction in which the map has to expand to
   * @return the expanded graph (new)
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  Graph expandMap(Graph fromGraph, Direction generationDirection);

  /**
   * Generates a new graph (chunk) based on a seed and connects it to the current residing graph
   *
   * @param fromGraph the graph (chunk) where it has to be expand from
   * @param seed generates the map that belongs to the seed
   * @param generationDirection the direction in which the map has to expand to
   * @return the expanded graph (new)
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  Graph expandMap(Graph fromGraph, long seed, Direction generationDirection);
}
