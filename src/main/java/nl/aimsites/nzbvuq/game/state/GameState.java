package nl.aimsites.nzbvuq.game.state;

import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.generators.MapGenerator;
import nl.aimsites.nzbvuq.game.generators.MapGeneratorFactory;
import nl.aimsites.nzbvuq.game.generators.TeamGenerator;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import nl.aimsites.nzbvuq.game.inputhandler.Printer;
import nl.aimsites.nzbvuq.game.networking.ITransactionHandler;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import nl.aimsites.nzbvuq.lobby.LobbyManager;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyListenerContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyManagerContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyUserContract;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The GameState class contains everything needed for the current game configuration.
 *
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Mark Jansen (MJ.Jansen4@student.han.nl)
 */
public class GameState implements IGameState, LobbyListenerContract {
  public static final int GRID_SIZE = 10;
  public static final int DEFAULT_START_X = 0;
  public static final int DEFAULT_START_Y = 0;
  private static GameState instance;
  private long worldSeed = 0;
  private Player player = new Player("Playername", "Jorrit");
  private Graph currentWorld;
  private WorldHandler worldHandler;
  private MapGenerator mapGenerator;
  private ITransactionHandler network;
  private TeamGenerator teamGenerator;
  private ItemEntityFactory itemFactory;
  private GameMode gameMode;
  private Printer printer;
  private List<Player> otherPlayers = new ArrayList<>();
  private List<Player> allPlayersInLobby = new ArrayList<>();

  public ITransactionHandler getNetwork() {
    return network;
  }


  /**
   * GameState constructor. Initializes the object with custom seed.
   *
   * @author Tom Esendam (ts.esendam@student.han.nl)
   */
  public GameState(long seed) {
    try {
      worldSeed = seed;
      itemFactory = new ItemEntityFactory();
      gameMode = GameMode.CAPTURE_THE_FLAG;

      this.mapGenerator =
        MapGeneratorFactory.getNoise4JMapGenerator(GRID_SIZE, gameMode, itemFactory);
      currentWorld = mapGenerator.generateMap(DEFAULT_START_X, DEFAULT_START_Y, seed);
      worldHandler = new WorldHandler(mapGenerator);
      worldHandler.addChunk(currentWorld);

      //      ReachableTile randomReachableTile = currentWorld.getRandomReachableTile();
      //      spawnEntity(player, randomReachableTile.getX(), randomReachableTile.getY());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   * Add listener for on start game
   *
   * @author Dave Quentin (DM.Quentin@student.han.nl)
   */
  public void addListener(LobbyManagerContract managerContract) {
    managerContract.listen(this);
  }

  public void setPrinter(Printer printer) {
    this.printer = printer;
  }

  /**
   * Gets the instance of the GameState. Creates a new one if there is no current instance.
   *
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public static GameState getInstance() {

    return instance;
  }

  /**
   * Gets the instance of the GameState. Creates a new one if there is no current instance.
   *
   * @author Tom Esendam (ts.esendam@student.han.nl)
   */
  public static void setInstance(GameState newInstance) {
    instance = newInstance;
  }

  /**
   * Clears the current instance. Handy for resetting the gamestate while unit testing
   *
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public static void clearInstance() {
    instance = null;
  }

  public ItemEntityFactory getItemFactory() {
    return itemFactory;
  }

  public void setPlayer(Player player) {
    this.player = player;
  }

  public void setCurrentWorld(Graph currentWorld) {
    this.currentWorld = currentWorld;
  }

  public Player getPlayer() {
    return player;
  }

  public Graph getCurrentWorld() {
    return worldHandler
      .findGraphFromPoint(player.getCurrentPosition().getCoordinates())
      .orElseThrow();
  }

  public WorldHandler getWorldHandler() {
    return worldHandler;
  }

  public GameMode getGameMode() {
    return gameMode;
  }

  public void setGameMode(GameMode gameMode) {
    this.gameMode = gameMode;
  }

  public MapGenerator getMapGenerator() {
    return mapGenerator;
  }

  /**
   * send an action to other peers
   *
   * @param action that needs be send
   * @return successful or not
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  public boolean sendAction(Action action) {
    return network.sendAction(action);
  }

  /**
   * syncs a player based on the action received
   *
   * @param action received from another peer
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  @Override
  public void syncPlayer(Action action) {
    // TODO: 12/17/2020 finish player sync on incoming message
  }

  /**
   * get the player based in playerID
   *
   * @param playerID id
   * @return found player or null
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  public Player getPlayer(String playerID) {
    for (Player player : otherPlayers) {
      if (player.getIdentificationKey().equals(playerID)) {
        return player;
      }
    }
    return null;
  }

  /**
   * Places an entity on a tile
   *
   * @param character that gets places on the map.
   * @param x         position
   * @param y         position
   * @author Mark Jansen (MJ.Jansen4@han.student.nl)
   */
  public void spawnEntity(Character character, int x, int y) {
    Optional<Tile> optionalTile = currentWorld.findTile(x, y);

    if (optionalTile.isPresent()) {
      Tile tile = optionalTile.get();
      if (tile instanceof ReachableTile) {
        ((ReachableTile) tile).addEntity(character);
        character.setCurrentPosition((ReachableTile) tile);
      } else {
        spawnEntity(character, x + 1, y);
      }
    } else {
      spawnEntity(character, x + 1, y);
    }
  }

  public void setNetwork(ITransactionHandler network) {
    this.network = network;
    this.network.addListener(new ActionHandler());
  }

  /**
   * Get all players in the lobby, doesn't include the player him/herself.
   *
   * @return list of players
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  public List<Player> getOtherPlayers() {
    return otherPlayers;
  }

  /**
   * Set all players in the lobby, doesnt include the player him/herself.
   *
   * @param otherPlayers list of players
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  public void setOtherPlayers(List<Player> otherPlayers) {
    this.otherPlayers = otherPlayers;
  }

  /**
   * Get all players in current lobby
   *
   * @return list of players
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  public List<Player> getAllPlayersInLobby() {
    return allPlayersInLobby;
  }

  /**
   * Sets all players in lobby
   *
   * @param allPlayersInLobby list of players
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  public void setAllPlayersInLobby(List<Player> allPlayersInLobby) {
    this.allPlayersInLobby = allPlayersInLobby;
  }

  /**
   * Get all players in current team through TeamGenerator
   *
   * @return list of players
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public List<Player> getAllPlayersInTeam() {
    if (player.getTeamNumber() == 1) {
      return teamGenerator.getTeam1();
    } else {
      return teamGenerator.getTeam2();
    }
  }

  /**
   * Set all players in current team of the player.
   *
   * @param allPlayersInTeam list of players
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  public void setAllPlayersInTeam(List<Player> allPlayersInTeam) {
    if (player.getTeamNumber() == 1) {
      teamGenerator.setTeam1(allPlayersInTeam);
    } else {
      teamGenerator.setTeam2(allPlayersInTeam);
    }
  }

  @Override
  public void onAdd(LobbyContract lobby) {
    //
  }

  @Override
  public void onUpdate(LobbyContract lobby) {
    //
  }

  @Override
  public void onRemove(LobbyContract lobby) {
    //
  }

  public void setPlayers(List<LobbyUserContract> newPlayers) {
    newPlayers.forEach(
      newPlayer -> {
        if (allPlayersInLobby.stream().noneMatch(n -> n.getIdentificationKey().equals(newPlayer.getIdentifier()))) {
          var player = new Player(newPlayer.getIdentifier(), newPlayer.getName());
          if (!network.getCurrentPlayer().equals(newPlayer.getIdentifier())) {
            this.otherPlayers.add(player);
          } else {
            this.player = player;
          }
          allPlayersInLobby.add(player);
        }
      });
  }

  @Override
  public void onStartGame(LobbyContract lobby) {
    System.out.println("SEED = " + worldSeed);
    setPlayers(lobby.getPlayers());
    teamGenerator = new TeamGenerator();
    teamGenerator.generateTeams(allPlayersInLobby);

    allPlayersInLobby.forEach(
      x -> {
        ReachableTile randomReachableTile = currentWorld.getRandomReachableTile();
        spawnEntity(x, randomReachableTile.getX(), randomReachableTile.getY());
      });
  }


  @Override
  public void onJoin(LobbyContract lobby) {
    //
  }

  /**
   * Sets TeamGenerator in GameState
   *
   * @param teamGenerator instance of TeamGenerator
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  public void setTeamGenerator(TeamGenerator teamGenerator) {
    this.teamGenerator = teamGenerator;
  }

  public Printer getPrinter() {
    return printer;
  }
}
