package nl.aimsites.nzbvuq.game.state;

/**
 * This enum determines the game mode of the {@link GameState}
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public enum GameMode {
  LAST_MAN_STANDING,
  CAPTURE_THE_FLAG
}
