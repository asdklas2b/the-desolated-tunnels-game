package nl.aimsites.nzbvuq.game.state;

import nl.aimsites.nzbvuq.game.inputhandler.parser.InputHandler;
import nl.aimsites.nzbvuq.game.networking.IReceiveAction;
import nl.aimsites.nzbvuq.game.networking.dto.Action;

/**
 * ActionHandler class updates the actions of other players
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class ActionHandler implements IReceiveAction {
  private final InputHandler inputHandler;

  /**
   * Creates a new instance of ActionHandler
   *
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public ActionHandler() {
    this(new InputHandler());
  }

  /**
   * Creates a new instance of ActionHandler with the given inputhandler
   *
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public ActionHandler(InputHandler inputHandler) {
    this.inputHandler = inputHandler;
  }

  @Override
  public void onUpdate(Action action) {
    inputHandler.handleAction(action);
  }
}
