package nl.aimsites.nzbvuq.game.state;

import nl.aimsites.nzbvuq.game.networking.dto.Action;

/**
 * The GameState class contains everything needed for the current game configuration.
 *
 * @author Martijn Woolschot (M.Woolschot@student.han.nl - mock creator)
 * @author Mark Jansen (MJ.Jansen4@student.han.nl)
 */
public interface IGameState {

  /**
   * sync player based on an action received
   *
   * @param action that needs to be synced to a player based on the action received
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  void syncPlayer(Action action);
}
