package nl.aimsites.nzbvuq.game.state;

import nl.aimsites.nzbvuq.game.generators.Direction;
import nl.aimsites.nzbvuq.game.generators.MapGenerator;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.MoveDirection;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This class is responsible for keeping the state of all {@link Graph} in the game. It is also
 * responsible for expanding the map when the edge has been reached.
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class WorldHandler {
  private final List<Graph> chunks;
  private final MapGenerator mapGenerator;
  private Graph lastGeneratedChunk;

  /**
   * Constructs an instance of {@link WorldHandler}.
   *
   * @param mapGenerator the map generator
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public WorldHandler(MapGenerator mapGenerator) {
    this.mapGenerator = mapGenerator;
    this.chunks = new ArrayList<>();
  }

  /**
   * Get the destination {@link Tile}, if it doesn't exist yet it will expand the {@link Graph}
   *
   * @param currentTile the tile where the player resides on
   * @param direction the direction in which the player wants to move to
   * @return the tile where the player wants to walk to
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public Optional<Tile> getDestinationTile( ReachableTile currentTile, MoveDirection direction) {
    final Point newPoint = calculateNewPoint(currentTile, direction);
    final Optional<Graph> foundGraph = findGraphFromPoint(newPoint);
    final GameState gameState = GameState.getInstance();

    if (foundGraph.isPresent()) {
      gameState.setCurrentWorld(foundGraph.get());
      return foundGraph.get().findTile(newPoint.getX(), newPoint.getY());
    } else {
      final Graph newlyGeneratedGraph =
          mapGenerator.expandMap(findGraphFromPoint(currentTile.getCoordinates()).orElseThrow(), getExpandDirectFromMoveDirection(direction));
      this.addChunk(newlyGeneratedGraph);

      this.lastGeneratedChunk = newlyGeneratedGraph;
      return newlyGeneratedGraph.findTile(newPoint.getX(), newPoint.getY());
    }
  }

  public Optional<Tile> getTitle(Point newPoint) {
    final Optional<Graph> foundGraph = findGraphFromPoint(newPoint);
    final GameState gameState = GameState.getInstance();

    if (foundGraph.isPresent()) {
      gameState.setCurrentWorld(foundGraph.get());
      return foundGraph.get().findTile(newPoint.getX(), newPoint.getY());
    }
    return Optional.empty();
  }

  public List<Graph> getChunks() {
    return chunks;
  }

  /**
   * Adds a new chunk ({@link Graph}) to the list
   *
   * @param graph the newly generated graph
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public void addChunk(Graph graph) {
    if (graph == null) {
      throw new IllegalArgumentException("Graph is null!");
    }

    if (chunks.contains(graph)) {
      throw new IllegalArgumentException("Duplicate graph!");
    }

    this.chunks.add(graph);
  }

  public Graph getLastGeneratedChunk() {
    return lastGeneratedChunk;
  }

  /**
   * Converts {@link MoveDirection} to {@link Direction}
   *
   * @param direction the direction in which the players wants to move to
   * @return the {@link MoveDirection} equivalent
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  private Direction getExpandDirectFromMoveDirection(MoveDirection direction) {
    return switch (direction) {
      case NORTH -> Direction.UP;
      case SOUTH -> Direction.DOWN;
      case EAST -> Direction.RIGHT;
      case WEST -> Direction.LEFT;
      default -> throw new IllegalArgumentException("Unknown direction passed in!");
    };
  }

  /**
   * Find the {@link Graph} which contains the passed in {@link Point}, it if can not find it will
   * return an empty {@link Optional}
   *
   * @param newPoint the point which it has to find
   * @return an optional object
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public Optional<Graph> findGraphFromPoint(Point newPoint) {
    final int x = newPoint.getX();
    final int y = newPoint.getY();

    for (Graph graph : chunks) {
      final int startX = graph.getStartX();
      final int startY = graph.getStartY();
      final int size = graph.getSize();

      final boolean pointExistInsideCurrentGraph =
          (x >= startX && x <= startX + size - 1) && (y >= startY && y <= startY + size - 1);

      if (pointExistInsideCurrentGraph) {
        return Optional.of(graph);
      }
    }

    return Optional.empty();
  }

  /**
   * Calculate a new {@link Point} based on the passed in {@link MoveDirection}.
   *
   * @param currentTile the tile where the player is standing on
   * @param direction the direction the players wants to move to
   * @return a new {@link Point}
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  private Point calculateNewPoint(ReachableTile currentTile, MoveDirection direction) {
    return switch (direction) {
      case NORTH -> new Point(currentTile.getX(), currentTile.getY() - 1);
      case SOUTH -> new Point(currentTile.getX(), currentTile.getY() + 1);
      case EAST -> new Point(currentTile.getX() + 1, currentTile.getY());
      case WEST -> new Point(currentTile.getX() - 1, currentTile.getY());
      default -> new Point(currentTile.getX(), currentTile.getY());
    };
  }
}
