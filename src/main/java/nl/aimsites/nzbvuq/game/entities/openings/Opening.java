package nl.aimsites.nzbvuq.game.entities.openings;

import nl.aimsites.nzbvuq.game.entities.Entity;

import java.util.HashMap;
import java.util.Map;

/**
 * A {@link Opening} is an {@link Entity} that can block the movement between two {@link
 * nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile}.
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class Opening extends Entity implements Cloneable {
  private boolean isOpened;
  protected Map<String, String> actionResponses = new HashMap<>();

  /**
   * Constructs an instance of {@link Opening}
   *
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public Opening() {}

  public boolean isOpened() {
    return isOpened;
  }

  public void setOpened(boolean opened) {
    isOpened = opened;
  }

  public void setActionResponses(Map<String, String> actionResponses) {
    this.actionResponses = actionResponses;
  }

  public Map<String, String> getActionResponses() {
    return actionResponses;
  }

  /**
   * Gets the response for the open action
   *
   * @return A message that explains the actions and the consequences of this action.
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public String open() {
    if (!actionResponses.containsKey("open")) {
      return "The target cannot be opened";
    }

    return actionResponses.get("open");
  }

  @Override
  public String toString() {
    return "Opening{" + "name='" + name + '\'' + ", isOpened=" + isOpened + '}';
  }

  @Override
  public Opening clone() {
    try {
      return (Opening) super.clone();
    } catch (CloneNotSupportedException e) {
      // This exception can only occur when a object does not define the 'clone' interface
      // That's why we throw a runtime exception here, because it cannot occur.
      throw new RuntimeException("Could not clone the object", e);
    }
  }
}
