package nl.aimsites.nzbvuq.game.entities;

import nl.aimsites.nzbvuq.game.entities.items.*;
import nl.aimsites.nzbvuq.game.entities.items.attributes.special.Flag;
import nl.aimsites.nzbvuq.game.utils.JsonUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Factory for creating items from json file and creating instances of items from the possible items.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class ItemEntityFactory extends BaseEntityFactory {
  private final List<BaseItem> availableItems = new ArrayList<>();

  /**
   * Starts the conversion from json to objects.
   *
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public ItemEntityFactory() throws FileNotFoundException {
    initialize();
  }

  /**
   * Gets all the items with the given type.
   *
   * @param itemType
   * @param <T>
   * @return
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public <T extends BaseItem> List<BaseItem> getItemsFromType(Class<T> itemType) {
    return availableItems.stream().filter(itemType::isInstance).collect(Collectors.toList());
  }

  /**
   * searches for an item with the given name
   *
   * @param itemName
   * @return
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public BaseItem getItemFromString(String itemName) {
    return availableItems.stream().filter(item -> item.getName().equalsIgnoreCase(itemName)).findFirst().orElse(null);
  }

  /**
   * Gives a clone back from the item given
   *
   * @param item The item that needs to be cloned.
   * @return A clone from the item.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public BaseItem getCloneFromItem(BaseItem item) {
    return item.clone();
  }

  /**
   * Populates the internal ArrayList with all available items from the JSON file.
   *
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
   @Override
   public void populateFromJson(String unparsedJson) {
    JSONArray jsonItems = new JSONObject(unparsedJson).getJSONArray(JSON_KEY_ITEMS);
    for (Object jsonObject : jsonItems) {
      JSONObject json = (JSONObject) jsonObject;
      BaseItem item = configureItemFromJson(json);

      if (item == null) {
        continue;
      }

      item.setWeight(JsonUtils.getInt(json, JSON_KEY_WEIGHT, 0));
      item.setName(json.getString(JSON_KEY_NAME).toLowerCase());
      item.setActionResponses(configureActionResponses(json));
      availableItems.add(item);
    }
  }

  /**
   * Parses json to get the correct type of item
   *
   * @param json json to parse into a type of item
   * @return A BaseItem
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  private BaseItem configureItemFromJson(JSONObject json)
  {
    return switch (json.getString(JSON_KEY_TYPE)) {
      case JSON_KEY_TYPE_STRENGTH -> configureStrength(json);
      case JSON_KEY_TYPE_WEAPON -> configureWeapon(json);
      case JSON_KEY_TYPE_ARMOR -> configureArmor(json);
      case JSON_KEY_TYPE_SPECIAL -> configureSpecial(json);
      default -> null;
    };
  }

  /**
   * Configures all unique elements of item type attack
   *
   * @param json information about the item as JSONObject
   * @return An item of type attack containing all information from the json.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  private BaseItem configureStrength(JSONObject json) {
    var strength = new Strength();

    strength.setStrengthAddition(json.getInt(JSON_KEY_STRENGTH_ADDITION));
    strength.setDropChance(JsonUtils.getDouble(json, JSON_KEY_DROP_CHANCE, ItemDropChanceConfig.STRENGTH_ITEM_DEFAULT_SPAWN_CHANCE));

    return strength;
  }

  /**
   * Configures all unique elements of item type weapon
   *
   * @param json information about the item as JSONObject
   * @return An item of type Weapon containing all information from the json.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  private BaseItem configureWeapon(JSONObject json) {
    var attack = new Weapon();

    attack.setUsageCost(json.getInt(JSON_KEY_USAGE_COST));
    attack.setDamage(json.getInt(JSON_KEY_DAMAGE));
    attack.setMaxStrengthAddition(json.getInt(JSON_KEY_MAX_STRENGTH_ADDITION));

    return attack;
  }

  /**
   * Configures all unique elements of item type armor
   *
   * @param json information about the item as JSONObject
   * @return An item of type Armor containing all information from the json.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  private BaseItem configureArmor(JSONObject json) {
    var armor = new Armor();

    armor.setStrengthAddition(json.getInt(JSON_KEY_MAX_STRENGTH_ADDITION));
    armor.setMaxStrengthAddition(json.getInt(JSON_KEY_MAX_STRENGTH_ADDITION));

    return armor;
  }

  /**
   * Configures all unique elements of item type special
   *
   * @param json information about the item as JSONObject
   * @return An item of type Special containing all information from the json.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  private BaseItem configureSpecial(JSONObject json) {
    if (!json.getString(JSON_KEY_NAME).equals(JSON_KEY_FLAG)) {
      return new Special();
    }

    Flag flag = new Flag();
    flag.setWeight(json.getInt(JSON_KEY_WEIGHT));
    return flag;
  }

  public List<BaseItem> getAvailableItems() {
    return availableItems;
  }

  @Override
  public String toString() {
    return "ItemFactory{" +
      "availableItems=" + availableItems +
      '}';
  }
}
