package nl.aimsites.nzbvuq.game.entities;

import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Handles the basic logic of the entity factories
 *
 * @author Jorden Willemsen (j.willemsen4@student.han.nl)
 */
public abstract class BaseEntityFactory {
  private final InputStream configurationJson =
      getClass().getClassLoader().getResourceAsStream("entityConfiguration.json");

  protected static final String JSON_KEY_ITEMS = "items";
  protected static final String JSON_KEY_TYPE = "type";
  protected static final String JSON_KEY_TYPE_STRENGTH = "Strength";
  protected static final String JSON_KEY_TYPE_WEAPON = "Weapon";
  protected static final String JSON_KEY_TYPE_ARMOR = "Armor";
  protected static final String JSON_KEY_TYPE_SPECIAL = "Special";
  protected static final String JSON_KEY_WEIGHT = "weight";
  protected static final String JSON_KEY_NAME = "name";
  protected static final String JSON_KEY_STRENGTH_ADDITION = "strengthAddition";
  protected static final String JSON_KEY_DROP_CHANCE = "dropChance";
  protected static final String JSON_KEY_USAGE_COST = "usageCost";
  protected static final String JSON_KEY_DAMAGE = "damage";
  protected static final String JSON_KEY_MAX_STRENGTH_ADDITION = "maxStrengthAddition";
  protected static final String JSON_KEY_FLAG = "Flag";
  protected static final String JSON_KEY_POSSIBLE_ACTION = "possibleActions";
  protected static final String JSON_KEY_ACTION = "action";
  protected static final String JSON_KEY_RESPONSE = "response";
  protected static final String JSON_KEY_STORAGES = "storages";
  protected static final String JSON_KEY_STORAGE = "Storage";
  protected static final String JSON_KEY_CAPACITY = "capacity";
  protected static final String JSON_KEY_OPENINGS = "openings";
  protected static final String JSON_KEY_MONSTERS = "monsters";
  protected static final String JSON_KEY_MONSTER_LOCATIONS = "locations";
  protected static final String JSON_KEY_MONSTER_STRENGTH = "strength";
  protected static final String JSON_KEY_MONSTER_OGRE = "Ogre";
  protected static final String JSON_KEY_MONSTER_TROLL = "Troll";
  protected static final String JSON_KEY_MONSTER_WITCH = "Witch";
  protected static final String JSON_KEY_MONSTER_GIANT = "Giant";
  protected static final String JSON_KEY_MONSTER_SPAWN_TILE = "tile";
  protected static final String JSON_KEY_MONSTER_TILE_SPAWN_CHANCE = "chance";

  protected abstract void populateFromJson(String unparsedJson);

  /**
   * Gets a hashmap with all the available action responses for the JSON node
   *
   * @param json The JSON object for the opening
   * @return HashMap with all the available action responses. Null when no actions are possible.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  protected Map<String, String> configureActionResponses(JSONObject json) {
    Map<String, String> actionResponses = new HashMap<>();

    if (!json.has(JSON_KEY_POSSIBLE_ACTION)) {
      return actionResponses;
    }

    for (Object actionResponseObject : json.getJSONArray(JSON_KEY_POSSIBLE_ACTION)) {
      JSONObject response = (JSONObject) actionResponseObject;
      actionResponses.put(
          response.getString(JSON_KEY_ACTION), response.getString(JSON_KEY_RESPONSE));
    }

    return actionResponses;
  }

  /**
   * Initializes the StorageFactory
   *
   * @author Vick Top (v.top@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   * @throws FileNotFoundException Exception will be thrown if the JSON file cannot be found
   */
  protected void initialize() throws FileNotFoundException {
    var unparsedJson = getJsonFileContents();
    if (unparsedJson == null) {
      throw new FileNotFoundException("No Json has been found.");
    }

    populateFromJson(unparsedJson);
  }

  /**
   * Gets the JSON string contents from the items configuration json file
   *
   * @return String with the contents of the JSON file
   * @author Vick Top (v.top@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  protected String getJsonFileContents() {
    try {
      return new String(Objects.requireNonNull(configurationJson).readAllBytes());
    } catch (IOException | NullPointerException e) {
      return null;
    }
  }
}
