package nl.aimsites.nzbvuq.game.entities.character;

import nl.aimsites.nzbvuq.game.entities.Entity;
import nl.aimsites.nzbvuq.game.entities.items.Armor;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Weapon;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.MoveDirection;

import java.util.ArrayList;
import java.util.List;

/**
 * A Character is a entity that can perform actions and change the state of the game.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 * @author Mark Jansen (MJ.Jansen4@student.han.nl)
 */
public abstract class Character extends Entity {
  private static final int DEFAULT_PLAYER_STARTING_STRENGTH = 1000;

  private int peerID;
  private int strength;
  private int maxStrength;
  private Weapon equippedWeapon;
  private Inventory inventory = new Inventory();
  private String identificationKey;
  private ReachableTile currentPosition;
  private List<TileMove> history;

  /**
   * This constructor constructs the character class
   *
   * @author Feida Wei (FD.Wei@student.han.nl)
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  protected Character() {
    this.history = new ArrayList<>();
  }

  /**
   * A Character is a entity that can perform actions and change the state of the game. This also
   * sets the identificationKey and name.
   *
   * @param identificationKey peer network identifier
   * @param name name of the character
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  protected Character(String identificationKey, String name) {
    super(name);
    this.identificationKey = identificationKey;
    this.history = new ArrayList<>();
    initialiseDefaultPlayerSettings();
  }

  /**
   * A Character is a entity that can perform actions and change the state of the game. This also
   * sets the identificationKey.
   *
   * @param identificationKey of the peer
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  protected Character(String identificationKey) {
    super();
    this.identificationKey = identificationKey;
    this.history = new ArrayList<>();
    initialiseDefaultPlayerSettings();
  }

  /**
   * Sets the default character settings
   *
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  private void initialiseDefaultPlayerSettings() {
    inventory = new Inventory();
    strength = DEFAULT_PLAYER_STARTING_STRENGTH;
    maxStrength = DEFAULT_PLAYER_STARTING_STRENGTH;
  }

  /**
   * Adding strength to the player. Will never exceed the maximumStrength.
   *
   * @param amount The amount of strength that needs to be added to the player
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public void addStrength(int amount) {
    updateMaxStrengthValue();
    if (strength + amount >= maxStrength) {
      strength = maxStrength;
    } else {
      strength += amount;
    }
  }

  /**
   * Removes strength from the player. Will never go below zero.
   *
   * @param amount The amount of strength that needs to be taken from the player
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public void removeStrength(int amount) {
    updateMaxStrengthValue();
    if (strength - amount <= 0) {
      strength = 0;
    } else {
      strength -= amount;
    }
  }

  /**
   * calculates the total weight of all of the items. Can not go below zero
   *
   * @return total weight of items.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public int getTravelCostFromItems() {
    return Math.max(0, inventory.getItems().stream().mapToInt(BaseItem::getWeight).sum());
  }

  public void addNewMoveToHistory(ReachableTile reachableTile, MoveDirection moveDirection) {
    if (history.size() == 10) {
      history.remove(0);
    }
    history.add(new TileMove(reachableTile, moveDirection));
  }

  public List<TileMove> getHistory() {
    return history;
  }

  public int getStrength() {
    updateMaxStrengthValue();
    return strength;
  }

  public void setMaxStrength(int maxStrength) {
    this.maxStrength = maxStrength;
  }

  public int getMaxStrength() {
    updateMaxStrengthValue();
    return maxStrength;
  }

  public Inventory getInventory() {
    return inventory;
  }

  public void setEquippedWeapon(Weapon weapon) {
    equippedWeapon = weapon;
  }

  public Weapon getEquippedWeapon() {
    return equippedWeapon;
  }

  public ReachableTile getCurrentPosition() {
    return currentPosition;
  }

  public void setCurrentPosition(ReachableTile currentPosition) {
    this.currentPosition = currentPosition;
  }

  public int getPeerID() {
    return peerID;
  }

  public void setIdentificationKey(String identificationKey) {
    this.identificationKey = identificationKey;
  }

  public String getIdentificationKey() {
    return identificationKey;
  }

  public static class TileMove {
    ReachableTile reachableTile;
    MoveDirection moveDirection;

    public TileMove(ReachableTile reachableTile, MoveDirection moveDirection) {
      this.reachableTile = reachableTile;
      this.moveDirection = moveDirection;
    }

    public ReachableTile getReachableTile() {
      return reachableTile;
    }
  }

  /**
   * Updates the max strength of the player according to the items in the inventory.
   *
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  private void updateMaxStrengthValue() {
    List<BaseItem> items = inventory.getItems();
    int newMaxStrength = DEFAULT_PLAYER_STARTING_STRENGTH;

    newMaxStrength += calculateAddedEquipmentStrength(items);

    if (getEquippedWeapon() != null) {
      newMaxStrength += getEquippedWeapon().getMaxStrengthAddition();
      }

      maxStrength = newMaxStrength;
    }

  /**
   +   * Returns the total amount of max strength for all the items
   +   *
   +   * @param items The items that where the total amount of max strength needs to be calculated with
   +   * @return The amount of max strength from all the items
   +   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   +   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   +   */
  private int calculateAddedEquipmentStrength(List<BaseItem> items) {
    return items.stream()
      .filter(item -> item instanceof Armor)
      .mapToInt(item -> ((Armor) item).getMaxStrengthAddition())
      .sum();
    }
}
