package nl.aimsites.nzbvuq.game.entities.items;

import nl.aimsites.nzbvuq.game.entities.Entity;

import java.util.HashMap;
import java.util.Map;

/**
 * BaseItem is a class which contains all essential elements of items.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public abstract class BaseItem extends Entity implements Cloneable {
  protected int weight = 0;
  protected double dropChance = ItemDropChanceConfig.BASE_ITEM_DEFAULT_SPAWN_CHANCE;
  protected Map<String, String> actionResponses = new HashMap<>();

  /**
   * Generates a String which can be given to the player which contains information about the action
   * and its consequences.
   *
   * @return A message that explains the actions and the consequences of this action.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public String look() {
    return "There is nothing special about this " + this.name + ".";
  }

  /**
   * Generates a String which can be given to the player which contains information about the action
   * and its consequences.
   *
   * @return A message that explains the actions and the consequences of this action.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public String consume() {
    return "This is not an item you can consume.";
  }

  /**
   * Generates a String which can be given to the player which contains information about the action
   * and its consequences.
   *
   * @return A message that explains the actions and the consequences of this action.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public String pickUp() {
    return "This is not an item you can pick up.";
  }

  /**
   * Generates a String which can be given to the player which contains information about the action
   * and its consequences.
   *
   * @return A message that explains the actions and the consequences of this action.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public String drop() {
    return "This is not an item you can drop down.";
  }

  /**
   * Generates a String which can be given to the player which contains information about the action
   * and its consequences.
   *
   * @return A message that explains the actions and the consequences of this action.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public String attack() {
    return "This is not an item you can fight with.";
  }

  /**
   * Generates a String which can be given to the player which contains information about the action
   * and its consequences.
   *
   * @return A message that explains the actions and the consequences of this action.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public String equip() {
    return "This is not an item you can equip.";
  }

  /**
   * Generates a String which can be given to the player which contains information about the action
   * and its consequences.
   *
   * @return A message that explains the actions and the consequences of this action.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public String open() {
    return "This is not an item you can open.";
  }

  /**
   * Generates a String which can be given to the player which contains information about the action
   * and its consequences.
   *
   * @return A message that explains the actions and the consequences of this action.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public String use() {
    return "This is not an item you can use.";
  }

  public void setWeight(int weight) {
    this.weight = weight;
  }

  public int getWeight() {
    return weight;
  }

  public void setActionResponses(Map<String, String> actionResponses) {
    this.actionResponses = actionResponses;
  }

  public Map<String, String> getActionResponses() {
    return actionResponses;
  }

  public double getDropChance() {
    return dropChance;
  }

  public void setDropChance(double dropChance) {
    this.dropChance = dropChance;
  }

  @Override
  public String toString() {
    return "BaseItem{" + "name='" + name + '\'' + ", weight=" + weight + '}';
  }

  @Override
  public BaseItem clone() {
    try {
      return (BaseItem) super.clone();
    } catch (CloneNotSupportedException e) {
      // This exception can only occur when a object does not define the 'clone' interface
      // That's why we throw a runtime exception here, because it cannot occur.
      throw new RuntimeException("Could not clone the object", e);
    }
  }
}
