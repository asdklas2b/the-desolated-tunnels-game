package nl.aimsites.nzbvuq.game.entities.character.monsters;

/**
 * A monster type ogre.
 *
 * @author Feida Wei (FD.Wei@student.han.nl)
 */
public class Ogre extends Monster {
  /**
   * This constructor constructs the ogre class
   *
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  public Ogre() {
    super();
  }

  /**
   * This constructor constructs the ogre class
   *
   * @param identificationKey identification key for a monster
   * @param name              name of the monster
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  public Ogre(String identificationKey, String name) {
    super(identificationKey, name);
  }

  @Override
  public String look() {
    if (!actionResponses.containsKey("look")) {
      return super.look();
    }

    return actionResponses.get("look");
  }

  @Override
  public String attack() {
    if (!actionResponses.containsKey("attack")) {
      return super.attack();
    }

    return actionResponses.get("attack");
  }
}
