package nl.aimsites.nzbvuq.game.entities;

import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Storage;
import nl.aimsites.nzbvuq.game.utils.JsonUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Factory for creating storages from json file and creating instances of storages from the possible
 * storages.
 *
 * @author Vick Top (v.top@student.han.nl)
 */
public class StorageEntityFactory extends BaseEntityFactory {
  private final List<Storage> availableStorages = new ArrayList<>();

  /**
   * Starts the conversion from json to objects.
   *
   * @author Vick Top (v.top@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public StorageEntityFactory() throws FileNotFoundException {
    initialize();
  }

  /**
   * Gets all the items with the given type.
   *
   * @param itemType
   * @param <T>
   * @return
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public <T extends BaseItem> List<BaseItem> getItemsFromType(Class<T> itemType) {
    return availableStorages.stream().filter(itemType::isInstance).collect(Collectors.toList());
  }

  /**
   * Gives a clone back from the item given
   *
   * @param item The item that needs to be cloned.
   * @return A clone from the item.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public BaseItem getCloneFromItem(BaseItem item) {
    return item.clone();
  }

  /**
   * Populates the internal ArrayList with all available storages from the JSON file.
   *
   * @author Vick Top (v.top@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public void populateFromJson(String unparsedJson) {
    JSONArray jsonItems = new JSONObject(unparsedJson).getJSONArray(JSON_KEY_STORAGES);
    for (Object jsonObject : jsonItems) {
      JSONObject json = (JSONObject) jsonObject;
      Storage storage;
      if (!JSON_KEY_STORAGE.equals(json.getString(JSON_KEY_TYPE))) {
        continue;
      }

      storage = configureStorage(json);

      storage.setActionResponses(configureActionResponses(json));
      availableStorages.add(storage);
    }
  }

  /**
   * Configures the storages
   *
   * @param json information about the storage as JSONObject
   * @return An item of type storage containing all information from the json.
   * @author Vick Top (v.top@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  private Storage configureStorage(JSONObject json) {
    var storage = new Storage();

    storage.setCapacity(JsonUtils.getInt(json, JSON_KEY_CAPACITY, 1));
    storage.setName(json.getString(JSON_KEY_NAME).toLowerCase());

    return storage;
  }

  @Override
  public String toString() {
    return "StorageFactory{" + "availableStorages=" + availableStorages + '}';
  }
}
