package nl.aimsites.nzbvuq.game.entities.character;

/**
 * A Player is a character that is in the game to win. A player can be controlled by a human.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 * @author Mark Jansen (MJ.Jansen4@student.han.nl)
 */
public class Player extends Character {
  private int teamNumber;

  /**
   * Creates an instance of a player that can be controlled by a human player. This also sets the name of the player.
   *
   * @param identificationKey identifier of the character
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  public Player(String identificationKey) {
    super(identificationKey);
  }

  /**
   * Creates an instance of a player that can be controlled by a human player. This also sets the identifier and name of the player.
   *
   * @param identificationKey peer network identifier
   * @param name name of the character
   * @author Mark Jansen (MJ.Jansen4@student.han.nl)
   */
  public Player(String identificationKey, String name) {
    super(identificationKey, name);
  }

  public void setTeamNumber(int teamNumber) {
    this.teamNumber = teamNumber;
  }

  public int getTeamNumber() {
    return teamNumber;
  }
}
