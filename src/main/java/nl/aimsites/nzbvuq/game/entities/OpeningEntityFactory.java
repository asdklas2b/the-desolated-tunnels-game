package nl.aimsites.nzbvuq.game.entities;

import nl.aimsites.nzbvuq.game.entities.openings.Opening;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Factory for creating openings from json file and creating instances of openings from the possible
 * openings.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class OpeningEntityFactory extends BaseEntityFactory {
  private final List<Opening> availableOpenings = new ArrayList<>();

  /**
   * Starts the conversion from json to objects.
   *
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public OpeningEntityFactory() throws FileNotFoundException {
    initialize();
  }

  /**
   * Gets all the openings with the given type.
   *
   * @param openingType
   * @param <T>
   * @return
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public <T extends Opening> List<Opening> getOpeningFromType(Class<T> openingType) {
    return availableOpenings.stream().filter(openingType::isInstance).collect(Collectors.toList());
  }

  /**
   * searches for an opening with the given name
   *
   * @param openingName
   * @return
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public Opening getOpeningFromString(String openingName) {
    return availableOpenings.stream()
        .filter(opening -> opening.getName().equalsIgnoreCase(openingName))
        .findFirst()
        .orElse(null);
  }

  /**
   * Gives a clone back from the opening given
   *
   * @param opening The opening that needs to be cloned.
   * @return A clone from the opening.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public Opening getCloneFromOpening(Opening opening) {
    return opening.clone();
  }

  /**
   * Populates the internal ArrayList with all available openings from the JSON file.
   *
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public void populateFromJson(String unparsedJson) {
    JSONArray jsonOpenings = new JSONObject(unparsedJson).getJSONArray(JSON_KEY_OPENINGS);
    for (Object jsonObject : jsonOpenings) {
      JSONObject json = (JSONObject) jsonObject;
      Opening opening = new Opening();
      opening.setName(json.getString(JSON_KEY_NAME));
      opening.setActionResponses(configureActionResponses(json));
      availableOpenings.add(opening);
    }
  }

  public List<Opening> getAvailableOpenings() {
    return availableOpenings;
  }

  @Override
  public String toString() {
    return "OpeningFactory{" + "availableOpenings=" + availableOpenings + '}';
  }
}
