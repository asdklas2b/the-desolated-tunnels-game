package nl.aimsites.nzbvuq.game.entities;

import nl.aimsites.nzbvuq.game.entities.character.monsters.*;
import nl.aimsites.nzbvuq.game.utils.JsonUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Factory for creating items from json file and creating instances of items from the possible items.
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class MonsterFactory extends BaseEntityFactory{
  private final ArrayList<Monster> availableMonsters = new ArrayList<>();

  /**
   * Monster constructor
   *
   * @throws Exception throws an exception if the content of the json file cannot be retrieved
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public MonsterFactory() throws Exception {
    initialize();
  }

  /**
   * Populates the internal ArrayList with all available monsters from the JSON file.
   *
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  @Override
  public void populateFromJson(String unparsedJson) {
    JSONArray jsonMonsters = new JSONObject(unparsedJson).getJSONArray(JSON_KEY_MONSTERS);
    for (Object jsonObject : jsonMonsters) {
      JSONObject json = (JSONObject) jsonObject;
      Monster monster = configureMonster(json);
      availableMonsters.add(monster);
    }
  }

  /**
   * Configures all monsters with the json information
   *
   * @param json information about the monster as JSONObject
   * @return An monster containing all information from the json.
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  private Monster configureMonster(JSONObject json) {
    Monster monster;
    switch (json.getString(JSON_KEY_NAME)) {
      case JSON_KEY_MONSTER_TROLL -> monster = new Troll();
      case JSON_KEY_MONSTER_GIANT -> monster = new Giant();
      case JSON_KEY_MONSTER_WITCH -> monster = new Witch();
      case JSON_KEY_MONSTER_OGRE -> monster = new Ogre();
      default -> throw new IllegalStateException("Unexpected value: " + json.getString(JSON_KEY_NAME));
    }
    monster.setName(JsonUtils.getString(json, JSON_KEY_NAME, ""));
    monster.setStrength(JsonUtils.getInt(json, JSON_KEY_MONSTER_STRENGTH, 0));
    monster.setLocations(configureTileChance(json));
    return monster;
  }

  /**
   * Gives a clone back from the item given
   *
   * @param monster The monster that needs to be cloned.
   * @return A clone from the monster.
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  public Monster getCloneFromMonster(Monster monster) {
    return monster.clone();
  }

  /**
   * Gets a hashmap with all the available action responses for the JSON node
   *
   * @param json The JSON object for the item
   * @return HashMap with all the available action responses. Null when no actions are possible.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  private Map<String, Double> configureTileChance(JSONObject json) {
    Map<String, Double> locations = new HashMap<>();
    for (Object location : json.getJSONArray(JSON_KEY_MONSTER_LOCATIONS)) {
      JSONObject response = (JSONObject) location;
      locations.put(response.getString(JSON_KEY_MONSTER_SPAWN_TILE), response.getDouble(JSON_KEY_MONSTER_TILE_SPAWN_CHANCE));
      }
    return locations;
  }

  public List<Monster> getAvailableMonsters() {
    return availableMonsters;
  }

  public <T extends Monster> List<Monster> getMonstersFromType(Class<T> monsterType) {
    return availableMonsters.stream().filter(monsterType::isInstance).collect(Collectors.toList());
  }

  @Override
  public String toString() {
    StringBuilder availableMonsterList = new StringBuilder("MonsterFactory{");
    for (Monster availableMonster : availableMonsters) {
      availableMonsterList.append(availableMonster).append(", locations").append(availableMonster.getLocations());
    }
    availableMonsterList.append("}");
    return availableMonsterList.toString();
  }
}
