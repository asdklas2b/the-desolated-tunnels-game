package nl.aimsites.nzbvuq.game.entities.character;

import nl.aimsites.nzbvuq.game.entities.items.BaseItem;

import java.util.ArrayList;
import java.util.List;

/**
 * A place for entities to store items.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class Inventory {
  private List<BaseItem> items = new ArrayList<>();
  private int maxSize = 10;

  /**
   * Inventory constructor
   *
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public Inventory() {
  }

  /**
   * Inventory constructor
   *
   * @param maxSize maximum slots for the inventory of the character
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public Inventory(int maxSize) {
    this.maxSize = maxSize;
  }

  /**
   * Adds a item to the inventory.
   *
   * @param item The item that needs to be added to the inventory
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public void addItem(BaseItem item) {
    if (items.size() < maxSize) {
      items.add(item);
    }
  }

  /**
   * Removes a specific item from the inventory.
   *
   * @param item The item that needs to be removed.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public void removeItem(BaseItem item) {
    items.remove(item);
  }

  /**
   * Tries to find the item with the given name in the inventory
   *
   * @param itemName Item to search for
   * @return The item that was found, null if no item could be found
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public BaseItem searchForItem(String itemName) {
    return items.stream()
        .filter(item -> item.getName().equalsIgnoreCase(itemName))
        .findFirst()
        .orElse(null);
  }

  public List<BaseItem> getItems() {
    return items;
  }

  public int getMaxSize() {
    return maxSize;
  }
}
