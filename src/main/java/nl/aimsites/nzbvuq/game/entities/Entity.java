package nl.aimsites.nzbvuq.game.entities;

/**
 * Base class for an entity that can be found in the game world
 *
 * @author Vick Top (v.top@student.han.nl)
 */
public abstract class Entity {
  protected String name;

  /**
   * This this constructor makes a new instance of an Entity object
   *
   * @author Vick Top (v.top@student.han.nl)
   */
  protected Entity() {}

  /**
   * This this constructor makes a new instance of an Entity object. Also sets the name
   *
   * @author Vick Top (v.top@student.han.nl)
   */
  protected Entity(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
