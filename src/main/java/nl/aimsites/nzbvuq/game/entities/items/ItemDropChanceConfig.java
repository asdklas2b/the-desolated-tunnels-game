package nl.aimsites.nzbvuq.game.entities.items;

/**
 * This class contains the config values of {@link BaseItem} and its subclasses
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class ItemDropChanceConfig {
  public static final double BASE_ITEM_DEFAULT_SPAWN_CHANCE = 0.1;
  public static final double STRENGTH_ITEM_DEFAULT_SPAWN_CHANCE = 0.2;
}
