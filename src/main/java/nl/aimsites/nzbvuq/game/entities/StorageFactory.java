package nl.aimsites.nzbvuq.game.entities;

import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Storage;
import nl.aimsites.nzbvuq.game.utils.JsonUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Factory for creating storages from json file and creating instances of storages from the possible
 * storages.
 *
 * @author Vick Top (v.top@student.han.nl)
 */
public class StorageFactory {

  public final InputStream STORAGE_CONFIGURATION_JSON =
      getClass().getClassLoader().getResourceAsStream("entityConfiguration.json");
  private final List<Storage> availableStorages = new ArrayList<>();

  /**
   * Starts the conversion from json to objects.
   *
   * @author Vick Top (v.top@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public StorageFactory() {
    initialize();
  }

  /**
   * Gets all the items with the given type.
   *
   * @param itemType
   * @param <T>
   * @return
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public <T extends BaseItem> List<BaseItem> getItemsFromType(Class<T> itemType) {
    return availableStorages.stream().filter(itemType::isInstance).collect(Collectors.toList());
  }

  /**
   * Gives a clone back from the item given
   *
   * @param item The item that needs to be cloned.
   * @return A clone from the item.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public BaseItem getCloneFromItem(BaseItem item) {
    return item.clone();
  }

  /**
   * Initializes the StorageFactory
   *
   * @author Vick Top (v.top@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   * @throws JSONException Exception will be thrown if the JSON file cannot be found
   */
  private void initialize() {
    var unparsedJson = getJsonFileContents();
    if (unparsedJson == null) {
      throw new JSONException("No Json has been found.");
    }

    populateFromJson(unparsedJson);
  }

  /**
   * Gets the JSON string contents from the items configuration json file
   *
   * @return String with the contents of the JSON file
   * @author Vick Top (v.top@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  private String getJsonFileContents() {
    try {
      return new String(Objects.requireNonNull(STORAGE_CONFIGURATION_JSON).readAllBytes());
    } catch (IOException | NullPointerException e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Populates the internal ArrayList with all available storages from the JSON file. TODO: Add
   * support for populating storages with JSON file.
   *
   * @author Vick Top (v.top@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  private void populateFromJson(String unparsedJson) {
    JSONArray jsonItems = new JSONObject(unparsedJson).getJSONArray("storages");
    for (Object jsonObject : jsonItems) {
      JSONObject json = (JSONObject) jsonObject;
      Storage storage;
      if ("Storage".equals(json.getString("type"))) {
        storage = configureStorage(json);
      } else {
        System.out.println("not a correct item type.");
        continue;
      }

      storage.setActionResponses(configureActionResponses(json));
      availableStorages.add(storage);
    }
  }

  /**
   * Configures the storages
   *
   * @param json information about the storage as JSONObject
   * @return An item of type storage containing all information from the json.
   * @author Vick Top (v.top@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  private Storage configureStorage(JSONObject json) {
    var storage = new Storage();

    storage.setCapacity(JsonUtils.getInt(json, "capacity", 1));
    storage.setName(json.getString("name").toLowerCase());

    return storage;
  }

  /**
   * Gets a hashmap with all the available action responses for the JSON node
   *
   * @param json The JSON object for the storage
   * @return HashMap with all the available action responses. Null when no actions are possible.
   * @author Vick Top (v.top@student.han.nl)
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  private Map<String, String> configureActionResponses(JSONObject json) {
    Map<String, String> actionResponses = new HashMap<>();

    if (!json.has("possibleActions")) {
      return actionResponses;
    }
    for (Object actionResponseObject : json.getJSONArray("possibleActions")) {
      JSONObject response = (JSONObject) actionResponseObject;
      actionResponses.put(response.getString("action"), response.getString("response"));
    }

    return actionResponses;
  }

  @Override
  public String toString() {
    return "StorageFactory{" + "availableStorages=" + availableStorages + '}';
  }
}
