package nl.aimsites.nzbvuq.game.entities.items.attributes.special;

import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.entities.items.Special;

/**
 * The flag class is a special item type
 * This class contains a team number to keep track which flag belongs to which team.
 *
 * @author Feida Wei (FD.Wei@student.han.nl)
 */
public class Flag extends Special {
  private int teamNumber;

  public int getTeamNumber() {
    return teamNumber;
  }

  public void setTeamNumber(int teamNumber) {
    this.teamNumber = teamNumber;
  }
}
