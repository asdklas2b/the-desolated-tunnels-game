package nl.aimsites.nzbvuq.game.entities.character.monsters;

import nl.aimsites.nzbvuq.game.entities.character.Character;

import java.util.HashMap;
import java.util.Map;

/**
 * Class for all monster in the game
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public abstract class Monster extends Character implements Cloneable {
  protected int strength = 0;
  protected Map<String, Double> locations = new HashMap<>();
  protected Map<String, String> actionResponses = new HashMap<>();

  /**
   * This constructor constructs a monster
   *
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  protected Monster() {}

  /**
   * This constructor constructs a monster
   *
   * @param identificationKey identification key for a monster
   * @param name              name of the monster
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  protected Monster(String identificationKey, String name) {
    super(identificationKey, name);
  }

  /**
   * Generates a String which can be given to the player which contains information about the action
   * and its consequences.
   *
   * @return A message that explains the actions and the consequences of this action.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public String look() {
    return "There is nothing special about this " + this.name + ".";
  }

  /**
   * Generates a String which can be given to the player which contains information about the action
   * and its consequences.
   *
   * @return A message that explains the actions and the consequences of this action.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public String attack() {
    return "The " + name + " attacked and did " + strength + " damage!";
  }

  /**
   * Generates a String which can be given to the player which contains information about the action
   * and its consequences.
   *
   * @return A message that explains the actions and the consequences of this action.
   * @author Evan Franciszok (EW.Franciszok@student.han.nl)
   */
  public void setActionResponses(Map<String, String> actionResponses) {
    this.actionResponses = actionResponses;
  }

  public Map<String, String> getActionResponses() {
    return actionResponses;
  }

  public Map<String, Double> getLocations() {
    return locations;
  }

  public void setLocations(Map<String, Double> locations) {
    this.locations = locations;
  }

  @Override
  public int getStrength() {
    return strength;
  }

  public void setStrength(int strength) {
    this.strength = strength;
  }

  @Override
  public String toString() {
    return "Monster{" + "name='" + name + '\'' + ", strength=" + strength + '}';
  }

  @Override
  public Monster clone() {
    try {
      return (Monster) super.clone();
    } catch (CloneNotSupportedException e) {
      throw new RuntimeException("Could not clone the object", e);
    }
  }
}
