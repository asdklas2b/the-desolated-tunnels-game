package nl.aimsites.nzbvuq.game.entities.items;

import java.util.HashMap;

/**
 * Strength items are items the give the player more strength.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class Strength extends BaseItem {
  private int strengthAddition;

  @Override
  public String look() {
    if (!actionResponses.containsKey("look")) {
      return super.look();
    }

    return actionResponses.get("look");
  }

  @Override
  public String pickUp() {
    if (!actionResponses.containsKey("pick up")) {
      return super.pickUp();
    }

    return actionResponses.get("pick up");
  }

  @Override
  public String drop() {
    if (!actionResponses.containsKey("drop")) {
      return super.drop();
    }

    return actionResponses.get("drop");
  }

  @Override
  public String consume() {
    if (!actionResponses.containsKey("consume")) {
      return super.consume();
    }

    return actionResponses.get("consume");
  }

  public void setActionResponses(HashMap<String, String> actionResponses) {
    this.actionResponses = actionResponses;
  }

  public void setStrengthAddition(int strengthAddition) {
    this.strengthAddition = strengthAddition;
  }

  public int getStrengthAddition() {
    return strengthAddition;
  }
}
