package nl.aimsites.nzbvuq.game.entities.items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Storage items can store items of type baseItem. A example could be a Chest.
 *
 * @author Vick Top (v.top@student.han.nl)
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class Storage extends BaseItem {
  private int capacity;
  private List<BaseItem> items = new ArrayList<>();

  @Override
  public String look() {
    if (!actionResponses.containsKey("look")) {
      return super.look();
    }

    return actionResponses.get("look");
  }

  @Override
  public String open() {
    if (!actionResponses.containsKey("open")) {
      return super.open();
    }

    return actionResponses.get("open");
  }

  @Override
  public String toString() {
    return "Storage{" + "name='" + name + '\'' + ", capacity=" + capacity + '}';
  }

  public void addItem(BaseItem itemToBeAdded){
    if(items.size() < capacity){
      items.add(itemToBeAdded);
    }
    else{
      throw new RuntimeException("This storage has reached the capacity");
    }
  }

  public void removeItem(BaseItem itemToBeRemoved) {
    items.remove(itemToBeRemoved);
  }

  public List<BaseItem> getItems() {
    return items;
  }

  public void setActionResponses(HashMap<String, String> actionResponses) {
    this.actionResponses = actionResponses;
  }

  public void setCapacity(int capacity) {
    this.capacity = capacity;
  }

  public int getCapacity() {
    return capacity;
  }
}
