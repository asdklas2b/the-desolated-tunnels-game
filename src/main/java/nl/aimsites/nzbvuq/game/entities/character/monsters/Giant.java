package nl.aimsites.nzbvuq.game.entities.character.monsters;

/**
 * A monster type giant.
 *
 * @author Feida Wei (FD.Wei@student.han.nl)
 */
public class Giant extends Monster {
  /**
   * This constructor constructs the giant class
   *
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  public Giant() {
    super();
  }

  /**
   * This constructor constructs the giant class
   *
   * @param identificationKey identification key for a monster
   * @param name name of the monster
   * @author Feida Wei (FD.Wei@student.han.nl)
   */
  public Giant(String identificationKey, String name) {
    super(identificationKey, name);
  }

  @Override
  public String look() {
    if (!actionResponses.containsKey("look")) {
      return super.look();
    }

    return actionResponses.get("look");
  }

  @Override
  public String attack() {
    if (!actionResponses.containsKey("attack")) {
      return super.attack();
    }

    return actionResponses.get("attack");
  }
}
