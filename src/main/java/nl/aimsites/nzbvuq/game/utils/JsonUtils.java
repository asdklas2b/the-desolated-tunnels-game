package nl.aimsites.nzbvuq.game.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Utilities for working with JSON objects.
 *
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class JsonUtils {
  /**
   * Gets the JSON value as integer. Returns the default value if the key is not found.
   *
   * @param jsonObject JSONObject to get the key from
   * @param key Key to get
   * @param defaultValue Default value if the key is not found
   * @return The value of the key, defaultValue if it is not found
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public static int getInt(JSONObject jsonObject, String key, int defaultValue) {
    try {
      if (!jsonObject.has(key)) {
        return defaultValue;
      }

      return jsonObject.getInt(key);
    } catch (JSONException ex) {
      return defaultValue;
    }
  }

  /**
   * Gets the JSON value as string. Returns the default value if the key is not found.
   *
   * @param jsonObject JSONObject to get the key from
   * @param key Key to get
   * @param defaultValue Default value if the key is not found
   * @return The value of the key, defaultValue if it is not found
   * @author Martijn Woolschot (M.Woolschot@student.han.nl)
   */
  public static String getString(JSONObject jsonObject, String key, String defaultValue) {
    try {
      if (!jsonObject.has(key)) {
        return defaultValue;
      }

      var value = jsonObject.getString(key);

      return (value == null ? defaultValue : value);
    } catch (JSONException ex) {
      return defaultValue;
    }
  }

  /**
   * Gets the JSON value as double. Returns the default value if the key is not found.
   *
   * @param jsonObject JSONObject to get the key from
   * @param key Key to get
   * @param defaultValue Default value if the key is not found
   * @return The value of the key, defaultValue if it is not found
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public static double getDouble(JSONObject jsonObject, String key, double defaultValue) {
    try {
      if (!jsonObject.has(key)) {
        return defaultValue;
      }

      double value = jsonObject.getDouble(key);

      return value <= 0 ? defaultValue : value;
    } catch (JSONException ex) {
      return defaultValue;
    }
  }
}
