package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

/**
 * ASTNode for Subject
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class Subject extends ASTNode {
  /**
   * Subject constructor
   *
   * @param name name of the subject
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public Subject(String name) {
    super(name);
  }
}
