package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

/**
 * ASTNode for Action.
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class Action extends ASTNode {
  /**
   * Action constructor
   *
   * @param name name of the action
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public Action(String name) {
    super(name);
  }
}
