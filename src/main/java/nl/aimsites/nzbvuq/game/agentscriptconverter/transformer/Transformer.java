package nl.aimsites.nzbvuq.game.agentscriptconverter.transformer;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.*;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.InventoryPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.MainPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option.InventoryPriorityOption;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option.MainPriorityOption;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.ConditionRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.PriorityRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleFoodRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleWeaponDefaultRule;

import java.util.ArrayList;
import java.util.List;
/**
 * Class for the Transformer.
 *
 * @author Azez Salami (A.salami@student.han.nl)
 */
public class Transformer {

  public static final int NUMBER_OF_FOOD = 6;
  public static final int NUMBER_OF_ARMOR = 3;
  public static final int NUMBER_OF_WEAPON = 1;
  public static final int DEFAULT_STRENGTH_FOR_FOOD_RULE = 400;
  public static final int DEFAULT_STRENGTH = 500;
  public static final String ACTION_FIGHT = "fight";
  public static final String ACTION_RUN = "run";
  public static final String ACTION_FLEE = "flee";
  public static final String ACTION_PICKUP = "pick it up";
  public static final String ACTION_REPLACE = "replace";
  public static final String WEAPON_RULE_BEST = "best";
  public static final String CONDITION_RULE_ENEMY = "enemy";
  public static final String CONDITION_RULE_MONSTER = "monster";
  public static final String CONDITION_RULE_ATTRIBUTE = "attribute";
  public static final String CONDITION_RULE_WEAPON = "weapon";
  public static final String CONDITION_SEEING = "seeing";
  public static final String INVENTORY_OPTION_FOOD = "food";
  public static final String INVENTORY_OPTION_ARMOR = "armor";
  public static final String INVENTORY_OPTION_WEAPON = "weapon";
  public static final String PRIORITY_EXPLORE = "explore";
  public static final String PRIORITY_KILL_ENEMY = "kill enemy";

  /**
   * Transform method starts at the root of the AST and will go through all elements.
   *
   * @param ast the ast
   * @author Azez Salami (A.salami@student.han.nl)
   */
  public void transform(AST ast) {
    transformAgentScript(ast.getRoot());
  }

  /**
   * Specific transform for the root of the AST (AgentScript).
   *
   * @param agentScript the agent script
   * @author Azez Salami (A.salami@student.han.nl)
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  public void transformAgentScript(AgentScript agentScript) {
    List<ConditionRule> conditionRuleList = new ArrayList<>();
    List<PriorityRule> priorityRuleList = new ArrayList<>();
    SimpleFoodRule simpleFoodRule = null;
    SimpleWeaponDefaultRule simpleWeaponDefaultRule = null;
    for (ASTNode child : agentScript.getChildren()) {
      if (child instanceof ConditionRule) {
        conditionRuleList.add((ConditionRule) child);
        agentScript.removeChild(child);
      } else if (child instanceof PriorityRule) {
        priorityRuleList.add((PriorityRule) child);
        agentScript.removeChild(child);
      } else if (child instanceof SimpleFoodRule) {
        simpleFoodRule = (SimpleFoodRule) child;
        agentScript.removeChild(child);
      } else if (child instanceof SimpleWeaponDefaultRule) {
        simpleWeaponDefaultRule = (SimpleWeaponDefaultRule) child;
        agentScript.removeChild(child);
      }
    }

    transformConditionRule(conditionRuleList).forEach(agentScript::addChild);

    transformPriorityRule(priorityRuleList).forEach(agentScript::addChild);

    if (simpleFoodRule == null) {
      simpleFoodRule = getDefaultSimpleFoodRule();
    }

    if (simpleWeaponDefaultRule == null) {
      simpleWeaponDefaultRule = new SimpleWeaponDefaultRule(WEAPON_RULE_BEST);
    }

    agentScript.addChild(simpleFoodRule);
    agentScript.addChild(simpleWeaponDefaultRule);
  }

  /**
   * Specific transform for ConditionRule.
   *
   * @param conditionRuleList list of all the condition rules
   * @return List of the conditionRules with default condition rules if one of them missed
   * @author Azez Salami (A.salami@student.han.nl)
   */
  public List<ConditionRule> transformConditionRule(List<ConditionRule> conditionRuleList) {
    List<ConditionRule> newConditionRuleList = new ArrayList<>();
    ConditionRule conditionRuleEnemy = null;
    ConditionRule conditionRuleMonster = null;
    ConditionRule conditionRuleAttribute = null;
    ConditionRule conditionRuleWeapon = null;
    for (ConditionRule child : conditionRuleList) {
      for (ASTNode conditionRuleChild : child.getChildren())
        if (conditionRuleChild instanceof Subject) {
          if (conditionRuleChild.getName().equals(CONDITION_RULE_ENEMY)) {
            conditionRuleEnemy = child;
          }

          if (conditionRuleChild.getName().equals(CONDITION_RULE_MONSTER)) {
            conditionRuleMonster = child;
          }

          if (conditionRuleChild.getName().equals(CONDITION_RULE_ATTRIBUTE)) {
            conditionRuleAttribute = child;
          }

          if (conditionRuleChild.getName().equals(CONDITION_RULE_WEAPON)) {
            conditionRuleWeapon = child;
          }
        }
    }

    if (conditionRuleEnemy == null) {
      conditionRuleEnemy = getDefaultConditionRuleMonsterEnemy(CONDITION_RULE_ENEMY);
    }

    if (conditionRuleMonster == null) {
      conditionRuleMonster = getDefaultConditionRuleMonsterEnemy(CONDITION_RULE_MONSTER);
    }

    if (conditionRuleAttribute == null) {
      conditionRuleAttribute = getDefaultConditionRuleAttribute();
    }

    if (conditionRuleWeapon == null) {
      conditionRuleWeapon = getDefaultConditionRuleWeapon();
    }

    transformConditionRuleEnemyMonster(conditionRuleEnemy);
    transformConditionRuleEnemyMonster(conditionRuleMonster);

    newConditionRuleList.add(conditionRuleEnemy);
    newConditionRuleList.add(conditionRuleMonster);
    newConditionRuleList.add(conditionRuleAttribute);
    newConditionRuleList.add(conditionRuleWeapon);
    return newConditionRuleList;
  }

  /**
   * Specific transform for ConditionRule with "monster" as a name.
   *
   * @param conditionRuleEnemy the condition rule
   * @author Azez Salami (A.salami@student.han.nl)
   */
  public void transformConditionRuleEnemyMonster(ConditionRule conditionRuleEnemy) {
    Action action = new Action(ACTION_FIGHT);
    IfClause ifClause = null;
    for (ASTNode child : conditionRuleEnemy.getChildren()) {
      if (child instanceof Action) {
        action = (Action) child;
      }

      if (child instanceof IfClause) {
        ifClause = (IfClause) child;
      }
    }

    if (ifClause == null) {
      ifClause = new IfClause();
    }

    transformIfClause(ifClause, action.getName());
  }

  /**
   * Specific transform for PriorityRule.
   *
   * @param priorityRuleList list of all the priority rules
   * @return List of the priorityRules with default priorityRule if one of them missed
   * @author Azez Salami (A.salami@student.han.nl)
   */
  public List<PriorityRule> transformPriorityRule(List<PriorityRule> priorityRuleList) {
    List<PriorityRule> newPriorityRule = new ArrayList<>();
    MainPriority mainPriority = null;
    InventoryPriority inventoryPriority = null;
    for (ASTNode child : priorityRuleList) {
      for (ASTNode priorityChild : child.getChildren()) {
        if (priorityChild instanceof MainPriority) {
          mainPriority = (MainPriority) priorityChild;
          child.removeChild(priorityChild);
        } else if (priorityChild instanceof InventoryPriority) {
          inventoryPriority = (InventoryPriority) priorityChild;
          child.removeChild(priorityChild);
        }
      }
    }

    if (mainPriority == null) {
      mainPriority = getDefaultMainPriority();
    }

    if (inventoryPriority == null) {
      inventoryPriority = getDefaultInventoryPriority();
    }

    PriorityRule mainPriorityRule = new PriorityRule();
    mainPriorityRule.addChild(mainPriority);
    newPriorityRule.add(mainPriorityRule);
    PriorityRule inventoryPriorityRule = new PriorityRule();
    inventoryPriorityRule.addChild(inventoryPriority);
    newPriorityRule.add(inventoryPriorityRule);

    return newPriorityRule;
  }

  /**
   * Specific checks for IfClause.
   *
   * @param ifClause the if clause
   * @param mainAction the main action
   * @author Azez Salami (A.salami@student.han.nl)
   */
  public void transformIfClause(IfClause ifClause, String mainAction) {
    Action action = null;
    IfCondition ifCondition = null;

    for (ASTNode child : ifClause.getChildren()) {
      if (child instanceof Action) {
        action = (Action) child;
      }
      if (child instanceof IfCondition) {
        ifCondition = (IfCondition) child;
      }
    }

    if (action == null && ifCondition != null) {
      if (mainAction.equals(ACTION_RUN)) {
        ifClause.addChild(new Action(ACTION_FIGHT));
      }
      if (mainAction.equals(ACTION_FIGHT)) {
        ifClause.addChild(new Action(ACTION_FLEE));
      }
    }

    if (ifCondition == null) {
      ifCondition = new IfCondition();
      ifCondition.addChild(new StrengthComparison(LessMore.LESS, DEFAULT_STRENGTH));
      ifClause.addChild(ifCondition);
      ifClause.addChild(new Action(ACTION_FLEE));
    }
  }

  /**
   * Method makes default conditionRule with enemy or monster as subject
   *
   * @param subject the name of the subject
   * @return The default conditionRule
   * @author Azez Salami (A.salami@student.han.nl)
   */
  private ConditionRule getDefaultConditionRuleMonsterEnemy(String subject) {
    ConditionRule conditionRule = new ConditionRule();
    conditionRule.addChild(new Condition(CONDITION_SEEING));
    conditionRule.addChild(new Subject(subject));
    conditionRule.addChild(new Action(ACTION_FIGHT));
    IfCondition ifCondition = new IfCondition();
    ifCondition.addChild(new StrengthComparison(LessMore.LESS, DEFAULT_STRENGTH));
    IfClause ifClause = new IfClause();
    ifClause.addChild(ifCondition);
    ifClause.addChild(new Action(ACTION_FLEE));
    conditionRule.addChild(ifClause);
    return conditionRule;
  }

  /**
   * Method makes default inventoryPriority
   *
   * @return The default inventoryPriority
   * @author Azez Salami (A.salami@student.han.nl)
   */
  private InventoryPriority getDefaultInventoryPriority() {
    InventoryPriority inventoryPriority = new InventoryPriority();
    inventoryPriority.addChild(new InventoryPriorityOption(NUMBER_OF_FOOD, INVENTORY_OPTION_FOOD));
    inventoryPriority.addChild(
        new InventoryPriorityOption(NUMBER_OF_ARMOR, INVENTORY_OPTION_ARMOR));
    inventoryPriority.addChild(
        new InventoryPriorityOption(NUMBER_OF_WEAPON, INVENTORY_OPTION_WEAPON));
    return inventoryPriority;
  }

  /**
   * Method makes default ConditionRule for the Weapon
   *
   * @return The default ConditionRule
   * @author Azez Salami (A.salami@student.han.nl)
   */
  private ConditionRule getDefaultConditionRuleWeapon() {
    ConditionRule conditionRule = new ConditionRule();
    conditionRule.addChild(new Condition(CONDITION_SEEING));
    conditionRule.addChild(new Subject(CONDITION_RULE_WEAPON));
    conditionRule.addChild(new Action(ACTION_REPLACE));
    IfCondition ifCondition = new IfCondition("current weapon has lower strength");
    IfClause ifClause = new IfClause();
    ifClause.addChild(ifCondition);
    conditionRule.addChild(ifClause);
    return conditionRule;
  }

  /**
   * Method makes default mainPriority
   *
   * @return The default mainPriority
   * @author Azez Salami (A.salami@student.han.nl)
   */
  private MainPriority getDefaultMainPriority() {
    MainPriority mainPriority = new MainPriority();
    mainPriority.addChild(new MainPriorityOption(PRIORITY_EXPLORE));
    mainPriority.addChild(new MainPriorityOption(PRIORITY_KILL_ENEMY));
    return mainPriority;
  }

  /**
   * Method makes default ConditionRule for the Attribute
   *
   * @return The default ConditionRule
   * @author Azez Salami (A.salami@student.han.nl)
   */
  private ConditionRule getDefaultConditionRuleAttribute() {
    ConditionRule conditionRule = new ConditionRule();
    conditionRule.addChild(new Condition(CONDITION_SEEING));
    conditionRule.addChild(new Subject(CONDITION_RULE_ATTRIBUTE));
    conditionRule.addChild(new Action(ACTION_PICKUP));
    return conditionRule;
  }

  /**
   * Method makes default SimpleFoodRule
   *
   * @return The default SimpleFoodRule
   * @author Azez Salami (A.salami@student.han.nl)
   */
  private SimpleFoodRule getDefaultSimpleFoodRule() {
    SimpleFoodRule simpleFoodRule = new SimpleFoodRule();
    simpleFoodRule.addChild(new Food(INVENTORY_OPTION_FOOD));
    StrengthComparison newStrengthComparison =
        new StrengthComparison(LessMore.LESS, DEFAULT_STRENGTH_FOR_FOOD_RULE);
    simpleFoodRule.addChild(newStrengthComparison);
    return simpleFoodRule;
  }
}
