package nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.ASTNode;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.Food;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.StrengthComparison;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.SimpleRule;

import java.util.ArrayList;

/**
 * ASTNode for SimpleFoodRule.
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class SimpleFoodRule extends SimpleRule {
  private Food food;
  private StrengthComparison strengthComparison;

  @Override
  public ArrayList<ASTNode> getChildren() {
    ArrayList<ASTNode> arrayList = new ArrayList<>();
    if (food != null) {
      arrayList.add(food);
    }
    if (strengthComparison != null) {
      arrayList.add(strengthComparison);
    }

    return arrayList;
  }

  @Override
  public void addChild(ASTNode child) {
    if (child instanceof Food) {
      food = (Food) child;
    } else if (child instanceof StrengthComparison) {
      strengthComparison = (StrengthComparison) child;
    }
  }

  @Override
  public void removeChild(ASTNode child) {
    if (child instanceof Food) {
      food = null;
    } else if (child instanceof StrengthComparison) {
      strengthComparison = null;
    }
  }

  public StrengthComparison getStrengthComparison() {
    return strengthComparison;
  }

  public Food getFood() {
    return food;
  }
}
