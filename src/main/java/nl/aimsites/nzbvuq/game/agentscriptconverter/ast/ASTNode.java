package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

import java.util.ArrayList;
import java.util.List;

/**
 * Parent abstract class for all ASTNodes in the ASTTree.
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public abstract class ASTNode {
  private List<ASTNode> body = new ArrayList<>();
  private String error;
  private String name;

  /**
   * ASTNode constructor
   *
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  protected ASTNode() {}

  /**
   * ASTNode constructor
   *
   * @param name name of the ASTNode
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  protected ASTNode(String name) {
    this.name = name;
  }

  /**
   * Returns simplified classname of the ASTNode + potential name
   *
   * @return returns a simplified name
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public String getNodeLabel() {
    if (name != null) {
      return getClass().getSimpleName() + ": " + name;
    }

    return getClass().getSimpleName();
  }

  /**
   * Returns a copy of body
   *
   * @return returns a copy of body
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public List<ASTNode> getChildren() {
    return new ArrayList<>(body);
  }

  /**
   * Adds the given ASTNode to the body
   *
   * @param child ASTNode to be added to the body
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public void addChild(ASTNode child) {
    body.add(child);
  }

  /**
   * Removes the given ASTNode from the body
   *
   * @param child ASTNode to be removed from the body
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public void removeChild(ASTNode child) {
    body.remove(child);
  }

  /**
   * Getter for error
   *
   * @return returns the error in the ASTNode
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public String getError() {
    return error;
  }

  /**
   * Setter for error
   *
   * @param description Error to be set to the ASTNode
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public void setError(String description) {
    this.error = description;
  }

  /**
   * Getter for name
   *
   * @return returns the name of the ASTNode
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name
   *
   * @param name new name to be set
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getNodeLabel()).append(System.lineSeparator());

    getChildren().forEach(child -> child.toString(stringBuilder, 1));

    return stringBuilder.toString();
  }

  /**
   * toString method for building a tree
   *
   * @param stringBuilder stringbuilder to append on
   * @param indent depth of the child in the tree
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public void toString(StringBuilder stringBuilder, int indent) {
    stringBuilder.append("  ".repeat(Math.max(0, indent)));

    if (!getChildren().isEmpty()) {
      stringBuilder.append(getNodeLabel()).append(System.lineSeparator());
      getChildren().forEach(child -> child.toString(stringBuilder, indent + 1));
    } else {
      stringBuilder.append(toString());
    }
  }
}
