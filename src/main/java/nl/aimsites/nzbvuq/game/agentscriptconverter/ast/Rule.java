package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

/**
 * Parent abstract class for all Rules
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public abstract class Rule extends ASTNode {}
