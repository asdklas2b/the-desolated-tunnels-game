package nl.aimsites.nzbvuq.game.agentscriptconverter;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.AST;
import nl.aimsites.nzbvuq.game.agentscriptconverter.checker.Checker;
import nl.aimsites.nzbvuq.game.agentscriptconverter.checker.IChecker;
import nl.aimsites.nzbvuq.game.agentscriptconverter.exception.UndefinedASTException;
import nl.aimsites.nzbvuq.game.agentscriptconverter.generator.Generator;
import nl.aimsites.nzbvuq.game.agentscriptconverter.generator.IGenerator;
import nl.aimsites.nzbvuq.game.agentscriptconverter.transformer.Transformer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.util.ArrayList;
import java.util.List;

/**
 * Pipeline to parse the script and save potential errors
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class Pipeline {
  private AST ast;
  private ArrayList<String> errors;
  private IChecker checker;
  private Transformer transformer;
  private IGenerator generator;

  /**
   * Pipeline constructor
   *
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public Pipeline() {
    errors = new ArrayList<>();
    checker = new Checker();
    transformer = new Transformer();
    generator = new Generator();
  }

  public AST getAST() {
    return ast;
  }

  public void setAst(AST ast) {
    this.ast = ast;
  }

  public List<String> getErrors() {
    return new ArrayList<>(errors);
  }

  public void setChecker(Checker checker) {
    this.checker = checker;
  }

  /**
   * Parses a string to verify if the input is a valid script.
   *
   * @param input contains the script to be parsed
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public void parseString(String input) {
    CharStream inputStream = CharStreams.fromString(input);
    AgentsGrammarLexer lexer = new AgentsGrammarLexer(inputStream);
    lexer.removeErrorListeners();
    errors.clear();
    try {
      CommonTokenStream tokens = new CommonTokenStream(lexer);
      AgentsGrammarParser parser = new AgentsGrammarParser(tokens);
      parser.removeErrorListeners();
      ParseTree parseTree = parser.agentScript();
      ASTListener listener = new ASTListener();
      ParseTreeWalker walker = new ParseTreeWalker();
      walker.walk(listener, parseTree);
      this.ast = listener.getAST();
    } catch (RecognitionException e) {
      this.ast = new AST();
      errors.add(e.getMessage());
    } catch (ParseCancellationException e) {
      this.ast = new AST();
      errors.add("Syntax error");
    } catch (Exception e) {
      errors.add("JSON file not found");
    }
  }

  /**
   * Checks if the AST does not contain invalid rules
   *
   * @throws UndefinedASTException
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public void checkAST() throws UndefinedASTException {
    if (ast == null) {
      throw new UndefinedASTException();
    }

    checker.check(ast);
  }

  /**
   * Transforms the AST to make it more efficient and add missing rules
   *
   * @throws UndefinedASTException
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public void transformerAST() throws UndefinedASTException {
    if (ast == null) {
      throw new UndefinedASTException();
    }
    transformer.transform(ast);
  }

  /**
   * Generates the Java code from the AST
   *
   * @throws UndefinedASTException
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public String generateJavaCode() throws UndefinedASTException {
    if (ast == null) {
      throw new UndefinedASTException();
    }
    return generator.generate(ast);
  }

  public void setGenerator(Generator generator) {
    this.generator = generator;
  }
}
