package nl.aimsites.nzbvuq.game.agentscriptconverter.generator;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.AST;

/**
 * The interface Generator. The Generator delivers the converted script in String format.
 *
 * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
 * */
public interface IGenerator {

  /**
   * Generates runnable Java code in String format from AST.
   *
   * @param ast the ast
   * @return Runnable Java code in String format
   * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
   */
  String generate(AST ast);
}
