package nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.SimpleRule;

/**
 * ASTNode for SimpleWeaponDefaultRule.
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class SimpleWeaponDefaultRule extends SimpleRule {
  private String type;

  /**
   * SimpleWeaponDefaultRule constructor
   *
   * @param type type of weapon that the agent should use as default.
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public SimpleWeaponDefaultRule(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  @Override
  public String toString() {
    return getNodeLabel() + ": " + type + System.lineSeparator();
  }
}
