package nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.Priority;

/**
 * ASTNode for InventoryPriority
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class InventoryPriority extends Priority {}
