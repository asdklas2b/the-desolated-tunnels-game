package nl.aimsites.nzbvuq.game.agentscriptconverter;

import nl.aimsites.nzbvuq.datastorage.dto.ai.AIDto;
import nl.aimsites.nzbvuq.game.entities.openings.Opening;
import nl.aimsites.nzbvuq.game.graph.Edge;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import nl.aimsites.nzbvuq.game.inputhandler.parser.IInputHandler;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.LessMore;
import nl.aimsites.nzbvuq.game.agentscriptconverter.exception.AgentScriptNotFoundException;
import nl.aimsites.nzbvuq.game.agentscriptconverter.generator.AgentBehaviourManager;
import nl.aimsites.nzbvuq.game.coderunner.CodeRunner;
import nl.aimsites.nzbvuq.game.entities.Entity;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.entities.character.monsters.Monster;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Strength;
import nl.aimsites.nzbvuq.game.entities.items.Weapon;
import nl.aimsites.nzbvuq.game.inputhandler.actions.*;
import nl.aimsites.nzbvuq.game.inputhandler.parser.InputHandler;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import nl.aimsites.nzbvuq.game.state.GameState;
import nl.aimsites.nzbvuq.datastorage.crud.AICrud;
import nl.aimsites.nzbvuq.datastorage.crud.AICrudImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class for retrieving the agent script from the DB and send it to the coderunner.
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class AgentScriptRunner {
  private String script;
  private CodeRunner codeRunner;
  private AICrud aiCrud = new AICrudImpl();
  private Player player;
  private  IInputHandler inputHandler;

  /**
   * AgentScriptRunner constructor
   *
   * @throws AgentScriptNotFoundException
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public AgentScriptRunner(IInputHandler inputHandler) {
    this.codeRunner = new CodeRunner();
    this.player = GameState.getInstance().getPlayer();
    this.inputHandler = inputHandler;
  }

  /**
   * Retrieves the agent script from the DB
   *
   * @param playerName name of the player
   * @param agentName name of the agent
   * @throws AgentScriptNotFoundException
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public void getScriptFromDB(String playerName, String agentName) throws AgentScriptNotFoundException {
    List<AIDto> list = aiCrud.read();
    for (AIDto item : list) {
      if (item.getPeer().getId().equals(playerName) && item.getName().equals(agentName)) {
        script = item.getCommand();
        return;
      }
    }
    throw new AgentScriptNotFoundException();
  }

  /**
   * Sends the Java code to the coderunner with the Player parameter
   *
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public void runScript() {
    codeRunner.run(script, new ArrayList<>() {
      {
        add(AgentBehaviourManager.class);
        add(Player.class);
        add(Collectors.class);
        add(Entity.class);
        add(LessMore.class);
        add(Monster.class);
        add(BaseItem.class);
        add(Weapon.class);
        add(Strength.class);
        add(List.class);
        add(InputHandler.class);
      }
    }, player, inputHandler);
  }

  public void setCodeRunner(CodeRunner codeRunner) {
    this.codeRunner = codeRunner;
  }

  public void setAiCrud(AICrud aiCrud) {
    this.aiCrud = aiCrud;
  }

  public String getScript() {
    return script;
  }

  public Player getPlayer() {
    return player;
  }

  public void setScript(String script) {
    this.script = script;
  }

  public void setPlayer(Player player) {
    this.player = player;
  }
}
