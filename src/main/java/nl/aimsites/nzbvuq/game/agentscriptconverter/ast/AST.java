package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

import java.util.Objects;

/**
 * Class for the AST.
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class AST {
  private AgentScript root;

  /**
   * AST constructor
   *
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public AST() {
    root = new AgentScript();
  }

  public void setRoot(AgentScript agentScript) {
    root = agentScript;
  }

  @Override
  public String toString() {
    return root.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    AST ast = (AST) o;
    return Objects.equals(root, ast.root);
  }

  @Override
  public int hashCode() {
    return Objects.hash(root);
  }

  public AgentScript getRoot() {
    return root;
  }
}
