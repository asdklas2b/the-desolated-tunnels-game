package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

import java.util.ArrayList;

/**
 * ASTNode for IfCondition
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class IfCondition extends ASTNode {
  private LessMore iHaveCondition;
  private String condition;
  private Subject subject;
  private StrengthComparison strengthComparison;

  /**
   * IfCondition constructor
   *
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public IfCondition() {}

  /**
   * IfCondition constructor
   *
   * @param iHaveCondition Enum (LESS / MORE) for condition beginning with: I have
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public IfCondition(LessMore iHaveCondition) {
    this.iHaveCondition = iHaveCondition;
  }

  /**
   * IfCondition constructor
   *
   * @param condition condition for conditions beginning with: Current weapon...
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public IfCondition(String condition) {
    this.condition = condition;
  }

  @Override
  public ArrayList<ASTNode> getChildren() {
    ArrayList<ASTNode> arrayList = new ArrayList<>();
    if (subject != null) {
      arrayList.add(subject);
    }
    if (strengthComparison != null) {
      arrayList.add(strengthComparison);
    }
    return arrayList;
  }

  @Override
  public void addChild(ASTNode child) {
    if (child instanceof Subject) {
      subject = (Subject) child;
    } else if (child instanceof StrengthComparison) {
      strengthComparison = (StrengthComparison) child;
    }
  }

  @Override
  public void removeChild(ASTNode child) {
    if (child instanceof Subject) {
      subject = null;
    } else if (child instanceof StrengthComparison) {
      strengthComparison = null;
    }
  }

  public String getCondition() {
    return condition;
  }

  public LessMore getIHaveCondition() {
    return iHaveCondition;
  }

  public Subject getSubject() {
    return subject;
  }

  public void setSubject(Subject subject) {
    this.subject = subject;
  }

  public StrengthComparison getStrengthComparison() {
    return strengthComparison;
  }

  public void setStrengthComparison(StrengthComparison strengthComparison) {
    this.strengthComparison = strengthComparison;
  }

  @Override
  public String toString() {
    if (condition != null) {
      return getNodeLabel() + ": " + condition + System.lineSeparator();
    }
    return super.toString();
  }
}
