package nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.ASTNode;

/**
 * ASTNode for InventoryPriorityOption
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class InventoryPriorityOption extends ASTNode {
  private int number;
  private String type;

  /**
   * InventoryPriorityOption constructor
   *
   * @param number maximum amount that the agent should collect of the given type
   * @param type type of item that the agent should collect.
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public InventoryPriorityOption(int number, String type) {
    this.number = number;
    this.type = type;
  }

  public int getNumber() {
    return number;
  }

  public void setNumber(int number) {
    this.number = number;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return getNodeLabel() + ": " + number + " " + type + System.lineSeparator();
  }
}
