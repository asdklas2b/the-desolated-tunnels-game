package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

/**
 * ASTNode for StrengthComparison
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class StrengthComparison extends ASTNode {
  private LessMore lessMore;
  private int number = -1;

  /**
   * StrengthComparison constructor
   *
   * @param lessMore Enum (LESS / MORE) for comparing strength to number
   * @param number number for comparison
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public StrengthComparison(LessMore lessMore, int number) {
    this.lessMore = lessMore;
    this.number = number;
  }

  @Override
  public String toString() {
    return getNodeLabel() + ": " + lessMore + " " + number + System.lineSeparator();
  }

  public LessMore getLessMore() {
    return lessMore;
  }

  public int getNumber() {
    return number;
  }
}
