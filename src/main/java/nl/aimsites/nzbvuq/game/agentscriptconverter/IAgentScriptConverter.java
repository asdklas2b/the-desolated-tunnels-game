package nl.aimsites.nzbvuq.game.agentscriptconverter;

import nl.aimsites.nzbvuq.game.agentscriptconverter.exception.ParserFailedException;

/**
 * Public interface for using AgentScriptConverter
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public interface IAgentScriptConverter {
  /**
   * Converts the given script into runnable Java code.
   *
   * @param input script to be parsed, checked, transformed and generated.
   * @return Java code in string.
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  String convert(String input) throws ParserFailedException;
}
