package nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.*;

import java.util.ArrayList;

/**
 * ASTNode for ConditionRule
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class ConditionRule extends Rule {
  private Condition condition;
  private Subject subject;
  private Action action;
  private IfClause ifClause;

  @Override
  public ArrayList<ASTNode> getChildren() {
    ArrayList<ASTNode> arrayList = new ArrayList<>();
    if (condition != null) {
      arrayList.add(condition);
    }
    if (subject != null) {
      arrayList.add(subject);
    }
    if (action != null) {
      arrayList.add(action);
    }
    if (ifClause != null) {
      arrayList.add(ifClause);
    }

    return arrayList;
  }

  @Override
  public void addChild(ASTNode child) {
    if (child instanceof Condition) {
      condition = (Condition) child;
    } else if (child instanceof Subject) {
      subject = (Subject) child;
    } else if (child instanceof Action) {
      action = (Action) child;
    } else if (child instanceof IfClause) {
      ifClause = (IfClause) child;
    }
  }

  @Override
  public void removeChild(ASTNode child) {
    if (child instanceof Condition) {
      condition = null;
    } else if (child instanceof Subject) {
      subject = null;
    } else if (child instanceof Action) {
      action = null;
    } else if (child instanceof IfClause) {
      ifClause = null;
    }
  }

  public Subject getSubject() {
    return subject;
  }

  public Action getAction() {
    return action;
  }

  public IfClause getIfClause() {
    return ifClause;
  }
}
