package nl.aimsites.nzbvuq.game.agentscriptconverter.generator;

import nl.aimsites.nzbvuq.game.agentscriptconverter.AgentScriptConverter;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.*;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.InventoryPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option.InventoryPriorityOption;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.ConditionRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.PriorityRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.SimpleRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleFoodRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleWeaponDefaultRule;

/**
 * Class used by the {@link AgentScriptConverter} to generate the executable code for an Agent.
 *
 * @author Stijn van Ewijk (TS.vanewijk@student.han.nl
 * @author Azez Salami (A.salami@student.han.nl)
 */
public class Generator implements IGenerator {
  private static final String RETURN_NULL = "return null;";

  /**
   * Generates the executable code.
   *
   * @param ast the AST
   * @return string with executable code
   * @author Azez Salami (A.salami@student.han.nl)
   */
  public String generate(AST ast) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("var player = (Player) parameter[0];" +
      "AgentBehaviourManager agentBehaviourManager = new AgentBehaviourManager();")
      .append("List<Entity> surroundingEntities =").append(System.lineSeparator())
      .append("player.getCurrentPosition().getOccupyingEntities().stream()").append(System.lineSeparator())
      .append(".filter(entity -> !entity.equals(player))").append(System.lineSeparator())
      .append(".collect(Collectors.toList());").append(System.lineSeparator())
      .append("for (Entity entity : surroundingEntities) { ").append(System.lineSeparator());

    InventoryPriority inventoryPriority = getInventoryPriority(ast.getRoot());
    generateConditionRules(ast.getRoot(), stringBuilder, inventoryPriority);
    stringBuilder.append(RETURN_NULL).append(System.lineSeparator()).append("}");
    generateSimpleRules(ast.getRoot(), stringBuilder);
    return stringBuilder.toString();
  }

  /**
   * Gets the inventory priority out of the AST root.
   *
   * @param agentScript the agent script from the AST
   * @return the inventory priority rule
   * @author Azez Salami (A.salami@student.han.nl)
   */
  public InventoryPriority getInventoryPriority(AgentScript agentScript) {
    InventoryPriority inventoryPriority = null;
    for (ASTNode child : agentScript.getChildren()) {
      if (child instanceof PriorityRule) {
        for (ASTNode priorityRuleChild : child.getChildren()) {
          if (priorityRuleChild instanceof InventoryPriority) {
            inventoryPriority = (InventoryPriority) priorityRuleChild;
          }
        }
      }
    }
    return inventoryPriority;
  }

  /**
   * Generates the conditional rules.
   *
   * @param agentScript the agent script from the AST
   * @param stringBuilder the string builder used in this class
   * @param inventoryPriority the inventory priority rule
   * @author Azez Salami (A.salami@student.han.nl)
   */
  public void generateConditionRules(AgentScript agentScript, StringBuilder stringBuilder, InventoryPriority inventoryPriority) {
    agentScript.getChildren().stream().filter(child -> child instanceof ConditionRule).forEach(child -> {
      String name = ((ConditionRule) child).getSubject().getName();
      switch (name) {
        case "monster", "enemy" -> stringBuilder.append(generateConditionRuleMonsterEnemy((ConditionRule) child));
        case "attribute" -> stringBuilder.append(generateConditionRuleAttribute(inventoryPriority));
        case "weapon" -> stringBuilder.append(generateConditionRuleWeapon((ConditionRule) child));
        default -> throw new IllegalStateException("Unexpected value: " + name);
      }
    });
  }

  /**
   * Generates the conditional rules for monsters and/or enemies.
   *
   * @param conditionRule the condition rule
   * @return the string to be added to the executable code
   * @author Azez Salami (A.salami@student.han.nl)
   */
  public String generateConditionRuleMonsterEnemy(ConditionRule conditionRule) {
    boolean fight = false;
    String lessMoreString = null;
    IfCondition ifCondition = null;

    if (conditionRule.getAction().getName().equals("fight")) {
      fight = true;
      if (conditionRule.getIfClause() != null) {
        ifCondition = conditionRule.getIfClause().getIfCondition();
      }
    }

    String subjectName = conditionRule.getSubject().getName().substring(0, 1).toUpperCase() + conditionRule.getSubject().getName().substring(1).toLowerCase();
    StrengthComparison strengthComparison;
    int monsterStrength = 0;

    if (ifCondition != null) {
      strengthComparison = ifCondition.getStrengthComparison() != null ? ifCondition.getStrengthComparison() : null;
      if (strengthComparison != null) {
        monsterStrength = strengthComparison.getNumber();
        LessMore lessMore = strengthComparison.getLessMore();
        if (lessMore.equals(LessMore.MORE)) {
          lessMoreString = "LessMore.MORE";
        } else if (lessMore.equals(LessMore.LESS)) {
          lessMoreString = "LessMore.LESS";
        } else {
          lessMoreString = "null";
        }
      }
    }

    if (subjectName.equals("Enemy")) {
      subjectName = "Player";
    }
    return "if (entity instanceof " + subjectName + ") {" + System.lineSeparator() +
      "if (agentBehaviourManager.getAttackDecision(" + fight + ",player.getStrength(), " + lessMoreString + ", " + monsterStrength + ")){" + System.lineSeparator() +
      "new AttackAction().execute(entity.getName());" + System.lineSeparator() +
      "}" + "else {" + System.lineSeparator() +
      "new MoveAction().execute(agentBehaviourManager.getNextMoveDirection(player.getCurrentPosition(), player.getHistory()).toString());" + System.lineSeparator() +
      "} " +
      RETURN_NULL + System.lineSeparator() +
      "}" + System.lineSeparator();
  }

  /**
   * Generates the conditional rules for attributes.
   *
   * @param inventoryPriority the inventory priority rule
   * @return the string to be added to the executable code
   * @author Azez Salami (A.salami@student.han.nl)
   */
  public String generateConditionRuleAttribute(InventoryPriority inventoryPriority) {
    int totalFood = 0;
    int totalArmor = 0;
    int totalWeapon = 0;
    for (ASTNode priorityOption : inventoryPriority.getChildren()) {
      if (priorityOption instanceof InventoryPriorityOption) {
        String type = ((InventoryPriorityOption) priorityOption).getType();
        if (type.equals("food")) {
          totalFood = ((InventoryPriorityOption) priorityOption).getNumber();
        } else if (type.equals("armor")) {
          totalArmor = ((InventoryPriorityOption) priorityOption).getNumber();
        } else if (type.equals("weapon")) {
          totalWeapon = ((InventoryPriorityOption) priorityOption).getNumber();
        }
      }
    }

    return "if (entity instanceof BaseItem) {" + System.lineSeparator() +
      "if (agentBehaviourManager.getPickupItemDecision((BaseItem) entity, player.getInventory(), " + totalFood + ", " + totalWeapon + ", " + totalArmor + ")) {" + System.lineSeparator() +
      "new PickupAction().execute(entity.getName());" + System.lineSeparator() +
      RETURN_NULL + System.lineSeparator() +
      "}";
  }

  /**
   * Generates the conditional rules for weapons.
   *
   * @param conditionRule the condition rule
   * @return the string to be added to the executable code
   * @author Stijn van Ewijk (TS.vanewijk@student.han.nl
   */
  public String generateConditionRuleWeapon(ConditionRule conditionRule) {
    LessMore lessMore = conditionRule.getIfClause().getIfCondition().getIHaveCondition();
    String comparison = lessMore == LessMore.LESS ? "best" : "worst";
    return "if (entity instanceof " + "Weapon" + ") {" + System.lineSeparator() +
      "if (agentBehaviourManager.getReplaceWeaponDecision(\"" + comparison + "\",player.getEquippedWeapon(), " + "(Weapon) entity" + ")){" + System.lineSeparator() +
      "new EquipAction().execute(entity.getName());" + System.lineSeparator() +
      "}" + System.lineSeparator() + "else {" + System.lineSeparator() +
      "new MoveAction().execute(agentBehaviourManager.getNextMoveDirection(player.getCurrentPosition(), player.getHistory()).toString());" + System.lineSeparator() +
      "} " + System.lineSeparator() +
      RETURN_NULL + System.lineSeparator() +
      "} }" + System.lineSeparator();
  }

  /**
   * Generates simple rules.
   *
   * @param agentScript the agent script from the AST
   * @param stringBuilder the string builder
   * @author Stijn van Ewijk (TS.vanewijk@student.han.nl
   */
  public void generateSimpleRules(AgentScript agentScript, StringBuilder stringBuilder) {
    agentScript.getChildren().stream().filter(child -> child instanceof SimpleRule).forEach(child -> {
      if (child instanceof SimpleFoodRule) {
        stringBuilder.append(generateSimpleFoodRule((SimpleFoodRule) child));
      } else if (child instanceof SimpleWeaponDefaultRule) {
        stringBuilder.append(generateSimpleWeaponDefaultRule((SimpleWeaponDefaultRule) child));
      }
    });
  }

  /**
   * Generates simple food rules.
   *
   * @param simpleFoodRule the simple food rule
   * @return the string to be added to the executable code
   * @author Stijn van Ewijk (TS.vanewijk@student.han.nl
   */
  public String generateSimpleFoodRule(SimpleFoodRule simpleFoodRule) {
    String comparison;
    String foodName = simpleFoodRule.getFood().getName();
    if (simpleFoodRule.getStrengthComparison().getLessMore() == LessMore.LESS) {
      comparison = "LessMore.LESS";
    } else {
      comparison = "LessMore.MORE";
    }

    return "for (BaseItem item : player.getInventory().getItems()) {" + System.lineSeparator() +
      "if (item instanceof Strength) {" + System.lineSeparator() +
      "if (agentBehaviourManager.getUseFoodDecision(\"" + foodName + "\", player.getStrength(), " +
      comparison + ", " + simpleFoodRule.getStrengthComparison().getNumber() + ", item.getName())) {" + System.lineSeparator() +
      "new UseAction().execute(\""+ foodName +"\");" + System.lineSeparator() +
      RETURN_NULL + System.lineSeparator() +
      "} } }";
  }

  /**
   * Generates simple weapon rules.
   *
   * @param simpleWeaponDefaultRule the simple weapon default rule
   * @return the string to be added to the executable code
   * @author Stijn van Ewijk (TS.vanewijk@student.han.nl
   */
  public String generateSimpleWeaponDefaultRule(SimpleWeaponDefaultRule simpleWeaponDefaultRule) {
    return "for (BaseItem item : player.getInventory().getItems()) {" + System.lineSeparator() +
      "if (item instanceof Weapon) {" + System.lineSeparator() +
      "if (agentBehaviourManager.getReplaceWeaponDecision(\"" + simpleWeaponDefaultRule.getType() +
      "\", player.getEquippedWeapon(), (Weapon) item)) {" + System.lineSeparator() +
      "new EquipAction().execute(item.getName());" + System.lineSeparator() +
      RETURN_NULL + System.lineSeparator() +
      "} } }";
  }
}