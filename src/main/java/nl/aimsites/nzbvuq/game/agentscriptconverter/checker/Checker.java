package nl.aimsites.nzbvuq.game.agentscriptconverter.checker;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.*;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.InventoryPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.MainPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option.InventoryPriorityOption;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option.MainPriorityOption;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.PriorityRule;

import java.util.*;

/**
 * Checker class. Implements the IChecker interface.
 *
 * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
 */
public class Checker implements IChecker {

  private final Map<String, List<String>> mapActionsToSubject = mapActionsToSubjects();

  /**
   * Implements the check method. Starts at the root of the AST and will go through all elements.
   *
   * @param ast the ast
   * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
   */
  public void check(AST ast) {
    checkAgentScript(ast.getRoot());
  }

  /**
   * Specific checks for the root of the AST (AgentScript).
   *
   * @param agentScript the agent script
   * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
   */
  public void checkAgentScript(AgentScript agentScript) {
    var children = agentScript.getChildren();
    for (ASTNode child : children) {
      if (child instanceof PriorityRule) {
        checkPriorityRule((PriorityRule) child);
      }
    }
  }

  /**
   * Specific checks for PriorityRule. Part of checks C6 & C7.
   *
   * @param priorityRule the priority rule
   * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  public void checkPriorityRule(PriorityRule priorityRule) {
    ArrayList<ASTNode> children = priorityRule.getChildren();
    IfClause ifClause = null;
    for (ASTNode child : children) {
      if (child instanceof MainPriority) {
        checkMainPriority((MainPriority) child);
      } else if (child instanceof InventoryPriority) {
        checkInventoryPriority((InventoryPriority) child);
      } else if (child instanceof IfClause) {
        ifClause = (IfClause) child;
      }
    }

    if (ifClause == null) {
      return;
    }

    ArrayList<ASTNode> ifClauseChildren = ifClause.getChildren();
    ifClauseChildren.stream()
        .filter(child -> child instanceof IfCondition)
        .flatMap(child -> child.getChildren().stream())
        .filter(astNode -> astNode instanceof Subject)
        .forEach(astNode -> astNode.setError("Subject not allowed in priority rules."));
  }

  /**
   * Specific checks for MainPriority.
   *
   * @param mainPriority the main priority
   * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  public void checkMainPriority(MainPriority mainPriority) {
    mainPriority.getChildren().stream()
        .filter(child -> child instanceof MainPriorityOption)
        .map(MainPriorityOption.class::cast)
        .forEach(this::checkMainPriorityOption);
  }

  /**
   * Specific checks for MainPriorityOption.
   *
   * @param mainPriorityOption the main priority option
   * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
   */
  public void checkMainPriorityOption(MainPriorityOption mainPriorityOption) {}

  /**
   * Specific checks for InventoryPriority.
   *
   * @param inventoryPriority the inventory priority
   * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
   * @author Jorden Willemsen (j.willemsen4@student.han.nl)
   */
  public void checkInventoryPriority(InventoryPriority inventoryPriority) {
    inventoryPriority.getChildren().stream()
        .filter(child -> child instanceof InventoryPriorityOption)
        .map(InventoryPriorityOption.class::cast)
        .forEach(this::checkInventoryPriorityOption);
  }

  /**
   * Specific checks for InventoryPriorityOption. Part of check C8.
   *
   * @param inventoryPriorityOption the inventory priority option
   * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
   */
  public void checkInventoryPriorityOption(InventoryPriorityOption inventoryPriorityOption) {
    if (inventoryPriorityOption.getNumber() > 10 || inventoryPriorityOption.getNumber() < 0) {
      inventoryPriorityOption.setError("Number must be between 0 and 10.");
    }
  }

  /**
   * Checks if action matches the subject. Part of checks C1 - C5.
   *
   * @param action the action
   * @param subject the subject
   * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
   */
  public void checkIfActionMatchesSubject(Action action, Subject subject) {
    List<String> possibleMatches = mapActionsToSubject.get(subject.getName());
    boolean matched = possibleMatches.stream().anyMatch(match -> match.equals(action.getName()));

    if (!matched) {
      action.setError("Action does not match subject.");
    }
  }

  /**
   * Checks if the action in IfClause matches the subject. Part of checks C1 - C5.
   *
   * @param ifClause the if clause
   * @param subject the subject
   * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
   */
  public void checkIfIfClauseMatchesSubject(IfClause ifClause, Subject subject) {
    ArrayList<ASTNode> children = ifClause.getChildren();
    Action action =
        (Action)
            children.stream().filter(child -> child instanceof Action).findFirst().orElse(null);

    if (action != null) {
      checkIfActionMatchesSubject(action, subject);
    }
  }

  /**
   * Maps actions to subjects. Part of checks C1 - C5.
   *
   * @return the hash map
   * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
   */
  public Map<String, List<String>> mapActionsToSubjects() {
    HashMap<String, List<String>> map = new HashMap<>();

    map.put("enemy", Arrays.asList("flee", "run", "fight"));
    map.put("monster", Arrays.asList("flee", "run", "fight"));
    map.put("attribute", Arrays.asList("pickup", "replace"));
    map.put("weapon", Arrays.asList("pickup", "replace"));

    return map;
  }
}
