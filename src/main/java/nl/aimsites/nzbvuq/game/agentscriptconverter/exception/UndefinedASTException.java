package nl.aimsites.nzbvuq.game.agentscriptconverter.exception;

/**
 * Exception for checkAST() in Pipeline
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class UndefinedASTException extends Exception {
}
