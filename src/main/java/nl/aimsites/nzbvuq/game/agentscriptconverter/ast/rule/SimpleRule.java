package nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.Rule;

/**
 * Parent abstract class for all SimpleRule ASTNodes: (SimpleDropWeaponRule, SimpleFoodRule,
 * SimpleWeaponDefaultRule)
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public abstract class SimpleRule extends Rule {}
