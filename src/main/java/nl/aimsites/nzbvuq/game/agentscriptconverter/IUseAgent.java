package nl.aimsites.nzbvuq.game.agentscriptconverter;

/**
 * Interface for using and stop using an agent
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 * @author Azez Salami (A.salami@student.han.nl)
 */
public interface IUseAgent {
  /**
   * starts using the given agent
   *
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   * @author Azez Salami (A.salami@student.han.nl)
   */
  void startUsingAgent(String playerName, String agentName);

  /**
   * stops using the given agent
   *
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   * @author Azez Salami (A.salami@student.han.nl)
   */
  void stopUsingAgent();
}
