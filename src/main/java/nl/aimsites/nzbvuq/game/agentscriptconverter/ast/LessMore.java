package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

/**
 * Enum for conditions beginning with: I have
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public enum LessMore {
  LESS,
  MORE
}
