package nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.ASTNode;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.IfClause;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.Priority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.Rule;

import java.util.ArrayList;

/**
 * ASTNode for PriorityRule
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class PriorityRule extends Rule {
  private Priority priority;
  private IfClause ifClause;

  @Override
  public ArrayList<ASTNode> getChildren() {
    ArrayList<ASTNode> arrayList = new ArrayList<>();
    if (priority != null) {
      arrayList.add(priority);
    }

    if (ifClause != null) {
      arrayList.add(ifClause);
    }

    return arrayList;
  }

  @Override
  public void addChild(ASTNode child) {
    if (child instanceof Priority) {
      priority = (Priority) child;
    } else if (child instanceof IfClause) {
      ifClause = (IfClause) child;
    }
  }

  @Override
  public void removeChild(ASTNode child) {
    if (child instanceof Priority) {
      priority = null;
    } else if (child instanceof IfClause) {
      ifClause = null;
    }
  }
}
