package nl.aimsites.nzbvuq.game.agentscriptconverter;

import nl.aimsites.nzbvuq.game.inputhandler.parser.IInputHandler;
import nl.aimsites.nzbvuq.game.inputhandler.parser.InputHandler;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Class to run the agentscript every few seconds
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class UseAgent implements IUseAgent {
  private AgentScriptRunner agentScriptRunner;
  private Timer timer;
  private static final int WAITING_TIME = 2000;
  private TimerTask task;
  private IInputHandler inputHandler;

  /**
   * UseAgent constructor
   *
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public UseAgent(IInputHandler inputHandler) {
    this.timer = new Timer("Timer");
    this.task = new TimerTask() {
      @Override
      public void run() {
        agentScriptRunner.runScript();
      }
    };
    this.inputHandler = inputHandler;
  }

  @Override
  public void startUsingAgent(String playerName, String agentName) {
    initAgentScriptRunner(playerName, agentName);

    timer.schedule(task, 1500, WAITING_TIME);
  }

  @Override
  public void stopUsingAgent() {
    timer.cancel();
  }

  /**
   * Initiates the agentScriptRunner if it doesn't exist already
   *
   * @param playerName name of the player
   * @param agentName name of the agent
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  private void initAgentScriptRunner(String playerName, String agentName) {
    if (agentScriptRunner == null) {
      agentScriptRunner = new AgentScriptRunner(inputHandler);
      agentScriptRunner.getScriptFromDB(playerName, agentName);
    }
  }

  public void setAgentScriptRunner(AgentScriptRunner agentScriptRunner) {
    this.agentScriptRunner = agentScriptRunner;
  }

  public AgentScriptRunner getAgentScriptRunner() {
    return agentScriptRunner;
  }

  public void setTimer(Timer timer) {
    this.timer = timer;
  }
}
