package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

/**
 * Root node for the AST.
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class AgentScript extends ASTNode {}
