package nl.aimsites.nzbvuq.game.agentscriptconverter.exception;

/**
 * Exception for when the agent script cannot be retrieved from the database
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class AgentScriptNotFoundException extends RuntimeException {

  /**
   * AgentScriptNotFoundException constructor
   *
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public AgentScriptNotFoundException() {
    super("The agent script was not able to be retrieved from the database");
  }
}
