package nl.aimsites.nzbvuq.game.agentscriptconverter.generator;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.LessMore;
import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.entities.character.Inventory;
import nl.aimsites.nzbvuq.game.entities.items.Armor;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Strength;
import nl.aimsites.nzbvuq.game.entities.items.Weapon;
import nl.aimsites.nzbvuq.game.graph.Edge;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.MoveDirection;

import java.security.SecureRandom;
import java.util.List;

/**
 * Class used by the generated java code from the Generator that contains the decides if
 * an agent should do an action or not.
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class AgentBehaviourManager {
  private static final SecureRandom RANDOM = new SecureRandom();

  /**
   * Decides if the agent should pick up the given item
   *
   * @param item         the item to pick up or not
   * @param inventory    the inventory of the agent
   * @param totalFood    the amount of food items the agent should pick up according to his behaviour.
   * @param totalWeapons the amount of weapons the agent should pick up according to his behaviour.
   * @param totalArmor   the amount of armor items the agent should pick up according to his behaviour.
   * @return true if the agent should pick up the item and false if not.
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public boolean getPickupItemDecision(
    BaseItem item, Inventory inventory, int totalFood, int totalWeapons, int totalArmor) {
    int total =
      (int)
        inventory.getItems().stream()
          .filter(
            entry ->
              entry.getClass().isAssignableFrom(item.getClass()))
          .count();

    if (item instanceof Strength) {
      return total < totalFood;
    } else if (item instanceof Weapon) {
      return total < totalWeapons;
    } else if (item instanceof Armor) {
      return total < totalArmor;
    }
    return false;
  }

  /**
   * Decides if the agent should attack a character
   *
   * @param fight           The action of the conditionrule given to the agent
   * @param subjectStrength the strength of the subject
   * @param lessMore        Enum for a strength comparison
   * @param threshold       threshhold for the strength comparison
   * @return true if the agent should attack the character and false if not.
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public boolean getAttackDecision(
    boolean fight, int subjectStrength, LessMore lessMore, int threshold) {
    if (lessMore == null) {
      return fight;
    }

    boolean result = calculateStrengthComparison(subjectStrength, lessMore, threshold);
    return result == fight;
  }

  /**
   * Decides if the agent should use food to restore strength
   *
   * @param foodTypeFromRule the food type specified in the simple food rule
   * @param playerStrength   strength of the player
   * @param lessMore         enum for strength comparison
   * @param threshold        threshold for the strength comparison
   * @param foodToBeEaten    the food type to be used or not
   * @return true if the agent should use food and false if not.
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public boolean getUseFoodDecision(
    String foodTypeFromRule,
    int playerStrength,
    LessMore lessMore,
    int threshold,
    String foodToBeEaten) {
    if (foodTypeFromRule.equals("food") || foodTypeFromRule.equals(foodToBeEaten)) {
      return calculateStrengthComparison(playerStrength, lessMore, threshold);
    }
    return false;
  }

  /**
   * Decides if the agent should use replace his current weapon with a new weapon
   *
   * @param currentWeapon current weapon being used by the agent
   * @param newWeapon     new weapon found by the agent
   * @return true if the agent should replace his weapon and false if not.
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public boolean getReplaceWeaponDecision(Weapon currentWeapon, Weapon newWeapon) {
    return getReplaceWeaponDecision("best", currentWeapon, newWeapon);
  }

  /**
   * Decides if the agent should use replace his current weapon with a new weapon
   *
   * @param rule          type of weapon if specified in the agent script
   * @param currentWeapon current weapon being used by the agent
   * @param newWeapon     new weapon found by the agent
   * @return true if the agent should replace his weapon and false if not.
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public boolean getReplaceWeaponDecision(String rule, Weapon currentWeapon, Weapon newWeapon) {
    if (rule.equals("best")) {
      return newWeapon.getDamage() > currentWeapon.getDamage();
    } else if (rule.equals("worst")) {
      return newWeapon.getDamage() < currentWeapon.getDamage();
    }
    return false;
  }

  /**
   * Decides where the agent should move to
   *
   * @param currentTile tile the agent is currently sitting on
   * @param history     past 10 tiles where the agent has been
   * @return the move direction to be given to an move action to move the agent to the next tile
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public MoveDirection getNextMoveDirection(
    ReachableTile currentTile, List<Character.TileMove> history) {
    final int RANDOM_INT = RANDOM.nextInt(currentTile.getAdjacentTiles().size());
    final List<Edge> adjacentTiles = currentTile.getAdjacentTiles();

//    for (int i = 0; i < adjacentTiles.size(); i++) {
//      ReachableTile randomTile =
//        (ReachableTile)
//          adjacentTiles.get((RANDOM_INT + i) % adjacentTiles.size()).getDestinationTile();
//      if (history.stream().noneMatch(tileMove -> tileMove.getReachableTile().equals(randomTile))) {
//        System.out.println(getRelativeDirection(currentTile, randomTile));
//        return getRelativeDirection(currentTile, randomTile);
//      }
//    }

    return getRelativeDirection(
      currentTile, (ReachableTile) adjacentTiles.get(RANDOM_INT).getDestinationTile());
  }

  /**
   * Calculates the strength comparison
   *
   * @param number1  number to compare to number2
   * @param lessMore enum if number1 should be higher or lower number2
   * @param number2  number to be compared to by number1
   * @return true or false depending on the numbers and enum
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  private boolean calculateStrengthComparison(int number1, LessMore lessMore, int number2) {
    if (lessMore == LessMore.MORE) {
      return number1 > number2;
    } else if (lessMore == LessMore.LESS) {
      return number1 < number2;
    }
    return false;
  }

  /**
   * Calculates the MoveDirection that should be used if you want to move from tile1 to tile 2
   *
   * @param startTile       tile to act as starting tile
   * @param destinationTile tile to act as destination tile
   * @return a MoveDirection that should be used if you want to move from tile1 to tile 2
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  private MoveDirection getRelativeDirection(ReachableTile startTile, ReachableTile destinationTile) {
    if (startTile.getY() < destinationTile.getY()) {
      return MoveDirection.NORTH;
    } else if (startTile.getX() < destinationTile.getX()) {
      return MoveDirection.EAST;
    } else if (startTile.getY() > destinationTile.getY()) {
      return MoveDirection.SOUTH;
    } else if (startTile.getX() > destinationTile.getX()) {
      return MoveDirection.WEST;
    }
    return null;
  }
}
