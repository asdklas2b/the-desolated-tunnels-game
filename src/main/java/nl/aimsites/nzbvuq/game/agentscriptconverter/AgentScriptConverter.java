package nl.aimsites.nzbvuq.game.agentscriptconverter;

import nl.aimsites.nzbvuq.game.agentscriptconverter.exception.ParserFailedException;
import nl.aimsites.nzbvuq.game.agentscriptconverter.exception.UndefinedASTException;

/**
 * Main class for AgentScriptConverter
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class AgentScriptConverter implements IAgentScriptConverter {
  private Pipeline pipeline;

  /**
   * AgentScriptConverter constructor
   *
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public AgentScriptConverter() {
    this.pipeline = new Pipeline();
  }

  @Override
  public String convert(String input) throws ParserFailedException {
    pipeline.parseString(input);
    try {
      pipeline.checkAST();
      pipeline.transformerAST();
      return pipeline.generateJavaCode();
    } catch (UndefinedASTException e) {
      throw new ParserFailedException(e);
    }
  }

  public void setPipeline(Pipeline pipeline) {
    this.pipeline = pipeline;
  }
}
