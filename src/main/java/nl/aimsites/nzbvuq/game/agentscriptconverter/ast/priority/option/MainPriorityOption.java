package nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.ASTNode;

/**
 * ASTNode for MainPriorityOption
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class MainPriorityOption extends ASTNode {

  /**
   * MainPriorityOption constructor
   *
   * @param name name of the priority option
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public MainPriorityOption(String name) {
    super(name);
  }
}
