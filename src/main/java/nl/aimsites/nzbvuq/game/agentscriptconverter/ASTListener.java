package nl.aimsites.nzbvuq.game.agentscriptconverter;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.*;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.InventoryPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.MainPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option.InventoryPriorityOption;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option.MainPriorityOption;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.ConditionRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.PriorityRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleFoodRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleWeaponDefaultRule;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Class for the ASTListener.
 *
 * @author Azez Salami (A.salami@student.han.nl)
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class ASTListener extends AgentsGrammarBaseListener {
  private final URL agentGrammarConfigJson =
      getClass().getClassLoader().getResource("agentGrammarConfig.json");
  private AST ast;
  private Stack<ASTNode> stack;
  private final HashMap<String, List<String>> KEYWORDS_ALTERNATIVES;

  /**
   * Constructor of the class, initializes the variables and run initHashMap methode
   *
   * @author Azez Salami (A.salami@student.han.nl)
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public ASTListener() throws FileNotFoundException {
    ast = new AST();
    stack = new Stack<>();
    KEYWORDS_ALTERNATIVES = new HashMap<>();
    initJson();
  }

  /**
   * Initializes the JSON file
   *
   * @throws FileNotFoundException If the JSON file cannot be found
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  private void initJson() throws FileNotFoundException {
    var unparsedJson = getJsonFileContents();
    if (unparsedJson == null) {
      throw new FileNotFoundException("No Json has been found.");
    }
    initHashMap(unparsedJson);
  }

  /**
   * Reads the json file and returns the content
   *
   * @return JSON file in String
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  private String getJsonFileContents() {
    try {
      return new String(
          Files.readAllBytes(Paths.get(Objects.requireNonNull(agentGrammarConfigJson).toURI())));
    } catch (IOException | NullPointerException | URISyntaxException e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Initializes the KEYWORDS_ALTERNATIVES with the contents from the JSON file
   *
   * @param unparsedJson JSON file in String
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  private void initHashMap(String unparsedJson) {
    JSONArray jsonKeywords = new JSONObject(unparsedJson).getJSONArray("keywords");
    for (Object jsonObjectKeyword : jsonKeywords) {
      JSONObject json = (JSONObject) jsonObjectKeyword;
      ArrayList<String> alternatives = new ArrayList<>();
      JSONArray jsonAlternatives = new JSONObject(json.toString()).getJSONArray("alternatives");
      for (Object jsonObjectAlternative : jsonAlternatives) {
        alternatives.add(jsonObjectAlternative.toString());
      }
      KEYWORDS_ALTERNATIVES.put(json.getString("key"), alternatives);
    }
  }

  /**
   * Getter for keyword in hashmap
   *
   * @param key the key is a phrase or word from the Context
   * @return the good string from the HashMap
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  private String getValueFromHashMap(String key) {
    for (Map.Entry<String, List<String>> entry : KEYWORDS_ALTERNATIVES.entrySet()) {
      if (entry.getKey().equalsIgnoreCase(key)) {
        return key.toLowerCase();
      }

      for (String item : entry.getValue()) {
        if (item.equalsIgnoreCase(key)) {
          return entry.getKey();
        }
      }
    }

    return "UNKNOWN";
  }

  public AST getAST() {
    return ast;
  }

  public Stack<ASTNode> getStack() {
    return stack;
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterAgentScript(AgentsGrammarParser.AgentScriptContext ctx) {
    stack.push(new AgentScript());
  }

  /**
   * Set the root of the AST
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitAgentScript(AgentsGrammarParser.AgentScriptContext ctx) {
    ast.setRoot((AgentScript) stack.pop());
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterConditionRule(AgentsGrammarParser.ConditionRuleContext ctx) {
    stack.push(new ConditionRule());
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitConditionRule(AgentsGrammarParser.ConditionRuleContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterCondition(AgentsGrammarParser.ConditionContext ctx) {
    stack.push(new Condition(getValueFromHashMap(ctx.getText())));
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitCondition(AgentsGrammarParser.ConditionContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterSubject(AgentsGrammarParser.SubjectContext ctx) {
    stack.push(new Subject(getValueFromHashMap(ctx.getText())));
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitSubject(AgentsGrammarParser.SubjectContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterAction(AgentsGrammarParser.ActionContext ctx) {
    stack.push(new Action(getValueFromHashMap(ctx.getText())));
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitAction(AgentsGrammarParser.ActionContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterIfClause(AgentsGrammarParser.IfClauseContext ctx) {
    stack.push(new IfClause());
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitIfClause(AgentsGrammarParser.IfClauseContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterIfCondition(AgentsGrammarParser.IfConditionContext ctx) {
    if (ctx.getChildCount() == 1) {
      if (ctx.getText().contains("current")) { // Current weapon
        stack.push(new IfCondition(ctx.getText()));
      } else { // Strength comparison without subject
        stack.push(new IfCondition());
      }
    } else if (ctx.getChildCount() == 2) {
      if (ctx.getChild(1).getText().charAt(0) == 's') { // Strength comparison with subject
        stack.push(new IfCondition());
      } else { // I have
        if (ctx.getChild(1).getText().charAt(0) == 'm') {
          stack.push(new IfCondition(LessMore.MORE));
        } else {
          stack.push(new IfCondition(LessMore.LESS));
        }
      }
    }
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitIfCondition(AgentsGrammarParser.IfConditionContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterPriorityRule(AgentsGrammarParser.PriorityRuleContext ctx) {
    stack.push(new PriorityRule());
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitPriorityRule(AgentsGrammarParser.PriorityRuleContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterMainPriority(AgentsGrammarParser.MainPriorityContext ctx) {
    stack.push(new MainPriority());
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitMainPriority(AgentsGrammarParser.MainPriorityContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterInventoryPriority(AgentsGrammarParser.InventoryPriorityContext ctx) {
    stack.push(new InventoryPriority());
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitInventoryPriority(AgentsGrammarParser.InventoryPriorityContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterSimpleFoodRule(AgentsGrammarParser.SimpleFoodRuleContext ctx) {
    stack.push(new SimpleFoodRule());
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitSimpleFoodRule(AgentsGrammarParser.SimpleFoodRuleContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterSimpleWeaponDefaultRule(AgentsGrammarParser.SimpleWeaponDefaultRuleContext ctx) {
    stack.push(new SimpleWeaponDefaultRule(ctx.getChild(1).getText()));
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitSimpleWeaponDefaultRule(AgentsGrammarParser.SimpleWeaponDefaultRuleContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterFood(AgentsGrammarParser.FoodContext ctx) {
    stack.push(new Food(getValueFromHashMap(ctx.getText())));
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitFood(AgentsGrammarParser.FoodContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterMainPriorityOption(AgentsGrammarParser.MainPriorityOptionContext ctx) {
    stack.push(new MainPriorityOption(getValueFromHashMap(ctx.getText())));
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitMainPriorityOption(AgentsGrammarParser.MainPriorityOptionContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterInventoryPriorityOption(AgentsGrammarParser.InventoryPriorityOptionContext ctx) {
    stack.push(
        new InventoryPriorityOption(
            Integer.parseInt(ctx.getChild(0).getText()), ctx.getChild(1).getText()));
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitInventoryPriorityOption(AgentsGrammarParser.InventoryPriorityOptionContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }

  /**
   * Push the specific object to the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void enterStrengthComparison(AgentsGrammarParser.StrengthComparisonContext ctx) {
    if (ctx.getChild(1).getText().equals(">")
        || ctx.getChild(1).getText().equals("more than")
        || ctx.getChild(1).getText().equals("over")) {
      stack.push(
          new StrengthComparison(LessMore.MORE, Integer.parseInt(ctx.getChild(2).getText())));
    }
    if (ctx.getChild(1).getText().equals("<")
        || ctx.getChild(1).getText().equals("less than")
        || ctx.getChild(1).getText().equals("under")) {
      stack.push(
          new StrengthComparison(LessMore.LESS, Integer.parseInt(ctx.getChild(2).getText())));
    }
  }

  /**
   * Add child to the last object in the stack
   *
   * @param ctx Context that generate by Antlr
   * @author Azez Salami (A.salami@student.han.nl)
   */
  @Override
  public void exitStrengthComparison(AgentsGrammarParser.StrengthComparisonContext ctx) {
    ASTNode astNode = stack.pop();
    stack.peek().addChild(astNode);
  }
}
