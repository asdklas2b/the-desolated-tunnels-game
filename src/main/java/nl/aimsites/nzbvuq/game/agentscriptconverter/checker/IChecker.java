package nl.aimsites.nzbvuq.game.agentscriptconverter.checker;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.AST;

/** The interface Checker. The Checker enables the the compiler to check the AST tree.
 *
 * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
 * */
public interface IChecker {

  /**
   * The check method will check the whole AST tree.
   *
   * @param ast the AST
   * @author Stijn van Ewijk (TS.vanEwijk@student.han.nl)
   */
  void check(AST ast);
}
