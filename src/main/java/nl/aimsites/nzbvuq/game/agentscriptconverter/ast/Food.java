package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

/**
 * ASTNode for Food
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class Food extends ASTNode {
  /**
   * Food constructor
   *
   * @param name name of the food
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public Food(String name) {
    super(name);
  }
}
