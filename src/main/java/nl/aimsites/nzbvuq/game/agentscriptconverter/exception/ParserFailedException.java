package nl.aimsites.nzbvuq.game.agentscriptconverter.exception;

/**
 * Exception for convert() in AgentScriptConverter
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class ParserFailedException extends RuntimeException {
  /**
   * ParserFailedException constructor
   *
   * @param cause Cause of the exception
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public ParserFailedException(Throwable cause) {
    super(cause);
  }
}
