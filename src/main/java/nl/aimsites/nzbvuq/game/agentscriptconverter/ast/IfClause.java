package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

import java.util.ArrayList;

/**
 * ASTNode for IfClause
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class IfClause extends ASTNode {
  private IfCondition ifCondition;
  private Action action;

  @Override
  public ArrayList<ASTNode> getChildren() {
    ArrayList<ASTNode> arrayList = new ArrayList<>();
    if (ifCondition != null) {
      arrayList.add(ifCondition);
    }
    if (action != null) {
      arrayList.add(action);
    }

    return arrayList;
  }

  @Override
  public void addChild(ASTNode child) {
    if (child instanceof IfCondition) {
      ifCondition = (IfCondition) child;
    } else if (child instanceof Action) {
      action = (Action) child;
    }
  }

  @Override
  public void removeChild(ASTNode child) {
    if (child instanceof IfCondition) {
      ifCondition = null;
    } else if (child instanceof Action) {
      action = null;
    }
  }

  public IfCondition getIfCondition() {
    return ifCondition;
  }
}
