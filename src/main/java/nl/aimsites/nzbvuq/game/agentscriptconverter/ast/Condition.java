package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

/**
 * ASTNode for Condition.
 *
 * @author Jorrit Schepers (J.Schepers3@student.han.nl)
 */
public class Condition extends ASTNode {
  /**
   * Condition constructor
   *
   * @param name name of the condition
   * @author Jorrit Schepers (J.Schepers3@student.han.nl)
   */
  public Condition(String name) {
    super(name);
  }
}
