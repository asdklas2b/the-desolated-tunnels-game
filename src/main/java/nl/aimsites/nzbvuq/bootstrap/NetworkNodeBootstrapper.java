package nl.aimsites.nzbvuq.bootstrap;

import java.util.List;

public interface NetworkNodeBootstrapper {
  /**
   * Start the bootstrapper should run in it's own thread.
   *
   * @param timeout Timeout in milliseconds
   */
  void bootstrap(int timeout);

  /**
   * Check if the node is connected
   *
   * @return Connection state
   */
  boolean isConnected();

  /**
   * Get a list of nodes that are on the network
   *
   * @return List of nodes that are on the network
   */
  List<NodeReference> relatives();
}
