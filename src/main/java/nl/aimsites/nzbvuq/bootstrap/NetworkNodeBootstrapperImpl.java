package nl.aimsites.nzbvuq.bootstrap;

import java.util.ArrayList;
import java.util.List;

public class NetworkNodeBootstrapperImpl implements NetworkNodeBootstrapper {

  @Override
  public void bootstrap(int timeout) {

  }

  @Override
  public boolean isConnected() {
    return false;
  }

  @Override
  public List<NodeReference> relatives() {
    List<NodeReference> relatives = new ArrayList<>();
    relatives.add(new NodeReference("LobbyClient"));
    relatives.add(new NodeReference("LobbyHost"));
    return relatives;
  }
}
