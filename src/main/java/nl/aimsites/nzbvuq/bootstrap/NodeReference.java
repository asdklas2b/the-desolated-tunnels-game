package nl.aimsites.nzbvuq.bootstrap;

import java.util.Objects;
import java.util.UUID;

public class NodeReference {

  private final String identifier;

  public static NodeReference random() {
    return new NodeReference(UUID.randomUUID().toString());
  }

  public NodeReference(String identifier) {
    this.identifier = identifier;
  }

  public String getIdentifier() {
    return identifier;
  }

  @Override
  public boolean equals(Object reference) {
    if (reference instanceof NodeReference) {
      return this.identifier.equals(((NodeReference) reference).getIdentifier());
    }

    return super.equals(reference);
  }

  @Override
  public int hashCode() {
    return Objects.hash(identifier);
  }
}
