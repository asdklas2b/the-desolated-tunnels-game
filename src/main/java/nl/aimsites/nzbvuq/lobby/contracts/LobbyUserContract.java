package nl.aimsites.nzbvuq.lobby.contracts;

/**
 * @author Jos Elbers (J.Elbers2@student.han.nl)
 */
public interface LobbyUserContract {
  /**
   * Returns the identifier of the user
   *
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   * @return the identifier of the user
   */
  String getIdentifier();

  /**
   * Returns the name of the user
   *
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   * @return the name of the user
   */
  String getName();
}
