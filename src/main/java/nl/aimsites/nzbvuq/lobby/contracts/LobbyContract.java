package nl.aimsites.nzbvuq.lobby.contracts;

import nl.aimsites.nzbvuq.game.state.GameState;

import java.util.List;

/**
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 * @author Jos Elbers (J.Elbers2@student.han.nl)
 */
public interface LobbyContract {
  /**
   * Get the identifier of the lobby
   *
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @return UUID that the lobby is identified by
   */
  String getIdentifier();

  /**
   * Get the name of the lobby
   *
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @return String containing the name of the lobby
   */
  String getName();

  /**
   * Get the max players that can join the given lobby
   *
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @return int defining the max players that can join the lobby
   */
  int getMaxPlayers();

  /**
   * Get the current player count that are in the given lobby
   *
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @return int defining the current amount of players in the lobby
   */
  int getPlayersCount();

  /**
   * Get the current player count that are in the given lobby
   *
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @return int defining the current amount of players in the lobby
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   *
   * @return a list of all players within the lobby
   */
  List<LobbyUserContract> getPlayers();

  /**
   * Add a player to the lobby
   *
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   *
   * @param player the player to be added
   */
  void addPlayer(LobbyUserContract player);

  boolean isStarted();

  void setStarted(boolean started);

  long getSeed();
  void setSeed(long seed);
}
