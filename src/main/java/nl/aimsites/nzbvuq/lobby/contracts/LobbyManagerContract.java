package nl.aimsites.nzbvuq.lobby.contracts;

import nl.aimsites.nzbvuq.lobby.exceptions.LobbyException;

import java.util.List;

/**
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 * @author Jos Elbers (J.Elbers2@student.han.nl)
 */
public interface LobbyManagerContract {
  /**
   *
   * Listen for lobby updates
   *
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @param listener Listener that recieves updates
   */
  void listen(LobbyListenerContract listener);

  /**
   * Get the current lobby list
   *
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @return List of lobbies
   */
  List<LobbyContract> getLobbies();

  /**
   * Join a lobby
   *
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @param lobby Lobby to join.
   */
  void join(LobbyContract lobby);

  /**
   * Leave a lobby
   *
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  void leave();

  /**
   * Create a lobby
   *
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @param name       Name of lobby.
   * @param maxPlayers maximum players a lobby van handle.
   */
  void create(String name, int maxPlayers);

  /**
   * Disband a lobby
   *
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  void disband();

  /**
   * Start the game from a lobby
   *
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   * @author Tom Esendam (ts.esendam@student.han.nl)
   */
  void startGame() throws LobbyException;

  /**
   * Gets the lobby created by the player
   *
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   *
   * @return the lobby hosted by the current player, returns null if no lobbies are found
   */
  LobbyContract getOwnLobby();

  /**
   * Gets the lobby the player has joined
   *
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   *
   * @return the lobby the current player has joined, returns null if no lobbies are found
   */
  LobbyContract getJoinedLobby();

  /**
   * Sets a different username for the current user
   *
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   *
   * @param name the username to be set
   */
  void setUsername(String name);

  String getUsername();
}
