package nl.aimsites.nzbvuq.lobby.contracts;

import nl.aimsites.nzbvuq.game.entities.character.Player;

import java.util.List;

/**
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 */
public interface LobbyListenerContract {
  /**
   * Called when a new lobby is added
   *
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @param lobby Lobby that is added to the lobbies list
   */
  void onAdd(LobbyContract lobby);

  /**
   * Called when a lobby is updated
   *
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @param lobby Lobby that is updated in the lobbies list
   */
  void onUpdate(LobbyContract lobby);

  /**
   * Called when a lobby is removed
   *
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @param lobby Lobby that is removed from the lobbies list
   */
  void onRemove(LobbyContract lobby);

  /**
   * Called when a lobby's game is started
   * @param lobby
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Tom Esendam (ts.esendam@student.han.nl)
   */
  void onStartGame(LobbyContract lobby);

  /**
   * Called when a lobby is joined
   *
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   * @param lobby the lobby that was joined
   */
  void onJoin(LobbyContract lobby);
}
