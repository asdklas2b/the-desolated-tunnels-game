package nl.aimsites.nzbvuq.lobby;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.bootstrapper.Bootstrapper;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.state.GameState;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyListenerContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyManagerContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyUserContract;
import nl.aimsites.nzbvuq.lobby.exceptions.LobbyException;
import nl.aimsites.nzbvuq.rpc.ConnectionRequestHandler;
import nl.aimsites.nzbvuq.rpc.RPC;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

/**
 * @author Jos Elbers (J.Elbers2@student.han.nl)
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 * @author Tom Esendam (ts.esendam@student.han.nl)
 */
public class LobbyManager implements LobbyManagerContract {

  private static final Logger LOGGER = Logger.getLogger(LobbyManager.class.getName());

  private final Gson GSON = new Gson();
  private final RPC rpc;
  private final LobbyHandler handler;
  private final LobbyUser user;
  private final List<LobbyListenerContract> listeners;
  private final Map<String, LobbyContract> lobbies;

  /**
   * @param rpc the rpc mechanic required to get identifiers
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  public LobbyManager(RPC rpc, Bootstrapper bootstrapper) {
    this.rpc = rpc;
    listeners = Collections.synchronizedList(new CopyOnWriteArrayList<>());
    lobbies = Collections.synchronizedMap(new ConcurrentHashMap<>());
    handler = new LobbyHandler(rpc, this, bootstrapper);
    user = new LobbyUser(rpc.getIdentifier());

    ConnectionRequestHandler.getInstance().addHandler(handler);
  }

  /**
   * @param listener Listener that receives updates
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  @Override
  public void listen(LobbyListenerContract listener) {
    synchronized (listeners) {
      listeners.add(listener);
    }
  }

  /**
   * @return returns all available known lobbies
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   * @author Fedor Soffers (FKA.Soffers@student.han.nl)
   */
  @Override
  public List<LobbyContract> getLobbies() {
    synchronized (lobbies) {
      return new ArrayList<>(lobbies.values());
    }
  }

  /**
   * Attempts to request the list with lobbies from the first relative and update the lobby list.
   *
   * @return List of lobbies
   * @author Fedor Soffers (FKA.Soffers@student.han.nl)
   */
  public List<LobbyContract> requestLobbies() throws LobbyException {
    try {
      List<LobbyContract> otherLobbies = handler.sendLobbiesRequest(GSON.toJson(rpc.getIdentifier()));

      // Adds the items that are uncommon in the otherLobbies list.
      synchronized (lobbies) {
        otherLobbies.forEach(lobby -> lobbies.put(lobby.getIdentifier(), lobby));
      }

    } catch (ExecutionException | LobbyException e) {
      throw new LobbyException(e);
    }
    return getLobbies();
  }

  /**
   * Attempts to join the lobby
   *
   * @param lobby Lobby to join.
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  @Override
  public void join(LobbyContract lobby) {
    if (!canJoin(lobby)) {
      return;
    }

    // Send request to receiveParticipant
    handler.sendNewParticipant(
      lobby.getIdentifier(), GSON.toJson(user));
  }

  /**
   * Attempts to create the lobby and distributes it among the known relatives
   *
   * @param name       Name of lobby.
   * @param maxPlayers maximum players a lobby van handle.
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  @Override
  public void create(String name, int maxPlayers) {
    var lobby =
      new Lobby(
        rpc.getIdentifier(),
        name,
        maxPlayers,
        List.of(user) // Add yourself
      );

    // Join your own lobby which will add the lobby to the network

    receiveUpdate(lobby);
    join(lobby);

    handler.sendLobby(GSON.toJson(lobby));
  }

  /**
   * Attempts to leave the lobby
   *
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  @Override
  public void leave() {
    var joinedLobby = getJoinedLobby();

    if (joinedLobby == null) {
      return;
    }

    receiveUpdate(joinedLobby);

    handler.sendUpdate(GSON.toJson(joinedLobby));
  }

  /**
   * Attempts to disband the lobby
   *
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  @Override
  public void disband() {
    LobbyContract ownedLobby = getOwnLobby();
    if (ownedLobby == null) {
      return;
    }

    handler.sendDisband(GSON.toJson(ownedLobby));
  }

  /**
   * Starts a game for the lobby
   *
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   * @author Tom Esendam (ts.esendam@student.han.nl)
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  @Override
  public void startGame() throws LobbyException {
    var ownLobby = getOwnLobby();
    if (ownLobby == null) {
      throw new LobbyException("Cannot start a lobby when you don't own one.");
    }

    if (ownLobby.isStarted()) {
      throw new LobbyException("Lobby with ID: " + ownLobby.getIdentifier() + " already started ");
    }

    ownLobby.setStarted(true);

    // make connection with the handler
    handler.sendStartGame(ownLobby);
  }

  /**
   * Receives the new participant and sends an update to the other participants
   *
   * @param player the new participant that has joined the lobby
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  public void receiveNewParticipant(LobbyUserContract player) {
    var ownLobby = getOwnLobby();

    if (ownLobby == null) {
      return;
    }
    if (ownLobby.getPlayers().size() >= ownLobby.getMaxPlayers()) {
      return;
    }

    ownLobby.addPlayer(player);

    receiveUpdate(ownLobby);

    // Send -> to all receiveUpdate
    handler.sendUpdate(GSON.toJson(ownLobby));
  }

  /**
   * Updates the lobby
   *
   * @param lobby the lobby to be updated
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  public void receiveUpdate(LobbyContract lobby) {
    synchronized (lobbies) {
      lobbies.put(lobby.getIdentifier(), lobby);
    }

    if (lobby.getPlayers().stream().anyMatch(p -> p.getIdentifier().equals(rpc.getIdentifier()))) {
      synchronized (listeners) {
        listeners.forEach(listener -> listener.onJoin(lobby));
      }
    }
    synchronized (listeners) {
      listeners.forEach(listener -> listener.onUpdate(lobby));
    }
  }

  /**
   * Disbands the lobby upon receiving the call to disband
   *
   * @param lobby lobby to be disbanded
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  public void receiveDisband(LobbyContract lobby) {
    synchronized (lobbies) {
      lobbies.remove(lobby.getIdentifier());
    }
    synchronized (listeners) {
      listeners.forEach(listener -> listener.onRemove(lobby));
    }
  }

  /**
   * Method called when a startGame event is received.
   * Changed the status of joined lobby and notifies listeners.
   *
   * @param receivedLobby the lobby on which the start event is to be triggered.
   * @throws LobbyException when received lobby isn't equal to the joined lobby.
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Tom Esendam (ts.esendam@student.han.nl)
   */
  public void receiveStartGame(LobbyContract receivedLobby) throws LobbyException {

    var joinedLobby = getJoinedLobby();
    if (!receivedLobby.getIdentifier().equals(joinedLobby.getIdentifier())) {
      throw new LobbyException("Cannot join un-joined lobby with ID: " + receivedLobby.getIdentifier());
    }

    joinedLobby.setStarted(true);

    LOGGER.info("Sending starts");
    synchronized (listeners) {
      listeners.stream().forEach(listener -> {
        LOGGER.info("Send start: " + listener.getClass().getName());
        listener.onStartGame(joinedLobby);
      });
    }
  }

  /**
   * @return returns the lobby owned by the current player
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  @Override
  public LobbyContract getOwnLobby() {
    synchronized (lobbies) {
      return lobbies.getOrDefault(rpc.getIdentifier(), null);
    }
  }
  /**
   * @return returns the lobby the current player is participating in
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  @Override
  public LobbyContract getJoinedLobby() {
    return getLobbies().stream()
      .filter(
        l ->
          l.getPlayers().stream()
            .anyMatch(p -> p.getIdentifier().equals(rpc.getIdentifier())))
      .findFirst()
      .orElse(null);
  }

  /**
   * Sets the username for the current user
   *
   * @param name the username to be set
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  @Override
  public void setUsername(String name) {
    user.setName(name);
  }

  @Override
  public String getUsername() {
    return user.getName();
  }

  /**
   * Validates whether the lobby can be joined
   *
   * @param lobby the lobby to be joined
   * @return returns whether the lobby can be joined (true/false)
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  private boolean canJoin(LobbyContract lobby) {
    if (getJoinedLobby() == null) {
      return lobby.getPlayersCount() < lobby.getMaxPlayers();
    }

    return false;
  }
}
