package nl.aimsites.nzbvuq.lobby;

import nl.aimsites.nzbvuq.game.state.GameState;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyUserContract;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Jos Elbers (J.Elbers2@student.han.nl)
 * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
 */
public class Lobby implements LobbyContract {

  private String identifier;
  private String name;
  private int maxPlayers;
  private List<LobbyUserContract> players;
  private boolean started;
  private LobbyUserContract currentUser;
  private long worldSeed;

  /**
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   *
   * @param identifier lobby owners identifier
   * @param name lobby name
   * @param maxPlayers max players allowed within lobby
   * @param players players within the lobby
   */
  public Lobby(String identifier, String name, int maxPlayers, List<LobbyUserContract> players) {
    this.identifier = identifier;
    this.name = name;
    this.maxPlayers = maxPlayers;
    this.players = players;
    this.started = false;
    this.worldSeed = new Random().nextLong();
  }

  public Lobby(String identifier, String name, int maxPlayers) {
    this(identifier, name, maxPlayers, new ArrayList<>());
  }


  @Override
  public String getIdentifier() {
    return identifier;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getMaxPlayers() {
    return maxPlayers;
  }

  @Override
  public int getPlayersCount() {
    return players.size();
  }

  @Override
  public List<LobbyUserContract> getPlayers() {
    return players;
  }

  /**
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   *
   * @param player the player that entered the lobby
   */
  @Override
  public void addPlayer(LobbyUserContract player) {
    players.add(player);
  }

  @Override
  public boolean isStarted() {
    return started;
  }

  @Override
  public void setStarted(boolean started) {
    this.started = started;
  }

  @Override
  public long getSeed() {
    return worldSeed;
  }

  @Override
  public void setSeed(long seed) {
    worldSeed = seed;
  }
}
