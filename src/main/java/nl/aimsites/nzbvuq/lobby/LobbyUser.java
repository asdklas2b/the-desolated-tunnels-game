package nl.aimsites.nzbvuq.lobby;

import nl.aimsites.nzbvuq.lobby.contracts.LobbyUserContract;

/**
 * @author Jos Elbers (J.Elbers2@student.han.nl)
 */
public class LobbyUser implements LobbyUserContract {
  private String identifier;
  private String name;

  /**
   * @param identifier the identifier of the user
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  public LobbyUser(String identifier) {
    this.identifier = identifier;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getIdentifier() {
    return identifier;
  }

  @Override
  public String getName() {
    return name == null ? identifier : name;
  }
}
