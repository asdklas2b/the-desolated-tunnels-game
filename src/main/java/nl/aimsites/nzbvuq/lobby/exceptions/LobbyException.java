package nl.aimsites.nzbvuq.lobby.exceptions;

/**
 * The type Lobby exception.
 *
 * @author Tom Esendam (ts.esendam@student.han.nl)
 */
public class LobbyException extends Exception {
  /**
   * Instantiates a new Lobby exception.
   *
   * @param e the e
   * @author Tom Esendam (ts.esendam@student.han.nl)
   */
  public LobbyException(Exception e) {
    super(e);
  }

  /**
   * Instantiates a new Lobby exception.
   *
   * @param e the e
   * @author Tom Esendam (ts.esendam@student.han.nl)
   */
  public LobbyException(String e) {
    super(e);
  }
}
