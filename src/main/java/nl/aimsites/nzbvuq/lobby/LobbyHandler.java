package nl.aimsites.nzbvuq.lobby;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import nl.aimsites.nzbvuq.bootstrapper.Bootstrapper;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyUserContract;
import nl.aimsites.nzbvuq.lobby.exceptions.LobbyException;
import nl.aimsites.nzbvuq.rpc.RPC;
import nl.aimsites.nzbvuq.rpc.message.Request;
import nl.aimsites.nzbvuq.rpc.message.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static nl.aimsites.nzbvuq.lobby.InterfaceSerializer.interfaceSerializer;

/**
 * @author Jos Elbers (J.Elbers2@student.han.nl)
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
 * @author Fedor Soffers (FKA.Soffers@student.han.nl)
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 * @author Tom Esendam (ts.esendam@student.han.nl)
 */
public class LobbyHandler {

  private static final Logger LOGGER = Logger.getLogger(LobbyHandler.class.getName());
  private final Gson GSON = new GsonBuilder()
    .registerTypeAdapter(LobbyContract.class, interfaceSerializer(Lobby.class))
    .registerTypeAdapter(LobbyUserContract.class, interfaceSerializer(LobbyUser.class))
    .create();

  private final RPC rpc;
  private final LobbyManager manager;
  private final Bootstrapper bootstrapper;

  /**
   * @param rpcListener the current listener
   * @param manager     the lobby manager
   * @author Jelmer van Vugt (JA.vanVugt@student.han.nl)
   */
  public LobbyHandler(RPC rpcListener, LobbyManager manager, Bootstrapper bootstrapper) {
    this.rpc = rpcListener;
    this.manager = manager;
    this.bootstrapper = bootstrapper;
  }

  /**
   * Sends an update to disband the lobby
   *
   * @param lobby the lobby to be disbanded
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void sendDisband(String lobby) {
    List<String> arguments = new ArrayList<>();
    arguments.add(GSON.toJson(lobby));
    Request request = new Request(LobbyHandler.class.getName(), "receiveDisband", arguments);

    // Letting other nodes on network know that lobby is created through RPC message
    bootstrapper.getRelatives()
      .forEach(node -> rpc.sendRequest(node, request));
  }

  /**
   * Receives the lobby to be disbanded
   *
   * @param arguments lobby to be disbanded
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void receiveDisband(List<String> arguments) {
    manager.receiveDisband(GSON.fromJson(arguments.get(0), LobbyContract.class));
  }

  /**
   * Sends the identifier of the new participant
   *
   * @param destination the request destination
   * @param payload     the identifier of the new participant
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void sendNewParticipant(String destination, String payload) {
    Request request =
      new Request(LobbyHandler.class.getName(), "receiveNewParticipant", List.of(payload));

    // Sending request
    rpc.sendRequest(destination, request);
  }

  /**
   * Receives the identifier of the new participant
   *
   * @param arguments the updated lobby with new participants
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void receiveNewParticipant(List<String> arguments) {
    var player = GSON.fromJson(arguments.get(0), LobbyUserContract.class);
    manager.receiveNewParticipant(player);
  }

  /**
   * Sends an update to all relatives
   *
   * @param update an update for all known relatives regarding lobbies
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void sendUpdate(String update) {
    // Creating request
    Request request = new Request(LobbyHandler.class.getName(), "receiveUpdate", List.of(update));

    // Sending request
    bootstrapper.getRelatives()
      .forEach(node -> rpc.sendRequest(node, request));
  }

  /**
   * Receives an update to lobbies
   *
   * @param arguments received update regarding lobbies
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void receiveUpdate(List<String> arguments) {
    manager.receiveUpdate(GSON.fromJson(arguments.get(0), LobbyContract.class));
  }

  /**
   * Sends the lobby among all known relatives
   *
   * @param lobby the lobby to be distributed among relatives
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
   */
  public void sendLobby(String lobby) {
    var request = new Request(LobbyHandler.class.getName(), "receiveLobby", List.of(lobby));

    bootstrapper.getRelatives()
      .forEach(node -> rpc.sendRequest(node, request));
  }

  /**
   * Receives a lobby that can be joined
   *
   * @param arguments a new lobby that can be joined
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  public void receiveLobby(List<String> arguments) {
    var lobby = GSON.fromJson(arguments.get(0), LobbyContract.class);

    manager.receiveUpdate(lobby);
  }

  /**
   * Let all players in lobby know game started.
   *
   * @param lobby the Lobby which game starts
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Tom Esendam (ts.esendam@student.han.nl)
   */
  public void sendStartGame(LobbyContract lobby) {
    LOGGER.log(Level.INFO, "ding is er ofzo");
    List<String> parameters = new ArrayList<>();
    parameters.add(GSON.toJson(lobby));
    Request request = new Request(LobbyHandler.class.getName(), "receiveStartGame", parameters);

    //Single threaded stream. This wont ever exceed 20 thus making it parallel will slow it down.
    lobby.getPlayers().forEach(player -> rpc.sendRequest(player.getIdentifier(), request));
  }

  /**
   * Used to receive sendGameStart
   *
   * @param parameters A list of arguments to be received
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Tom Esendam (ts.esendam@student.han.nl)
   */
  public void receiveStartGame(List<String> parameters) {
    LOGGER.log(Level.INFO,"received" + parameters.get(0));
    try {
      manager.receiveStartGame(GSON.fromJson(parameters.get(0), LobbyContract.class));
    } catch (LobbyException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
  }

  /**
   * Sends a request to receiveLobbiesRequest to get the lobbies from the first relative.
   *
   * @return the list with the lobbies from the first relative, or if non-existent an empty list.
   * @author Fedor Soffers (FKA.Soffers@student.han.nl)
   */
  public List<LobbyContract> sendLobbiesRequest(String identifier) throws ExecutionException, LobbyException {
    // Make the request
    var request = new Request(LobbyHandler.class.getName(), "receiveLobbiesRequest", List.of(identifier));

    // Get the first relative, and filter yourself from it. can be empty.
    Optional<String> firstRelative = bootstrapper.getRelatives().stream().filter(relative -> !relative.equals(identifier)).findFirst();

    // If there is a first relative send a request and add the lobbies to the arraylist, return the lobbies list
    if (firstRelative.isPresent()) {
      try {
        Response response = rpc.sendRequest(firstRelative.get(), request).get();
        List<String> parameters = response.getParameters();
        List<LobbyContract> lobbies = new ArrayList<>();

        parameters.forEach(parameter -> {
          lobbies.add(GSON.fromJson(parameter, Lobby.class));
        });

        return lobbies;
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        throw new LobbyException(e);
      } catch (ExecutionException e) {
        throw e;
      }
    } else {
      return new ArrayList<>();
    }
  }

  /**
   * Receives the request from sendRequestLobbies.
   *
   * @return a list String list with lobbies.
   * @author Fedor Soffers (FKA.Soffers@student.han.nl)
   */
  public List<String> receiveLobbiesRequest(List<String> parameters) {
    return manager.getLobbies().stream().map(GSON::toJson).collect(Collectors.toList());
  }
}
