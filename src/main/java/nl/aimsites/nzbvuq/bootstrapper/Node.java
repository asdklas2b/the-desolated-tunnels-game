package nl.aimsites.nzbvuq.bootstrapper;

/**
 * Interface for a node
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 */
public interface Node {
  String getIdentifier();
}
