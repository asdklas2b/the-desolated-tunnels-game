package nl.aimsites.nzbvuq.bootstrapper;

import nl.aimsites.nzbvuq.rpc.RPC;
import nl.aimsites.nzbvuq.rpc.message.Request;
import nl.aimsites.nzbvuq.rpc.message.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

/**
 * The type NodeReference.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 */
public class RPCNode implements BootstrapperNode {
  private final String identifier;

  /**
   * @param identifier for this node
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  public RPCNode(String identifier) {
    this.identifier = identifier;
  }

  /**
   * Turns a list of strings into a list of BootstrapperNodes
   *
   * @param strings list of strings
   * @return list of BootstrapperNodes
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  public static List<BootstrapperNode> stringsToNodes(List<String> strings) {
    List<BootstrapperNode> references = new ArrayList<>();
    for (String string : strings) {
      BootstrapperNode reference = new RPCNode(string);
      if (!references.contains(reference)) {
        references.add(reference);
      }
    }
    return references;
  }

  /**
   * Turns a list of BootstrapperNodes into a list of strings
   *
   * @param references list of BootstrapperNode
   * @return list of strings
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  public static List<String> nodesToStrings(List<BootstrapperNode> references) {
    List<String> strings = new ArrayList<>();
    for (BootstrapperNode reference : references) {
      String string = reference.getIdentifier();
      if (!strings.contains(string)) {
        strings.add(string);
      }
    }
    return strings;
  }

  @Override
  public String getIdentifier() {
    return identifier;
  }

  /**
   * Checks if this node is actually this application's not
   *
   * @param rpc to use for verifying this application's identifier
   * @return true if this node belongs to this application, false if not
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  @Override
  public boolean isSelf(RPC rpc) {
    return rpc.getIdentifier().equals(this.identifier);
  }

  /**
   * Pings this node, returns a list of its relatives if it's online, an empty list if it's offline
   *
   * @param rpc to use for pinging this node
   * @return list of this node's relatives, empty if the node is offline
   * @throws BootstrapperException if an exception occurs when trying to send a request
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  @Override
  public List<String> ping(RPC rpc) throws BootstrapperException {
    List<String> parameters = new ArrayList<>();
    parameters.add(rpc.getIdentifier());
    Request request =
        new Request(BootstrapperHandler.class.getName(), "ping", parameters);
    try {
      Response response = rpc.sendRequest(this.identifier, request).get();
      return response.getParameters();
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      throw new BootstrapperException(e);
    } catch (ExecutionException e) {
      throw new BootstrapperException(e);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    RPCNode that = (RPCNode) o;
    return Objects.equals(identifier, that.identifier);
  }

  @Override
  public int hashCode() {
    return Objects.hash(identifier);
  }
}
