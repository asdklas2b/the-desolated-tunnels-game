package nl.aimsites.nzbvuq.bootstrapper;

import nl.aimsites.nzbvuq.rpc.ConnectionRequestHandler;
import nl.aimsites.nzbvuq.rpc.RPC;
import nl.aimsites.nzbvuq.rpc.message.Request;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bootstrapper that handles heartbeats, and keeps a list of relatives.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 */
public class RPCBootstrapper implements Bootstrapper, RelativesList {
  private static final String BOOTSTRAPPER_BOOTSTRAP_HANDLER_CLASS =
      "nl.aimsites.nzbvuq.bootstrapper.BootstrapHandler";
  private static final String BOOTSTRAPPER_IDENTIFIER = "Bootstrapper";
  private static final int HEARTBEAT_INTERVAL = 5000;
  private static final Logger LOGGER = Logger.getLogger(RPCBootstrapper.class.getName());
  private final RPC rpc;
  private final List<BootstrapperNode> relatives;

  /**
   * Instantiates a new RPC bootstrapper.
   *
   * @param rpc the RPC implementation
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  public RPCBootstrapper(RPC rpc) {
    this.rpc = rpc;
    this.relatives = new ArrayList<>();

    BootstrapperHandler bootstrapperHandler = new BootstrapperHandler(this);
    ConnectionRequestHandler.getInstance().addHandler(bootstrapperHandler);
  }

  /**
   * Returns a list of relatives that are currently active on the network.
   *
   * @return list of relatives
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  @Override
  public List<String> getRelatives() {
    return RPCNode.nodesToStrings(relatives);
  }

  @Override
  public void addRelative(String relative) {
    addRelative(new RPCNode(relative));
  }

  @Override
  public void addRelative(BootstrapperNode relative) {
    if (!this.relatives.contains(relative)) {
      relatives.add(relative);
    }
  }

  @Override
  public void removeRelative(BootstrapperNode relative) {
    relatives.remove(relative);
  }

  /**
   * Starts a thread running the init function
   *
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  @Override
  public void start() {
    new Thread(this::init).start();
  }

  /**
   * Initializes the bootstrapper thread. Obtains a list of last identified peers from the
   * bootstrapper, and checks all these peers for their online status. Then activates the heartbeat
   * timer to keep pinging them
   *
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  private void init() {
    Request request =
        new Request(
            BOOTSTRAPPER_BOOTSTRAP_HANDLER_CLASS,
            "getPeers",
            Collections.singletonList(rpc.getIdentifier()));

    try {
      List<String> peers = rpc.sendRequest(BOOTSTRAPPER_IDENTIFIER, request).get().getParameters();

      addRelative(new RPCNode(rpc.getIdentifier()));

      List<BootstrapperNode> activePeers = getActivePeers(peers).get();

      if (activePeers.isEmpty()) {
        TimeUnit.MILLISECONDS.sleep(HEARTBEAT_INTERVAL);
        peers = rpc.sendRequest(BOOTSTRAPPER_IDENTIFIER, request).get().getParameters();
        activePeers = getActivePeers(peers).get();
      }

      for (BootstrapperNode activePeer : activePeers) {
        addRelative(activePeer);
      }

      ScheduledExecutorService scheduledExecutorService =
          Executors.newSingleThreadScheduledExecutor();
      scheduledExecutorService.scheduleAtFixedRate(
          this::pingRelatives, 0, HEARTBEAT_INTERVAL, TimeUnit.MILLISECONDS);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    } catch (ExecutionException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
  }

  /**
   * Loops through a list of peers and checks if they're online. If they are, it takes their list of
   * relatives and returns it. Otherwise it returns an empty list
   *
   * @param peers identifiers of the peers to loop through
   * @return list of relatives of the first online node, or an empty list if there's no peer online
   * @throws BootstrapperException if an exception occurs when trying to send a request
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  private CompletableFuture<List<BootstrapperNode>> getActivePeers(List<String> peers) {
    CompletableFuture<List<BootstrapperNode>> completableFuture = new CompletableFuture<>();

    new Thread(
            () -> {
              peers.stream()
                  .parallel()
                  .forEach(
                      peer -> {
                        try {
                          BootstrapperNode node = new RPCNode(peer);
                          if (!node.isSelf(rpc)) {
                            List<String> receivedPeers = node.ping(rpc);
                            if (!receivedPeers.isEmpty()) {
                              completableFuture.complete(RPCNode.stringsToNodes(receivedPeers));
                            }
                          }
                        } catch (BootstrapperException e) {
                          LOGGER.log(Level.SEVERE, e.getMessage(), e);
                        }
                      });
              completableFuture.complete(new ArrayList<>());
            })
        .start();

    return completableFuture;
  }

  /**
   * Pings all relatives, removes them if they're offline Also updates the list of peers in case a
   * relative has relatives this application doesn't
   *
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  private void pingRelatives() {
    try {
      for (BootstrapperNode relative : relatives) {
        List<BootstrapperNode> relativePeers = RPCNode.stringsToNodes(relative.ping(rpc));

        if (relativePeers.isEmpty()) {
          removeRelative(relative);
        } else {
          relativePeers.stream()
              .filter(relativePeer -> !relatives.contains(relativePeer) && !relativePeer.getIdentifier().equals(BOOTSTRAPPER_IDENTIFIER))
              .forEach(relatives::add);
        }
      }

      Request request =
          new Request(
              BOOTSTRAPPER_BOOTSTRAP_HANDLER_CLASS,
              "getPeers",
              Collections.singletonList(rpc.getIdentifier()));
      rpc.sendRequest(BOOTSTRAPPER_IDENTIFIER, request).get();

      TimeUnit.MILLISECONDS.sleep(HEARTBEAT_INTERVAL);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    } catch (ExecutionException | BootstrapperException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
  }
}
