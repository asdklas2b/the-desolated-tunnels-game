package nl.aimsites.nzbvuq.bootstrapper;

import java.util.List;

/**
 * Interface for a list to store relatives
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 */
public interface RelativesList {
  List<String> getRelatives();

  void addRelative(String relative);

  void addRelative(BootstrapperNode relative);

  void removeRelative(BootstrapperNode relative);
}
