package nl.aimsites.nzbvuq.bootstrapper;

import java.util.List;

/**
 * The type Bootstrapper handler, manages a list of relatives and the ability to ping them
 *
 * @author Joost Lawerman (joostlawerman@users.noreply.github.com)
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 */
public class BootstrapperHandler {
  private final RelativesList relativesList;

  /**
   * Instantiates a new Bootstrapper handler.
   *
   * @param relativesList the relatives list
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  public BootstrapperHandler(RelativesList relativesList) {
    this.relativesList = relativesList;
  }

  /**
   * Returns a list of relatives.
   *
   * @param parameters the parameters
   * @return the list
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  public List<String> ping(List<String> parameters) {
    relativesList.addRelative(parameters.get(0));
    return relativesList.getRelatives();
  }
}
