package nl.aimsites.nzbvuq.bootstrapper;

import nl.aimsites.nzbvuq.rpc.RPC;

import java.util.List;

/**
 * Interface for a node, more specific to this package
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 */
public interface BootstrapperNode extends Node {
  /**
   * Checks if this node is actually this application's not
   *
   * @param rpc to use for verifying this application's identifier
   * @return true if this node belongs to this application, false if not
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  boolean isSelf(RPC rpc);

  /**
   * Pings this node, returns a list of its relatives if it's online, an empty list if it's offline
   *
   * @param rpc to use for pinging this node
   * @return list of this node's relatives, empty if the node is offline
   * @throws BootstrapperException if an exception occurs when trying to send a request
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  List<String> ping(RPC rpc) throws BootstrapperException;
}
