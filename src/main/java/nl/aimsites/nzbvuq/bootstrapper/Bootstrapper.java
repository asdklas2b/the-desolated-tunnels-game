package nl.aimsites.nzbvuq.bootstrapper;

import java.util.List;

/**
 * Interface for the bootstrapper, to be used outside this package
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 */
public interface Bootstrapper {
  /**
   * Initializes the bootstrapper and starts a heartbeat interval
   *
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  void start();

  /**
   * Gets a list of all online relatives
   *
   * @return a list of all online relatives
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  List<String> getRelatives();
}
