package nl.aimsites.nzbvuq.bootstrapper;

/**
 * Global exception to be thrown within the bootstrapper package
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 */
public class BootstrapperException extends Exception {
  /**
   * Constructor for this exception, takes a throwable
   *
   * @param cause Whatever caused this exception to be thrown
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   * @author Fedor Soffers (fka.soffers@student.han.nl)
   */
  public BootstrapperException(Throwable cause) {
    super(cause);
  }
}
