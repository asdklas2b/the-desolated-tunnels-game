// Generated from D:/GooGle Drive/Jaar 3 ASD-Project/Project repos/the-desolated-tunnels-game/src/main/antlr/nl/aimsites/nzbvuq/game/agentscriptconverter\AgentsGrammar.g4 by ANTLR 4.9
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link AgentsGrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface AgentsGrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#agentScript}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAgentScript(AgentsGrammarParser.AgentScriptContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#conditionRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditionRule(AgentsGrammarParser.ConditionRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition(AgentsGrammarParser.ConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#subject}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubject(AgentsGrammarParser.SubjectContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#action}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAction(AgentsGrammarParser.ActionContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#ifClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfClause(AgentsGrammarParser.IfClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#ifCondition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfCondition(AgentsGrammarParser.IfConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#priorityRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPriorityRule(AgentsGrammarParser.PriorityRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#mainPriority}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMainPriority(AgentsGrammarParser.MainPriorityContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#mainPriorityOption}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMainPriorityOption(AgentsGrammarParser.MainPriorityOptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#inventoryPriority}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInventoryPriority(AgentsGrammarParser.InventoryPriorityContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#inventoryPriotityOption}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInventoryPriotityOption(AgentsGrammarParser.InventoryPriotityOptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#simpleRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleRule(AgentsGrammarParser.SimpleRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#simpleFoodRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleFoodRule(AgentsGrammarParser.SimpleFoodRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#simpleDropWeaponRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleDropWeaponRule(AgentsGrammarParser.SimpleDropWeaponRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#simpleWeaponDefaultRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSimpleWeaponDefaultRule(AgentsGrammarParser.SimpleWeaponDefaultRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#food}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFood(AgentsGrammarParser.FoodContext ctx);
	/**
	 * Visit a parse tree produced by {@link AgentsGrammarParser#strengthComparison}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStrengthComparison(AgentsGrammarParser.StrengthComparisonContext ctx);
}