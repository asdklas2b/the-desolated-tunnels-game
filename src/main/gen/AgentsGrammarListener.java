// Generated from D:/GooGle Drive/Jaar 3 ASD-Project/Project repos/the-desolated-tunnels-game/src/main/antlr/nl/aimsites/nzbvuq/game/agentscriptconverter\AgentsGrammar.g4 by ANTLR 4.9
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link AgentsGrammarParser}.
 */
public interface AgentsGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#agentScript}.
	 * @param ctx the parse tree
	 */
	void enterAgentScript(AgentsGrammarParser.AgentScriptContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#agentScript}.
	 * @param ctx the parse tree
	 */
	void exitAgentScript(AgentsGrammarParser.AgentScriptContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#conditionRule}.
	 * @param ctx the parse tree
	 */
	void enterConditionRule(AgentsGrammarParser.ConditionRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#conditionRule}.
	 * @param ctx the parse tree
	 */
	void exitConditionRule(AgentsGrammarParser.ConditionRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(AgentsGrammarParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(AgentsGrammarParser.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#subject}.
	 * @param ctx the parse tree
	 */
	void enterSubject(AgentsGrammarParser.SubjectContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#subject}.
	 * @param ctx the parse tree
	 */
	void exitSubject(AgentsGrammarParser.SubjectContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#action}.
	 * @param ctx the parse tree
	 */
	void enterAction(AgentsGrammarParser.ActionContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#action}.
	 * @param ctx the parse tree
	 */
	void exitAction(AgentsGrammarParser.ActionContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#ifClause}.
	 * @param ctx the parse tree
	 */
	void enterIfClause(AgentsGrammarParser.IfClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#ifClause}.
	 * @param ctx the parse tree
	 */
	void exitIfClause(AgentsGrammarParser.IfClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#ifCondition}.
	 * @param ctx the parse tree
	 */
	void enterIfCondition(AgentsGrammarParser.IfConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#ifCondition}.
	 * @param ctx the parse tree
	 */
	void exitIfCondition(AgentsGrammarParser.IfConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#priorityRule}.
	 * @param ctx the parse tree
	 */
	void enterPriorityRule(AgentsGrammarParser.PriorityRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#priorityRule}.
	 * @param ctx the parse tree
	 */
	void exitPriorityRule(AgentsGrammarParser.PriorityRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#mainPriority}.
	 * @param ctx the parse tree
	 */
	void enterMainPriority(AgentsGrammarParser.MainPriorityContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#mainPriority}.
	 * @param ctx the parse tree
	 */
	void exitMainPriority(AgentsGrammarParser.MainPriorityContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#mainPriorityOption}.
	 * @param ctx the parse tree
	 */
	void enterMainPriorityOption(AgentsGrammarParser.MainPriorityOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#mainPriorityOption}.
	 * @param ctx the parse tree
	 */
	void exitMainPriorityOption(AgentsGrammarParser.MainPriorityOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#inventoryPriority}.
	 * @param ctx the parse tree
	 */
	void enterInventoryPriority(AgentsGrammarParser.InventoryPriorityContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#inventoryPriority}.
	 * @param ctx the parse tree
	 */
	void exitInventoryPriority(AgentsGrammarParser.InventoryPriorityContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#inventoryPriotityOption}.
	 * @param ctx the parse tree
	 */
	void enterInventoryPriotityOption(AgentsGrammarParser.InventoryPriotityOptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#inventoryPriotityOption}.
	 * @param ctx the parse tree
	 */
	void exitInventoryPriotityOption(AgentsGrammarParser.InventoryPriotityOptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#simpleRule}.
	 * @param ctx the parse tree
	 */
	void enterSimpleRule(AgentsGrammarParser.SimpleRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#simpleRule}.
	 * @param ctx the parse tree
	 */
	void exitSimpleRule(AgentsGrammarParser.SimpleRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#simpleFoodRule}.
	 * @param ctx the parse tree
	 */
	void enterSimpleFoodRule(AgentsGrammarParser.SimpleFoodRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#simpleFoodRule}.
	 * @param ctx the parse tree
	 */
	void exitSimpleFoodRule(AgentsGrammarParser.SimpleFoodRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#simpleDropWeaponRule}.
	 * @param ctx the parse tree
	 */
	void enterSimpleDropWeaponRule(AgentsGrammarParser.SimpleDropWeaponRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#simpleDropWeaponRule}.
	 * @param ctx the parse tree
	 */
	void exitSimpleDropWeaponRule(AgentsGrammarParser.SimpleDropWeaponRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#simpleWeaponDefaultRule}.
	 * @param ctx the parse tree
	 */
	void enterSimpleWeaponDefaultRule(AgentsGrammarParser.SimpleWeaponDefaultRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#simpleWeaponDefaultRule}.
	 * @param ctx the parse tree
	 */
	void exitSimpleWeaponDefaultRule(AgentsGrammarParser.SimpleWeaponDefaultRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#food}.
	 * @param ctx the parse tree
	 */
	void enterFood(AgentsGrammarParser.FoodContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#food}.
	 * @param ctx the parse tree
	 */
	void exitFood(AgentsGrammarParser.FoodContext ctx);
	/**
	 * Enter a parse tree produced by {@link AgentsGrammarParser#strengthComparison}.
	 * @param ctx the parse tree
	 */
	void enterStrengthComparison(AgentsGrammarParser.StrengthComparisonContext ctx);
	/**
	 * Exit a parse tree produced by {@link AgentsGrammarParser#strengthComparison}.
	 * @param ctx the parse tree
	 */
	void exitStrengthComparison(AgentsGrammarParser.StrengthComparisonContext ctx);
}