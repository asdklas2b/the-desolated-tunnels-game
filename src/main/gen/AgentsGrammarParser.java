// Generated from D:/GooGle Drive/Jaar 3 ASD-Project/Project repos/the-desolated-tunnels-game/src/main/antlr/nl/aimsites/nzbvuq/game/agentscriptconverter\AgentsGrammar.g4 by ANTLR 4.9
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class AgentsGrammarParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		When=1, ISee=2, IFind=3, LessStrength=4, MoreStrength=5, IHave=6, CurrentWeaponHigherStrength=7, 
		CurrentWeaponLowerStrength=8, Enemy=9, Monster=10, Attribute=11, Weapon=12, 
		Run=13, Fight=14, Flee=15, Pickup=16, Replace=17, If=18, Otherwise=19, 
		Priority=20, PriorityDestroyFlag=21, PriorityDefendFlag=22, PriorityEnemies=23, 
		PriorityMonsters=24, PriorityExplore=25, Set=26, Main=27, Inventory=28, 
		Armor=29, DropWeaponIfNotUsed=30, WeaponAsDefault=31, Best=32, Worst=33, 
		Light=34, Medium=35, Heavy=36, Food=37, Apple=38, Cookie=39, Soup=40, 
		Strength=41, Comma=42, And=43, Use=44, LessThan=45, GreaterThan=46, Dot=47, 
		WS=48, Number=49;
	public static final int
		RULE_agentScript = 0, RULE_conditionRule = 1, RULE_condition = 2, RULE_subject = 3, 
		RULE_action = 4, RULE_ifClause = 5, RULE_ifCondition = 6, RULE_priorityRule = 7, 
		RULE_mainPriority = 8, RULE_mainPriorityOption = 9, RULE_inventoryPriority = 10, 
		RULE_inventoryPriotityOption = 11, RULE_simpleRule = 12, RULE_simpleFoodRule = 13, 
		RULE_simpleDropWeaponRule = 14, RULE_simpleWeaponDefaultRule = 15, RULE_food = 16, 
		RULE_strengthComparison = 17;
	private static String[] makeRuleNames() {
		return new String[] {
			"agentScript", "conditionRule", "condition", "subject", "action", "ifClause", 
			"ifCondition", "priorityRule", "mainPriority", "mainPriorityOption", 
			"inventoryPriority", "inventoryPriotityOption", "simpleRule", "simpleFoodRule", 
			"simpleDropWeaponRule", "simpleWeaponDefaultRule", "food", "strengthComparison"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, "'less strength'", "'more strength'", null, "'current weapon has higher strength'", 
			"'current weapon has lower strength'", null, null, null, null, "'run'", 
			"'fight'", "'flee'", null, "'replace'", "'if'", "'otherwise'", null, 
			"'destroy flag'", "'defend flag'", null, null, "'explore'", null, "'main'", 
			"'inventory'", "'armor'", null, "'weapon as default'", "'best'", "'worst'", 
			"'light'", "'medium'", "'heavy'", "'food'", null, null, null, null, "','", 
			"'and'", null, null, null, "'.'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "When", "ISee", "IFind", "LessStrength", "MoreStrength", "IHave", 
			"CurrentWeaponHigherStrength", "CurrentWeaponLowerStrength", "Enemy", 
			"Monster", "Attribute", "Weapon", "Run", "Fight", "Flee", "Pickup", "Replace", 
			"If", "Otherwise", "Priority", "PriorityDestroyFlag", "PriorityDefendFlag", 
			"PriorityEnemies", "PriorityMonsters", "PriorityExplore", "Set", "Main", 
			"Inventory", "Armor", "DropWeaponIfNotUsed", "WeaponAsDefault", "Best", 
			"Worst", "Light", "Medium", "Heavy", "Food", "Apple", "Cookie", "Soup", 
			"Strength", "Comma", "And", "Use", "LessThan", "GreaterThan", "Dot", 
			"WS", "Number"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "AgentsGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public AgentsGrammarParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class AgentScriptContext extends ParserRuleContext {
		public List<TerminalNode> Dot() { return getTokens(AgentsGrammarParser.Dot); }
		public TerminalNode Dot(int i) {
			return getToken(AgentsGrammarParser.Dot, i);
		}
		public List<ConditionRuleContext> conditionRule() {
			return getRuleContexts(ConditionRuleContext.class);
		}
		public ConditionRuleContext conditionRule(int i) {
			return getRuleContext(ConditionRuleContext.class,i);
		}
		public List<PriorityRuleContext> priorityRule() {
			return getRuleContexts(PriorityRuleContext.class);
		}
		public PriorityRuleContext priorityRule(int i) {
			return getRuleContext(PriorityRuleContext.class,i);
		}
		public List<SimpleRuleContext> simpleRule() {
			return getRuleContexts(SimpleRuleContext.class);
		}
		public SimpleRuleContext simpleRule(int i) {
			return getRuleContext(SimpleRuleContext.class,i);
		}
		public AgentScriptContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_agentScript; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterAgentScript(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitAgentScript(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitAgentScript(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AgentScriptContext agentScript() throws RecognitionException {
		AgentScriptContext _localctx = new AgentScriptContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_agentScript);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(43); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(39);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case When:
				case Run:
				case Fight:
				case Flee:
				case Pickup:
				case Replace:
					{
					setState(36);
					conditionRule();
					}
					break;
				case Set:
					{
					setState(37);
					priorityRule();
					}
					break;
				case DropWeaponIfNotUsed:
				case Use:
					{
					setState(38);
					simpleRule();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(41);
				match(Dot);
				}
				}
				setState(45); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << When) | (1L << Run) | (1L << Fight) | (1L << Flee) | (1L << Pickup) | (1L << Replace) | (1L << Set) | (1L << DropWeaponIfNotUsed) | (1L << Use))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionRuleContext extends ParserRuleContext {
		public TerminalNode When() { return getToken(AgentsGrammarParser.When, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public SubjectContext subject() {
			return getRuleContext(SubjectContext.class,0);
		}
		public ActionContext action() {
			return getRuleContext(ActionContext.class,0);
		}
		public TerminalNode Comma() { return getToken(AgentsGrammarParser.Comma, 0); }
		public IfClauseContext ifClause() {
			return getRuleContext(IfClauseContext.class,0);
		}
		public ConditionRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterConditionRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitConditionRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitConditionRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConditionRuleContext conditionRule() throws RecognitionException {
		ConditionRuleContext _localctx = new ConditionRuleContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_conditionRule);
		int _la;
		try {
			setState(67);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case When:
				enterOuterAlt(_localctx, 1);
				{
				setState(47);
				match(When);
				setState(48);
				condition();
				setState(49);
				subject();
				setState(51);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(50);
					match(Comma);
					}
				}

				setState(53);
				action();
				setState(55);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==If) {
					{
					setState(54);
					ifClause();
					}
				}

				}
				break;
			case Run:
			case Fight:
			case Flee:
			case Pickup:
			case Replace:
				enterOuterAlt(_localctx, 2);
				{
				setState(57);
				action();
				setState(58);
				match(When);
				setState(59);
				condition();
				setState(60);
				subject();
				setState(62);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(61);
					match(Comma);
					}
				}

				setState(65);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==If) {
					{
					setState(64);
					ifClause();
					}
				}

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public TerminalNode ISee() { return getToken(AgentsGrammarParser.ISee, 0); }
		public TerminalNode IFind() { return getToken(AgentsGrammarParser.IFind, 0); }
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitCondition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_condition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69);
			_la = _input.LA(1);
			if ( !(_la==ISee || _la==IFind) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubjectContext extends ParserRuleContext {
		public TerminalNode Enemy() { return getToken(AgentsGrammarParser.Enemy, 0); }
		public TerminalNode Monster() { return getToken(AgentsGrammarParser.Monster, 0); }
		public TerminalNode Attribute() { return getToken(AgentsGrammarParser.Attribute, 0); }
		public TerminalNode Weapon() { return getToken(AgentsGrammarParser.Weapon, 0); }
		public SubjectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subject; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterSubject(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitSubject(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitSubject(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SubjectContext subject() throws RecognitionException {
		SubjectContext _localctx = new SubjectContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_subject);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Enemy) | (1L << Monster) | (1L << Attribute) | (1L << Weapon))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ActionContext extends ParserRuleContext {
		public TerminalNode Run() { return getToken(AgentsGrammarParser.Run, 0); }
		public TerminalNode Fight() { return getToken(AgentsGrammarParser.Fight, 0); }
		public TerminalNode Pickup() { return getToken(AgentsGrammarParser.Pickup, 0); }
		public TerminalNode Flee() { return getToken(AgentsGrammarParser.Flee, 0); }
		public TerminalNode Replace() { return getToken(AgentsGrammarParser.Replace, 0); }
		public ActionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_action; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterAction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitAction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitAction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ActionContext action() throws RecognitionException {
		ActionContext _localctx = new ActionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_action);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Run) | (1L << Fight) | (1L << Flee) | (1L << Pickup) | (1L << Replace))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfClauseContext extends ParserRuleContext {
		public TerminalNode If() { return getToken(AgentsGrammarParser.If, 0); }
		public IfConditionContext ifCondition() {
			return getRuleContext(IfConditionContext.class,0);
		}
		public List<TerminalNode> Comma() { return getTokens(AgentsGrammarParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(AgentsGrammarParser.Comma, i);
		}
		public TerminalNode Otherwise() { return getToken(AgentsGrammarParser.Otherwise, 0); }
		public ActionContext action() {
			return getRuleContext(ActionContext.class,0);
		}
		public IfClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterIfClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitIfClause(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitIfClause(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfClauseContext ifClause() throws RecognitionException {
		IfClauseContext _localctx = new IfClauseContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_ifClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(75);
			match(If);
			setState(76);
			ifCondition();
			setState(81);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==Comma) {
				{
				setState(77);
				match(Comma);
				setState(78);
				match(Otherwise);
				setState(79);
				match(Comma);
				setState(80);
				action();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfConditionContext extends ParserRuleContext {
		public TerminalNode LessStrength() { return getToken(AgentsGrammarParser.LessStrength, 0); }
		public TerminalNode MoreStrength() { return getToken(AgentsGrammarParser.MoreStrength, 0); }
		public TerminalNode IHave() { return getToken(AgentsGrammarParser.IHave, 0); }
		public TerminalNode CurrentWeaponLowerStrength() { return getToken(AgentsGrammarParser.CurrentWeaponLowerStrength, 0); }
		public TerminalNode CurrentWeaponHigherStrength() { return getToken(AgentsGrammarParser.CurrentWeaponHigherStrength, 0); }
		public StrengthComparisonContext strengthComparison() {
			return getRuleContext(StrengthComparisonContext.class,0);
		}
		public TerminalNode Enemy() { return getToken(AgentsGrammarParser.Enemy, 0); }
		public TerminalNode Monster() { return getToken(AgentsGrammarParser.Monster, 0); }
		public IfConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifCondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterIfCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitIfCondition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitIfCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfConditionContext ifCondition() throws RecognitionException {
		IfConditionContext _localctx = new IfConditionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_ifCondition);
		int _la;
		try {
			setState(93);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LessStrength:
			case MoreStrength:
			case IHave:
				enterOuterAlt(_localctx, 1);
				{
				setState(84);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==IHave) {
					{
					setState(83);
					match(IHave);
					}
				}

				setState(86);
				_la = _input.LA(1);
				if ( !(_la==LessStrength || _la==MoreStrength) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case CurrentWeaponLowerStrength:
				enterOuterAlt(_localctx, 2);
				{
				setState(87);
				match(CurrentWeaponLowerStrength);
				}
				break;
			case CurrentWeaponHigherStrength:
				enterOuterAlt(_localctx, 3);
				{
				setState(88);
				match(CurrentWeaponHigherStrength);
				}
				break;
			case Enemy:
			case Monster:
			case Strength:
				enterOuterAlt(_localctx, 4);
				{
				setState(90);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Enemy || _la==Monster) {
					{
					setState(89);
					_la = _input.LA(1);
					if ( !(_la==Enemy || _la==Monster) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(92);
				strengthComparison();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PriorityRuleContext extends ParserRuleContext {
		public TerminalNode Set() { return getToken(AgentsGrammarParser.Set, 0); }
		public MainPriorityContext mainPriority() {
			return getRuleContext(MainPriorityContext.class,0);
		}
		public InventoryPriorityContext inventoryPriority() {
			return getRuleContext(InventoryPriorityContext.class,0);
		}
		public IfClauseContext ifClause() {
			return getRuleContext(IfClauseContext.class,0);
		}
		public PriorityRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_priorityRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterPriorityRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitPriorityRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitPriorityRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PriorityRuleContext priorityRule() throws RecognitionException {
		PriorityRuleContext _localctx = new PriorityRuleContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_priorityRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(95);
			match(Set);
			setState(98);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case Main:
				{
				setState(96);
				mainPriority();
				}
				break;
			case Inventory:
				{
				setState(97);
				inventoryPriority();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(101);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==If) {
				{
				setState(100);
				ifClause();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainPriorityContext extends ParserRuleContext {
		public TerminalNode Main() { return getToken(AgentsGrammarParser.Main, 0); }
		public TerminalNode Priority() { return getToken(AgentsGrammarParser.Priority, 0); }
		public List<MainPriorityOptionContext> mainPriorityOption() {
			return getRuleContexts(MainPriorityOptionContext.class);
		}
		public MainPriorityOptionContext mainPriorityOption(int i) {
			return getRuleContext(MainPriorityOptionContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(AgentsGrammarParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(AgentsGrammarParser.Comma, i);
		}
		public MainPriorityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mainPriority; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterMainPriority(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitMainPriority(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitMainPriority(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MainPriorityContext mainPriority() throws RecognitionException {
		MainPriorityContext _localctx = new MainPriorityContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_mainPriority);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(103);
			match(Main);
			setState(104);
			match(Priority);
			setState(109); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(106);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma) {
					{
					setState(105);
					match(Comma);
					}
				}

				setState(108);
				mainPriorityOption();
				}
				}
				setState(111); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PriorityDestroyFlag) | (1L << PriorityDefendFlag) | (1L << PriorityEnemies) | (1L << PriorityMonsters) | (1L << PriorityExplore) | (1L << Comma))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MainPriorityOptionContext extends ParserRuleContext {
		public TerminalNode PriorityDestroyFlag() { return getToken(AgentsGrammarParser.PriorityDestroyFlag, 0); }
		public TerminalNode PriorityDefendFlag() { return getToken(AgentsGrammarParser.PriorityDefendFlag, 0); }
		public TerminalNode PriorityEnemies() { return getToken(AgentsGrammarParser.PriorityEnemies, 0); }
		public TerminalNode PriorityMonsters() { return getToken(AgentsGrammarParser.PriorityMonsters, 0); }
		public TerminalNode PriorityExplore() { return getToken(AgentsGrammarParser.PriorityExplore, 0); }
		public MainPriorityOptionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mainPriorityOption; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterMainPriorityOption(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitMainPriorityOption(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitMainPriorityOption(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MainPriorityOptionContext mainPriorityOption() throws RecognitionException {
		MainPriorityOptionContext _localctx = new MainPriorityOptionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_mainPriorityOption);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(113);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PriorityDestroyFlag) | (1L << PriorityDefendFlag) | (1L << PriorityEnemies) | (1L << PriorityMonsters) | (1L << PriorityExplore))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InventoryPriorityContext extends ParserRuleContext {
		public TerminalNode Inventory() { return getToken(AgentsGrammarParser.Inventory, 0); }
		public TerminalNode Priority() { return getToken(AgentsGrammarParser.Priority, 0); }
		public List<InventoryPriotityOptionContext> inventoryPriotityOption() {
			return getRuleContexts(InventoryPriotityOptionContext.class);
		}
		public InventoryPriotityOptionContext inventoryPriotityOption(int i) {
			return getRuleContext(InventoryPriotityOptionContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(AgentsGrammarParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(AgentsGrammarParser.Comma, i);
		}
		public List<TerminalNode> And() { return getTokens(AgentsGrammarParser.And); }
		public TerminalNode And(int i) {
			return getToken(AgentsGrammarParser.And, i);
		}
		public InventoryPriorityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inventoryPriority; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterInventoryPriority(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitInventoryPriority(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitInventoryPriority(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InventoryPriorityContext inventoryPriority() throws RecognitionException {
		InventoryPriorityContext _localctx = new InventoryPriorityContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_inventoryPriority);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(115);
			match(Inventory);
			setState(116);
			match(Priority);
			setState(121); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(118);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==Comma || _la==And) {
					{
					setState(117);
					_la = _input.LA(1);
					if ( !(_la==Comma || _la==And) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
				}

				setState(120);
				inventoryPriotityOption();
				}
				}
				setState(123); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Comma) | (1L << And) | (1L << Number))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InventoryPriotityOptionContext extends ParserRuleContext {
		public TerminalNode Number() { return getToken(AgentsGrammarParser.Number, 0); }
		public TerminalNode Food() { return getToken(AgentsGrammarParser.Food, 0); }
		public TerminalNode Armor() { return getToken(AgentsGrammarParser.Armor, 0); }
		public TerminalNode Weapon() { return getToken(AgentsGrammarParser.Weapon, 0); }
		public InventoryPriotityOptionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inventoryPriotityOption; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterInventoryPriotityOption(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitInventoryPriotityOption(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitInventoryPriotityOption(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InventoryPriotityOptionContext inventoryPriotityOption() throws RecognitionException {
		InventoryPriotityOptionContext _localctx = new InventoryPriotityOptionContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_inventoryPriotityOption);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			match(Number);
			setState(126);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Weapon) | (1L << Armor) | (1L << Food))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleRuleContext extends ParserRuleContext {
		public SimpleFoodRuleContext simpleFoodRule() {
			return getRuleContext(SimpleFoodRuleContext.class,0);
		}
		public SimpleDropWeaponRuleContext simpleDropWeaponRule() {
			return getRuleContext(SimpleDropWeaponRuleContext.class,0);
		}
		public SimpleWeaponDefaultRuleContext simpleWeaponDefaultRule() {
			return getRuleContext(SimpleWeaponDefaultRuleContext.class,0);
		}
		public SimpleRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterSimpleRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitSimpleRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitSimpleRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleRuleContext simpleRule() throws RecognitionException {
		SimpleRuleContext _localctx = new SimpleRuleContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_simpleRule);
		try {
			setState(131);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(128);
				simpleFoodRule();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(129);
				simpleDropWeaponRule();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(130);
				simpleWeaponDefaultRule();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleFoodRuleContext extends ParserRuleContext {
		public TerminalNode Use() { return getToken(AgentsGrammarParser.Use, 0); }
		public FoodContext food() {
			return getRuleContext(FoodContext.class,0);
		}
		public TerminalNode If() { return getToken(AgentsGrammarParser.If, 0); }
		public StrengthComparisonContext strengthComparison() {
			return getRuleContext(StrengthComparisonContext.class,0);
		}
		public SimpleFoodRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleFoodRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterSimpleFoodRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitSimpleFoodRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitSimpleFoodRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleFoodRuleContext simpleFoodRule() throws RecognitionException {
		SimpleFoodRuleContext _localctx = new SimpleFoodRuleContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_simpleFoodRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(133);
			match(Use);
			setState(134);
			food();
			setState(135);
			match(If);
			setState(136);
			strengthComparison();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleDropWeaponRuleContext extends ParserRuleContext {
		public TerminalNode DropWeaponIfNotUsed() { return getToken(AgentsGrammarParser.DropWeaponIfNotUsed, 0); }
		public SimpleDropWeaponRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleDropWeaponRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterSimpleDropWeaponRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitSimpleDropWeaponRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitSimpleDropWeaponRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleDropWeaponRuleContext simpleDropWeaponRule() throws RecognitionException {
		SimpleDropWeaponRuleContext _localctx = new SimpleDropWeaponRuleContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_simpleDropWeaponRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(138);
			match(DropWeaponIfNotUsed);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleWeaponDefaultRuleContext extends ParserRuleContext {
		public TerminalNode Use() { return getToken(AgentsGrammarParser.Use, 0); }
		public TerminalNode WeaponAsDefault() { return getToken(AgentsGrammarParser.WeaponAsDefault, 0); }
		public TerminalNode Best() { return getToken(AgentsGrammarParser.Best, 0); }
		public TerminalNode Worst() { return getToken(AgentsGrammarParser.Worst, 0); }
		public TerminalNode Light() { return getToken(AgentsGrammarParser.Light, 0); }
		public TerminalNode Medium() { return getToken(AgentsGrammarParser.Medium, 0); }
		public TerminalNode Heavy() { return getToken(AgentsGrammarParser.Heavy, 0); }
		public SimpleWeaponDefaultRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleWeaponDefaultRule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterSimpleWeaponDefaultRule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitSimpleWeaponDefaultRule(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitSimpleWeaponDefaultRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SimpleWeaponDefaultRuleContext simpleWeaponDefaultRule() throws RecognitionException {
		SimpleWeaponDefaultRuleContext _localctx = new SimpleWeaponDefaultRuleContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_simpleWeaponDefaultRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140);
			match(Use);
			setState(141);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Best) | (1L << Worst) | (1L << Light) | (1L << Medium) | (1L << Heavy))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(142);
			match(WeaponAsDefault);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FoodContext extends ParserRuleContext {
		public TerminalNode Food() { return getToken(AgentsGrammarParser.Food, 0); }
		public TerminalNode Apple() { return getToken(AgentsGrammarParser.Apple, 0); }
		public TerminalNode Cookie() { return getToken(AgentsGrammarParser.Cookie, 0); }
		public TerminalNode Soup() { return getToken(AgentsGrammarParser.Soup, 0); }
		public FoodContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_food; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterFood(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitFood(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitFood(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FoodContext food() throws RecognitionException {
		FoodContext _localctx = new FoodContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_food);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(144);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << Food) | (1L << Apple) | (1L << Cookie) | (1L << Soup))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StrengthComparisonContext extends ParserRuleContext {
		public TerminalNode Strength() { return getToken(AgentsGrammarParser.Strength, 0); }
		public TerminalNode Number() { return getToken(AgentsGrammarParser.Number, 0); }
		public TerminalNode LessThan() { return getToken(AgentsGrammarParser.LessThan, 0); }
		public TerminalNode GreaterThan() { return getToken(AgentsGrammarParser.GreaterThan, 0); }
		public StrengthComparisonContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_strengthComparison; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).enterStrengthComparison(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AgentsGrammarListener ) ((AgentsGrammarListener)listener).exitStrengthComparison(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AgentsGrammarVisitor ) return ((AgentsGrammarVisitor<? extends T>)visitor).visitStrengthComparison(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StrengthComparisonContext strengthComparison() throws RecognitionException {
		StrengthComparisonContext _localctx = new StrengthComparisonContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_strengthComparison);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(146);
			match(Strength);
			setState(147);
			_la = _input.LA(1);
			if ( !(_la==LessThan || _la==GreaterThan) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(148);
			match(Number);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\63\u0099\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\3\2\3\2\3\2\5\2*\n\2\3\2\3\2\6\2.\n\2\r\2\16\2/\3\3\3\3\3\3"+
		"\3\3\5\3\66\n\3\3\3\3\3\5\3:\n\3\3\3\3\3\3\3\3\3\3\3\5\3A\n\3\3\3\5\3"+
		"D\n\3\5\3F\n\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\5\7T\n"+
		"\7\3\b\5\bW\n\b\3\b\3\b\3\b\3\b\5\b]\n\b\3\b\5\b`\n\b\3\t\3\t\3\t\5\t"+
		"e\n\t\3\t\5\th\n\t\3\n\3\n\3\n\5\nm\n\n\3\n\6\np\n\n\r\n\16\nq\3\13\3"+
		"\13\3\f\3\f\3\f\5\fy\n\f\3\f\6\f|\n\f\r\f\16\f}\3\r\3\r\3\r\3\16\3\16"+
		"\3\16\5\16\u0086\n\16\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\21\3\21\3\21"+
		"\3\21\3\22\3\22\3\23\3\23\3\23\3\23\3\23\2\2\24\2\4\6\b\n\f\16\20\22\24"+
		"\26\30\32\34\36 \"$\2\r\3\2\4\5\3\2\13\16\3\2\17\23\3\2\6\7\3\2\13\f\3"+
		"\2\27\33\3\2,-\5\2\16\16\37\37\'\'\3\2\"&\3\2\'*\3\2/\60\2\u009c\2-\3"+
		"\2\2\2\4E\3\2\2\2\6G\3\2\2\2\bI\3\2\2\2\nK\3\2\2\2\fM\3\2\2\2\16_\3\2"+
		"\2\2\20a\3\2\2\2\22i\3\2\2\2\24s\3\2\2\2\26u\3\2\2\2\30\177\3\2\2\2\32"+
		"\u0085\3\2\2\2\34\u0087\3\2\2\2\36\u008c\3\2\2\2 \u008e\3\2\2\2\"\u0092"+
		"\3\2\2\2$\u0094\3\2\2\2&*\5\4\3\2\'*\5\20\t\2(*\5\32\16\2)&\3\2\2\2)\'"+
		"\3\2\2\2)(\3\2\2\2*+\3\2\2\2+,\7\61\2\2,.\3\2\2\2-)\3\2\2\2./\3\2\2\2"+
		"/-\3\2\2\2/\60\3\2\2\2\60\3\3\2\2\2\61\62\7\3\2\2\62\63\5\6\4\2\63\65"+
		"\5\b\5\2\64\66\7,\2\2\65\64\3\2\2\2\65\66\3\2\2\2\66\67\3\2\2\2\679\5"+
		"\n\6\28:\5\f\7\298\3\2\2\29:\3\2\2\2:F\3\2\2\2;<\5\n\6\2<=\7\3\2\2=>\5"+
		"\6\4\2>@\5\b\5\2?A\7,\2\2@?\3\2\2\2@A\3\2\2\2AC\3\2\2\2BD\5\f\7\2CB\3"+
		"\2\2\2CD\3\2\2\2DF\3\2\2\2E\61\3\2\2\2E;\3\2\2\2F\5\3\2\2\2GH\t\2\2\2"+
		"H\7\3\2\2\2IJ\t\3\2\2J\t\3\2\2\2KL\t\4\2\2L\13\3\2\2\2MN\7\24\2\2NS\5"+
		"\16\b\2OP\7,\2\2PQ\7\25\2\2QR\7,\2\2RT\5\n\6\2SO\3\2\2\2ST\3\2\2\2T\r"+
		"\3\2\2\2UW\7\b\2\2VU\3\2\2\2VW\3\2\2\2WX\3\2\2\2X`\t\5\2\2Y`\7\n\2\2Z"+
		"`\7\t\2\2[]\t\6\2\2\\[\3\2\2\2\\]\3\2\2\2]^\3\2\2\2^`\5$\23\2_V\3\2\2"+
		"\2_Y\3\2\2\2_Z\3\2\2\2_\\\3\2\2\2`\17\3\2\2\2ad\7\34\2\2be\5\22\n\2ce"+
		"\5\26\f\2db\3\2\2\2dc\3\2\2\2eg\3\2\2\2fh\5\f\7\2gf\3\2\2\2gh\3\2\2\2"+
		"h\21\3\2\2\2ij\7\35\2\2jo\7\26\2\2km\7,\2\2lk\3\2\2\2lm\3\2\2\2mn\3\2"+
		"\2\2np\5\24\13\2ol\3\2\2\2pq\3\2\2\2qo\3\2\2\2qr\3\2\2\2r\23\3\2\2\2s"+
		"t\t\7\2\2t\25\3\2\2\2uv\7\36\2\2v{\7\26\2\2wy\t\b\2\2xw\3\2\2\2xy\3\2"+
		"\2\2yz\3\2\2\2z|\5\30\r\2{x\3\2\2\2|}\3\2\2\2}{\3\2\2\2}~\3\2\2\2~\27"+
		"\3\2\2\2\177\u0080\7\63\2\2\u0080\u0081\t\t\2\2\u0081\31\3\2\2\2\u0082"+
		"\u0086\5\34\17\2\u0083\u0086\5\36\20\2\u0084\u0086\5 \21\2\u0085\u0082"+
		"\3\2\2\2\u0085\u0083\3\2\2\2\u0085\u0084\3\2\2\2\u0086\33\3\2\2\2\u0087"+
		"\u0088\7.\2\2\u0088\u0089\5\"\22\2\u0089\u008a\7\24\2\2\u008a\u008b\5"+
		"$\23\2\u008b\35\3\2\2\2\u008c\u008d\7 \2\2\u008d\37\3\2\2\2\u008e\u008f"+
		"\7.\2\2\u008f\u0090\t\n\2\2\u0090\u0091\7!\2\2\u0091!\3\2\2\2\u0092\u0093"+
		"\t\13\2\2\u0093#\3\2\2\2\u0094\u0095\7+\2\2\u0095\u0096\t\f\2\2\u0096"+
		"\u0097\7\63\2\2\u0097%\3\2\2\2\24)/\659@CESV\\_dglqx}\u0085";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}