// Generated from D:/GooGle Drive/Jaar 3 ASD-Project/Project repos/the-desolated-tunnels-game/src/main/antlr/nl/aimsites/nzbvuq/game/agentscriptconverter\AgentsGrammar.g4 by ANTLR 4.9
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class AgentsGrammarLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.9", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		When=1, ISee=2, IFind=3, LessStrength=4, MoreStrength=5, IHave=6, CurrentWeaponHigherStrength=7, 
		CurrentWeaponLowerStrength=8, Enemy=9, Monster=10, Attribute=11, Weapon=12, 
		Run=13, Fight=14, Flee=15, Pickup=16, Replace=17, If=18, Otherwise=19, 
		Priority=20, PriorityDestroyFlag=21, PriorityDefendFlag=22, PriorityEnemies=23, 
		PriorityMonsters=24, PriorityExplore=25, Set=26, Main=27, Inventory=28, 
		Armor=29, DropWeaponIfNotUsed=30, WeaponAsDefault=31, Best=32, Worst=33, 
		Light=34, Medium=35, Heavy=36, Food=37, Apple=38, Cookie=39, Soup=40, 
		Strength=41, Comma=42, And=43, Use=44, LessThan=45, GreaterThan=46, Dot=47, 
		WS=48, Number=49;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"When", "ISee", "IFind", "LessStrength", "MoreStrength", "IHave", "CurrentWeaponHigherStrength", 
			"CurrentWeaponLowerStrength", "Enemy", "Monster", "Attribute", "Weapon", 
			"Run", "Fight", "Flee", "Pickup", "Replace", "If", "Otherwise", "Priority", 
			"PriorityDestroyFlag", "PriorityDefendFlag", "PriorityEnemies", "PriorityMonsters", 
			"PriorityExplore", "Set", "Main", "Inventory", "Armor", "DropWeaponIfNotUsed", 
			"WeaponAsDefault", "Best", "Worst", "Light", "Medium", "Heavy", "Food", 
			"Apple", "Cookie", "Soup", "Strength", "Comma", "And", "Use", "LessThan", 
			"GreaterThan", "Dot", "WS", "Number"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, "'less strength'", "'more strength'", null, "'current weapon has higher strength'", 
			"'current weapon has lower strength'", null, null, null, null, "'run'", 
			"'fight'", "'flee'", null, "'replace'", "'if'", "'otherwise'", null, 
			"'destroy flag'", "'defend flag'", null, null, "'explore'", null, "'main'", 
			"'inventory'", "'armor'", null, "'weapon as default'", "'best'", "'worst'", 
			"'light'", "'medium'", "'heavy'", "'food'", null, null, null, null, "','", 
			"'and'", null, null, null, "'.'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "When", "ISee", "IFind", "LessStrength", "MoreStrength", "IHave", 
			"CurrentWeaponHigherStrength", "CurrentWeaponLowerStrength", "Enemy", 
			"Monster", "Attribute", "Weapon", "Run", "Fight", "Flee", "Pickup", "Replace", 
			"If", "Otherwise", "Priority", "PriorityDestroyFlag", "PriorityDefendFlag", 
			"PriorityEnemies", "PriorityMonsters", "PriorityExplore", "Set", "Main", 
			"Inventory", "Armor", "DropWeaponIfNotUsed", "WeaponAsDefault", "Best", 
			"Worst", "Light", "Medium", "Heavy", "Food", "Apple", "Cookie", "Soup", 
			"Strength", "Comma", "And", "Use", "LessThan", "GreaterThan", "Dot", 
			"WS", "Number"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public AgentsGrammarLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "AgentsGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\63\u02f7\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t"+
		"+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\5\2n\n\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\5\3\u0080\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4\u0095\n\4\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u00c5\n\7\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\5\n\u0119\n\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u012b\n\13\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\5\f\u0142\n\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\5\r\u0152\n\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u0174\n\21\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24"+
		"\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u01a1"+
		"\n\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30"+
		"\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30"+
		"\3\30\3\30\3\30\3\30\3\30\3\30\5\30\u01d2\n\30\3\31\3\31\3\31\3\31\3\31"+
		"\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31"+
		"\3\31\3\31\3\31\3\31\3\31\3\31\5\31\u01ed\n\31\3\32\3\32\3\32\3\32\3\32"+
		"\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33\5\33\u01fd\n\33\3\34\3\34"+
		"\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\36"+
		"\3\36\3\36\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37"+
		"\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\5\37\u0242\n\37\3 \3 \3"+
		" \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3 \3!\3!\3!\3!\3!\3\"\3\""+
		"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3$\3$\3%\3%\3%\3%\3"+
		"%\3%\3&\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\5\'\u0283"+
		"\n\'\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\3(\5(\u0291\n(\3)\3)\3)\3)\3)\3"+
		")\3)\3)\5)\u029b\n)\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\3*\5"+
		"*\u02ad\n*\3+\3+\3,\3,\3,\3,\3-\3-\3-\3-\3-\3-\5-\u02bb\n-\3.\3.\3.\3"+
		".\3.\3.\3.\3.\3.\3.\3.\3.\3.\3.\3.\5.\u02cc\n.\3/\3/\3/\3/\3/\3/\3/\3"+
		"/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\3/\5/\u02e8\n/\3"+
		"\60\3\60\3\61\6\61\u02ed\n\61\r\61\16\61\u02ee\3\61\3\61\3\62\6\62\u02f4"+
		"\n\62\r\62\16\62\u02f5\2\2\63\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13"+
		"\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61"+
		"\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61"+
		"a\62c\63\3\2\4\5\2\13\f\17\17\"\"\3\2\62;\2\u0314\2\3\3\2\2\2\2\5\3\2"+
		"\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21"+
		"\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2"+
		"\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3"+
		"\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3"+
		"\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3"+
		"\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2"+
		"\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2"+
		"Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\3m\3"+
		"\2\2\2\5\177\3\2\2\2\7\u0094\3\2\2\2\t\u0096\3\2\2\2\13\u00a4\3\2\2\2"+
		"\r\u00c4\3\2\2\2\17\u00c6\3\2\2\2\21\u00e9\3\2\2\2\23\u0118\3\2\2\2\25"+
		"\u012a\3\2\2\2\27\u0141\3\2\2\2\31\u0151\3\2\2\2\33\u0153\3\2\2\2\35\u0157"+
		"\3\2\2\2\37\u015d\3\2\2\2!\u0173\3\2\2\2#\u0175\3\2\2\2%\u017d\3\2\2\2"+
		"\'\u0180\3\2\2\2)\u01a0\3\2\2\2+\u01a2\3\2\2\2-\u01af\3\2\2\2/\u01d1\3"+
		"\2\2\2\61\u01ec\3\2\2\2\63\u01ee\3\2\2\2\65\u01fc\3\2\2\2\67\u01fe\3\2"+
		"\2\29\u0203\3\2\2\2;\u020d\3\2\2\2=\u0241\3\2\2\2?\u0243\3\2\2\2A\u0255"+
		"\3\2\2\2C\u025a\3\2\2\2E\u0260\3\2\2\2G\u0266\3\2\2\2I\u026d\3\2\2\2K"+
		"\u0273\3\2\2\2M\u0282\3\2\2\2O\u0290\3\2\2\2Q\u029a\3\2\2\2S\u02ac\3\2"+
		"\2\2U\u02ae\3\2\2\2W\u02b0\3\2\2\2Y\u02ba\3\2\2\2[\u02cb\3\2\2\2]\u02e7"+
		"\3\2\2\2_\u02e9\3\2\2\2a\u02ec\3\2\2\2c\u02f3\3\2\2\2ef\7Y\2\2fg\7j\2"+
		"\2gh\7g\2\2hn\7p\2\2ij\7y\2\2jk\7j\2\2kl\7g\2\2ln\7p\2\2me\3\2\2\2mi\3"+
		"\2\2\2n\4\3\2\2\2op\7K\2\2pq\7\"\2\2qr\7u\2\2rs\7g\2\2s\u0080\7g\2\2t"+
		"u\7k\2\2uv\7\"\2\2vw\7u\2\2wx\7g\2\2x\u0080\7g\2\2yz\7u\2\2z{\7g\2\2{"+
		"|\7g\2\2|}\7k\2\2}~\7p\2\2~\u0080\7i\2\2\177o\3\2\2\2\177t\3\2\2\2\177"+
		"y\3\2\2\2\u0080\6\3\2\2\2\u0081\u0082\7K\2\2\u0082\u0083\7\"\2\2\u0083"+
		"\u0084\7h\2\2\u0084\u0085\7k\2\2\u0085\u0086\7p\2\2\u0086\u0095\7f\2\2"+
		"\u0087\u0088\7k\2\2\u0088\u0089\7\"\2\2\u0089\u008a\7h\2\2\u008a\u008b"+
		"\7k\2\2\u008b\u008c\7p\2\2\u008c\u0095\7f\2\2\u008d\u008e\7h\2\2\u008e"+
		"\u008f\7k\2\2\u008f\u0090\7p\2\2\u0090\u0091\7f\2\2\u0091\u0092\7k\2\2"+
		"\u0092\u0093\7p\2\2\u0093\u0095\7i\2\2\u0094\u0081\3\2\2\2\u0094\u0087"+
		"\3\2\2\2\u0094\u008d\3\2\2\2\u0095\b\3\2\2\2\u0096\u0097\7n\2\2\u0097"+
		"\u0098\7g\2\2\u0098\u0099\7u\2\2\u0099\u009a\7u\2\2\u009a\u009b\7\"\2"+
		"\2\u009b\u009c\7u\2\2\u009c\u009d\7v\2\2\u009d\u009e\7t\2\2\u009e\u009f"+
		"\7g\2\2\u009f\u00a0\7p\2\2\u00a0\u00a1\7i\2\2\u00a1\u00a2\7v\2\2\u00a2"+
		"\u00a3\7j\2\2\u00a3\n\3\2\2\2\u00a4\u00a5\7o\2\2\u00a5\u00a6\7q\2\2\u00a6"+
		"\u00a7\7t\2\2\u00a7\u00a8\7g\2\2\u00a8\u00a9\7\"\2\2\u00a9\u00aa\7u\2"+
		"\2\u00aa\u00ab\7v\2\2\u00ab\u00ac\7t\2\2\u00ac\u00ad\7g\2\2\u00ad\u00ae"+
		"\7p\2\2\u00ae\u00af\7i\2\2\u00af\u00b0\7v\2\2\u00b0\u00b1\7j\2\2\u00b1"+
		"\f\3\2\2\2\u00b2\u00b3\7k\2\2\u00b3\u00b4\7\"\2\2\u00b4\u00b5\7j\2\2\u00b5"+
		"\u00b6\7c\2\2\u00b6\u00b7\7x\2\2\u00b7\u00c5\7g\2\2\u00b8\u00b9\7K\2\2"+
		"\u00b9\u00ba\7\"\2\2\u00ba\u00bb\7j\2\2\u00bb\u00bc\7c\2\2\u00bc\u00bd"+
		"\7x\2\2\u00bd\u00c5\7g\2\2\u00be\u00bf\7j\2\2\u00bf\u00c0\7c\2\2\u00c0"+
		"\u00c1\7x\2\2\u00c1\u00c2\7k\2\2\u00c2\u00c3\7p\2\2\u00c3\u00c5\7i\2\2"+
		"\u00c4\u00b2\3\2\2\2\u00c4\u00b8\3\2\2\2\u00c4\u00be\3\2\2\2\u00c5\16"+
		"\3\2\2\2\u00c6\u00c7\7e\2\2\u00c7\u00c8\7w\2\2\u00c8\u00c9\7t\2\2\u00c9"+
		"\u00ca\7t\2\2\u00ca\u00cb\7g\2\2\u00cb\u00cc\7p\2\2\u00cc\u00cd\7v\2\2"+
		"\u00cd\u00ce\7\"\2\2\u00ce\u00cf\7y\2\2\u00cf\u00d0\7g\2\2\u00d0\u00d1"+
		"\7c\2\2\u00d1\u00d2\7r\2\2\u00d2\u00d3\7q\2\2\u00d3\u00d4\7p\2\2\u00d4"+
		"\u00d5\7\"\2\2\u00d5\u00d6\7j\2\2\u00d6\u00d7\7c\2\2\u00d7\u00d8\7u\2"+
		"\2\u00d8\u00d9\7\"\2\2\u00d9\u00da\7j\2\2\u00da\u00db\7k\2\2\u00db\u00dc"+
		"\7i\2\2\u00dc\u00dd\7j\2\2\u00dd\u00de\7g\2\2\u00de\u00df\7t\2\2\u00df"+
		"\u00e0\7\"\2\2\u00e0\u00e1\7u\2\2\u00e1\u00e2\7v\2\2\u00e2\u00e3\7t\2"+
		"\2\u00e3\u00e4\7g\2\2\u00e4\u00e5\7p\2\2\u00e5\u00e6\7i\2\2\u00e6\u00e7"+
		"\7v\2\2\u00e7\u00e8\7j\2\2\u00e8\20\3\2\2\2\u00e9\u00ea\7e\2\2\u00ea\u00eb"+
		"\7w\2\2\u00eb\u00ec\7t\2\2\u00ec\u00ed\7t\2\2\u00ed\u00ee\7g\2\2\u00ee"+
		"\u00ef\7p\2\2\u00ef\u00f0\7v\2\2\u00f0\u00f1\7\"\2\2\u00f1\u00f2\7y\2"+
		"\2\u00f2\u00f3\7g\2\2\u00f3\u00f4\7c\2\2\u00f4\u00f5\7r\2\2\u00f5\u00f6"+
		"\7q\2\2\u00f6\u00f7\7p\2\2\u00f7\u00f8\7\"\2\2\u00f8\u00f9\7j\2\2\u00f9"+
		"\u00fa\7c\2\2\u00fa\u00fb\7u\2\2\u00fb\u00fc\7\"\2\2\u00fc\u00fd\7n\2"+
		"\2\u00fd\u00fe\7q\2\2\u00fe\u00ff\7y\2\2\u00ff\u0100\7g\2\2\u0100\u0101"+
		"\7t\2\2\u0101\u0102\7\"\2\2\u0102\u0103\7u\2\2\u0103\u0104\7v\2\2\u0104"+
		"\u0105\7t\2\2\u0105\u0106\7g\2\2\u0106\u0107\7p\2\2\u0107\u0108\7i\2\2"+
		"\u0108\u0109\7v\2\2\u0109\u010a\7j\2\2\u010a\22\3\2\2\2\u010b\u010c\7"+
		"c\2\2\u010c\u010d\7p\2\2\u010d\u010e\7\"\2\2\u010e\u010f\7g\2\2\u010f"+
		"\u0110\7p\2\2\u0110\u0111\7g\2\2\u0111\u0112\7o\2\2\u0112\u0119\7{\2\2"+
		"\u0113\u0114\7g\2\2\u0114\u0115\7p\2\2\u0115\u0116\7g\2\2\u0116\u0117"+
		"\7o\2\2\u0117\u0119\7{\2\2\u0118\u010b\3\2\2\2\u0118\u0113\3\2\2\2\u0119"+
		"\24\3\2\2\2\u011a\u011b\7c\2\2\u011b\u011c\7\"\2\2\u011c\u011d\7o\2\2"+
		"\u011d\u011e\7q\2\2\u011e\u011f\7p\2\2\u011f\u0120\7u\2\2\u0120\u0121"+
		"\7v\2\2\u0121\u0122\7g\2\2\u0122\u012b\7t\2\2\u0123\u0124\7o\2\2\u0124"+
		"\u0125\7q\2\2\u0125\u0126\7p\2\2\u0126\u0127\7u\2\2\u0127\u0128\7v\2\2"+
		"\u0128\u0129\7g\2\2\u0129\u012b\7t\2\2\u012a\u011a\3\2\2\2\u012a\u0123"+
		"\3\2\2\2\u012b\26\3\2\2\2\u012c\u012d\7c\2\2\u012d\u012e\7p\2\2\u012e"+
		"\u012f\7\"\2\2\u012f\u0130\7c\2\2\u0130\u0131\7v\2\2\u0131\u0132\7v\2"+
		"\2\u0132\u0133\7t\2\2\u0133\u0134\7k\2\2\u0134\u0135\7d\2\2\u0135\u0136"+
		"\7w\2\2\u0136\u0137\7v\2\2\u0137\u0142\7g\2\2\u0138\u0139\7c\2\2\u0139"+
		"\u013a\7v\2\2\u013a\u013b\7v\2\2\u013b\u013c\7t\2\2\u013c\u013d\7k\2\2"+
		"\u013d\u013e\7d\2\2\u013e\u013f\7w\2\2\u013f\u0140\7v\2\2\u0140\u0142"+
		"\7g\2\2\u0141\u012c\3\2\2\2\u0141\u0138\3\2\2\2\u0142\30\3\2\2\2\u0143"+
		"\u0144\7c\2\2\u0144\u0145\7\"\2\2\u0145\u0146\7y\2\2\u0146\u0147\7g\2"+
		"\2\u0147\u0148\7c\2\2\u0148\u0149\7r\2\2\u0149\u014a\7q\2\2\u014a\u0152"+
		"\7p\2\2\u014b\u014c\7y\2\2\u014c\u014d\7g\2\2\u014d\u014e\7c\2\2\u014e"+
		"\u014f\7r\2\2\u014f\u0150\7q\2\2\u0150\u0152\7p\2\2\u0151\u0143\3\2\2"+
		"\2\u0151\u014b\3\2\2\2\u0152\32\3\2\2\2\u0153\u0154\7t\2\2\u0154\u0155"+
		"\7w\2\2\u0155\u0156\7p\2\2\u0156\34\3\2\2\2\u0157\u0158\7h\2\2\u0158\u0159"+
		"\7k\2\2\u0159\u015a\7i\2\2\u015a\u015b\7j\2\2\u015b\u015c\7v\2\2\u015c"+
		"\36\3\2\2\2\u015d\u015e\7h\2\2\u015e\u015f\7n\2\2\u015f\u0160\7g\2\2\u0160"+
		"\u0161\7g\2\2\u0161 \3\2\2\2\u0162\u0163\7r\2\2\u0163\u0164\7k\2\2\u0164"+
		"\u0165\7e\2\2\u0165\u0166\7m\2\2\u0166\u0167\7\"\2\2\u0167\u0168\7k\2"+
		"\2\u0168\u0169\7v\2\2\u0169\u016a\7\"\2\2\u016a\u016b\7w\2\2\u016b\u0174"+
		"\7r\2\2\u016c\u016d\7r\2\2\u016d\u016e\7k\2\2\u016e\u016f\7e\2\2\u016f"+
		"\u0170\7m\2\2\u0170\u0171\7\"\2\2\u0171\u0172\7w\2\2\u0172\u0174\7r\2"+
		"\2\u0173\u0162\3\2\2\2\u0173\u016c\3\2\2\2\u0174\"\3\2\2\2\u0175\u0176"+
		"\7t\2\2\u0176\u0177\7g\2\2\u0177\u0178\7r\2\2\u0178\u0179\7n\2\2\u0179"+
		"\u017a\7c\2\2\u017a\u017b\7e\2\2\u017b\u017c\7g\2\2\u017c$\3\2\2\2\u017d"+
		"\u017e\7k\2\2\u017e\u017f\7h\2\2\u017f&\3\2\2\2\u0180\u0181\7q\2\2\u0181"+
		"\u0182\7v\2\2\u0182\u0183\7j\2\2\u0183\u0184\7g\2\2\u0184\u0185\7t\2\2"+
		"\u0185\u0186\7y\2\2\u0186\u0187\7k\2\2\u0187\u0188\7u\2\2\u0188\u0189"+
		"\7g\2\2\u0189(\3\2\2\2\u018a\u018b\7r\2\2\u018b\u018c\7t\2\2\u018c\u018d"+
		"\7k\2\2\u018d\u018e\7q\2\2\u018e\u018f\7t\2\2\u018f\u0190\7k\2\2\u0190"+
		"\u0191\7v\2\2\u0191\u0192\7k\2\2\u0192\u0193\7g\2\2\u0193\u01a1\7u\2\2"+
		"\u0194\u0195\7r\2\2\u0195\u0196\7t\2\2\u0196\u0197\7k\2\2\u0197\u0198"+
		"\7q\2\2\u0198\u0199\7t\2\2\u0199\u019a\7k\2\2\u019a\u019b\7v\2\2\u019b"+
		"\u01a1\7{\2\2\u019c\u019d\7r\2\2\u019d\u019e\7t\2\2\u019e\u019f\7k\2\2"+
		"\u019f\u01a1\7q\2\2\u01a0\u018a\3\2\2\2\u01a0\u0194\3\2\2\2\u01a0\u019c"+
		"\3\2\2\2\u01a1*\3\2\2\2\u01a2\u01a3\7f\2\2\u01a3\u01a4\7g\2\2\u01a4\u01a5"+
		"\7u\2\2\u01a5\u01a6\7v\2\2\u01a6\u01a7\7t\2\2\u01a7\u01a8\7q\2\2\u01a8"+
		"\u01a9\7{\2\2\u01a9\u01aa\7\"\2\2\u01aa\u01ab\7h\2\2\u01ab\u01ac\7n\2"+
		"\2\u01ac\u01ad\7c\2\2\u01ad\u01ae\7i\2\2\u01ae,\3\2\2\2\u01af\u01b0\7"+
		"f\2\2\u01b0\u01b1\7g\2\2\u01b1\u01b2\7h\2\2\u01b2\u01b3\7g\2\2\u01b3\u01b4"+
		"\7p\2\2\u01b4\u01b5\7f\2\2\u01b5\u01b6\7\"\2\2\u01b6\u01b7\7h\2\2\u01b7"+
		"\u01b8\7n\2\2\u01b8\u01b9\7c\2\2\u01b9\u01ba\7i\2\2\u01ba.\3\2\2\2\u01bb"+
		"\u01bc\7m\2\2\u01bc\u01bd\7k\2\2\u01bd\u01be\7n\2\2\u01be\u01bf\7n\2\2"+
		"\u01bf\u01c0\7\"\2\2\u01c0\u01c1\7g\2\2\u01c1\u01c2\7p\2\2\u01c2\u01c3"+
		"\7g\2\2\u01c3\u01c4\7o\2\2\u01c4\u01c5\7k\2\2\u01c5\u01c6\7g\2\2\u01c6"+
		"\u01d2\7u\2\2\u01c7\u01c8\7m\2\2\u01c8\u01c9\7k\2\2\u01c9\u01ca\7n\2\2"+
		"\u01ca\u01cb\7n\2\2\u01cb\u01cc\7\"\2\2\u01cc\u01cd\7g\2\2\u01cd\u01ce"+
		"\7p\2\2\u01ce\u01cf\7g\2\2\u01cf\u01d0\7o\2\2\u01d0\u01d2\7{\2\2\u01d1"+
		"\u01bb\3\2\2\2\u01d1\u01c7\3\2\2\2\u01d2\60\3\2\2\2\u01d3\u01d4\7m\2\2"+
		"\u01d4\u01d5\7k\2\2\u01d5\u01d6\7n\2\2\u01d6\u01d7\7n\2\2\u01d7\u01d8"+
		"\7\"\2\2\u01d8\u01d9\7o\2\2\u01d9\u01da\7q\2\2\u01da\u01db\7p\2\2\u01db"+
		"\u01dc\7u\2\2\u01dc\u01dd\7v\2\2\u01dd\u01de\7g\2\2\u01de\u01df\7t\2\2"+
		"\u01df\u01ed\7u\2\2\u01e0\u01e1\7m\2\2\u01e1\u01e2\7k\2\2\u01e2\u01e3"+
		"\7n\2\2\u01e3\u01e4\7n\2\2\u01e4\u01e5\7\"\2\2\u01e5\u01e6\7o\2\2\u01e6"+
		"\u01e7\7q\2\2\u01e7\u01e8\7p\2\2\u01e8\u01e9\7u\2\2\u01e9\u01ea\7v\2\2"+
		"\u01ea\u01eb\7g\2\2\u01eb\u01ed\7t\2\2\u01ec\u01d3\3\2\2\2\u01ec\u01e0"+
		"\3\2\2\2\u01ed\62\3\2\2\2\u01ee\u01ef\7g\2\2\u01ef\u01f0\7z\2\2\u01f0"+
		"\u01f1\7r\2\2\u01f1\u01f2\7n\2\2\u01f2\u01f3\7q\2\2\u01f3\u01f4\7t\2\2"+
		"\u01f4\u01f5\7g\2\2\u01f5\64\3\2\2\2\u01f6\u01f7\7U\2\2\u01f7\u01f8\7"+
		"g\2\2\u01f8\u01fd\7v\2\2\u01f9\u01fa\7u\2\2\u01fa\u01fb\7g\2\2\u01fb\u01fd"+
		"\7v\2\2\u01fc\u01f6\3\2\2\2\u01fc\u01f9\3\2\2\2\u01fd\66\3\2\2\2\u01fe"+
		"\u01ff\7o\2\2\u01ff\u0200\7c\2\2\u0200\u0201\7k\2\2\u0201\u0202\7p\2\2"+
		"\u02028\3\2\2\2\u0203\u0204\7k\2\2\u0204\u0205\7p\2\2\u0205\u0206\7x\2"+
		"\2\u0206\u0207\7g\2\2\u0207\u0208\7p\2\2\u0208\u0209\7v\2\2\u0209\u020a"+
		"\7q\2\2\u020a\u020b\7t\2\2\u020b\u020c\7{\2\2\u020c:\3\2\2\2\u020d\u020e"+
		"\7c\2\2\u020e\u020f\7t\2\2\u020f\u0210\7o\2\2\u0210\u0211\7q\2\2\u0211"+
		"\u0212\7t\2\2\u0212<\3\2\2\2\u0213\u0214\7F\2\2\u0214\u0215\7t\2\2\u0215"+
		"\u0216\7q\2\2\u0216\u0217\7r\2\2\u0217\u0218\7\"\2\2\u0218\u0219\7y\2"+
		"\2\u0219\u021a\7g\2\2\u021a\u021b\7c\2\2\u021b\u021c\7r\2\2\u021c\u021d"+
		"\7q\2\2\u021d\u021e\7p\2\2\u021e\u021f\7\"\2\2\u021f\u0220\7k\2\2\u0220"+
		"\u0221\7h\2\2\u0221\u0222\7\"\2\2\u0222\u0223\7p\2\2\u0223\u0224\7q\2"+
		"\2\u0224\u0225\7v\2\2\u0225\u0226\7\"\2\2\u0226\u0227\7w\2\2\u0227\u0228"+
		"\7u\2\2\u0228\u0229\7g\2\2\u0229\u0242\7f\2\2\u022a\u022b\7f\2\2\u022b"+
		"\u022c\7t\2\2\u022c\u022d\7q\2\2\u022d\u022e\7r\2\2\u022e\u022f\7\"\2"+
		"\2\u022f\u0230\7y\2\2\u0230\u0231\7g\2\2\u0231\u0232\7c\2\2\u0232\u0233"+
		"\7r\2\2\u0233\u0234\7q\2\2\u0234\u0235\7p\2\2\u0235\u0236\7\"\2\2\u0236"+
		"\u0237\7k\2\2\u0237\u0238\7h\2\2\u0238\u0239\7\"\2\2\u0239\u023a\7p\2"+
		"\2\u023a\u023b\7q\2\2\u023b\u023c\7v\2\2\u023c\u023d\7\"\2\2\u023d\u023e"+
		"\7w\2\2\u023e\u023f\7u\2\2\u023f\u0240\7g\2\2\u0240\u0242\7f\2\2\u0241"+
		"\u0213\3\2\2\2\u0241\u022a\3\2\2\2\u0242>\3\2\2\2\u0243\u0244\7y\2\2\u0244"+
		"\u0245\7g\2\2\u0245\u0246\7c\2\2\u0246\u0247\7r\2\2\u0247\u0248\7q\2\2"+
		"\u0248\u0249\7p\2\2\u0249\u024a\7\"\2\2\u024a\u024b\7c\2\2\u024b\u024c"+
		"\7u\2\2\u024c\u024d\7\"\2\2\u024d\u024e\7f\2\2\u024e\u024f\7g\2\2\u024f"+
		"\u0250\7h\2\2\u0250\u0251\7c\2\2\u0251\u0252\7w\2\2\u0252\u0253\7n\2\2"+
		"\u0253\u0254\7v\2\2\u0254@\3\2\2\2\u0255\u0256\7d\2\2\u0256\u0257\7g\2"+
		"\2\u0257\u0258\7u\2\2\u0258\u0259\7v\2\2\u0259B\3\2\2\2\u025a\u025b\7"+
		"y\2\2\u025b\u025c\7q\2\2\u025c\u025d\7t\2\2\u025d\u025e\7u\2\2\u025e\u025f"+
		"\7v\2\2\u025fD\3\2\2\2\u0260\u0261\7n\2\2\u0261\u0262\7k\2\2\u0262\u0263"+
		"\7i\2\2\u0263\u0264\7j\2\2\u0264\u0265\7v\2\2\u0265F\3\2\2\2\u0266\u0267"+
		"\7o\2\2\u0267\u0268\7g\2\2\u0268\u0269\7f\2\2\u0269\u026a\7k\2\2\u026a"+
		"\u026b\7w\2\2\u026b\u026c\7o\2\2\u026cH\3\2\2\2\u026d\u026e\7j\2\2\u026e"+
		"\u026f\7g\2\2\u026f\u0270\7c\2\2\u0270\u0271\7x\2\2\u0271\u0272\7{\2\2"+
		"\u0272J\3\2\2\2\u0273\u0274\7h\2\2\u0274\u0275\7q\2\2\u0275\u0276\7q\2"+
		"\2\u0276\u0277\7f\2\2\u0277L\3\2\2\2\u0278\u0279\7c\2\2\u0279\u027a\7"+
		"r\2\2\u027a\u027b\7r\2\2\u027b\u027c\7n\2\2\u027c\u0283\7g\2\2\u027d\u027e"+
		"\7C\2\2\u027e\u027f\7r\2\2\u027f\u0280\7r\2\2\u0280\u0281\7n\2\2\u0281"+
		"\u0283\7g\2\2\u0282\u0278\3\2\2\2\u0282\u027d\3\2\2\2\u0283N\3\2\2\2\u0284"+
		"\u0285\7e\2\2\u0285\u0286\7q\2\2\u0286\u0287\7q\2\2\u0287\u0288\7m\2\2"+
		"\u0288\u0289\7k\2\2\u0289\u0291\7g\2\2\u028a\u028b\7E\2\2\u028b\u028c"+
		"\7q\2\2\u028c\u028d\7q\2\2\u028d\u028e\7m\2\2\u028e\u028f\7k\2\2\u028f"+
		"\u0291\7g\2\2\u0290\u0284\3\2\2\2\u0290\u028a\3\2\2\2\u0291P\3\2\2\2\u0292"+
		"\u0293\7u\2\2\u0293\u0294\7q\2\2\u0294\u0295\7w\2\2\u0295\u029b\7r\2\2"+
		"\u0296\u0297\7U\2\2\u0297\u0298\7q\2\2\u0298\u0299\7w\2\2\u0299\u029b"+
		"\7r\2\2\u029a\u0292\3\2\2\2\u029a\u0296\3\2\2\2\u029bR\3\2\2\2\u029c\u029d"+
		"\7u\2\2\u029d\u029e\7v\2\2\u029e\u029f\7t\2\2\u029f\u02a0\7g\2\2\u02a0"+
		"\u02a1\7p\2\2\u02a1\u02a2\7i\2\2\u02a2\u02a3\7v\2\2\u02a3\u02ad\7j\2\2"+
		"\u02a4\u02a5\7U\2\2\u02a5\u02a6\7v\2\2\u02a6\u02a7\7t\2\2\u02a7\u02a8"+
		"\7g\2\2\u02a8\u02a9\7p\2\2\u02a9\u02aa\7i\2\2\u02aa\u02ab\7v\2\2\u02ab"+
		"\u02ad\7j\2\2\u02ac\u029c\3\2\2\2\u02ac\u02a4\3\2\2\2\u02adT\3\2\2\2\u02ae"+
		"\u02af\7.\2\2\u02afV\3\2\2\2\u02b0\u02b1\7c\2\2\u02b1\u02b2\7p\2\2\u02b2"+
		"\u02b3\7f\2\2\u02b3X\3\2\2\2\u02b4\u02b5\7w\2\2\u02b5\u02b6\7u\2\2\u02b6"+
		"\u02bb\7g\2\2\u02b7\u02b8\7W\2\2\u02b8\u02b9\7u\2\2\u02b9\u02bb\7g\2\2"+
		"\u02ba\u02b4\3\2\2\2\u02ba\u02b7\3\2\2\2\u02bbZ\3\2\2\2\u02bc\u02bd\7"+
		"n\2\2\u02bd\u02be\7g\2\2\u02be\u02bf\7u\2\2\u02bf\u02c0\7u\2\2\u02c0\u02c1"+
		"\7\"\2\2\u02c1\u02c2\7v\2\2\u02c2\u02c3\7j\2\2\u02c3\u02c4\7c\2\2\u02c4"+
		"\u02cc\7p\2\2\u02c5\u02c6\7w\2\2\u02c6\u02c7\7p\2\2\u02c7\u02c8\7f\2\2"+
		"\u02c8\u02c9\7g\2\2\u02c9\u02cc\7t\2\2\u02ca\u02cc\7>\2\2\u02cb\u02bc"+
		"\3\2\2\2\u02cb\u02c5\3\2\2\2\u02cb\u02ca\3\2\2\2\u02cc\\\3\2\2\2\u02cd"+
		"\u02ce\7o\2\2\u02ce\u02cf\7q\2\2\u02cf\u02d0\7t\2\2\u02d0\u02d1\7g\2\2"+
		"\u02d1\u02d2\7\"\2\2\u02d2\u02d3\7v\2\2\u02d3\u02d4\7j\2\2\u02d4\u02d5"+
		"\7c\2\2\u02d5\u02e8\7p\2\2\u02d6\u02d7\7i\2\2\u02d7\u02d8\7t\2\2\u02d8"+
		"\u02d9\7g\2\2\u02d9\u02da\7c\2\2\u02da\u02db\7v\2\2\u02db\u02dc\7g\2\2"+
		"\u02dc\u02dd\7t\2\2\u02dd\u02de\7\"\2\2\u02de\u02df\7v\2\2\u02df\u02e0"+
		"\7j\2\2\u02e0\u02e1\7c\2\2\u02e1\u02e8\7p\2\2\u02e2\u02e3\7q\2\2\u02e3"+
		"\u02e4\7x\2\2\u02e4\u02e5\7g\2\2\u02e5\u02e8\7t\2\2\u02e6\u02e8\7@\2\2"+
		"\u02e7\u02cd\3\2\2\2\u02e7\u02d6\3\2\2\2\u02e7\u02e2\3\2\2\2\u02e7\u02e6"+
		"\3\2\2\2\u02e8^\3\2\2\2\u02e9\u02ea\7\60\2\2\u02ea`\3\2\2\2\u02eb\u02ed"+
		"\t\2\2\2\u02ec\u02eb\3\2\2\2\u02ed\u02ee\3\2\2\2\u02ee\u02ec\3\2\2\2\u02ee"+
		"\u02ef\3\2\2\2\u02ef\u02f0\3\2\2\2\u02f0\u02f1\b\61\2\2\u02f1b\3\2\2\2"+
		"\u02f2\u02f4\t\3\2\2\u02f3\u02f2\3\2\2\2\u02f4\u02f5\3\2\2\2\u02f5\u02f3"+
		"\3\2\2\2\u02f5\u02f6\3\2\2\2\u02f6d\3\2\2\2\32\2m\177\u0094\u00c4\u0118"+
		"\u012a\u0141\u0151\u0173\u01a0\u01d1\u01ec\u01fc\u0241\u0282\u0290\u029a"+
		"\u02ac\u02ba\u02cb\u02e7\u02ee\u02f5\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}