package nl.aimsites.nzbvuq.game.state;

import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.generators.MapGeneratorFactory;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.generators.TeamGenerator;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.inputhandler.actions.MoveAction;
import nl.aimsites.nzbvuq.game.networking.ITransactionHandler;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class GameStateTest {

  private static final int GRAPH_SEED = 1234;
  public static final int UNITTEST_GRID_SIZE = 32;

  @Mock
  private ITransactionHandler networking;

  @Mock
  private TeamGenerator teamGenerator;

  @InjectMocks
  private GameState sut;

  @BeforeEach
  void setUp() throws Exception {
    sut = GameState.getInstance();
    MockitoAnnotations.openMocks(this);

    var gen =
        MapGeneratorFactory.getNoise4JMapGenerator(
            UNITTEST_GRID_SIZE, GameMode.CAPTURE_THE_FLAG, new ItemEntityFactory());
    var graph = gen.generateMap(0, 0, GRAPH_SEED);

    GameState.getInstance().setCurrentWorld(graph);
  }

  private List<Player> initializePlayersList() {
    List<Player> expectedTeam = new ArrayList<>();
    expectedTeam.add(new Player("Jan"));
    expectedTeam.add(new Player("Bas"));
    expectedTeam.add(new Player("Tineke"));
    return expectedTeam;
  }

  @Test
  void testSpawnEntityOnMap() {
    Player player = sut.getPlayer();

    ReachableTile currentPosition = player.getCurrentPosition();

    assertNotNull(currentPosition);
  }

  @Test
  void getInstancesGetsTheSameInstance() {
    assertEquals(sut, GameState.getInstance());
  }

  @Test
  void getInstanceGetsDifferentInstance() {
    GameState.clearInstance();
    assertNotEquals(sut, GameState.getInstance());
  }

  @Test
  void testSendAction() {
    when(networking.sendAction(any())).thenReturn(true);
    assertTrue(sut.sendAction(new Action(MoveAction.class, "n", "ae4e0390-e1d9-4206-8c3d-1c19665f4371", new Point(0, 0))));
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }
}
