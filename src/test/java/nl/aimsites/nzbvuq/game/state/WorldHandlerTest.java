package nl.aimsites.nzbvuq.game.state;

import nl.aimsites.nzbvuq.game.generators.Direction;
import nl.aimsites.nzbvuq.game.generators.MapGenerator;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Room;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.MoveDirection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class WorldHandlerTest {
  private WorldHandler sut;
  @Mock private MapGenerator mapGenerator;
  private Graph leftGraph;
  private Graph downGraph;
  private Graph upGraph;
  private Graph centralGraph;
  private Graph rightGraph;
  private int graphSize = 10;

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);

    sut = new WorldHandler(mapGenerator);

    leftGraph = new Graph(-10, 0, graphSize, 5);

    for (int i = -10; i < 0; i++) {
      for (int j = 0; j < graphSize; j++) {
        leftGraph.addTile(new Room(new Point(i, j), 0));
      }
    }

    downGraph = new Graph(0, 10, graphSize, 5);

    for (int i = 0; i < graphSize; i++) {
      for (int j = 10; j < graphSize + 10; j++) {
        downGraph.addTile(new Room(new Point(i, j), 0));
      }
    }

    upGraph = new Graph(0, -10, graphSize, 5);

    for (int i = 0; i < graphSize; i++) {
      for (int j = -10; j < 0; j++) {
        upGraph.addTile(new Room(new Point(i, j), 0));
      }
    }

    centralGraph = new Graph(0, 0, graphSize, 7);

    for (int i = 0; i < graphSize; i++) {
      for (int j = 0; j < graphSize; j++) {
        centralGraph.addTile(new Room(new Point(i, j), 0));
      }
    }

    rightGraph = new Graph(10, 0, graphSize, 6);

    for (int i = 10; i < graphSize + 10; i++) {
      for (int j = 0; j < graphSize; j++) {
        rightGraph.addTile(new Room(new Point(i, j), 0));
      }
    }

    when(mapGenerator.expandMap(any(), eq(Direction.LEFT))).thenReturn(leftGraph);
    when(mapGenerator.expandMap(any(), eq(Direction.RIGHT))).thenReturn(rightGraph);
    when(mapGenerator.expandMap(any(), eq(Direction.DOWN))).thenReturn(downGraph);
    when(mapGenerator.expandMap(any(), eq(Direction.UP))).thenReturn(upGraph);
  }

  @Test
  void worldHandlerGivesCorrectDestinationTileBackWhenMovingToLeft() {
    final Tile expectedTile = leftGraph.findTile(-1, 0).orElse(null);
    final Optional<Tile> actualTile =
        sut.getDestinationTile((ReachableTile) centralGraph.getTiles()[0][0], MoveDirection.WEST);

    Assertions.assertEquals(expectedTile, actualTile.orElseThrow());
  }

  @Test
  void worldHandlerGivesCorrectDestinationTileBackWhenMovingToRight() {
    final Tile expectedTile = rightGraph.findTile(10, 0).orElse(null);
    final Optional<Tile> actualTile =
        sut.getDestinationTile(
            (ReachableTile) centralGraph.getTiles()[0][centralGraph.getTiles().length - 1],
            MoveDirection.EAST);

    Assertions.assertEquals(expectedTile, actualTile.orElseThrow());
  }

  @Test
  void worldHandlerGivesCorrectDestinationTileBackWhenMovingDown() {
    final Tile expectedTile = downGraph.findTile(0, 10).orElse(null);
    final Optional<Tile> actualTile =
        sut.getDestinationTile(
            (ReachableTile) centralGraph.getTiles()[centralGraph.getTiles().length - 1][0],
            MoveDirection.SOUTH);

    Assertions.assertEquals(expectedTile, actualTile.orElseThrow());
  }

  @Test
  void worldHandlerGivesCorrectDestinationTileBackWhenMovingUp() {
    final Tile expectedTile = upGraph.findTile(0, -1).orElse(null);
    final Optional<Tile> actualTile =
        sut.getDestinationTile( (ReachableTile) centralGraph.getTiles()[0][0], MoveDirection.NORTH);

    Assertions.assertEquals(expectedTile, actualTile.orElseThrow());
  }

  @Test
  void addingSameChunkThrowsException() {
    sut.addChunk(leftGraph);

    Assertions.assertThrows(IllegalArgumentException.class, () -> sut.addChunk(leftGraph));
  }

  @Test
  void addingDifferentChunkDoesNotThrowException() {
    sut.addChunk(leftGraph);

    Assertions.assertDoesNotThrow(() -> sut.addChunk(rightGraph));
  }

  @Test
  void lastGeneratedChunkGetsUpdated() {
    Assertions.assertNull(sut.getLastGeneratedChunk());

    sut.getDestinationTile((ReachableTile) centralGraph.getTiles()[0][0], MoveDirection.WEST);

    Assertions.assertNotNull(sut.getLastGeneratedChunk());
  }

  @Test
  void worldHandlerHandlesAlreadyExistingGraphsCorrectly() {
    final Tile expectedTile = leftGraph.findTile(-1, 0).orElse(null);

    sut.addChunk(leftGraph);

    Optional<Tile> actualTile =
        sut.getDestinationTile((ReachableTile) centralGraph.getTiles()[0][0], MoveDirection.WEST);

    Assertions.assertEquals(expectedTile, actualTile.orElseThrow());
  }
}
