package nl.aimsites.nzbvuq.game.state;

import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.inputhandler.actions.MoveAction;
import nl.aimsites.nzbvuq.game.inputhandler.parser.InputHandler;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class ActionHandlerTest {
  private ActionHandler sut;
  private InputHandler mockedInputHandler;

  @BeforeEach
  void setUp() {
    mockedInputHandler = Mockito.mock(InputHandler.class);

    sut = new ActionHandler(mockedInputHandler);
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }

  @Test
  void assertOnUpdateCallsHandleAction() {
    var action = new Action(MoveAction.class, "n", "player", new Point(1, 1));

    sut.onUpdate(action);

    verify(mockedInputHandler, times(1)).handleAction(action);
  }
}
