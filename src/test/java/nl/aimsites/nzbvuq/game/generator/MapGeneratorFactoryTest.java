package nl.aimsites.nzbvuq.game.generator;

import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.generators.MapGeneratorFactory;
import nl.aimsites.nzbvuq.game.generators.Noise4JMapGenerator;
import nl.aimsites.nzbvuq.game.state.GameMode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MapGeneratorFactoryTest {

  @Test
  void factoryReturnsNoise4JMapGenerator() throws Exception {
    Assertions.assertTrue(
        MapGeneratorFactory.getNoise4JMapGenerator(
                16, GameMode.CAPTURE_THE_FLAG, new ItemEntityFactory())
            instanceof Noise4JMapGenerator);
  }
}
