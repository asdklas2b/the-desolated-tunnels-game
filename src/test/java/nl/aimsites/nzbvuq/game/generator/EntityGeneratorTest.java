package nl.aimsites.nzbvuq.game.generator;

import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.entities.OpeningEntityFactory;
import nl.aimsites.nzbvuq.game.entities.StorageEntityFactory;
import nl.aimsites.nzbvuq.game.entities.MonsterFactory;
import nl.aimsites.nzbvuq.game.entities.StorageFactory;
import nl.aimsites.nzbvuq.game.entities.character.monsters.Monster;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Storage;
import nl.aimsites.nzbvuq.game.entities.items.Strength;
import nl.aimsites.nzbvuq.game.entities.items.attributes.special.Flag;
import nl.aimsites.nzbvuq.game.generators.EntityGenerator;
import nl.aimsites.nzbvuq.game.generators.FlagHandler;
import nl.aimsites.nzbvuq.game.graph.Edge;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.*;
import nl.aimsites.nzbvuq.game.state.GameMode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.mockito.Mockito.*;

class EntityGeneratorTest {
  private EntityGenerator entityGenerator;
  private Graph graph;
  private Graph graphRoomFields;
  private final double strengthDropChance = 0.2;
  private final double storageDropChance = 0.2;
  private ReachableTile tile;
  @Mock ReachableTile tile2;
  private final int graphSize = 4;
  @Mock private Random random;
  private List<BaseItem> strengthList;
  private List<BaseItem> storageList;
  @Mock private Strength strength;
  @Mock private Storage storage;
  @Mock private Flag flag;
  @Mock private ItemEntityFactory mockedItemFactory;
  @Mock private StorageEntityFactory mockedStorageFactory;
  @Mock private Monster monster;
  @Mock private FlagHandler mockedFlagHandler;
  private OpeningEntityFactory openingFactory;
  private List<Monster> monsterList;
  @Mock private MonsterFactory mockedMonsterFactory;

  @BeforeEach
  void setup() throws Exception {
    MockitoAnnotations.openMocks(this);
    openingFactory = new OpeningEntityFactory();
    entityGenerator = new EntityGenerator(random);
    entityGenerator.setItemFactory(mockedItemFactory);
    entityGenerator.setStorageFactory(mockedStorageFactory);
    entityGenerator.setOpeningFactory(openingFactory);
    entityGenerator.setMonsterFactory(mockedMonsterFactory);
    entityGenerator.setFlagHandler(new FlagHandler(mockedItemFactory));
    storageList = new ArrayList<>();
    storageList.add(storage);
    when(storage.getDropChance()).thenReturn(storageDropChance);
    when(mockedStorageFactory.getItemsFromType(Storage.class)).thenReturn(storageList);
    when(mockedStorageFactory.getCloneFromItem(storage)).thenReturn(storage);
    strengthList = new ArrayList<>();
    strengthList.add(strength);
    when(strength.getDropChance()).thenReturn(strengthDropChance);
    when(mockedItemFactory.getItemsFromType(Strength.class)).thenReturn(strengthList);
    when(mockedItemFactory.getCloneFromItem(strength)).thenReturn(strength);
    when(mockedItemFactory.getItemsFromType(Flag.class))
        .thenReturn(Collections.singletonList(flag));
    when(mockedItemFactory.getCloneFromItem(flag)).thenReturn(flag);
    when(flag.clone()).thenReturn(flag);

    monsterList = new ArrayList<>();
    monsterList.add(monster);
    when(mockedMonsterFactory.getMonstersFromType(Monster.class)).thenReturn(monsterList);

    graph = new Graph(0, 0, graphSize, graphSize);
    for (int i = 0; i < graphSize; i++) {
      for (int j = 0; j < graphSize; j++) {
        graph.addTile(new Room(new Point(i, j), 1, RoomType.HOUSE));
      }
    }

    graphRoomFields = new Graph(0, 0, graphSize, graphSize);

    Room room = new Room(new Point(0, 0), 1);
    room.setType(RoomType.HOUSE);
    Field f1 = new Field(new Point(0, 1), 1);
    Field f2 = new Field(new Point(1, 0), 1);
    Field f3 = new Field(new Point(1, 1), 1);
    Corridor f4 = new Corridor(new Point(1, 2), 1);
    Room room2 = new Room(new Point(1, 3), 1);
    room2.setType(RoomType.CASTLE);

    graphRoomFields.addTile(room);
    graphRoomFields.addTile(room2);
    graphRoomFields.addTile(f1);
    graphRoomFields.addTile(f2);
    graphRoomFields.addTile(f4);

    room.addAdjacentTile(new Edge(f1));
    room.addAdjacentTile(new Edge(f2));
    room.addAdjacentTile(new Edge(f3));
    room.addAdjacentTile(new Edge(f4));
    f4.addAdjacentTile(new Edge(room));

    tile = (ReachableTile) graph.findTile(0, 0).get();
    tile2 = (ReachableTile) graph.findTile(0, 1).get();
  }

  @Test
  void strengthGetsPlacedIfUnderItsDropChance() {
    when(random.nextDouble()).thenReturn(strengthDropChance - 0.01);

    entityGenerator.generateEntities(this.graph);

    Tile[][] tiles = graph.getTiles();

    Assertions.assertTrue(((ReachableTile) tiles[0][0]).getOccupyingEntities().contains(strength));
  }

  @Test
  void strengthDoesNotGetsPlacedIfAboveItsDropChance() {
    when(random.nextDouble()).thenReturn(strengthDropChance + 0.01);

    entityGenerator.generateEntities(this.graph);

    Tile[][] tiles = this.graph.getTiles();

    Assertions.assertFalse(((ReachableTile) tiles[0][0]).getOccupyingEntities().contains(strength));
  }

  @Test
  void storageGetsPlacedIfUnderItsDropChance() {
    when(random.nextDouble()).thenReturn(storageDropChance - 0.01);

    entityGenerator.generateEntities(this.graph);

    Tile[][] tiles = graph.getTiles();

    Assertions.assertTrue(((ReachableTile) tiles[0][0]).getOccupyingEntities().contains(storage));
  }

  @Test
  void storageDoesNotGetsPlacedIfAboveItsDropChance() {
    when(random.nextDouble()).thenReturn(storageDropChance + 0.01);

    entityGenerator.generateEntities(this.graph);

    Tile[][] tiles = this.graph.getTiles();

    Assertions.assertFalse(((ReachableTile) tiles[0][0]).getOccupyingEntities().contains(storage));
  }

  @Test
  void entityGeneratorSetsBlockagesCorrectlyWhenTileIsOfTypeRoomAndDestinationIsField() {
    when(random.nextInt()).thenReturn(0);

    entityGenerator.generateEntities(this.graphRoomFields);

    ReachableTile tile = (ReachableTile) this.graphRoomFields.getTiles()[0][0];

    Assertions.assertNotNull(tile.getAdjacentTiles().get(0).getOpeningEntity());
    Assertions.assertNotNull(tile.getAdjacentTiles().get(1).getOpeningEntity());
    Assertions.assertNull(tile.getAdjacentTiles().get(2).getOpeningEntity());
    Assertions.assertNull(tile.getAdjacentTiles().get(3).getOpeningEntity());
  }

  @Test
  void entityGeneratorDoesNotSetBlockagesIfTileIsNotOfTypeRoom() {
    when(random.nextInt()).thenReturn(0);

    entityGenerator.generateEntities(this.graphRoomFields);

    ReachableTile tile = (ReachableTile) this.graphRoomFields.getTiles()[2][1];

    Assertions.assertNull(tile.getAdjacentTiles().get(0).getOpeningEntity());
  }

  @Test
  void storageItemExceedsCapacity() {
    when(random.nextDouble()).thenReturn(storageDropChance + 0.01);

    entityGenerator.generateEntities(this.graph);

    Tile[][] tiles = this.graph.getTiles();

    Assertions.assertFalse(((ReachableTile) tiles[0][0]).getOccupyingEntities().contains(storage));
  }

  @Test
  void entityGeneratorCallsFlagHandlerIfGameModeIsCaptureTheFlag() {
    entityGenerator.setFlagHandler(mockedFlagHandler);
    entityGenerator.setGameMode(GameMode.CAPTURE_THE_FLAG);

    entityGenerator.generateEntities(this.graph);
    verify(mockedFlagHandler).generateFlagsIfConditionsMet(this.graph);
  }

  @Test
  void entityGeneratorDoesNotCallFlagHandlerIfGameModeIsNotCaptureTheFlag() {
    entityGenerator.setFlagHandler(mockedFlagHandler);
    entityGenerator.setGameMode(GameMode.LAST_MAN_STANDING);

    entityGenerator.generateEntities(this.graph);
    verify(mockedFlagHandler, times(0)).generateFlagsIfConditionsMet(this.graph);
  }

  @Test
  void monsterGetsPlacedOnTile() {
    Map<String, Double> locations = new HashMap<>();
    locations.put("house", 0.01);
    when(monster.getLocations()).thenReturn(locations);
    when(monster.clone()).thenReturn(monster);
    when(random.nextDouble()).thenReturn(0.0);
    when(mockedMonsterFactory.getCloneFromMonster(monster)).thenReturn(monster);

    entityGenerator.generateEntities(this.graph);

    Tile[][] tiles = this.graph.getTiles();

    Assertions.assertTrue(((ReachableTile) tiles[0][0]).getOccupyingEntities().contains(monster));
  }

  @Test
  void monsterDoesNotGetPlacedOnTile() {
    Map<String, Double> locations = new HashMap<>();
    locations.put("house", 0.01);
    when(monster.getLocations()).thenReturn(locations);
    when(monster.clone()).thenReturn(monster);
    when(random.nextDouble()).thenReturn(1.0);
    when(mockedMonsterFactory.getCloneFromMonster(monster)).thenReturn(monster);

    entityGenerator.generateEntities(this.graph);

    Tile[][] tiles = this.graph.getTiles();

    Assertions.assertFalse(((ReachableTile) tiles[0][0]).getOccupyingEntities().contains(monster));
  }
}
