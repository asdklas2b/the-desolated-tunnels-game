package nl.aimsites.nzbvuq.game.generator;

import nl.aimsites.nzbvuq.game.generators.Direction;
import nl.aimsites.nzbvuq.game.generators.GraphConnector;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Room;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GraphConnectorTest {
  private Graph firstGraph;
  private Graph secondGraph;
  private int graphSize = 10;

  @BeforeEach
  void setup() {
    firstGraph = new Graph(-10, 0, graphSize, 5);
    for (int i = -10; i < 0; i++) {
      for (int j = 0; j < graphSize; j++) {
        firstGraph.addTile(new Room(new Point(i, j), 0));
      }
    }
    secondGraph = new Graph(0, 0, graphSize, 0);
    for (int i = 0; i < graphSize; i++) {
      for (int j = 0; j < graphSize; j++) {
        secondGraph.addTile(new Room(new Point(i, j), 0));
      }
    }
  }

  @Test
  void graphConnectorConnectsToOtherGraphCorrectly() {
    GraphConnector.connectTwoGraph(secondGraph, firstGraph, Direction.LEFT);

    Tile expectedTile = firstGraph.findTile(-1, 0).orElse(null);

    Assertions.assertTrue(
        (((ReachableTile) secondGraph.findTile(0, 0).orElseThrow()))
            .getAdjacentTiles().stream()
                .anyMatch(tile -> tile.getDestinationTile().equals(expectedTile)));
  }
}
