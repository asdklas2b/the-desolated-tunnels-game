package nl.aimsites.nzbvuq.game.generator;

import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.attributes.special.Flag;
import nl.aimsites.nzbvuq.game.generators.FlagHandler;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.Forest;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Room;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

class FlagHandlerTest {
  private FlagHandler sut;
  @Mock
  private ItemEntityFactory mockedItemFactory;
  private Graph graph;
  private Graph unreachableGraph;
  private final int graphSize = 20;
  private final int maxFlags = 2;
  private final int minSeparationDistanceBetweenFlags = 5;
  private final int maxBoundary = 10;
  private List<BaseItem> list;
  private Flag flag = new Flag();

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);

    sut = new FlagHandler(mockedItemFactory, maxFlags, minSeparationDistanceBetweenFlags, maxBoundary);
    list = new ArrayList<>();
    list.add(flag);

    when(mockedItemFactory.getItemsFromType(Flag.class)).thenReturn(list);
    when(mockedItemFactory.getCloneFromItem(flag)).thenReturn(flag);

    graph = new Graph(-10, -10, graphSize, 0);

    for (int i = -maxBoundary; i < maxBoundary; i++) {
      for (int j = -maxBoundary; j < maxBoundary; j++) {
        graph.addTile(new Room(new Point(i, j), 1));
      }
    }

    unreachableGraph = new Graph(-10, -10, graphSize, 0);

    for (int i = -maxBoundary; i < maxBoundary; i++) {
      for (int j = -maxBoundary; j < maxBoundary; j++) {
        unreachableGraph.addTile(new Forest(new Point(i, j)));
      }
    }
  }

  @Test
  void flagHandlerThrowsExceptionIfMinSeparationDistanceIsBiggerThanMaxBoundary() {
    Assertions.assertThrows(IllegalStateException.class, () -> new FlagHandler(mockedItemFactory, 2, 10, 5));
  }

  @Test
  void FlagHandlerGeneratesFlagOnCorrectTile() {
    int flagCounter = 0;
    sut.generateFlagsIfConditionsMet(graph);

    Tile[][] tiles = graph.getTiles();

    for (Tile[] tileArray : tiles) {
      for (Tile tile : tileArray) {
        if (tile instanceof ReachableTile) {
          ReachableTile reachableTile = (ReachableTile) tile;
          if (reachableTile.getOccupyingEntities().size() > 0 && reachableTile.getOccupyingEntities().get(0) instanceof Flag) {
            flagCounter++;
          }
        }
      }
    }

    Assertions.assertEquals(maxFlags, flagCounter);
  }

  @Test
  void FlagHandlerGeneratesFlagOnCorrectTileWhenFirstGraphIsUnreachable() {
    int flagCounter = 0;
    sut.generateFlagsIfConditionsMet(unreachableGraph);
    sut.generateFlagsIfConditionsMet(graph);
    Tile[][] tiles = graph.getTiles();

    for (Tile[] tileArray : tiles) {
      for (Tile tile : tileArray) {
        if (tile instanceof ReachableTile) {
          ReachableTile reachableTile = (ReachableTile) tile;
          if (reachableTile.getOccupyingEntities().size() > 0 && reachableTile.getOccupyingEntities().get(0) instanceof Flag) {
            flagCounter++;
          }
        }
      }
    }

    Assertions.assertEquals(maxFlags, flagCounter);
  }
}
