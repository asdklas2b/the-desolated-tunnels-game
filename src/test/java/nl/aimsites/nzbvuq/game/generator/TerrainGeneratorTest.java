package nl.aimsites.nzbvuq.game.generator;

import com.github.czyzby.noise4j.map.Grid;
import com.github.czyzby.noise4j.map.generator.room.dungeon.DungeonGenerator;
import nl.aimsites.nzbvuq.game.generators.TerrainGenerator;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.tiles.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.Random;

import static org.mockito.Mockito.when;

class TerrainGeneratorTest {
  private final int startX = 0;
  private final int startY = 0;
  private final int gridSize = 5;
  private final float fieldValue = 1.0f;
  private final float roomValue = 0.5f;
  private final float corridorValue = 0.0f;
  private TerrainGenerator sut;
  @Mock
  private Grid grid;
  @Mock
  private Random random;
  @Mock
  private DungeonGenerator dungeonGenerator;

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);
    int castleSize = 3;
    sut = new TerrainGenerator(castleSize, random);
    when(grid.getHeight()).thenReturn(2);
    when(grid.getWidth()).thenReturn(2);
  }

  @Test
  void terrainGeneratorCreatesCorrectArraySize() {
    Graph graph = sut.generateTerrain(grid, gridSize, startX, startY, 1);
    Tile[][] tiles = graph.getTiles();

    final int expectedArraySize = gridSize * gridSize;
    int actualArraySize = 0;

    for (Tile[] tile : tiles) {
      actualArraySize += tile.length;
    }

    Assertions.assertEquals(expectedArraySize, actualArraySize);
  }

  @Test
  void terrainGeneratorAssignsRoom() {
    boolean isRoom = false;
    when(grid.get(0, 0)).thenReturn(roomValue);
    Graph graph = sut.generateTerrain(grid, gridSize, startX, startY, 1);
    Tile[][] tiles = graph.getTiles();
    if (tiles[0][0] instanceof Room) {
      isRoom = true;
    }
    Assertions.assertTrue(isRoom);
  }

  @Test
  void terrainGeneratorAssignsCorridor() {
    boolean isCorridor = false;
    when(grid.get(0, 1)).thenReturn(corridorValue);
    Graph graph = sut.generateTerrain(grid, gridSize, startX, startY, 1);
    Tile[][] tiles = graph.getTiles();
    if (tiles[0][1] instanceof Corridor) {
      isCorridor = true;
    }
    Assertions.assertTrue(isCorridor);
  }

  @Test
  void terrainGeneratorRoomIsTypeCastle() {
    boolean isCastle = false;
    when(grid.getHeight()).thenReturn(5);
    when(grid.getWidth()).thenReturn(5);
    when(grid.get(0, 0)).thenReturn(roomValue);
    when(grid.get(0, 1)).thenReturn(roomValue);
    when(grid.get(0, 2)).thenReturn(roomValue);
    when(grid.get(1, 0)).thenReturn(roomValue);
    when(grid.get(1, 1)).thenReturn(roomValue);
    when(grid.get(1, 2)).thenReturn(roomValue);
    when(grid.get(2, 0)).thenReturn(roomValue);
    when(grid.get(2, 1)).thenReturn(roomValue);
    when(grid.get(2, 2)).thenReturn(roomValue);

    Graph graph = sut.generateTerrain(grid, gridSize, startX, startY, 1);
    Tile[][] tiles = graph.getTiles();
    if (((Room) tiles[1][1]).getType() == RoomType.CASTLE) {
      isCastle = true;
    }
    Assertions.assertTrue(isCastle);
  }

  @Test
  void terrainGeneratorRoomIsNotTypeCastle() {
    boolean isCastle = false;
    when(grid.getHeight()).thenReturn(5);
    when(grid.getWidth()).thenReturn(5);
    when(grid.get(0, 0)).thenReturn(fieldValue);
    when(grid.get(1, 1)).thenReturn(roomValue);

    Graph graph = sut.generateTerrain(grid, gridSize, startX, startY, 1);
    Tile[][] tiles = graph.getTiles();
    if (((Room) tiles[1][1]).getType() == RoomType.CASTLE) {
      isCastle = true;
    }
    Assertions.assertFalse(isCastle);
  }

  @Test
  void terrainGeneratorRoomIsHouse() {
    boolean isHouse = false;
    when(grid.getHeight()).thenReturn(5);
    when(grid.getWidth()).thenReturn(5);
    when(grid.get(1, 1)).thenReturn(roomValue);

    Graph graph = sut.generateTerrain(grid, gridSize, startX, startY, 1);
    Tile[][] tiles = graph.getTiles();
    if (((Room) tiles[1][1]).getType() == RoomType.HOUSE) {
      isHouse = true;
    }
    Assertions.assertTrue(isHouse);
  }

  @Test
  void terrainGeneratorGeneratesFieldEveryFieldGenerationExceptFifth() {
    when(grid.getHeight()).thenReturn(10);
    when(grid.getWidth()).thenReturn(10);
    when(grid.get(0, 1)).thenReturn(fieldValue);
    when(grid.get(0, 2)).thenReturn(fieldValue);
    when(grid.get(0, 3)).thenReturn(fieldValue);
    when(grid.get(0, 4)).thenReturn(fieldValue);
    when(grid.get(0, 5)).thenReturn(fieldValue);

    Graph graph = sut.generateTerrain(grid, 10, 0, 0, 0);

    Optional<Tile> tile = graph.findTile(0, 2);
    tile.ifPresent(t -> Assertions.assertTrue(t instanceof Field));
  }

  @Test
  void terrainGeneratorGeneratesForestEveryFiveFieldGeneration() {
    when(grid.getHeight()).thenReturn(10);
    when(grid.getWidth()).thenReturn(10);
    when(grid.get(0, 1)).thenReturn(fieldValue);
    when(grid.get(0, 2)).thenReturn(fieldValue);
    when(grid.get(0, 3)).thenReturn(fieldValue);
    when(grid.get(0, 4)).thenReturn(fieldValue);
    when(grid.get(0, 5)).thenReturn(fieldValue);

    Graph graph = sut.generateTerrain(grid, 10, 0, 0, 0);

    Optional<Tile> tile = graph.findTile(0, 1);
    tile.ifPresent(t -> Assertions.assertTrue(t instanceof Forest));
  }
}
