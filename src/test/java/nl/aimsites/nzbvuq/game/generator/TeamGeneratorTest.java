package nl.aimsites.nzbvuq.game.generator;

import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.generators.TeamGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit tests for TeamGenerator
 *
 * @author Tim Weening (T.Weening@student.han.nl)
 */
public class TeamGeneratorTest {
  private List<Player> equalAmountOfPlayers;
  private List<Player> unequalAmountOfPlayers;
  private TeamGenerator sut;

  @BeforeEach
  void setup() {
    equalAmountOfPlayers = new ArrayList<>();
    unequalAmountOfPlayers = new ArrayList<>();
    initializePlayers();

    sut = new TeamGenerator();
  }

  /**
   * Initializes a list with all players that are supposed to be in the lobby
   *
   * @author Tim Weening (T.Weening@student.han.nl)
   */
  private void initializePlayers() {
    Player player1 = new Player("Tim");
    Player player2 = new Player("Martijn");
    Player player3 = new Player("Dennis");
    Player player4 = new Player("Ingrid");
    Player player5 = new Player("Tineke");
    Player player6 = new Player("Joost");

    equalAmountOfPlayers.add(player1);
    equalAmountOfPlayers.add(player2);
    equalAmountOfPlayers.add(player3);
    equalAmountOfPlayers.add(player4);
    equalAmountOfPlayers.add(player5);
    equalAmountOfPlayers.add(player6);

    unequalAmountOfPlayers.add(player1);
    unequalAmountOfPlayers.add(player2);
    unequalAmountOfPlayers.add(player3);
    unequalAmountOfPlayers.add(player4);
    unequalAmountOfPlayers.add(player5);
  }

  @Test
  void uTestGenerateTeamsGeneratesTwoEqualTeams() {
    //ACT
    int expectedAmountOfPlayersInEachTeam = 3;

    //ARRANGE
    sut.generateTeams(equalAmountOfPlayers);

    //ASSERT
    Assertions.assertEquals(expectedAmountOfPlayersInEachTeam, sut.getTeam1().size());
    Assertions.assertEquals(expectedAmountOfPlayersInEachTeam, sut.getTeam2().size());
  }

  @Test
  void uTestGenerateTeamsGeneratesTwoUnequalTeams() {
    //ACT
    int expectedAmountOfPlayersInTeam1 = 2;
    int expectedAmountOfPlayersInTeam2 = 3;

    //ARRANGE
    sut.generateTeams(unequalAmountOfPlayers);

    //ASSERT
    Assertions.assertEquals(expectedAmountOfPlayersInTeam1, sut.getTeam1().size());
    Assertions.assertEquals(expectedAmountOfPlayersInTeam2, sut.getTeam2().size());
  }
}
