package nl.aimsites.nzbvuq.game.generator;

import com.github.czyzby.noise4j.map.Grid;
import com.github.czyzby.noise4j.map.generator.room.dungeon.DungeonGenerator;
import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.generators.*;
import nl.aimsites.nzbvuq.game.graph.Graph;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.Corridor;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import nl.aimsites.nzbvuq.game.state.GameMode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class Noise4JMapGeneratorTest {
  private final int startX = 0;
  private final int startY = 0;
  private Noise4JMapGenerator sut;
  @Mock private DungeonGenerator mockedDungeonGenerator;
  @Mock private TerrainGenerator mockedConverter;
  @Mock private EntityGenerator mockedEntityGenerator;
  @Mock private ItemEntityFactory itemFactory;
  @Mock private Grid mockedGrid;
  @Mock private Graph mockedGraph;
  private Tile[][] array;
  @Mock private ReachableTile mockedTile;

  @BeforeEach
  void setup() throws Exception {
    MockitoAnnotations.openMocks(this);
    sut =
        new Noise4JMapGenerator(
            4,
            mockedDungeonGenerator,
            mockedConverter,
            mockedEntityGenerator,
            itemFactory,
            GameMode.CAPTURE_THE_FLAG);
    when(mockedConverter.generateTerrain(any(), anyInt(), anyInt(), anyInt(), anyLong()))
        .thenReturn(mockedGraph);
    array = new Tile[16][16];
    array[0][0] = mockedTile;
    array[0][15] = mockedTile;
    when(mockedGraph.getTiles()).thenReturn(array);
    when(mockedTile.getCoordinates()).thenReturn(new Point(0, 0));
  }

  @Test
  void generatorGeneratesCorrectMapSize() {
    Graph graph = sut.generateMap(startX, startY);

    final int expectedSize = 16;
    final int actualSize = graph.getTiles().length;

    Assertions.assertEquals(expectedSize, actualSize);
  }

  @Test
  void generatorGeneratesMapWithCorrectStartXY() {
    Graph graph = sut.generateMap(startX, startY);

    final Point expectedPoint = new Point(startX, startY);
    final Point actualPoint = graph.getTiles()[0][0].getCoordinates();

    Assertions.assertEquals(expectedPoint, actualPoint);
  }

  @Test
  void generatorExpandsMapWithoutSeedCorrectSize() {
    Graph graph = sut.generateMap(startX, startY);
    Graph graph1 = sut.expandMap(graph, Direction.RIGHT);

    final int expectedSize = 16;
    final int actualSize = graph1.getTiles().length;

    Assertions.assertEquals(expectedSize, actualSize);
  }

  @Test
  void generatorExpandsMapWithSeedCorrectSize() {
    Graph graph = sut.generateMap(startX, startY);
    Graph graph1 = sut.expandMap(graph, 0, Direction.RIGHT);

    final int expectedSize = 16;
    final int actualSize = graph1.getTiles().length;

    Assertions.assertEquals(expectedSize, actualSize);
  }

  @Test
  void generatorExpandsMapCorrectlyWhenExpandingToDirectionRight() throws Exception {
    final MapGenerator sut = MapGeneratorFactory.getNoise4JMapGenerator(2, GameMode.CAPTURE_THE_FLAG, new ItemEntityFactory());
    Graph graph = new Graph(0, 0, 2, 10);
    graph.addTile(new Corridor(new Point(0, 0), 0));
    graph.addTile(new Corridor(new Point(0, 1), 0));
    graph.addTile(new Corridor(new Point(1, 0), 0));
    graph.addTile(new Corridor(new Point(1, 1), 0));

    Graph graph1 = sut.expandMap(graph, Direction.RIGHT);

    final int expectedX = 2;
    final int expectedY = 0;
    final int actualX = graph1.getTiles()[0][0].getX();
    final int actualY = graph1.getTiles()[0][0].getY();

    Assertions.assertEquals(expectedX, actualX);
    Assertions.assertEquals(expectedY, actualY);
  }

  @Test
  void generatorExpandsMapCorrectlyWhenExpandingToDirectionLeft() throws Exception {
    final MapGenerator sut = MapGeneratorFactory.getNoise4JMapGenerator(2, GameMode.CAPTURE_THE_FLAG, new ItemEntityFactory());
    Graph graph = new Graph(0, 0, 2, 10);
    graph.addTile(new Corridor(new Point(0, 0), 0));
    graph.addTile(new Corridor(new Point(0, 1), 0));
    graph.addTile(new Corridor(new Point(1, 0), 0));
    graph.addTile(new Corridor(new Point(1, 1), 0));

    Graph graph1 = sut.expandMap(graph, Direction.LEFT);

    final int expectedX = -2;
    final int expectedY = 0;
    final int actualX = graph1.getTiles()[0][0].getX();
    final int actualY = graph1.getTiles()[0][0].getY();

    Assertions.assertEquals(expectedX, actualX);
    Assertions.assertEquals(expectedY, actualY);
  }

  @Test
  void generatorExpandsMapCorrectlyWhenExpandingToDirectionUp() throws Exception {
    final MapGenerator sut = MapGeneratorFactory.getNoise4JMapGenerator(2, GameMode.CAPTURE_THE_FLAG, new ItemEntityFactory());
    Graph graph = new Graph(0, 0, 2, 10);
    graph.addTile(new Corridor(new Point(0, 0), 0));
    graph.addTile(new Corridor(new Point(0, 1), 0));
    graph.addTile(new Corridor(new Point(1, 0), 0));
    graph.addTile(new Corridor(new Point(1, 1), 0));

    Graph graph1 = sut.expandMap(graph, Direction.UP);

    final int expectedX = 0;
    final int expectedY = -2;
    final int actualX = graph1.getTiles()[0][0].getX();
    final int actualY = graph1.getTiles()[0][0].getY();

    Assertions.assertEquals(expectedX, actualX);
    Assertions.assertEquals(expectedY, actualY);
  }

  @Test
  void generatorExpandsMapCorrectlyWhenExpandingToDirectionDown() throws Exception {
    final MapGenerator sut =
        MapGeneratorFactory.getNoise4JMapGenerator(2, GameMode.CAPTURE_THE_FLAG, new ItemEntityFactory());
    Graph graph = new Graph(0, 0, 2, 10);
    graph.addTile(new Corridor(new Point(0, 0), 0));
    graph.addTile(new Corridor(new Point(0, 1), 0));
    graph.addTile(new Corridor(new Point(1, 0), 0));
    graph.addTile(new Corridor(new Point(1, 1), 0));

    Graph graph1 = sut.expandMap(graph, Direction.DOWN);

    final int expectedX = 0;
    final int expectedY = 2;
    final int actualX = graph1.getTiles()[0][0].getX();
    final int actualY = graph1.getTiles()[0][0].getY();

    Assertions.assertEquals(expectedX, actualX);
    Assertions.assertEquals(expectedY, actualY);
  }
}
