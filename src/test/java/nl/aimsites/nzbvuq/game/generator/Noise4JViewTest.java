package nl.aimsites.nzbvuq.game.generator;

import com.github.czyzby.noise4j.map.Grid;
import nl.aimsites.nzbvuq.game.generators.Noise4JView;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.mockito.Mockito.when;

class Noise4JViewTest {
  private Noise4JView sut;
  @Mock private Grid mockedGrid;

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);

    sut = new Noise4JView(mockedGrid);
    when(mockedGrid.getWidth()).thenReturn(2);
    when(mockedGrid.getHeight()).thenReturn(1);
    when(mockedGrid.get(0, 0)).thenReturn(0.0f);
    when(mockedGrid.get(0, 1)).thenReturn(0.0f);
  }

  @Test
  void displaysGridCorrectly() {
    final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outContent));
    System.setErr(new PrintStream(errContent));

    sut.display();

    final String expectedValue = "||||||" + System.lineSeparator();

    Assertions.assertEquals(expectedValue, outContent.toString());
  }
}
