package nl.aimsites.nzbvuq.game.generator;

import nl.aimsites.nzbvuq.game.generators.TileConnector;
import nl.aimsites.nzbvuq.game.graph.Edge;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.Forest;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.graph.tiles.Room;
import nl.aimsites.nzbvuq.game.graph.tiles.Tile;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import java.util.List;

class TileConnectorTest {
  private TileConnector sut;
  private final int gridSize = 4;
  private Tile[][] tileArray;

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);
    sut = new TileConnector();
    tileArray = new Tile[gridSize][gridSize];
  }

  @Test
  void tileConnectorConnectsReachableTiles() {
    tileArray[0][0] = new Room(new Point(0, 0), 0);
    tileArray[1][0] = new Room(new Point(0, 1), 0);
    tileArray[0][1] = new Room(new Point(1, 0), 0);
    tileArray[1][1] = new Room(new Point(1, 1), 0);
    tileArray[2][1] = new Room(new Point(1, 2), 0);
    tileArray[3][1] = new Room(new Point(1, 3), 0);

    sut.connectTiles(tileArray);

    List<Edge> adjacentTiles = ((ReachableTile) tileArray[0][1]).getAdjacentTiles();

    Assertions.assertTrue(
        adjacentTiles.stream()
            .map(Edge::getDestinationTile)
            .anyMatch(n -> n.equals(tileArray[0][0])));

    Assertions.assertTrue(
        adjacentTiles.stream()
            .map(Edge::getDestinationTile)
            .anyMatch(n -> n.equals(tileArray[1][1])));
  }

  @Test
  void tileConnectorDoesNotConnectUnreachableTiles() {
    tileArray[0][0] = new Room(new Point(0, 0), 0);
    tileArray[0][1] = new Forest(new Point(1, 0));
    tileArray[1][0] = new Room(new Point(0, 1), 0);

    sut.connectTiles(tileArray);

    List<Edge> adjacentTiles = ((ReachableTile) tileArray[0][0]).getAdjacentTiles();

    Assertions.assertTrue(
        adjacentTiles.stream()
            .map(Edge::getDestinationTile)
            .noneMatch(n -> n.equals(tileArray[0][1])));

    Assertions.assertTrue(
        adjacentTiles.stream()
            .map(Edge::getDestinationTile)
            .anyMatch(n -> n.equals(tileArray[1][0])));
  }
}
