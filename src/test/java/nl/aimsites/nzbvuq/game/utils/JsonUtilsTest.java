package nl.aimsites.nzbvuq.game.utils;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JsonUtilsTest {
  private JSONObject jsonObject;

  @BeforeEach
  void setUp() {
    jsonObject = new JSONObject("{\"myInt\": 300, \"myString\": \"Unittesting\", \"nullString\": null}");
  }

  @Test
  void testThatGetIntReturnsTheCorrectNumber() {
    var actualResult = JsonUtils.getInt(jsonObject, "myInt", -1);

    assertEquals(300, actualResult);
  }

  @Test
  void testThatGetIntReturnsTheDefaultValueIfTheKeyDoesNotExist() {
    var actualResult = JsonUtils.getInt(jsonObject, "doesNotExist", -1);

    assertEquals(-1, actualResult);
  }

  @Test
  void testThatGetIntReturnsTheDefaultValueIfTheValueIsNotAnInteger() {
    var actualResult = JsonUtils.getInt(jsonObject, "myString", -1);

    assertEquals(-1, actualResult);
  }

  @Test
  void testThatGetStringReturnsTheCorrectString() {
    var actualResult = JsonUtils.getString(jsonObject, "myString", "default");

    assertEquals("Unittesting", actualResult);
  }

  @Test
  void testThatGetStringReturnsTheDefaultValueIfTheKeyDoesNotExist() {
    var actualResult = JsonUtils.getString(jsonObject, "doesNotExist", "default");

    assertEquals("default", actualResult);
  }

  @Test
  void testThatGetStringReturnsTheDefaultValueIfTheValueIsNotAString() {
    var actualResult = JsonUtils.getString(jsonObject, "myInt", "default");

    assertEquals("default", actualResult);
  }

  @Test
  void testThatGetStringReturnsTheDefaultValueIfTheValueIsNull() {
    var actualResult = JsonUtils.getString(jsonObject, "nullString", "default");

    assertEquals("default", actualResult);
  }
}
