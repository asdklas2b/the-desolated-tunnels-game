package nl.aimsites.nzbvuq.game.agentscriptconverter.checker;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.*;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.InventoryPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.MainPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option.InventoryPriorityOption;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option.MainPriorityOption;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.ConditionRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.PriorityRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleFoodRule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

class CheckerTest {

  private Checker sut;

  @BeforeEach
  void setUp() {
    sut = new Checker();
  }

  @Test
  void testThatCheckCallsCorrectMethod() {
    sut = Mockito.spy(new Checker());
    AST ast = new AST();
    sut.check(ast);
    verify(sut, times(1)).checkAgentScript(ast.getRoot());
  }

  @Test
  void testThatAgentScriptCallsCorrectMethods() {
    sut = Mockito.spy(new Checker());
    AgentScript agentScript = new AgentScript();
    agentScript.addChild(new PriorityRule());
    agentScript.addChild(new PriorityRule());
    agentScript.addChild(new SimpleFoodRule());
    agentScript.addChild(new SimpleFoodRule());
    agentScript.addChild(new SimpleFoodRule());
    agentScript.addChild(new ConditionRule());
    agentScript.addChild(new IfCondition());

    sut.checkAgentScript(agentScript);

    verify(sut, times(2)).checkPriorityRule(any());
  }

  @Test
  void testThatPriorityRuleChecksChildrenAndForSubject() {
    sut = Mockito.spy(sut);
    PriorityRule priorityRule = new PriorityRule();
    priorityRule.addChild(new MainPriority());
    Subject subject = new Subject("Subject");
    IfCondition ifCondition = new IfCondition();
    ifCondition.addChild(subject);
    IfClause ifClause = new IfClause();
    ifClause.addChild(ifCondition);
    priorityRule.addChild(ifClause);

    sut.checkPriorityRule(priorityRule);

    verify(sut, times(1)).checkMainPriority(any());
    assertEquals("Subject not allowed in priority rules.", subject.getError());

    // New priority replaces the former priority, so this one is separate.
    priorityRule.addChild(new InventoryPriority());

    sut.checkPriorityRule(priorityRule);

    verify(sut, times(1)).checkInventoryPriority(any());
  }

  @Test
  void testThatMainPriorityChecksChildren() {
    sut = Mockito.spy(sut);
    MainPriority mainPriority = new MainPriority();
    mainPriority.addChild(new MainPriorityOption("Option"));

    sut.checkMainPriority(mainPriority);

    verify(sut, times(1)).checkMainPriorityOption(any());
  }

  @Test
  void testThatInventoryPriorityChecksChildren() {
    sut = Mockito.spy(sut);
    InventoryPriority inventoryPriority = new InventoryPriority();
    inventoryPriority.addChild(new InventoryPriorityOption(1, "Type"));

    sut.checkInventoryPriority(inventoryPriority);

    verify(sut, times(1)).checkInventoryPriorityOption(any());
  }

  @Test
  void testThatInventoryOptionSetsErrorWhenIncorrectNumber() {
    InventoryPriorityOption inventoryPriorityOption = new InventoryPriorityOption(11, "Type");

    sut.checkInventoryPriorityOption(inventoryPriorityOption);

    assertEquals("Number must be between 0 and 10.", inventoryPriorityOption.getError());
  }

  @Test
  void testActionMatchesSubject() {
    Action action = new Action("flee");
    Subject subject = new Subject("monster");

    sut.checkIfActionMatchesSubject(action, subject);

    assertNull(action.getError());
  }

  @Test
  void testActionDoesNotMatchSubject() {
    Action action = new Action("pickup");
    Subject subject = new Subject("monster");

    sut.checkIfActionMatchesSubject(action, subject);

    assertEquals("Action does not match subject.", action.getError());
  }

  @Test
  void testThatIfClauseChecksIfActionMatchesSubject() {
    sut = Mockito.spy(sut);
    IfClause ifClause = new IfClause();
    Action action = new Action("Action");
    ifClause.addChild(action);
    Subject subject = new Subject("Subject");
    doNothing().when(sut).checkIfActionMatchesSubject(action, subject);

    sut.checkIfIfClauseMatchesSubject(ifClause, subject);

    verify(sut, times(1)).checkIfActionMatchesSubject(action, subject);
  }
}
