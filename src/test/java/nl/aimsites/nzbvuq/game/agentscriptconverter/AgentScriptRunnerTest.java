package nl.aimsites.nzbvuq.game.agentscriptconverter;

import nl.aimsites.nzbvuq.datastorage.crud.AICrud;
import nl.aimsites.nzbvuq.datastorage.crud.AICrudImpl;
import nl.aimsites.nzbvuq.datastorage.dto.PeerDto;
import nl.aimsites.nzbvuq.datastorage.dto.ai.AIDto;
import nl.aimsites.nzbvuq.datastorage.dto.ai.AgentDto;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.LessMore;
import nl.aimsites.nzbvuq.game.agentscriptconverter.exception.AgentScriptNotFoundException;
import nl.aimsites.nzbvuq.game.agentscriptconverter.generator.AgentBehaviourManager;
import nl.aimsites.nzbvuq.game.coderunner.CodeRunner;
import nl.aimsites.nzbvuq.game.entities.Entity;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.entities.character.monsters.Monster;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Strength;
import nl.aimsites.nzbvuq.game.entities.items.Weapon;
import nl.aimsites.nzbvuq.game.inputhandler.actions.*;
import nl.aimsites.nzbvuq.game.state.GameState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class AgentScriptRunnerTest {
  private final String PLAYER_NAME = "TestPlayerName";
  private final String AGENT_NAME = "TestAgentName";
  private AgentScriptRunner sut;
  private final CodeRunner mockedCodeRunner = Mockito.mock(CodeRunner.class);
  private final AICrud mockedAiCrud = Mockito.mock(AICrudImpl.class);

  @BeforeEach
  void setUp() {
    sut = new AgentScriptRunner(null);
    sut.setCodeRunner(mockedCodeRunner);
    sut.setAiCrud(mockedAiCrud);
  }

  @Test
  void getScriptFromDBWithValidInput() {
    String command = "TestCommand";
    PeerDto peer = new PeerDto();
    peer.setId(PLAYER_NAME);
    AIDto aiDto = new AgentDto();
    aiDto.setName(AGENT_NAME);
    aiDto.setPeer(peer);
    aiDto.setCommand(command);
    List<AIDto> list = new ArrayList<>();
    list.add(aiDto);

    Mockito.when(mockedAiCrud.read()).thenReturn(list);
    sut.getScriptFromDB(PLAYER_NAME, AGENT_NAME);
    assertEquals(command, sut.getScript());
  }

  @Test
  void getScriptFromDBWithUnknownInput() {
    PeerDto peer = new PeerDto();
    peer.setId(PLAYER_NAME);
    AIDto aiDto = new AgentDto();
    aiDto.setName(AGENT_NAME);
    aiDto.setPeer(peer);
    List<AIDto> list = new ArrayList<>();
    list.add(aiDto);

    Mockito.when(mockedAiCrud.read()).thenReturn(list);
    assertThrows(AgentScriptNotFoundException.class, () -> sut.getScriptFromDB("bla", "bla"));
  }

  @Test
  void getPlayerFromGameState() {
    assertEquals("Jorrit", sut.getPlayer().getName());
  }

  @Test
  void runScriptCallsCodeRunner() {
    String script = "testScript";
    Player player = new Player("12345");
    sut.setScript(script);
    sut.setPlayer(player);
    sut.runScript();
    var list = new ArrayList<>() {
      {
        add(AgentBehaviourManager.class);
        add(Player.class);
        add(Collectors.class);
        add(Entity.class);
        add(LessMore.class);
        add(AttackAction.class);
        add(MoveAction.class);
        add(Monster.class);
        add(BaseItem.class);
        add(PickupAction.class);
        add(Weapon.class);
        add(Strength.class);
        add(EquipAction.class);
        add(UseAction.class);
        add(List.class);
      }
    };
    Mockito.verify(mockedCodeRunner, Mockito.times(1)).run(sut.getScript(), list, player);
  }
}
