package nl.aimsites.nzbvuq.game.agentscriptconverter.generator;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.LessMore;
import nl.aimsites.nzbvuq.game.entities.character.Character;
import nl.aimsites.nzbvuq.game.entities.character.Inventory;
import nl.aimsites.nzbvuq.game.entities.items.*;
import nl.aimsites.nzbvuq.game.graph.Edge;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.Field;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.inputhandler.actions.plugins.MoveDirection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class AgentBehaviourManagerTest {
  private final Strength FOOD = new Strength();
  private final Weapon WEAPON = new Weapon();
  private final Armor ARMOR = new Armor();
  private final Weapon GOOD_WEAPON;
  private final Weapon BAD_WEAPON;
  private final ReachableTile TILE_LEFT = new Field(new Point(4, 5), 10);
  private final ReachableTile TILE_RIGHT = new Field(new Point(6, 5), 10);
  private final ReachableTile TILE_UP = new Field(new Point(5, 6), 10);
  private final ReachableTile TILE_DOWN = new Field(new Point(5, 4), 10);
  private ReachableTile currentTile = new Field(new Point(5, 5), 10);
  private AgentBehaviourManager sut;
  private Inventory inventory;

  public AgentBehaviourManagerTest() {
    this.GOOD_WEAPON = new Weapon();
    GOOD_WEAPON.setDamage(300);
    this.BAD_WEAPON = new Weapon();
    BAD_WEAPON.setDamage(10);
    currentTile.addAdjacentTile(new Edge(TILE_LEFT));
    currentTile.addAdjacentTile(new Edge(TILE_RIGHT));
    currentTile.addAdjacentTile(new Edge(TILE_UP));
    currentTile.addAdjacentTile(new Edge(TILE_DOWN));
  }

  @BeforeEach
  void setUp() {
    sut = new AgentBehaviourManager();
  }

  @Test
  void getPickupItemDecisionWithFoodReturnsTrue() {
    inventory = new Inventory();

    assertTrue(sut.getPickupItemDecision(FOOD, inventory, 1, 0, 0));
  }

  @Test
  void getPickupItemDecisionWithFoodReturnsFalse() {
    inventory = new Inventory();
    inventory.addItem(FOOD);

    assertFalse(sut.getPickupItemDecision(FOOD, inventory, 1, 0, 0));
  }

  @Test
  void getPickupItemDecisionWithWeaponReturnsTrue() {
    inventory = new Inventory();

    assertTrue(sut.getPickupItemDecision(WEAPON, inventory, 0, 1, 0));
  }

  @Test
  void getPickupItemDecisionWithWeaponReturnsFalse() {
    inventory = new Inventory();
    inventory.addItem(WEAPON);

    assertFalse(sut.getPickupItemDecision(WEAPON, inventory, 0, 1, 0));
  }

  @Test
  void getPickupItemDecisionWithArmorReturnsTrue() {
    Inventory inventory = new Inventory();

    assertTrue(sut.getPickupItemDecision(ARMOR, inventory, 0, 0, 1));
  }

  @Test
  void getPickupItemDecisionWithArmorReturnsFalse() {
    inventory = new Inventory();
    inventory.addItem(ARMOR);

    assertFalse(sut.getPickupItemDecision(ARMOR, inventory, 0, 0, 1));
  }

  @Test
  void getPickupItemDecisionWithInvalidItemReturnsFalse() {
    inventory = new Inventory();

    assertFalse(sut.getPickupItemDecision(new Storage(), inventory, 1, 1, 1));
  }

  @Test
  void getAttackDecisionReturnsTrueWhenPlayerIsStronger() {
    assertTrue(sut.getAttackDecision(true, 600, LessMore.MORE, 300));
  }

  @Test
  void getAttackDecisionReturnsFalseWhenPlayerIsWeaker() {
    assertFalse(sut.getAttackDecision(true, 300, LessMore.MORE, 600));
  }

  @Test
  void getAttackDecisionReturnsFightWhenLessMoreIsNull() {
    assertTrue(sut.getAttackDecision(true, 300, null, 600));
    assertFalse(sut.getAttackDecision(false, 300, null, 600));
  }

  @Test
  void getUseFoodDecisionReturnsTrueWhenUsingFood() {
    assertTrue(sut.getUseFoodDecision("food", 300, LessMore.LESS, 600, "apple"));
  }

  @Test
  void getUseFoodDecisionReturnsTrueWhenUsingApple() {
    assertTrue(sut.getUseFoodDecision("apple", 300, LessMore.LESS, 600, "apple"));
  }

  @Test
  void getUseFoodDecisionReturnsFalseWhenStrengthIsToHigh() {
    assertFalse(sut.getUseFoodDecision("food", 800, LessMore.LESS, 600, "apple"));
  }

  @Test
  void getUseFoodDecisionReturnsFalseWhenFoodTypeIsInvalid() {
    assertFalse(sut.getUseFoodDecision("apple", 300, LessMore.LESS, 600, "soup"));
  }

  @Test
  void getReplaceWeaponDecisionReturnsTrueWhenFindingBetterWeapon() {
    assertTrue(sut.getReplaceWeaponDecision(BAD_WEAPON, GOOD_WEAPON));
    assertTrue(sut.getReplaceWeaponDecision("best", BAD_WEAPON, GOOD_WEAPON));
  }

  @Test
  void getReplaceWeaponDecisionReturnsFalseWhenFindingWorseWeapon() {
    assertFalse(sut.getReplaceWeaponDecision(GOOD_WEAPON, BAD_WEAPON));
    assertFalse(sut.getReplaceWeaponDecision("best", GOOD_WEAPON, BAD_WEAPON));
  }

  @Test
  void getReplaceWeaponDecisionReturnsTrueForWorseWeapon() {
    assertTrue(sut.getReplaceWeaponDecision("worst", GOOD_WEAPON, BAD_WEAPON));
  }

  @Test
  void getReplaceWeaponDecisionReturnsFalseWithInvalidInput() {
    assertFalse(sut.getReplaceWeaponDecision("invalid input", GOOD_WEAPON, BAD_WEAPON));
  }

  @Test
  void getNextMoveDirectionReturnsNorth() {
    ArrayList<Character.TileMove> history = new ArrayList<>();
    history.add(new Character.TileMove(TILE_LEFT, MoveDirection.SOUTH));
    history.add(new Character.TileMove(TILE_RIGHT, MoveDirection.SOUTH));
    history.add(new Character.TileMove(TILE_DOWN, MoveDirection.SOUTH));

    assertEquals(MoveDirection.NORTH, sut.getNextMoveDirection(currentTile, history));
  }

  @Test
  void getNextMoveDirectionReturnsEast() {
    ArrayList<Character.TileMove> history = new ArrayList<>();
    history.add(new Character.TileMove(TILE_LEFT, MoveDirection.SOUTH));
    history.add(new Character.TileMove(TILE_UP, MoveDirection.SOUTH));
    history.add(new Character.TileMove(TILE_DOWN, MoveDirection.SOUTH));

    assertEquals(MoveDirection.EAST, sut.getNextMoveDirection(currentTile, history));
  }

  @Test
  void getNextMoveDirectionReturnsSouth() {
    ArrayList<Character.TileMove> history = new ArrayList<>();
    history.add(new Character.TileMove(TILE_LEFT, MoveDirection.SOUTH));
    history.add(new Character.TileMove(TILE_UP, MoveDirection.SOUTH));
    history.add(new Character.TileMove(TILE_RIGHT, MoveDirection.SOUTH));

    assertEquals(MoveDirection.SOUTH, sut.getNextMoveDirection(currentTile, history));
  }

  @Test
  void getNextMoveDirectionReturnsWest() {
    ArrayList<Character.TileMove> history = new ArrayList<>();
    history.add(new Character.TileMove(TILE_RIGHT, MoveDirection.SOUTH));
    history.add(new Character.TileMove(TILE_UP, MoveDirection.SOUTH));
    history.add(new Character.TileMove(TILE_DOWN, MoveDirection.SOUTH));

    assertEquals(MoveDirection.WEST, sut.getNextMoveDirection(currentTile, history));
  }

  @Test
  void getNextMoveDirectionReturnsRandomMoveDirection() {
    ArrayList<Character.TileMove> history = new ArrayList<>();
    history.add(new Character.TileMove(TILE_LEFT, MoveDirection.SOUTH));
    history.add(new Character.TileMove(TILE_UP, MoveDirection.SOUTH));
    history.add(new Character.TileMove(TILE_RIGHT, MoveDirection.SOUTH));

    assertNotNull(sut.getNextMoveDirection(currentTile, history));
  }
}
