package nl.aimsites.nzbvuq.game.agentscriptconverter;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.AST;
import nl.aimsites.nzbvuq.game.agentscriptconverter.exception.ParserFailedException;
import nl.aimsites.nzbvuq.game.agentscriptconverter.exception.UndefinedASTException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

class AgentScriptConverterTest {
  private AgentScriptConverter sut;
  private Pipeline mockedPipeline;
  private static final String SCRIPT = "Run if i see an enemy;";
  private static final AST AST = new AST();

  @BeforeEach
  void setUp() {
    sut = new AgentScriptConverter();
    mockedPipeline = mock(Pipeline.class);
    sut.setPipeline(mockedPipeline);
  }

  @Test
  void testIfConvertCallsPipeline() throws ParserFailedException {
    when(mockedPipeline.getAST()).thenReturn(AST);
    sut.convert(SCRIPT);
    Mockito.verify(mockedPipeline, times(1)).parseString(SCRIPT);
  }
}
