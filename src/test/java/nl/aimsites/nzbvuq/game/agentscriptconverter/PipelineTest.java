package nl.aimsites.nzbvuq.game.agentscriptconverter;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.AST;
import nl.aimsites.nzbvuq.game.agentscriptconverter.checker.Checker;
import nl.aimsites.nzbvuq.game.agentscriptconverter.exception.ParserFailedException;
import nl.aimsites.nzbvuq.game.agentscriptconverter.exception.UndefinedASTException;
import nl.aimsites.nzbvuq.game.agentscriptconverter.generator.Generator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class PipelineTest {
  private static final String SCRIPT = "Run when I see an enemy.";
  private Pipeline sut;
  private static final Checker mockedChecker = mock(Checker.class);
  private static final Generator mockedGenerator = mock(Generator.class);

  @BeforeEach
  void setup() {
    sut = new Pipeline();
  }

  @Test
  void testParseString() {
    sut.parseString(SCRIPT);
    assertNotNull(sut.getAST());
    assertEquals(new ArrayList<>(), sut.getErrors());
  }

  @Test
  void testIfCheckASTCallsChecker() throws UndefinedASTException {
    AST ast = new AST();
    sut.setAst(ast);
    sut.setChecker(mockedChecker);
    sut.checkAST();
    verify(mockedChecker, Mockito.times(1)).check(ast);
  }

  @Test
  void testIfCheckASTThrowsExceptionIfASTIsNull() {
    sut.setChecker(mockedChecker);
    assertThrows(UndefinedASTException.class, () -> sut.checkAST());
  }

  @Test
  void testIfGenerateJavaCodeCallsGenerator() throws UndefinedASTException {
    AST ast = new AST();
    sut.setAst(ast);
    sut.setGenerator(mockedGenerator);
    sut.generateJavaCode();
    verify(mockedGenerator, Mockito.times(1)).generate(ast);
  }

  @Test
  void testIfGenerateJavaCodeThrowsExceptionIfASTIsNull() {
    sut.setGenerator(mockedGenerator);
    assertThrows(UndefinedASTException.class, () -> sut.generateJavaCode());
  }
}
