package nl.aimsites.nzbvuq.game.agentscriptconverter.transformer;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.*;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.InventoryPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.MainPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.ConditionRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.PriorityRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleFoodRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleWeaponDefaultRule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class TransformerTest {

  private Transformer sut;

  @BeforeEach
  void setUp() {
    sut = new Transformer();
  }

  @Test
  void testTransformAgentScript() {
    sut = Mockito.spy(new Transformer());
    AST ast = new AST();
    sut.transform(ast);
    verify(sut, times(1)).transformAgentScript(ast.getRoot());
  }

  @Test
  void testThatAgentScriptCallsCorrectMethods() {
    sut = Mockito.spy(new Transformer());
    AgentScript agentScript = new AgentScript();
    agentScript.addChild(new PriorityRule());
    agentScript.addChild(new PriorityRule());
    agentScript.addChild(new SimpleFoodRule());
    agentScript.addChild(new SimpleFoodRule());
    agentScript.addChild(new SimpleFoodRule());
    agentScript.addChild(new ConditionRule());
    agentScript.addChild(new IfCondition());
    agentScript.addChild(new SimpleWeaponDefaultRule("best"));

    sut.transformAgentScript(agentScript);

    verify(sut, times(1)).transformPriorityRule(any());
    verify(sut, times(1)).transformConditionRule(any());
  }

  @Test
  void testConditionRuleDefault() {
    sut = Mockito.spy(sut);
    final int numberOfDefaultChilderen = 4;
    List<ConditionRule> conditionRule = new ArrayList<>();
    conditionRule = sut.transformConditionRule(conditionRule);

    assertEquals(numberOfDefaultChilderen, conditionRule.size());
  }

  @Test
  void testConditionRuleWithChildren() {
    sut = Mockito.spy(sut);
    final int numberOfDefaultChildren = 4;
    List<ConditionRule> conditionRuleList = new ArrayList<>();
    ConditionRule conditionRule = mock(ConditionRule.class);
    conditionRuleList.add(conditionRule);
    conditionRuleList.add(conditionRule);
    conditionRuleList.add(conditionRule);
    conditionRuleList.add(conditionRule);
    conditionRuleList.add(conditionRule);
    conditionRuleList.add(conditionRule);
    conditionRuleList = sut.transformConditionRule(conditionRuleList);

    assertEquals(numberOfDefaultChildren, conditionRuleList.size());
  }

  @Test
  void testPriorityTransformDefault() {
    sut = Mockito.spy(sut);
    List<PriorityRule> priorityRuleList = sut.transformPriorityRule(new ArrayList<>());

    assertEquals("PriorityRule", priorityRuleList.get(0).getNodeLabel());
  }

  @Test
  void testTransformIfClauseDefault() {
    sut = Mockito.spy(sut);
    IfClause ifClause = new IfClause();

    sut.transformIfClause(ifClause, "fight");

    assertEquals("IfCondition", ifClause.getChildren().get(0).getNodeLabel());
    assertEquals("Action: flee", ifClause.getChildren().get(1).getNodeLabel());
  }

  @Test
  void testTransformIfClauseWithoutActionFight() {
    sut = Mockito.spy(sut);
    IfClause ifClause = new IfClause();
    ifClause.addChild(new IfCondition());
    sut.transformIfClause(ifClause, "fight");

    assertEquals("Action: flee", ifClause.getChildren().get(1).getNodeLabel());
  }

  @Test
  void testTransformIfClauseWithoutActionRun() {
    sut = Mockito.spy(sut);
    IfClause ifClause = new IfClause();
    ifClause.addChild(new IfCondition());
    sut.transformIfClause(ifClause, "run");

    assertEquals("Action: fight", ifClause.getChildren().get(1).getNodeLabel());
  }

  @Test
  void testTransformConditionRuleEnemy() {
    sut = Mockito.spy(sut);
    final int expectedNumberOfDefaultRules = 4;
    List<ConditionRule> conditionRuleList = new ArrayList<>();
    var conditionRule = new ConditionRule();
    conditionRule.addChild(new Condition("seeing"));
    conditionRule.addChild(new Subject("enemy"));
    conditionRuleList.add(conditionRule);
    var newConditionRuleList = sut.transformConditionRule(conditionRuleList);

    assertEquals(expectedNumberOfDefaultRules, newConditionRuleList.size());
  }

  @Test
  void testTransformConditionRuleMonster() {
    sut = Mockito.spy(sut);
    final int expectedNumberOfDefaultRules = 4;
    List<ConditionRule> conditionRuleList = new ArrayList<>();
    var conditionRule = new ConditionRule();
    conditionRule.addChild(new Condition("seeing"));
    conditionRule.addChild(new Subject("monster"));
    conditionRuleList.add(conditionRule);
    var newConditionRuleList = sut.transformConditionRule(conditionRuleList);

    assertEquals(expectedNumberOfDefaultRules, newConditionRuleList.size());
  }

  @Test
  void testTransformConditionRuleAttribute() {
    sut = Mockito.spy(sut);
    final int expectedNumberOfDefaultRules = 4;
    List<ConditionRule> conditionRuleList = new ArrayList<>();
    var conditionRule = new ConditionRule();
    conditionRule.addChild(new Condition("seeing"));
    conditionRule.addChild(new Subject("attribute"));
    conditionRuleList.add(conditionRule);
    var newConditionRuleList = sut.transformConditionRule(conditionRuleList);

    assertEquals(expectedNumberOfDefaultRules, newConditionRuleList.size());
  }

  @Test
  void testTransformConditionRuleWeapon() {
    sut = Mockito.spy(sut);
    final int expectedNumberOfDefaultRules = 4;
    List<ConditionRule> conditionRuleList = new ArrayList<>();
    var conditionRule = new ConditionRule();
    conditionRule.addChild(new Condition("seeing"));
    conditionRule.addChild(new Subject("weapon"));
    conditionRuleList.add(conditionRule);
    var newConditionRuleList = sut.transformConditionRule(conditionRuleList);

    assertEquals(expectedNumberOfDefaultRules, newConditionRuleList.size());
  }

  @Test
  void testTransformPriorityRuleWithMainPriorityRule() {
    sut = Mockito.spy(sut);
    final int expectedNumberOfDefaultRules = 2;
    List<PriorityRule> priorityRuleList = new ArrayList<>();
    MainPriority mainPriority = new MainPriority();
    PriorityRule mainPriorityRule = new PriorityRule();
    mainPriorityRule.addChild(mainPriority);

    priorityRuleList.add(mainPriorityRule);
    List<PriorityRule> newPriorityRule = sut.transformPriorityRule(priorityRuleList);

    assertEquals(expectedNumberOfDefaultRules, newPriorityRule.size());
  }

  @Test
  void testTransformPriorityRuleWithInventoryPriority() {
    sut = Mockito.spy(sut);
    final int expectedNumberOfDefaultRules = 2;
    List<PriorityRule> priorityRuleList = new ArrayList<>();
    InventoryPriority inventoryPriority = new InventoryPriority();
    PriorityRule inventoryPriorityRule = new PriorityRule();
    inventoryPriorityRule.addChild(inventoryPriority);

    priorityRuleList.add(inventoryPriorityRule);
    List<PriorityRule> newPriorityRule = sut.transformPriorityRule(priorityRuleList);

    assertEquals(expectedNumberOfDefaultRules, newPriorityRule.size());
  }
}
