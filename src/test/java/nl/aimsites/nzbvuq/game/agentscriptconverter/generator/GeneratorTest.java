package nl.aimsites.nzbvuq.game.agentscriptconverter.generator;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.*;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.InventoryPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option.InventoryPriorityOption;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.ConditionRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.PriorityRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleFoodRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleWeaponDefaultRule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class GeneratorTest {
  private Generator sut;
  private AST ast;

  @BeforeEach
  void setUp() {
    sut = new Generator();
    ast = new AST();
  }

  @Test
  void generateGeneratesCorrectStringWithEmptyAST() {
    String expectedString =
        "var player = (Player) parameter[0];"
            + "AgentBehaviourManager agentBehaviourManager = new AgentBehaviourManager();"
            + "List<Entity> surroundingEntities ="
            + System.lineSeparator()
            + "player.getCurrentPosition().getOccupyingEntities().stream()"
            + System.lineSeparator()
            + ".filter(entity -> !entity.equals(player))"
            + System.lineSeparator()
            + ".collect(Collectors.toList());"
            + System.lineSeparator()
            + "for (Entity entity : surroundingEntities) { "
            + System.lineSeparator()
            + "return null;"
            + System.lineSeparator()
            + "}";

    String actual = sut.generate(ast);

    assertEquals(expectedString, actual);
  }

  @Test
  void generateGeneratesCorrectStringWithFilledAST() {
    AgentScript agentScript = new AgentScript();
    PriorityRule priorityRule = new PriorityRule();
    InventoryPriority expectedInventoryPriority = new InventoryPriority();
    priorityRule.addChild(expectedInventoryPriority);
    agentScript.addChild(priorityRule);

    InventoryPriority actual = sut.getInventoryPriority(agentScript);

    assertEquals(expectedInventoryPriority, actual);
  }

  @Test
  void generateConditionRulesCallsAndAppendsCorrectMethods() {
    AgentScript agentScript = new AgentScript();
    ConditionRule conditionRule1 = new ConditionRule();
    conditionRule1.addChild(new Subject("enemy"));
    ConditionRule conditionRule2 = new ConditionRule();
    conditionRule2.addChild(new Subject("enemy"));
    ConditionRule conditionRule3 = new ConditionRule();
    conditionRule3.addChild(new Subject("monster"));
    ConditionRule conditionRule4 = new ConditionRule();
    conditionRule4.addChild(new Subject("attribute"));
    ConditionRule conditionRule5 = new ConditionRule();
    conditionRule5.addChild(new Subject("weapon"));
    agentScript.addChild(conditionRule1);
    agentScript.addChild(conditionRule2);
    agentScript.addChild(conditionRule3);
    agentScript.addChild(conditionRule4);
    agentScript.addChild(conditionRule5);
    StringBuilder stringBuilder = new StringBuilder();
    InventoryPriority inventoryPriority = new InventoryPriority();
    sut = Mockito.spy(sut);
    doReturn("monster").when(sut).generateConditionRuleMonsterEnemy(any());
    doReturn("attribute").when(sut).generateConditionRuleAttribute(any());
    doReturn("weapon").when(sut).generateConditionRuleWeapon(any());
    String expectedString = "monster" + "monster" + "monster" + "attribute" + "weapon";

    sut.generateConditionRules(agentScript, stringBuilder, inventoryPriority);

    verify(sut, times(1)).generateConditionRuleMonsterEnemy(conditionRule1);
    verify(sut, times(1)).generateConditionRuleMonsterEnemy(conditionRule2);
    verify(sut, times(1)).generateConditionRuleMonsterEnemy(conditionRule3);
    verify(sut, times(1)).generateConditionRuleAttribute(inventoryPriority);
    verify(sut, times(1)).generateConditionRuleWeapon(conditionRule5);
    assertEquals(expectedString, stringBuilder.toString());
  }

  @Test
  void testThatGenerateMonsterEnemyReturnsCorrectString() {
    ConditionRule conditionRule = new ConditionRule();
    conditionRule.addChild(new Condition("i find"));
    conditionRule.addChild(new Subject("monster"));
    conditionRule.addChild(new Action("run"));
    String expected =
        "if (entity instanceof Monster) {"
            + System.lineSeparator()
            + "if (agentBehaviourManager.getAttackDecision(false,player.getStrength(), null, 0)){"
            + System.lineSeparator()
            + "new AttackAction().execute(entity.getName());"
            + System.lineSeparator()
            + "}else {"
            + System.lineSeparator()
            + "new MoveAction().execute(agentBehaviourManager.getNextMoveDirection(player.getCurrentPosition(), player.getHistory()).toString());"
            + System.lineSeparator()
            + "} return null;"
            + System.lineSeparator()
            + "}"
            + System.lineSeparator();

    String actual = sut.generateConditionRuleMonsterEnemy(conditionRule);

    assertEquals(expected, actual);
  }

  @Test
  void testThatGenerateConditionRuleAttributeReturnsCorrectString() {
    InventoryPriority inventoryPriority = new InventoryPriority();
    inventoryPriority.addChild(new InventoryPriorityOption(5, "food"));
    inventoryPriority.addChild(new InventoryPriorityOption(5, "armor"));
    inventoryPriority.addChild(new InventoryPriorityOption(5, "weapon"));
    String expected =
        "if (entity instanceof BaseItem) {"
            + System.lineSeparator()
            + "if (agentBehaviourManager.getPickupItemDecision((BaseItem) entity, player.getInventory(), 5, 5, 5)) {"
            + System.lineSeparator()
            + "new PickupAction().execute(entity.getName());"
            + System.lineSeparator()
            + "return null;"
            + System.lineSeparator()
            + "}";

    String actual = sut.generateConditionRuleAttribute(inventoryPriority);

    assertEquals(expected, actual);
  }

  @Test
  void testThatGenerateConditionRuleWeaponReturnsCorrectString() {
    ConditionRule conditionRule = new ConditionRule();
    IfClause ifClause = new IfClause();
    IfCondition ifCondition = new IfCondition(LessMore.LESS);
    ifClause.addChild(ifCondition);
    conditionRule.addChild(ifClause);
    String expected =
        "if (entity instanceof Weapon) {"
            + System.lineSeparator()
            + "if (agentBehaviourManager.getReplaceWeaponDecision(\"best\",player.getEquippedWeapon(), (Weapon) entity)){"
            + System.lineSeparator()
            + "new EquipAction().execute(entity.getName());"
            + System.lineSeparator()
            + "}"
            + System.lineSeparator()
            + "else {"
            + System.lineSeparator()
            + "new MoveAction().execute(agentBehaviourManager.getNextMoveDirection(player.getCurrentPosition(), player.getHistory()).toString());"
            + System.lineSeparator()
            + "} "
            + System.lineSeparator()
            + "return null;"
            + System.lineSeparator()
            + "} }"
            + System.lineSeparator();

    String actual = sut.generateConditionRuleWeapon(conditionRule);

    assertEquals(expected, actual);
  }

  @Test
  void generateSimpleRulesCallsAndAppendsCorrectMethods() {
    AgentScript agentScript = new AgentScript();
    SimpleFoodRule simpleFoodRule = new SimpleFoodRule();
    SimpleWeaponDefaultRule simpleWeaponDefaultRule = new SimpleWeaponDefaultRule("Test");
    agentScript.addChild(simpleFoodRule);
    agentScript.addChild(simpleWeaponDefaultRule);
    StringBuilder stringBuilder = new StringBuilder();
    sut = Mockito.spy(sut);
    doReturn("SimpleFoodRule").when(sut).generateSimpleFoodRule(any());
    doReturn("SimpleWeaponDefaultRule").when(sut).generateSimpleWeaponDefaultRule(any());
    String expectedString = "SimpleFoodRule" + "SimpleWeaponDefaultRule";

    sut.generateSimpleRules(agentScript, stringBuilder);

    verify(sut, times(1)).generateSimpleFoodRule(simpleFoodRule);
    verify(sut, times(1)).generateSimpleWeaponDefaultRule(simpleWeaponDefaultRule);
    assertEquals(expectedString, stringBuilder.toString());
  }

  @Test
  void testThatGenerateSimpleFoodRuleReturnsCorrectString() {
    SimpleFoodRule simpleFoodRule = new SimpleFoodRule();
    simpleFoodRule.addChild(new Food("soup"));
    simpleFoodRule.addChild(new StrengthComparison(LessMore.LESS, 500));
    String expected =
        "for (BaseItem item : player.getInventory().getItems()) {"
            + System.lineSeparator()
            + "if (item instanceof Strength) {"
            + System.lineSeparator()
            + "if (agentBehaviourManager.getUseFoodDecision(\"soup\", player.getStrength(), LessMore.LESS, 500, item.getName())) {"
            + System.lineSeparator()
            + "new UseAction().execute(\"soup\");"
            + System.lineSeparator()
            + "return null;"
            + System.lineSeparator()
            + "} } }";

    String actual = sut.generateSimpleFoodRule(simpleFoodRule);

    assertEquals(expected, actual);
  }

  @Test
  void testThatGenerateSimpleWeaponDefaultRuleReturnsCorrectString() {
    SimpleWeaponDefaultRule simpleWeaponDefaultRule = new SimpleWeaponDefaultRule("best");
    String expected =
        "for (BaseItem item : player.getInventory().getItems()) {"
            + System.lineSeparator()
            + "if (item instanceof Weapon) {"
            + System.lineSeparator()
            + "if (agentBehaviourManager.getReplaceWeaponDecision(\"best\", player.getEquippedWeapon(), (Weapon) item)) {"
            + System.lineSeparator()
            + "new EquipAction().execute(item.getName());"
            + System.lineSeparator()
            + "return null;"
            + System.lineSeparator()
            + "} } }";

    String actual = sut.generateSimpleWeaponDefaultRule(simpleWeaponDefaultRule);

    assertEquals(expected, actual);
  }
}
