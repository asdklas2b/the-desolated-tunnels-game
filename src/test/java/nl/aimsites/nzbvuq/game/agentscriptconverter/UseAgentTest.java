package nl.aimsites.nzbvuq.game.agentscriptconverter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Timer;

import static org.mockito.Mockito.mock;

class UseAgentTest {
  private UseAgent sut;
  private AgentScriptRunner agentScriptRunner = mock(AgentScriptRunner.class);
  private Timer timer = mock(Timer.class);

  @BeforeEach
  void setup() {
    sut = new UseAgent(null);
    sut.setAgentScriptRunner(agentScriptRunner);
    sut.setTimer(timer);
  }

  @Test
  void startUsingAgent() {
    String playerName = "Jorrit";
    String agentName = "TestScript";

    sut.startUsingAgent(playerName, agentName);
    Assertions.assertNotNull(sut.getAgentScriptRunner());
  }

  @Test
  void stopUsingAgent() {
    sut.stopUsingAgent();
    Mockito.verify(timer).cancel();
  }
}
