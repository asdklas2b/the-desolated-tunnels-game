package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class IfConditionTest {
  private IfCondition sut;
  private static final Subject SUBJECT = new Subject("Test");
  private static final StrengthComparison STRENGTH_COMPARISON =
      new StrengthComparison(LessMore.MORE, 300);

  @BeforeEach
  void setUp() {
    sut = new IfCondition();
  }

  @Test
  void getChildren() {
    sut.setSubject(SUBJECT);
    sut.setStrengthComparison(STRENGTH_COMPARISON);
    ArrayList<ASTNode> expected = new ArrayList<>();
    expected.add(SUBJECT);
    expected.add(STRENGTH_COMPARISON);
    assertEquals(expected.get(0).getNodeLabel(), sut.getChildren().get(0).getNodeLabel());
    assertEquals(expected.get(1).getNodeLabel(), sut.getChildren().get(1).getNodeLabel());
  }

  @Test
  void addChildSubject() {
    sut.addChild(SUBJECT);
    assertEquals(SUBJECT.getNodeLabel(), sut.getSubject().getNodeLabel());
  }

  @Test
  void addChildStrengthComparison() {
    sut.addChild(STRENGTH_COMPARISON);
    assertEquals(STRENGTH_COMPARISON.getNodeLabel(), sut.getStrengthComparison().getNodeLabel());
  }

  @Test
  void removeChildSubject() {
    sut.setSubject(SUBJECT);
    sut.removeChild(SUBJECT);
    assertNull(sut.getSubject());
  }

  @Test
  void removeChildStrengthComparison() {
    sut.setStrengthComparison(STRENGTH_COMPARISON);
    sut.removeChild(STRENGTH_COMPARISON);
    assertNull(sut.getStrengthComparison());
  }
}
