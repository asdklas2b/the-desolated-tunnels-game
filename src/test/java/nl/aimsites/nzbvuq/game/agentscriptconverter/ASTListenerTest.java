package nl.aimsites.nzbvuq.game.agentscriptconverter;

import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.*;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.InventoryPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.MainPriority;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option.InventoryPriorityOption;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.priority.option.MainPriorityOption;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.ConditionRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.PriorityRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleFoodRule;
import nl.aimsites.nzbvuq.game.agentscriptconverter.ast.rule.simplerule.SimpleWeaponDefaultRule;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class ASTListenerTest {
  private ASTListener sut;

  @BeforeEach
  void setup() throws Exception {
    sut = new ASTListener();
    sut.getStack().push(new AgentScript());
  }

  @Test
  void testEnterAgentScript() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.AgentScriptContext.class);

    final String expect = "AgentScript";
    sut.enterAgentScript(ctx);
    // Assert
    assertEquals(expect, sut.getStack().get(1).getNodeLabel());
  }

  @Test
  void testExitAgentScript() {
    // Arrange
    sut.getStack().push(new AgentScript());
    sut.exitAgentScript(mock(AgentsGrammarParser.AgentScriptContext.class));
    // Assert
    assertEquals(AgentScript.class, sut.getAST().getRoot().getClass());
  }

  @Test
  void testEnterCondition() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.ConditionContext.class);

    Mockito.when(ctx.getText()).thenReturn("I see");
    sut.enterCondition(ctx);
    // Assert
    String expected = "seeing";
    assertEquals(expected, sut.getStack().get(1).getName());
  }

  @Test
  void testExitCondition() {
    // Arrange
    sut.getStack().push(new Condition("I see"));
    sut.exitCondition(mock(AgentsGrammarParser.ConditionContext.class));
    // Assert
    assertEquals(Condition.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterSubject() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.SubjectContext.class);

    Mockito.when(ctx.getText()).thenReturn("an enemy");
    sut.enterSubject(ctx);
    // Assert
    String expected = "enemy";
    assertEquals(expected, sut.getStack().get(1).getName());
  }

  @Test
  void testExitSubject() {
    // Arrange
    sut.getStack().push(new Subject("enemy"));
    sut.exitSubject(mock(AgentsGrammarParser.SubjectContext.class));
    // Assert
    assertEquals(Subject.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterAction() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.ActionContext.class);

    Mockito.when(ctx.getText()).thenReturn("Run");
    sut.enterAction(ctx);
    // Assert
    String expected = "run";
    assertEquals(expected, sut.getStack().get(1).getName());
  }

  @Test
  void testExitAction() {
    // Arrange
    sut.getStack().push(new Action("run"));
    sut.exitAction(mock(AgentsGrammarParser.ActionContext.class));
    // Assert
    assertEquals(Action.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterConditionRule() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.ConditionRuleContext.class);
    final String expect = "ConditionRule";
    sut.enterConditionRule(ctx);
    // Assert
    assertEquals(expect, sut.getStack().get(1).getNodeLabel());
  }

  @Test
  void testExitConditionRule() {
    // Arrange
    sut.getStack().push(new ConditionRule());
    sut.exitConditionRule(mock(AgentsGrammarParser.ConditionRuleContext.class));
    // Assert
    assertEquals(ConditionRule.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterIfConditionCurrentWeapon() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.IfConditionContext.class);
    final var text = "current weapon has lower strength";
    // Assert
    Mockito.when(ctx.getChildCount()).thenReturn(1);
    Mockito.when(ctx.getText()).thenReturn(text);
    sut.enterIfCondition(ctx);

    assertEquals(
      new IfCondition(text).getCondition(), ((IfCondition) sut.getStack().get(1)).getCondition());
  }

  @Test
  void testEnterIfConditionStrengthComparisonWithoutSubject() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.IfConditionContext.class);
    final String text = "strength < 500";
    final String expect = "IfCondition";

    // Assert
    Mockito.when(ctx.getChildCount()).thenReturn(1);
    Mockito.when(ctx.getText()).thenReturn(text);
    sut.enterIfCondition(ctx);

    assertEquals(expect, sut.getStack().get(1).getNodeLabel());
  }

  @Test
  void testEnterIfConditionStrengthComparisonWithSubject() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.IfConditionContext.class);
    var strengthComparisonCtx = mock(AgentsGrammarParser.StrengthComparisonContext.class);
    final String text = "monster strength < 500";
    final String expect = "IfCondition";

    // Assert
    Mockito.when(ctx.getChildCount()).thenReturn(2);
    Mockito.when(ctx.getText()).thenReturn(text);
    Mockito.when(strengthComparisonCtx.getText()).thenReturn("strength < 500");
    Mockito.when(ctx.getChild(1)).thenReturn(strengthComparisonCtx);
    sut.enterIfCondition(ctx);

    assertEquals(expect, sut.getStack().get(1).getNodeLabel());
  }

  @Test
  void testEnterIfConditionIHaveLess() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.IfConditionContext.class);
    var terminalNodeImplCTX1 = mock(TerminalNodeImpl.class);
    var terminalNodeImplCTX2 = mock(TerminalNodeImpl.class);
    final String text1 = "i have";
    final String text2 = "less strength";

    var less = LessMore.LESS;
    // Assert
    Mockito.when(ctx.getChildCount()).thenReturn(2);
    Mockito.when(terminalNodeImplCTX1.getText()).thenReturn(text1);
    Mockito.when(terminalNodeImplCTX2.getText()).thenReturn(text2);
    Mockito.when(ctx.getChild(0)).thenReturn(terminalNodeImplCTX1);
    Mockito.when(ctx.getChild(1)).thenReturn(terminalNodeImplCTX2);

    sut.enterIfCondition(ctx);

    assertEquals(
      new IfCondition(less).getIHaveCondition(),
      ((IfCondition) sut.getStack().get(1)).getIHaveCondition());
  }

  @Test
  void testEnterIfConditionIHaveMore() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.IfConditionContext.class);
    var terminalNodeImplCTX1 = mock(TerminalNodeImpl.class);
    var terminalNodeImplCTX2 = mock(TerminalNodeImpl.class);
    final String text1 = "i have";
    final String text2 = "more strength";

    var more = LessMore.MORE;
    // Assert
    Mockito.when(ctx.getChildCount()).thenReturn(2);
    Mockito.when(terminalNodeImplCTX1.getText()).thenReturn(text1);
    Mockito.when(terminalNodeImplCTX2.getText()).thenReturn(text2);
    Mockito.when(ctx.getChild(0)).thenReturn(terminalNodeImplCTX1);
    Mockito.when(ctx.getChild(1)).thenReturn(terminalNodeImplCTX2);

    sut.enterIfCondition(ctx);

    assertEquals(
      new IfCondition(more).getIHaveCondition(),
      ((IfCondition) sut.getStack().get(1)).getIHaveCondition());
  }

  @Test
  void testExitIfCondition() {
    // Arrange
    sut.getStack().push(new IfCondition(""));
    sut.exitIfCondition(mock(AgentsGrammarParser.IfConditionContext.class));
    // Assert
    assertEquals(IfCondition.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterIfClause() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.IfClauseContext.class);

    final String expect = "IfClause";
    sut.enterIfClause(ctx);
    // Assert
    assertEquals(expect, sut.getStack().get(1).getNodeLabel());
  }

  @Test
  void testExitIfClause() {
    // Arrange
    sut.getStack().push(new IfClause());
    sut.exitIfClause(mock(AgentsGrammarParser.IfClauseContext.class));
    // Assert
    assertEquals(IfClause.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterPriorityRule() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.PriorityRuleContext.class);

    final String expect = "PriorityRule";
    sut.enterPriorityRule(ctx);
    // Assert
    assertEquals(expect, sut.getStack().get(1).getNodeLabel());
  }

  @Test
  void testExitPriorityRule() {
    // Arrange
    sut.getStack().push(new PriorityRule());
    sut.exitPriorityRule(mock(AgentsGrammarParser.PriorityRuleContext.class));
    // Assert
    assertEquals(PriorityRule.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterMainPriority() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.MainPriorityContext.class);

    final String expect = "MainPriority";
    sut.enterMainPriority(ctx);
    // Assert
    assertEquals(expect, sut.getStack().get(1).getNodeLabel());
  }

  @Test
  void testExitMainPriority() {
    // Arrange
    sut.getStack().push(new MainPriority());
    sut.exitMainPriority(mock(AgentsGrammarParser.MainPriorityContext.class));
    // Assert
    assertEquals(MainPriority.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterInventoryPriority() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.InventoryPriorityContext.class);

    final String expect = "InventoryPriority";
    sut.enterInventoryPriority(ctx);
    // Assert
    assertEquals(expect, sut.getStack().get(1).getNodeLabel());
  }

  @Test
  void testExitInventoryPriority() {
    // Arrange
    sut.getStack().push(new InventoryPriority());
    sut.exitInventoryPriority(mock(AgentsGrammarParser.InventoryPriorityContext.class));
    // Assert
    assertEquals(InventoryPriority.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterSimpleFoodRule() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.SimpleFoodRuleContext.class);

    final String expect = "SimpleFoodRule";
    sut.enterSimpleFoodRule(ctx);
    // Assert
    assertEquals(expect, sut.getStack().get(1).getNodeLabel());
  }

  @Test
  void testExitSimpleFoodRule() {
    // Arrange
    sut.getStack().push(new SimpleFoodRule());
    sut.exitSimpleFoodRule(mock(AgentsGrammarParser.SimpleFoodRuleContext.class));
    // Assert
    assertEquals(SimpleFoodRule.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterSimpleWeaponDefaultRule() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.SimpleWeaponDefaultRuleContext.class);
    var terminalNodeImplCTX = mock(TerminalNodeImpl.class);

    String expected = "best";
    Mockito.when(terminalNodeImplCTX.getText()).thenReturn(expected);
    Mockito.when(ctx.getChild(1)).thenReturn(terminalNodeImplCTX);

    sut.enterSimpleWeaponDefaultRule(ctx);
    // Assert
    assertEquals(expected, ((SimpleWeaponDefaultRule) sut.getStack().get(1)).getType());
  }

  @Test
  void testExitSimpleWeaponDefaultRule() {
    // Arrange

    sut.getStack().push(new SimpleWeaponDefaultRule("best"));
    sut.exitSimpleWeaponDefaultRule(mock(AgentsGrammarParser.SimpleWeaponDefaultRuleContext.class));
    // Assert
    assertEquals(SimpleWeaponDefaultRule.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterFood() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.FoodContext.class);

    String expected = "apple";
    Mockito.when(ctx.getText()).thenReturn(expected);
    sut.enterFood(ctx);
    // Assert
    assertEquals(expected, sut.getStack().get(1).getName());
  }

  @Test
  void testEnterFoodWrongValue() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.FoodContext.class);

    Mockito.when(ctx.getText()).thenReturn("car");
    sut.enterFood(ctx);
    // Assert
    String expected = "UNKNOWN";
    assertEquals(expected, sut.getStack().get(1).getName());
  }

  @Test
  void testExitFood() {
    // Arrange
    sut.getStack().push(new Food("apple"));
    sut.exitFood(mock(AgentsGrammarParser.FoodContext.class));
    // Assert
    assertEquals(Food.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterMainPriorityOption() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.MainPriorityOptionContext.class);

    String expected = "kill enemies";
    Mockito.when(ctx.getText()).thenReturn(expected);
    sut.enterMainPriorityOption(ctx);
    // Assert
    assertEquals(expected, sut.getStack().get(1).getName());
  }

  @Test
  void testExitMainPriorityOption() {
    // Arrange
    sut.getStack().push(new MainPriorityOption("kill enemies"));
    sut.exitMainPriorityOption(mock(AgentsGrammarParser.MainPriorityOptionContext.class));
    // Assert
    assertEquals(MainPriorityOption.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterInventoryPriorityOption() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.InventoryPriorityOptionContext.class);
    var terminalNodeImplCTX1 = mock(TerminalNodeImpl.class);
    var terminalNodeImplCTX2 = mock(TerminalNodeImpl.class);

    Mockito.when(terminalNodeImplCTX1.getText()).thenReturn("10");
    Mockito.when(terminalNodeImplCTX2.getText()).thenReturn("food");
    Mockito.when(ctx.getChild(0)).thenReturn(terminalNodeImplCTX1);
    Mockito.when(ctx.getChild(1)).thenReturn(terminalNodeImplCTX2);

    sut.enterInventoryPriorityOption(ctx);
    // Assert
    int expectedNumber = 10;
    assertEquals(expectedNumber, ((InventoryPriorityOption) sut.getStack().get(1)).getNumber());
    String expectedType = "food";
    assertEquals(expectedType, ((InventoryPriorityOption) sut.getStack().get(1)).getType());
  }

  @Test
  void testExitInventoryPriorityOption() {
    // Arrange

    sut.getStack().push(new InventoryPriorityOption(10, "food"));
    sut.exitInventoryPriorityOption(mock(AgentsGrammarParser.InventoryPriorityOptionContext.class));
    // Assert
    assertEquals(InventoryPriorityOption.class, getFirstChildOfRoot().getClass());
  }

  @Test
  void testEnterStrengthComparisonLess() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.StrengthComparisonContext.class);
    var terminalNodeImplCTX1 = mock(TerminalNodeImpl.class);
    var terminalNodeImplCTX2 = mock(TerminalNodeImpl.class);

    Mockito.when(terminalNodeImplCTX1.getText()).thenReturn("<");
    Mockito.when(terminalNodeImplCTX2.getText()).thenReturn("500");
    Mockito.when(ctx.getChild(1)).thenReturn(terminalNodeImplCTX1);
    Mockito.when(ctx.getChild(2)).thenReturn(terminalNodeImplCTX2);

    sut.enterStrengthComparison(ctx);
    // Assert
    assertEquals(LessMore.LESS, ((StrengthComparison) sut.getStack().get(1)).getLessMore());
    int expectedNumber = 500;
    assertEquals(expectedNumber, ((StrengthComparison) sut.getStack().get(1)).getNumber());
  }

  @Test
  void testEnterStrengthComparisonMore() {
    // Arrange
    var ctx = mock(AgentsGrammarParser.StrengthComparisonContext.class);
    var terminalNodeImplCTX1 = mock(TerminalNodeImpl.class);
    var terminalNodeImplCTX2 = mock(TerminalNodeImpl.class);

    Mockito.when(terminalNodeImplCTX1.getText()).thenReturn("over");
    Mockito.when(terminalNodeImplCTX2.getText()).thenReturn("100");
    Mockito.when(ctx.getChild(1)).thenReturn(terminalNodeImplCTX1);
    Mockito.when(ctx.getChild(2)).thenReturn(terminalNodeImplCTX2);

    sut.enterStrengthComparison(ctx);
    // Assert
    assertEquals(LessMore.MORE, ((StrengthComparison) sut.getStack().get(1)).getLessMore());
    int expectedNumber = 100;
    assertEquals(expectedNumber, ((StrengthComparison) sut.getStack().get(1)).getNumber());
  }

  @Test
  void testExitStrengthComparison() {
    // Arrange
    sut.getStack().push(new StrengthComparison(LessMore.MORE, 300));
    sut.exitStrengthComparison(mock(AgentsGrammarParser.StrengthComparisonContext.class));
    // Assert
    assertEquals(StrengthComparison.class, getFirstChildOfRoot().getClass());
  }

  private ASTNode getFirstChildOfRoot() {
    return sut.getStack().get(0).getChildren().get(0);
  }
}
