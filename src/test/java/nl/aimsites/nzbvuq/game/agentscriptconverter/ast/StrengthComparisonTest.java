package nl.aimsites.nzbvuq.game.agentscriptconverter.ast;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StrengthComparisonTest {
  private StrengthComparison sut;
  private static final LessMore MORE = LessMore.MORE;
  private static final int NUMBER = 300;

  @BeforeEach
  void setUp() {
    sut = new StrengthComparison(MORE, NUMBER);
  }

  @Test
  void testToString() {
    assertEquals("StrengthComparison: " + MORE + " " + NUMBER + System.lineSeparator(), sut.toString());
  }
}
