package nl.aimsites.nzbvuq.game.graph;

import nl.aimsites.nzbvuq.game.graph.tiles.Field;
import nl.aimsites.nzbvuq.game.graph.tiles.Room;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EdgeTest {

  @Test
  void nullDestinationThrowsIllegalArgumentException() {
    Assertions.assertThrows(IllegalArgumentException.class, () -> new Edge(null));
  }

  @Test
  void nullDestinationThrowsIllegalArgumentExceptionWithCorrectMessage() {
    final String expectedMessage = "Destination Tile can not be null!";

    final String actualMessage =
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Edge(null)).getMessage();

    Assertions.assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void edgeEqualityCheckWorksCorrectly() {
    Edge edge = new Edge(new Room(new Point(1, 1), 1));
    Edge edge2 = new Edge(new Room(new Point(1, 1), 1));

    Assertions.assertEquals(edge2, edge);
  }

  @Test
  void edgeEqualityCheckWorksCorrectlyWhenObjectNotSame() {
    Edge edge = new Edge(new Field(new Point(1, 1), 1));
    Edge edge2 = new Edge(new Room(new Point(1, 0), 1));

    Assertions.assertNotEquals(edge, edge2);
  }
}
