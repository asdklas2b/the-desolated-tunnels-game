package nl.aimsites.nzbvuq.game.graph;

import nl.aimsites.nzbvuq.game.entities.Entity;
import nl.aimsites.nzbvuq.game.graph.tiles.Field;
import nl.aimsites.nzbvuq.game.graph.tiles.Room;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;

class TileTest {
  private Room sut;

  @Mock private Edge mockedEdge;
  @Mock private Entity mockedEntity;

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);

    sut = new Room(new Point(1, 1), 1);
  }

  @Test
  void connectTileToItselfThrowsIllegalArgumentException() {
    when(mockedEdge.getDestinationTile()).thenReturn(sut);

    var edge = new Edge(sut);

    Assertions.assertThrows(
        IllegalArgumentException.class, () -> sut.addAdjacentTile(edge));
  }

  @Test
  void connectTileToItselfThrowsIllegalArgumentExceptionWithCorrectMessage() {
    when(mockedEdge.getDestinationTile()).thenReturn(sut);

    final String expectedMessage = "You can not connect a tile to itself!";
    final String actualMessage =
        Assertions.assertThrows(
                IllegalArgumentException.class, () -> sut.addAdjacentTile(mockedEdge))
            .getMessage();

    Assertions.assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addDuplicateEdgeThrowsIllegalArgumentException() {
    when(mockedEdge.getDestinationTile()).thenReturn(new Field(new Point(0, 0), 1));
    when(mockedEdge.getDestinationTile()).thenReturn(new Room(new Point(0, 0), 1));

    sut.addAdjacentTile(mockedEdge);

    Assertions.assertThrows(IllegalArgumentException.class, () -> sut.addAdjacentTile(mockedEdge));
  }

  @Test
  void addDuplicateEdgeThrowsIllegalArgumentExceptionWithCorrectMessage() {
    when(mockedEdge.getDestinationTile()).thenReturn(new Field(new Point(0, 0), 1));
    when(mockedEdge.getDestinationTile()).thenReturn(new Room(new Point(0, 0), 1));

    sut.addAdjacentTile(mockedEdge);

    final String expectedMessage = "Duplicate edge!";
    final String actualMessage =
        Assertions.assertThrows(
                IllegalArgumentException.class, () -> sut.addAdjacentTile(mockedEdge))
            .getMessage();

    Assertions.assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addEdgeWithNoDestinationTileThrowsIllegalArgumentException() {
    when(mockedEdge.getDestinationTile()).thenReturn(null);

    Assertions.assertThrows(IllegalArgumentException.class, () -> sut.addAdjacentTile(mockedEdge));
  }

  @Test
  void addWithNoDestinationTileThrowsIllegalArgumentExceptionWithCorrectMessage() {
    when(mockedEdge.getDestinationTile()).thenReturn(null);

    final String expectedMessage = "You can not add an edge that has no destination tile!";
    final String actualMessage =
        Assertions.assertThrows(
                IllegalArgumentException.class, () -> sut.addAdjacentTile(mockedEdge))
            .getMessage();

    Assertions.assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addEntityThatIsNullThrowsIllegalArgumentException() {
    Assertions.assertThrows(IllegalArgumentException.class, () -> sut.addEntity(null));
  }

  @Test
  void addEntityThatIsNullThrowsIllegalArgumentExceptionWithCorrectMessage() {

    final String expectedMessage = "You can not add an entity that is null!";
    final String actualMessage =
        Assertions.assertThrows(IllegalArgumentException.class, () -> sut.addEntity(null))
            .getMessage();

    Assertions.assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void addEntityThatAlreadyExistsInTheOccupyingEntitiesListThrowsIllegalArgumentException() {
    sut.addEntity(mockedEntity);

    Assertions.assertThrows(IllegalArgumentException.class, () -> sut.addEntity(mockedEntity));
  }

  @Test
  void
      addEntityThatAlreadyExistsInTheOccupyingEntitiesListThrowsIllegalArgumentExceptionWithCorrectMessage() {
    sut.addEntity(mockedEntity);

    final String expectedMessage = "Duplicate entity!";
    final String actualMessage =
        Assertions.assertThrows(IllegalArgumentException.class, () -> sut.addEntity(mockedEntity))
            .getMessage();

    Assertions.assertEquals(expectedMessage, actualMessage);
  }

  @Test
  void constructingTileWorksCorrectly() {
    Room tile = new Room(new Point(1, 1), 1);

    Assertions.assertEquals(new Point(1, 1), tile.getCoordinates());
    Assertions.assertEquals(1, tile.getCostOfTraversal());
  }
}
