package nl.aimsites.nzbvuq.game.graph;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GraphTest {
  private Graph sut;

  @BeforeEach
  void setup() {
    sut = new Graph(0, 0, 2, 1);
  }

  @Test
  void passingNullAsArgumentThrowsIllegalArgumentException() {
    Assertions.assertThrows(IllegalArgumentException.class, () -> sut.addTile(null));
  }
}
