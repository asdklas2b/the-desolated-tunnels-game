package nl.aimsites.nzbvuq.game.graph;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PointTest {

  @Test
  void pointEqualityCheckWorksCorrectly() {
    Point point1 = new Point(1, 1);
    Point point2 = new Point(1, 1);

    Assertions.assertEquals(point1, point2);
  }

  @Test
  void pointReferenceEqualityCheckWorksCorrectly() {
    Point point1 = new Point(1, 1);

    Assertions.assertEquals(point1, point1);
  }

  @Test
  void nullEqualityReturnsFalse() {
    Point point1 = new Point(1, 1);

    Assertions.assertNotEquals(null, point1);
  }

  @Test
  void pointEqualityCheckWorksCorrectlyWhenObjectNotSame() {
    Point point1 = new Point(1, 1);
    Point point2 = new Point(1, 2);

    Assertions.assertNotEquals(point1, point2);
  }
}
