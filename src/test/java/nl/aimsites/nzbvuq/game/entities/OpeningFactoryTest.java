package nl.aimsites.nzbvuq.game.entities;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OpeningFactoryTest {
  private static final String EXPECTED_OPENINGS_STRING =
      "OpeningFactory{availableOpenings=[Opening{name='Wooden door', isOpened=false}, Opening{name='Window', isOpened=false}]}";

  @Test
  void testThatOpeningFactoryGetsTheRightItems() {
    assertDoesNotThrow(
        () -> {
          var sut = new OpeningEntityFactory();

          assertEquals(EXPECTED_OPENINGS_STRING, sut.toString());
        });
  }
}
