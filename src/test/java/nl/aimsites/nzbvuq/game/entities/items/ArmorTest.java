package nl.aimsites.nzbvuq.game.entities.items;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ArmorTest {
  private static final int WEIGHT = 10;
  private static final String NAME = "Sword";

  Armor sut;

  @BeforeEach
  void setUp() {
    sut = new Armor();
    sut.setName(NAME);
    sut.setWeight(WEIGHT);
  }

  @Test
  void lookWithConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("look", "You are looking");
    sut.setActionResponses(response);

    String retrieved = sut.look();

    assertEquals("You are looking", retrieved);
  }

  @Test
  void lookWithoutConfiguration() {
    String retrieved = sut.look();

    assertEquals("There is nothing special about this " + NAME + ".", retrieved);
  }

  @Test
  void pickUpWithConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("pick up", "You are looking");
    sut.setActionResponses(response);

    String retrieved = sut.pickUp();

    assertEquals("You are looking", retrieved);
  }

  @Test
  void pickUpWithoutConfiguration() {
    String retrieved = sut.pickUp();

    assertEquals("This is not an item you can pick up.", retrieved);
  }

  @Test
  void dropWithoutConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("drop", "You have picked up the item");
    sut.setActionResponses(response);

    String retrieved = sut.drop();

    assertEquals("You have picked up the item", retrieved);
  }

  @Test
  void dropWithConfiguration() {
    String retrieved = sut.drop();

    assertEquals("This is not an item you can drop down.", retrieved);
  }

  @Test
  void cloneGivesClone() {
    var armor = new Armor();

    armor.setWeight(239);
    armor.setName("Unittest Body Armor");
    armor.setStrengthAddition(90);

    var clone = (Armor) armor.clone();

    assertEquals(armor.getName(), clone.getName());
    assertEquals(armor.getWeight(), clone.getWeight());
    assertEquals(armor.getStrengthAddition(), clone.getStrengthAddition());
    assertEquals(armor.getMaxStrengthAddition(), clone.getMaxStrengthAddition());
    assertEquals(clone.getActionResponses(), armor.getActionResponses());
  }
}
