package nl.aimsites.nzbvuq.game.entities;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class StorageFactoryTest {
  private static final String EXPECTED_STORAGES_STRING =
      "StorageFactory{availableStorages=[Storage{name='chest', capacity=5}, Storage{name='tree hollow', capacity=3}, Storage{name='hole', capacity=3}, Storage{name='closet', capacity=10}, Storage{name='table', capacity=5}]}";

  @Test
  void testThatItemFactoryGetsTheRightItems() {
    assertDoesNotThrow(() -> {
      var sut = new StorageEntityFactory();

      assertEquals(EXPECTED_STORAGES_STRING, sut.toString());
    });
  }
}
