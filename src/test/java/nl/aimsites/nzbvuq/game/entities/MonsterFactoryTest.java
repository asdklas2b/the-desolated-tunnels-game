package nl.aimsites.nzbvuq.game.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MonsterFactoryTest {
  private static final String EXPECTED_MONSTERS_STRING =
    "MonsterFactory{Monster{name='Troll', strength=300}, locations{castle=0.3}Monster{name='Witch', strength=350}, locations{field=0.15}Monster{name='Giant', strength=2000}, locations{field=0.05}Monster{name='Ogre', strength=550}, locations{corridor=0.01, castle=0.01, field=0.01, house=0.01}}";
  private MonsterFactory sut;

  @BeforeEach
  void setUp() throws Exception {
    sut = new MonsterFactory();
  }

  @Test
  void testThatItemFactoryGetsTheRightItems() {
    assertDoesNotThrow(() -> {
      assertEquals(EXPECTED_MONSTERS_STRING, sut.toString());
    });
  }
}