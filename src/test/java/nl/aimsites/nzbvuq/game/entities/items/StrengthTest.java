package nl.aimsites.nzbvuq.game.entities.items;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StrengthTest {
  private static final int WEIGHT = 10;
  private static final String NAME = "Apple";

  Strength sut;

  @BeforeEach
  void setUp() {
    sut = new Strength();
    sut.setName(NAME);
    sut.setWeight(WEIGHT);
  }

  @Test
  void lookWithConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("look", "You are looking");
    sut.setActionResponses(response);

    String retrieved = sut.look();

    assertEquals("You are looking", retrieved);
  }

  @Test
  void lookWithoutConfiguration() {
    String retrieved = sut.look();

    assertEquals("There is nothing special about this " + NAME + ".", retrieved);
  }

  @Test
  void pickUpWithConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("pick up", "You are picking up the item");
    sut.setActionResponses(response);

    String retrieved = sut.pickUp();

    assertEquals("You are picking up the item", retrieved);
  }

  @Test
  void pickUpWithoutConfiguration() {
    String retrieved = sut.pickUp();

    assertEquals("This is not an item you can pick up.", retrieved);
  }

  @Test
  void dropWithoutConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("drop", "You have dropped the item");
    sut.setActionResponses(response);

    String retrieved = sut.drop();

    assertEquals("You have dropped the item", retrieved);
  }

  @Test
  void dropWithConfiguration() {
    String retrieved = sut.drop();

    assertEquals("This is not an item you can drop down.", retrieved);
  }

  @Test
  void consumeWithoutConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("consume", "You ate the apple and it was delicious.");
    sut.setActionResponses(response);

    String retrieved = sut.consume();

    assertEquals("You ate the apple and it was delicious.", retrieved);
  }

  @Test
  void consumeWithConfiguration() {
    String retrieved = sut.consume();

    assertEquals("This is not an item you can consume.", retrieved);
  }

  @Test
  void cloneGivesClone() {
    var strength = new Strength();

    strength.setWeight(239);
    strength.setName("Unittest Strength");
    strength.setStrengthAddition(88);
    strength.setActionResponses(
        new HashMap<>() {
          {
            put("pick up", "You picked the item up.");
          }
        });

    var clone = (Strength) strength.clone();

    assertEquals(strength.getName(), clone.getName());
    assertEquals(strength.getWeight(), clone.getWeight());
    assertEquals(strength.getStrengthAddition(), clone.getStrengthAddition());
    assertEquals(strength.getActionResponses(), clone.getActionResponses());
  }
}
