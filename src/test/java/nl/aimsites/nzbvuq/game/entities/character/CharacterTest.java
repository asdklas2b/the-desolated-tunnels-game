package nl.aimsites.nzbvuq.game.entities.character;

import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Strength;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

  Character sut;

  @BeforeEach
  void setUp() {
    sut = new Player("");
  }

  @Test
  void assertAddStrengthDoesAddGivenStrengthToStrengthOfCharacter() {
    sut.removeStrength(sut.getStrength()/2);
    int addition = sut.getStrength()/4;
    int expectedStrength = sut.getStrength() + addition;

    sut.addStrength(addition);

    assertEquals(expectedStrength, sut.getStrength());
  }

  @Test
  void assertAddStrengthDoesNotGoBeyondMaxStrength() {
    sut.removeStrength(sut.getMaxStrength()/2);
    int addition = sut.getMaxStrength();

    sut.addStrength(addition);

    assertEquals(sut.getMaxStrength(), sut.getStrength());
  }

  @Test
  void assertRemoveStrengthDoesRemoveGivenStrengthFromStrengthOfCharacter() {
    sut.removeStrength(sut.getStrength()/2);
    int subtraction = sut.getStrength()/4;
    int expectedStrength = sut.getStrength() - subtraction;

    sut.removeStrength(subtraction);

    assertEquals(expectedStrength, sut.getStrength());
  }

  @Test
  void assertRemoveStrengthDoesNotGoBeyondMaxStrength() {
    sut.removeStrength(sut.getMaxStrength()/2);
    int subtraction = sut.getMaxStrength();

    sut.removeStrength(subtraction);

    assertEquals(0, sut.getStrength());
  }

  @Test
  void assertGetTravelCostWithItemsReturnsSumOfWeightFromItems() {
    BaseItem itemOne = new Strength();
    itemOne.setWeight(3);
    BaseItem itemTwo = new Strength();
    itemTwo.setWeight(5);
    sut.getInventory().addItem(itemOne);
    sut.getInventory().addItem(itemTwo);

    assertEquals(8, sut.getTravelCostFromItems());
  }
}