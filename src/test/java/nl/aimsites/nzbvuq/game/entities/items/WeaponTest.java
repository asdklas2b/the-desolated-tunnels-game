package nl.aimsites.nzbvuq.game.entities.items;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

class WeaponTest {
  private static final int WEIGHT = 10;
  private static final String NAME = "Apple";

  Weapon sut;

  @BeforeEach
  void setUp() {
    sut = new Weapon();
    sut.setName(NAME);
    sut.setWeight(WEIGHT);
  }

  @Test
  void lookWithConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("look", "You are looking");
    sut.setActionResponses(response);

    String retrieved = sut.look();

    assertEquals("You are looking",retrieved);
  }

  @Test
  void lookWithoutConfiguration() {
    String retrieved = sut.look();

    assertEquals("There is nothing special about this " + NAME + ".",retrieved);
  }

  @Test
  void pickUpWithConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("pick up", "You have picked up the item.");
    sut.setActionResponses(response);

    String retrieved = sut.pickUp();

    assertEquals("You have picked up the item.",retrieved);
  }

  @Test
  void pickUpWithoutConfiguration() {
    String retrieved = sut.pickUp();

    assertEquals("This is not an item you can pick up.",retrieved);
  }

  @Test
  void dropWithoutConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("drop", "You have picked up the item");
    sut.setActionResponses(response);

    String retrieved = sut.drop();

    assertEquals("You have picked up the item",retrieved);
  }

  @Test
  void dropWithConfiguration() {
    String retrieved = sut.drop();

    assertEquals("This is not an item you can drop down.",retrieved);
  }

  @Test
  void cloneGivesClone() {
    var weapon = new Weapon();

    weapon.setWeight(239);
    weapon.setName("Unittest Strength");
    weapon.setUsageCost(10);
    weapon.setMaxStrengthAddition(7);
    weapon.setDamage(78);
    weapon.setActionResponses(
        new HashMap<>() {
          {
            put("use", "You hit your enemy.");
          }
        });

    var clone = (Weapon) weapon.clone();

    assertEquals(weapon.getName(), clone.getName());
    assertEquals(weapon.getWeight(), clone.getWeight());
    assertEquals(weapon.getUsageCost(), clone.getUsageCost());
    assertEquals(weapon.getMaxStrengthAddition(), clone.getMaxStrengthAddition());
    assertEquals(weapon.getDamage(), clone.getDamage());
    assertEquals(weapon.getActionResponses(), clone.getActionResponses());
  }
}
