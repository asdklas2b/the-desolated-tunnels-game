package nl.aimsites.nzbvuq.game.entities.items;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BaseItemTest {
  private MockedItem sut;

  @BeforeEach
  void setUp() {
    sut = new MockedItem();
  }

  @Test
  void look() {
    var actualResult = sut.look();

    assertEquals("There is nothing special about this Mocked Item.", actualResult);
  }

  @Test
  void consume() {
    var actualResult = sut.consume();

    assertEquals("This is not an item you can consume.", actualResult);
  }

  @Test
  void pickUp() {
    var actualResult = sut.pickUp();

    assertEquals("This is not an item you can pick up.", actualResult);
  }

  @Test
  void drop() {
    var actualResult = sut.drop();

    assertEquals("This is not an item you can drop down.", actualResult);
  }

  @Test
  void attack() {
    var actualResult = sut.attack();

    assertEquals("This is not an item you can fight with.", actualResult);
  }

  @Test
  void equip() {
    var actualResult = sut.equip();

    assertEquals("This is not an item you can equip.", actualResult);
  }

  @Test
  void open() {
    var actualResult = sut.open();

    assertEquals("This is not an item you can open.", actualResult);
  }

  @Test
  void use() {
    var actualResult = sut.use();

    assertEquals("This is not an item you can use.", actualResult);
  }

  @Test
  void setName() {
    sut.setName("New Mocked Item");

    assertEquals("New Mocked Item", sut.getName());
  }

  @Test
  void setWeight() {
    sut.setWeight(42);

    assertEquals(42, sut.getWeight());
  }

  /** This class is used for testing the functionality of the abstract BaseItem class */
  private static class MockedItem extends BaseItem {
    public MockedItem() {
      this.name = "Mocked Item";
    }

    public String getName() {
      return this.name;
    }

    public int getWeight() {
      return this.weight;
    }
  }
}
