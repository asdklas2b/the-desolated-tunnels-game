package nl.aimsites.nzbvuq.game.entities.character;

import nl.aimsites.nzbvuq.game.entities.character.monsters.*;
import nl.aimsites.nzbvuq.game.entities.character.monsters.Monster;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MonsterTest {
  private List<Monster> sut;

  @BeforeEach
  void setUp() {
    sut = new ArrayList<>();
    String fakeIdentificationKey = "dhaijerqfda";
    sut.add(new Giant(fakeIdentificationKey, "fakeGiant"));
    sut.add(new Troll(fakeIdentificationKey, "fakeTroll"));
    sut.add(new Witch(fakeIdentificationKey, "fakeWitch"));
    sut.add(new Ogre(fakeIdentificationKey, "fakeOgre"));
  }

  @Test
  void lookWithConfiguration() {
    for (Monster monster : sut) {
      HashMap<String, String> response = new HashMap<>();
      response.put("look", "this is a " + monster.getName());
      monster.setActionResponses(response);

      String retrieved = monster.look();

      assertEquals("this is a " + monster.getName(), retrieved);
    }
  }

  @Test
  void lookWithoutConfiguration() {
    for (Monster monster : sut) {
      String retrieved = monster.look();

      assertEquals("There is nothing special about this " + monster.getName() + ".", retrieved);
    }
  }

  @Test
  void attackWithConfiguration() {
    for (Monster monster : sut) {
      HashMap<String, String> response = new HashMap<>();
      response.put("attack", "The " + monster.getName() + " attacked and did " + monster.getStrength() + " damage!");
      monster.setActionResponses(response);

      String retrieved = monster.attack();

      assertEquals("The " + monster.getName() + " attacked and did " + monster.getStrength() + " damage!", retrieved);
    }
  }

  @Test
  void attackWithoutConfiguration() {
    for (Monster monster : sut) {
      String retrieved = monster.attack();

      assertEquals("The " + monster.getName() + " attacked and did " + monster.getStrength() + " damage!", retrieved);
    }
  }

  @Test
  void cloneGivesClone() {
    for (Monster monster : sut) {
      monster.setStrength(monster.getStrength());
      monster.setName(monster.getName());
      monster.setActionResponses(
          new HashMap<>() {
            {
              put("look", "This " + monster.getName() +  " is used for testing.");
            }
          });

      var clone = (Monster) monster.clone();

      assertEquals(monster.getName(), clone.getName());
      assertEquals(monster.getStrength(), clone.getStrength());
      assertEquals(monster.getActionResponses(), clone.getActionResponses());
    }
  }
}
