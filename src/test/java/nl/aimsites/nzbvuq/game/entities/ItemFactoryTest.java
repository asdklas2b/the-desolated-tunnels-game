package nl.aimsites.nzbvuq.game.entities;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemFactoryTest {
  private static final String EXPECTED_ITEMS_STRING =
      "ItemFactory{availableItems=[BaseItem{name='apple', weight=7}, BaseItem{name='broadsword', weight=4}, BaseItem{name='shield', weight=7}, BaseItem{name='lifesteal', weight=6}, BaseItem{name='flag', weight=25}]}";

  @Test
  void testThatItemFactoryGetsTheRightItems() {
    assertDoesNotThrow(() -> {
      var sut = new ItemEntityFactory();

      assertEquals(EXPECTED_ITEMS_STRING, sut.toString());
    });
  }
}
