package nl.aimsites.nzbvuq.game.entities.character;

import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Strength;
import nl.aimsites.nzbvuq.game.entities.items.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {

  Inventory sut;

  @BeforeEach
  void setUp() {
    sut = new Inventory();
  }

  @Test
  void assertAddItemAddsItem() {
    BaseItem item = new Strength();

    sut.addItem(item);

    assertEquals(item,sut.getItems().get(0));
  }

  @Test
  void assertAddItemDoesntAddItemIfOverMaxSize() {
    for (int i = 0; i < sut.getMaxSize(); i++) {
      sut.addItem(new Strength());
    }
    BaseItem itemToMuch = new Weapon();

    sut.addItem(itemToMuch);

    assertEquals(sut.getMaxSize(),sut.getItems().size());
  }

  @Test
  void assertRemoveItemRemovesItem() {
    BaseItem item = new Strength();

    sut.addItem(item);

    sut.removeItem(item);

    assertEquals(0, sut.getItems().size());
  }

  @Test
  void assertSearchForItemReturnsCorrectItem() {
    BaseItem apple = new Strength();
    apple.setName("apple");
    sut.addItem(apple);

    BaseItem beans = new Strength();
    beans.setName("beans");
    sut.addItem(beans);

    assertEquals(apple, sut.searchForItem("Apple"));
  }
}