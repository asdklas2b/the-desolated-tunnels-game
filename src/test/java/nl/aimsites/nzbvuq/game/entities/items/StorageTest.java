package nl.aimsites.nzbvuq.game.entities.items;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StorageTest {
  private static final int CAPACITY = 200;
  private static final String NAME = "Chest";

  Storage sut;

  @BeforeEach
  void setUp() {
    sut = new Storage();
    sut.setName(NAME);
    sut.setCapacity(CAPACITY);
  }

  @Test
  void openWithConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("open", "You are opening");
    sut.setActionResponses(response);

    String retrieved = sut.open();

    assertEquals("You are opening", retrieved);
  }

  @Test
  void openWithoutConfiguration() {
    String retrieved = sut.open();

    assertEquals("This is not an item you can open.", retrieved);
  }

  @Test
  void cloneGivesClone() {
    var storage = new Storage();

    storage.setCapacity(200);
    storage.setName("Chest");
    storage.setActionResponses(
        new HashMap<>() {
          {
            put("pick up", "You picked the item up.");
          }
        });

    var clone = (Storage) storage.clone();

    assertEquals(storage.getName(), clone.getName());
    assertEquals(storage.getCapacity(), clone.getCapacity());
    assertEquals(storage.getActionResponses(), clone.getActionResponses());
  }

  @Test
  void insertIntoStorageWhenCapacityNotReachedInsertSuccesfully() {
    int capacity = sut.getCapacity() - 1;
    for (int i = 0; i < capacity; i++) {
      sut.addItem(new Strength());
    }

    assertEquals(capacity, sut.getItems().size());
  }

  @Test
  void insertIntoStorageWhenCapacityReachedThrowsException() {
    int capacity = sut.getCapacity();
    for (int i = 0; i < capacity; i++) {
      sut.addItem(new Strength());
    }

    var strength = new Strength();

    assertThrows(RuntimeException.class, () -> sut.addItem(strength));
  }
}
