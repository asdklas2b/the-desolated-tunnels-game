package nl.aimsites.nzbvuq.game.entities.items;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SpecialTest {
  private static final int WEIGHT = 10;
  private static final String NAME = "LifeSteal";

  Special sut;

  @BeforeEach
  void setUp() {
    sut = new Special();
    sut.setName(NAME);
    sut.setWeight(WEIGHT);
  }

  @Test
  void lookWithConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("look", "You are looking");
    sut.setActionResponses(response);

    String retrieved = sut.look();

    assertEquals("You are looking", retrieved);
  }

  @Test
  void lookWithoutConfiguration() {
    String retrieved = sut.look();

    assertEquals("There is nothing special about this " + NAME + ".", retrieved);
  }

  @Test
  void pickUpWithConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("pick up", "You are looking");
    sut.setActionResponses(response);

    String retrieved = sut.pickUp();

    assertEquals("You are looking", retrieved);
  }

  @Test
  void pickUpWithoutConfiguration() {
    String retrieved = sut.pickUp();

    assertEquals("This is not an item you can pick up.", retrieved);
  }

  @Test
  void dropWithoutConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("drop", "You have picked up the item");
    sut.setActionResponses(response);

    String retrieved = sut.drop();

    assertEquals("You have picked up the item", retrieved);
  }

  @Test
  void dropWithConfiguration() {
    String retrieved = sut.drop();

    assertEquals("This is not an item you can drop down.", retrieved);
  }

  @Test
  void consumeWithoutConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("consume", "You consumed the item");
    sut.setActionResponses(response);

    String retrieved = sut.consume();

    assertEquals("You consumed the item", retrieved);
  }

  @Test
  void consumeWithConfiguration() {
    String retrieved = sut.consume();

    assertEquals("This is not an item you can consume.", retrieved);
  }

  @Test
  void useWithoutConfiguration() {
    HashMap<String, String> response = new HashMap<>();
    response.put("use", "You used the item");
    sut.setActionResponses(response);

    String retrieved = sut.use();

    assertEquals("You used the item", retrieved);
  }

  @Test
  void useWithConfiguration() {
    String retrieved = sut.use();

    assertEquals("This is not an item you can use.", retrieved);
  }

  @Test
  void cloneGivesClone() {
    var special = new Special();

    special.setWeight(239);
    special.setName("Unittest Special");
    special.setActionResponses(
        new HashMap<>() {
          {
            put("look", "This unit test item is very special.");
          }
        });

    var clone = (Special) special.clone();

    assertEquals(special.getName(), clone.getName());
    assertEquals(special.getWeight(), clone.getWeight());
    assertEquals(special.getActionResponses(), clone.getActionResponses());
  }
}
