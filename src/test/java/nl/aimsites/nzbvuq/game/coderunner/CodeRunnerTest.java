package nl.aimsites.nzbvuq.game.coderunner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * WARNING: "public" is required for this testcase. If removed, the tests will fail because
 * Burningwave Core cannot access the class to compile code.
 */
public class CodeRunnerTest {
  private CodeRunner sut;

  @BeforeEach
  void setUp() {
    sut = new CodeRunner();
  }

  @Test
  void testThatRunAsyncRunsCodeInTheBackground() {
    var thread =
        sut.runAsync(
            "System.out.println(\"unittesting\"); try { Thread.sleep(1000); } catch (InterruptedException ignored) {} return null;",
            new ArrayList<>() {
              {
                add(Thread.class);
              }
            });

    assertTrue(thread.isAlive());

    assertDoesNotThrow(() -> Thread.sleep(3000));

    assertFalse(thread.isAlive());
  }

  @Test
  void testThatRunAsyncAcceptsParametersWithoutType() {
    var model = new UnitTestModel();

    var thread =
        sut.runAsync(
            "((nl.aimsites.nzbvuq.game.coderunner.CodeRunnerTest.UnitTestModel) parameter[0]).setMessage(\"unittesting\"); return null;",
            model);

    assertTrue(thread.isAlive());

    assertDoesNotThrow(() -> Thread.sleep(3000));

    assertEquals("unittesting", model.getMessage());
  }

  @Test
  void testThatRunAcceptsParameters() {
    var model = new UnitTestModel();

    sut.run(
        "var model = (UnitTestModel) parameter[0]; model.setMessage(\"unittesting\"); return null;",
        new ArrayList<>() {
          {
            add(UnitTestModel.class);
          }
        },
        model);

    assertEquals("unittesting", model.getMessage());
  }

  @Test
  void testThatRunAcceptsParametersWithoutType() {
    var model = new UnitTestModel();

    sut.run(
        "((nl.aimsites.nzbvuq.game.coderunner.CodeRunnerTest.UnitTestModel) parameter[0]).setMessage(\"unittesting\"); return null;",
        model);

    assertEquals("unittesting", model.getMessage());
  }

  public static class UnitTestModel {
    private String message;

    public String getMessage() {
      return message;
    }

    @SuppressWarnings("unused")
    public void setMessage(String message) {
      this.message = message;
    }
  }
}
