package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.entities.items.Strength;
import nl.aimsites.nzbvuq.game.networking.ITransactionHandler;
import nl.aimsites.nzbvuq.game.state.GameState;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ConsumeActionTest {
  private static final String ITEM = "apple";

  private ConsumeAction sut;

  private final ITransactionHandler transactionHandler = mock(ITransactionHandler.class);

  @BeforeEach
  void setUp() {
    var player = new Player("Unittester");

    GameState.getInstance().setPlayer(player);
    GameState.getInstance().setNetwork(transactionHandler);
    GameState.getInstance().spawnEntity(player, 1, 1);

    sut = new ConsumeAction();

    when(transactionHandler.sendAction(any())).thenReturn(true);
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }

  @Test
  void assertConsumeActionReturnsUnknownItemString() {
    assertEquals(
        "You don't have an " + ITEM + " in your inventory.",
        sut.execute(ITEM, GameState.getInstance().getPlayer()));
  }

  @Test
  void assertConsumeActionReturnsConsumedString() {
    var apple = new Strength();

    apple.setName("UnittestApple");
    apple.setActionResponses(
        new HashMap<>() {
          {
            put("consume", "You ate the apple.");
          }
        });

    GameState.getInstance().getPlayer().getInventory().addItem(apple);

    assertEquals(
        "You ate the apple.", sut.execute("UnittestApple", GameState.getInstance().getPlayer()));
  }

  @Test
  void assertExecuteReturnsMessageWhenANetworkErrorOccurs() {
    when(transactionHandler.sendAction(any())).thenReturn(false);

    var apple = new Strength();

    apple.setName("UnittestApple");
    apple.setActionResponses(
        new HashMap<>() {
          {
            put("consume", "You ate the apple.");
          }
        });

    GameState.getInstance().getPlayer().getInventory().addItem(apple);

    assertEquals(
        "You did not eat the item, network error.",
        sut.execute("UnittestApple", GameState.getInstance().getPlayer()));
  }
}
