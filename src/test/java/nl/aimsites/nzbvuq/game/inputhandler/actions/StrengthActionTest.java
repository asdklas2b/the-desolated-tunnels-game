package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.entities.items.Armor;
import nl.aimsites.nzbvuq.game.state.GameState;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StrengthActionTest {
  private StrengthAction sut;
  private Player player;

  @BeforeEach
  void setUp() {
    player = new Player("UnitTester");
    GameState.getInstance().setPlayer(player);

    sut = new StrengthAction();
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }

  @Test
  void assertStrengthActionReturnsHealthBarWhenMaxStrengthIsTheSameAsStrength() {
    Armor armor = new Armor();
    armor.setMaxStrengthAddition(203);
    player.getInventory().addItem(armor);
    player.removeStrength(player.getStrength());
    player.addStrength(player.getMaxStrength());

    assertEquals(
        "Your current strength is 1203."
            + System.lineSeparator()
            + "0 |##################################################|1203| 1203"
            + System.lineSeparator()
            + "Travel cost is 0 per move.",
        sut.execute(null, GameState.getInstance().getPlayer()));
  }

  @Test
  void assertStrengthActionReturnsHealthBarWhenStrengthIsTheLowerThanMaxStrength() {
    Armor armor = new Armor();
    armor.setMaxStrengthAddition(403);
    player.getInventory().addItem(armor);
    player.removeStrength(player.getStrength());
    player.addStrength(player.getMaxStrength() / 3);

    assertEquals(
        "Your current strength is 467."
            + System.lineSeparator()
            + "0 |################|467------------------------------| 1403"
            + System.lineSeparator()
            + "Travel cost is 0 per move.",
        sut.execute(null, GameState.getInstance().getPlayer()));
  }
}
