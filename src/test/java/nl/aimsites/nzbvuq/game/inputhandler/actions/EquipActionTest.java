package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.entities.items.Strength;
import nl.aimsites.nzbvuq.game.entities.items.Weapon;
import nl.aimsites.nzbvuq.game.networking.ITransactionHandler;
import nl.aimsites.nzbvuq.game.state.GameState;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class EquipActionTest {

  private EquipAction sut;

  private final ITransactionHandler transactionHandler = mock(ITransactionHandler.class);

  @BeforeEach
  void setUp() {
    var player = new Player("Unittester");

    GameState.getInstance().setPlayer(player);
    GameState.getInstance().setNetwork(transactionHandler);
    GameState.getInstance().spawnEntity(player, 1, 1);

    sut = new EquipAction();

    when(transactionHandler.sendAction(any())).thenReturn(true);
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }

  @Test
  void assertTargetIsEquipped() {
    var weapon = new Weapon();

    weapon.setName("UnittestSword");
    weapon.setActionResponses(
        new HashMap<>() {
          {
            put("equip", "Sword is now equipped");
          }
        });

    GameState.getInstance().getPlayer().getInventory().addItem(weapon);

    assertEquals(
        "Sword is now equipped The UnittestSword increased your max strength with 0. You now have a max strength of 1000.",
        sut.execute("UnittestSword", GameState.getInstance().getPlayer()));
  }

  @Test
  void assertTargetIsNotInInventory() {
    assertEquals(
        "You don't have an item named UnittestSword.",
        sut.execute("UnittestSword", GameState.getInstance().getPlayer()));
  }

  @Test
  void
      assertEquipActionReturnsCorrectResponseWhenEquippingAWeaponWhenThereIsAlreadyAWeaponEquipped() {
    var oldweapon = new Weapon();

    oldweapon.setMaxStrengthAddition(50);
    oldweapon.setName("UnittestSword");
    oldweapon.setActionResponses(
        new HashMap<>() {
          {
            put("equip", "Sword is now equipped");
          }
        });

    GameState.getInstance().getPlayer().getInventory().addItem(oldweapon);
    GameState.getInstance().getPlayer().setEquippedWeapon(oldweapon);

    var newWeapon = new Weapon();

    newWeapon.setMaxStrengthAddition(100);
    newWeapon.setName("newTestSword");
    newWeapon.setActionResponses(
        new HashMap<>() {
          {
            put("equip", "New sword is now equipped");
          }
        });

    GameState.getInstance().getPlayer().getInventory().addItem(newWeapon);

    assertEquals(
        "New sword is now equipped The newTestSword is stronger than the UnittestSword. Your max strength increased by 50. You now have a max strength of 1100.",
        sut.execute("newTestSword", GameState.getInstance().getPlayer()));
  }

  @Test
  void assertExecuteReturnsMessageWhenANetworkErrorOccurs() {
    when(transactionHandler.sendAction(any())).thenReturn(false);

    var weapon = new Weapon();

    weapon.setMaxStrengthAddition(100);
    weapon.setName("unittestSword");
    weapon.setActionResponses(
        new HashMap<>() {
          {
            put("equip", "New sword is now equipped");
          }
        });

    GameState.getInstance().getPlayer().getInventory().addItem(weapon);

    assertEquals(
        "You did not equip the sword, network error.",
        sut.execute("unittestSword", GameState.getInstance().getPlayer()));
  }

  @Test
  void assertExecuteReturnsCantEquipWhenAnAppleIsEquipped() {
    var apple = new Strength();
    apple.setName("apple");

    GameState.getInstance().getPlayer().getInventory().addItem(apple);

    assertEquals(
        "You can not equip an apple as it can not be used as a weapon.",
        sut.execute("apple", GameState.getInstance().getPlayer()));
  }
}
