package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.entities.items.Armor;
import nl.aimsites.nzbvuq.game.entities.items.Weapon;
import nl.aimsites.nzbvuq.game.generators.MapGeneratorFactory;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.networking.ITransactionHandler;
import nl.aimsites.nzbvuq.game.state.GameMode;
import nl.aimsites.nzbvuq.game.state.GameState;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class DropActionTest {

  private static final int GRAPH_SEED = 1234;
  public static final int UNITTEST_GRID_SIZE = 32;

  private DropAction sut;

  private final ITransactionHandler transactionHandler = mock(ITransactionHandler.class);

  @BeforeEach
  void setUp() throws Exception {
    var gen =
        MapGeneratorFactory.getNoise4JMapGenerator(
            UNITTEST_GRID_SIZE, GameMode.CAPTURE_THE_FLAG, new ItemEntityFactory());
    var graph = gen.generateMap(0, 0, GRAPH_SEED);

    GameState.getInstance().setPlayer(new Player("Unittester"));
    GameState.getInstance().setCurrentWorld(graph);
    GameState.getInstance().setNetwork(transactionHandler);

    var worldTile = graph.findTile(1, 1);
    if (worldTile.isEmpty() || !((worldTile.get()) instanceof ReachableTile)) {
      fail("Could not initialize a correct world");
    }

    var playerTile = (ReachableTile) worldTile.get();

    GameState.getInstance().getPlayer().setCurrentPosition(playerTile);

    sut = new DropAction();

    when(transactionHandler.sendAction(any())).thenReturn(true);
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }

  @Test
  void assertDropActionDropsTarget() {
    var sword = new Weapon();
    sword.setName("UnittestSword");
    sword.setActionResponses(
        new HashMap<>() {
          {
            put("drop", "You dropped the UnittestSword.");
          }
        });

    var inventory = GameState.getInstance().getPlayer().getInventory();
    inventory.addItem(sword);

    assertNotNull(inventory.searchForItem("UnittestSword"));
    assertEquals(
        "You dropped the UnittestSword.",
        sut.execute("UnittestSword", GameState.getInstance().getPlayer()));
    assertNull(inventory.searchForItem("UnittestSword"));

    var tileItems = GameState.getInstance().getPlayer().getCurrentPosition();
    assertTrue(tileItems.getOccupyingEntities().contains(sword));
  }

  @Test
  void assertDropActionReturnsUnknownItemString() {
    assertEquals(
        "You don't have UnittestSword in your inventory",
        sut.execute("UnittestSword", GameState.getInstance().getPlayer()));
  }

  @Test
  void assertDropItemRemovesStrengthOfPlayerAndReturnsCorrectResponse() {
    Armor testShield = new Armor();
    testShield.setStrengthAddition(100);
    testShield.setMaxStrengthAddition(testShield.getStrengthAddition());
    testShield.setName("testShield");
    testShield.setActionResponses(
        new HashMap<>() {
          {
            put("drop", "shield dropped.");
          }
        });
    GameState.getInstance().getPlayer().getInventory().addItem(testShield);

    assertEquals(
        "shield dropped. Because you dropped this testShield your strength and max strength decreased with 100. You now have a max strength of 1000.",
        sut.execute("testShield", GameState.getInstance().getPlayer()));
  }

  @Test
  void assertDropItemRemovesStrengthButDoesntKillPlayerWhenOtherwiseBelowZeroStrength() {
    Armor testShield = new Armor();
    testShield.setStrengthAddition(200);
    testShield.setMaxStrengthAddition(testShield.getStrengthAddition());
    testShield.setName("testShield");
    testShield.setActionResponses(
        new HashMap<>() {
          {
            put("drop", "shield dropped.");
          }
        });
    GameState.getInstance().getPlayer().getInventory().addItem(testShield);
    GameState.getInstance().getPlayer().removeStrength(900);

    assertEquals(
        "shield dropped. Because you dropped this testShield your strength decreased by 99 and your max strength by 200. You now have a max strength of 1000.",
        sut.execute("testShield", GameState.getInstance().getPlayer()));
    assertEquals(1, GameState.getInstance().getPlayer().getStrength());
  }
}
