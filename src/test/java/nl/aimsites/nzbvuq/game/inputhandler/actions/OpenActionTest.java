package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.Entity;
import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.entities.character.Inventory;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.entities.items.*;
import nl.aimsites.nzbvuq.game.generators.MapGeneratorFactory;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.networking.ITransactionHandler;
import nl.aimsites.nzbvuq.game.state.GameMode;
import nl.aimsites.nzbvuq.game.state.GameState;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class OpenActionTest {
  public static final int UNITTEST_GRID_SIZE = 32;
  private static final int GRAPH_SEED = 1234;
  private OpenAction sut;
  private ReachableTile playerTile;
  private Storage storage;

  private final ITransactionHandler transactionHandler = Mockito.mock(ITransactionHandler.class);

  @BeforeEach
  void setUp() throws Exception {
    sut = new OpenAction();

    GameState instance = GameState.getInstance();
    instance.setNetwork(transactionHandler);

    var gen =
        MapGeneratorFactory.getNoise4JMapGenerator(
            UNITTEST_GRID_SIZE, GameMode.CAPTURE_THE_FLAG, new ItemEntityFactory());
    var graph = gen.generateMap(0, 0, GRAPH_SEED);

    Player player = GameState.getInstance().getPlayer();
    player.setCurrentPosition((ReachableTile) graph.findTile(7, 6).orElseThrow());

    GameState.getInstance().getWorldHandler().getChunks().clear();
    GameState.getInstance().getWorldHandler().addChunk(graph);

    GameState.getInstance().setCurrentWorld(graph);

    playerTile = instance.getPlayer().getCurrentPosition();

    for (Entity entity : playerTile.getOccupyingEntities()) {
      playerTile.removeEntity(entity);
    }

    storage = new Storage();
    storage.setName("chest");
    storage.setActionResponses(
        new HashMap<>() {
          {
            put("open", "This is a test storage unit. Inside is the following:");
          }
        });

    when(transactionHandler.sendAction(any())).thenReturn(true);
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }

  @Test
  void assertOpenDoorActionReturnsUnableToOpenResponse() {
    assertEquals(
        "No valid door found to open", sut.execute("door", GameState.getInstance().getPlayer()));
  }

  @Test
  void assertOpenWindowActionReturnsOpenedResponse() {
    var player = GameState.getInstance().getPlayer();
    var tileWithWindow = GameState.getInstance().getCurrentWorld().findTile(1, 1);

    if (tileWithWindow.isEmpty()) {
      fail("Could not find a tile at pos 1,1");
    }

    player.setCurrentPosition((ReachableTile) tileWithWindow.get());

    var actualResult = sut.execute("window", GameState.getInstance().getPlayer());

    assertEquals("You slid the window open.", actualResult);
  }

  @Test
  void assertOpenDoorActionReturnsAlreadyOpenedResponse() {
    var player = GameState.getInstance().getPlayer();
    var tileWithDoor = GameState.getInstance().getCurrentWorld().findTile(1, 1);

    if (tileWithDoor.isEmpty()) {
      fail("Could not find a tile at pos 1,1");
    }

    player.setCurrentPosition((ReachableTile) tileWithDoor.get());

    sut.execute("door", GameState.getInstance().getPlayer());
    var actualResult = sut.execute("door", GameState.getInstance().getPlayer());

    assertEquals("The wooden door is already open", actualResult);
  }

  @Test
  void assertOpenInventoryActionWithEmptyInventoryReturnsThatInventoryIsEmpty() {
    assertEquals(
        "There is nothing to see in your inventory because it's empty.",
        sut.execute("inventory", GameState.getInstance().getPlayer()));
  }

  @Test
  void assertOpenInventoryActionReturnsInventoryContents() {
    BaseItem testStrength = new Strength();
    testStrength.setName("testStrength");
    BaseItem testWeapon = new Weapon();
    testWeapon.setName("testWeapon");
    Inventory inventory = GameState.getInstance().getPlayer().getInventory();
    inventory.addItem(testStrength);
    inventory.addItem(testWeapon);

    assertEquals(
        "Your inventory contains the following items:"
            + System.lineSeparator()
            + "- "
            + testStrength.getName().substring(0, 1).toUpperCase()
            + testStrength.getName().substring(1)
            + System.lineSeparator()
            + "- "
            + testWeapon.getName().substring(0, 1).toUpperCase()
            + testWeapon.getName().substring(1),
        sut.execute("inventory", GameState.getInstance().getPlayer()));

    inventory.removeItem(testStrength);
    inventory.removeItem(testWeapon);
  }

  @Test
  void assertOpenInventoryActionReturnsInventoryContentsEvenWhenUsingDifferentWordsForInventory() {
    BaseItem testWeapon = new Weapon();
    testWeapon.setName("testWeapon");
    Inventory inventory = GameState.getInstance().getPlayer().getInventory();
    inventory.addItem(testWeapon);

    assertEquals(
        "Your inventory contains the following items:"
            + System.lineSeparator()
            + "- "
            + testWeapon.getName().substring(0, 1).toUpperCase()
            + testWeapon.getName().substring(1),
        sut.execute("inventory", GameState.getInstance().getPlayer()));
    assertEquals(
        "Your inventory contains the following items:"
            + System.lineSeparator()
            + "- "
            + testWeapon.getName().substring(0, 1).toUpperCase()
            + testWeapon.getName().substring(1),
        sut.execute("backpack", GameState.getInstance().getPlayer()));
    assertEquals(
        "Your inventory contains the following items:"
            + System.lineSeparator()
            + "- "
            + testWeapon.getName().substring(0, 1).toUpperCase()
            + testWeapon.getName().substring(1),
        sut.execute("items", GameState.getInstance().getPlayer()));
    assertEquals(
        "Your inventory contains the following items:"
            + System.lineSeparator()
            + "- "
            + testWeapon.getName().substring(0, 1).toUpperCase()
            + testWeapon.getName().substring(1),
        sut.execute("i", GameState.getInstance().getPlayer()));

    inventory.removeItem(testWeapon);
  }

  @Test
  void assertOpenActionOnInvalidTargetReturnsValidUnopenableResponse() {
    assertEquals(
        "You can't open a candybar here.",
        sut.execute("candybar", GameState.getInstance().getPlayer()));
  }

  @Test
  public void assertOpenStorageActionReturnsStorageContents() {
    BaseItem testArmor = new Armor();
    testArmor.setName("testArmor");
    BaseItem testWeapon = new Weapon();
    testWeapon.setName("testWeapon");
    storage.setCapacity(2);
    storage.addItem(testArmor);
    storage.addItem(testWeapon);
    playerTile.addEntity(storage);

    String actualResponse = sut.execute(storage.getName(), GameState.getInstance().getPlayer());

    assertEquals(
        "This is a test storage unit. Inside is the following:"
            + System.lineSeparator()
            + "- "
            + testArmor.getName().substring(0, 1).toUpperCase()
            + testArmor.getName().substring(1)
            + System.lineSeparator()
            + "- "
            + testWeapon.getName().substring(0, 1).toUpperCase()
            + testWeapon.getName().substring(1),
        actualResponse);
  }

  @Test
  void assertExecuteReturnsMessageWhenANetworkErrorOccurs() {
    when(transactionHandler.sendAction(any())).thenReturn(false);

    assertEquals(
        "You could not open, network error.",
        sut.execute("door", GameState.getInstance().getPlayer()));
  }
}
