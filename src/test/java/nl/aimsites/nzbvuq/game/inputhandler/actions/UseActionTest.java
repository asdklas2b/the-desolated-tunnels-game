package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.items.Strength;
import nl.aimsites.nzbvuq.game.entities.items.Weapon;
import nl.aimsites.nzbvuq.game.networking.ITransactionHandler;
import nl.aimsites.nzbvuq.game.state.GameState;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UseActionTest {

  private UseAction sut;

  private final ITransactionHandler transactionHandler = mock(ITransactionHandler.class);

  @BeforeEach
  void setUp() {
    sut = new UseAction();

    GameState.getInstance().setNetwork(transactionHandler);

    when(transactionHandler.sendAction(any())).thenReturn(true);
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }

  @Test
  void assertUseActionsForItemThatIsNotAnExistingItem() {
    String target = "nonExistingItem";
    assertEquals(
        "The word " + target + " is not recognised. It is not an item.",
        sut.execute(target, GameState.getInstance().getPlayer()));
  }

  @Test
  void assertUseActionsForItemThatIsNotInInventoryButExists() {
    Weapon sword =
        (Weapon) GameState.getInstance().getItemFactory().getItemsFromType(Weapon.class).get(0);
    sword.setActionResponses(
        new HashMap<>() {
          {
            put("equip", "this should not be the response because the weapon is not in inventory");
          }
        });

    assertEquals(
        "You don't have this item in your inventory. So you cannot use it.",
        sut.execute(sword.getName(), GameState.getInstance().getPlayer()));
  }

  @Test
  void assertUseActionsForItemThatIsInInventoryAndItemIsOfTypeWeaponThatIsEquipped() {
    Weapon sword =
        (Weapon) GameState.getInstance().getItemFactory().getItemsFromType(Weapon.class).get(0);
    sword.setActionResponses(
        new HashMap<>() {
          {
            put("equip", "this should not be the response because the weapon is already equipped");
          }
        });
    GameState.getInstance().getPlayer().getInventory().addItem(sword);
    GameState.getInstance().getPlayer().setEquippedWeapon(sword);

    assertEquals(
        "You attacked: " + sword.getName(),
        sut.execute(sword.getName(), GameState.getInstance().getPlayer()));

    GameState.getInstance().getPlayer().setEquippedWeapon(null);
    GameState.getInstance().getPlayer().getInventory().removeItem(sword);
  }

  @Test
  void assertUseActionsForItemThatIsInInventoryAndItemIsOfTypeWeaponThatIsNotEquipped() {
    String message = "This is the message.";

    Weapon sword =
        (Weapon) GameState.getInstance().getItemFactory().getItemsFromType(Weapon.class).get(0);
    sword.setActionResponses(
        new HashMap<>() {
          {
            put("equip", message);
          }
        });
    GameState.getInstance().getPlayer().getInventory().addItem(sword);

    String expectedResponse =
        message
            + " The broadsword increased your max strength with 200. You now have a max strength of 1200.";

    assertEquals(
        expectedResponse, sut.execute(sword.getName(), GameState.getInstance().getPlayer()));

    GameState.getInstance().getPlayer().getInventory().removeItem(sword);
  }

  @Test
  void assertUseActionsForItemThatIsInInventoryAndItemIsOfTypeStrength() {
    String expectedResponse = "eating unittests";

    Strength food =
        (Strength) GameState.getInstance().getItemFactory().getItemsFromType(Strength.class).get(0);
    food.setActionResponses(
        new HashMap<>() {
          {
            put("consume", expectedResponse);
          }
        });
    GameState.getInstance().getPlayer().getInventory().addItem(food);

    assertEquals(
        expectedResponse, sut.execute(food.getName(), GameState.getInstance().getPlayer()));
  }
}
