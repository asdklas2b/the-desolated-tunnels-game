package nl.aimsites.nzbvuq.game.inputhandler.parser;

import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.inputhandler.actions.*;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import nl.aimsites.nzbvuq.game.state.GameState;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class InputHandlerTest {
  private InputHandler sut;

  @BeforeEach
  void setUp() {
    sut = new InputHandler();
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }

  @Test
  void assertAllActionsAreRegistered() {
    List<IAction> allPossibleActions =
      new ArrayList<>() {
        {
          add(new AttackAction());
          add(new ConsumeAction());
          add(new DropAction());
          add(new EquipAction());
          add(new LookAction());
          add(new MoveAction());
          add(new OpenAction());
          add(new PickupAction());
          add(new UseAction());
          add(new HelpAction());
          add(new StrengthAction());
        }
      };

    assertEquals(allPossibleActions.size(), (long) sut.getActions().size());
  }

  @Test
  void assertInputHandlerReturnsNullOnUnknownCommand() {
    String input = "this should not work";
    Scanner commandScanner = new Scanner(input);

//    assertEquals("Unknown command, type `help` for more information", sut.handleInput(commandScanner));
  }

  @Test
  void assertInputHandlerReturnsNullOnEmptyInput() {
    String input = "";
    Scanner commandScanner = new Scanner(input);

    sut.handleInput(commandScanner);
  }

  @Test
  void assertInputHandlerHandlesAction() {
    var sutSpy = spy(sut);
    var player = new Player("player");
    GameState.getInstance().spawnEntity(player, 1, 1);

    GameState.getInstance().setOtherPlayers(Collections.singletonList(player));

    var action = new Action(MoveAction.class, "n", "player", new Point(1, 1));

    sutSpy.handleAction(action);

    verify(sutSpy, times(1)).removePlayerAtCurrentPosition(player);
  }
}
