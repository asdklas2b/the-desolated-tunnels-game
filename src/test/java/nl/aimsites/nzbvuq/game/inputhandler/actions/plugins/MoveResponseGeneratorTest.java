package nl.aimsites.nzbvuq.game.inputhandler.actions.plugins;

import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.graph.tiles.Corridor;
import nl.aimsites.nzbvuq.game.graph.tiles.Field;
import nl.aimsites.nzbvuq.game.graph.tiles.Room;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MoveResponseGeneratorTest {

  MoveResponseGenerator sut;

  @BeforeEach
  void setUp() {
    sut = new MoveResponseGenerator();
  }

  @Test
  void testToStringReturnsStringThatBelongsToTheRightCombinationRoomToField() {
    sut.setOldTile(new Room(new Point(1, 1), 1));
    sut.setNewTile(new Field(new Point(1, 1), 1));
    String actualResponse = sut.toString();
    assertEquals("You leave the room and go outside. It is a bit colder out here.", actualResponse);
  }

  @Test
  void testToStringReturnsStringThatBelongsToTheRightCombinationRoomToCorridor() {
    sut.setOldTile(new Room(new Point(1, 1), 1));
    sut.setNewTile(new Corridor(new Point(1, 1), 1));
    String actualResponse = sut.toString();
    assertEquals("As you enter the corridor you hear the cold air rush out of the tunnels into the room.", actualResponse);
  }

  @Test
  void testToStringReturnsStringThatBelongsToTheRightCombinationCorridorToCorridor() {
    sut.setOldTile(new Corridor(new Point(1, 1), 1));
    sut.setNewTile(new Corridor(new Point(1, 1), 1));
    String actualResponse = sut.toString();
    assertEquals("you stay in the corridor.", actualResponse);
  }
}
