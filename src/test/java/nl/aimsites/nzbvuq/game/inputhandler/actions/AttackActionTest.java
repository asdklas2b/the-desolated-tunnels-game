package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.networking.ITransactionHandler;
import nl.aimsites.nzbvuq.game.state.GameState;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class AttackActionTest {

  private AttackAction sut;

  private final ITransactionHandler transactionHandler = mock(ITransactionHandler.class);

  @BeforeEach
  void setUp() {
    GameState.getInstance().setNetwork(transactionHandler);

    sut = new AttackAction();

    when(transactionHandler.sendAction(any())).thenReturn(true);
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }

  @Test
  void assertAttackActionReturnsAttackedTargetString() {
    assertEquals(
        "You attacked: barricade", sut.execute("barricade", GameState.getInstance().getPlayer()));
  }

  @Test
  void assertExecuteReturnsMessageWhenANetworkErrorOccurs() {
    when(transactionHandler.sendAction(any())).thenReturn(false);

    assertEquals(
        "You did not attack, network error.",
        sut.execute("barricade", GameState.getInstance().getPlayer()));
  }
}
