package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.Entity;
import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.entities.items.*;
import nl.aimsites.nzbvuq.game.generators.MapGeneratorFactory;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.networking.ITransactionHandler;
import nl.aimsites.nzbvuq.game.state.GameMode;
import nl.aimsites.nzbvuq.game.state.GameState;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PickupActionTest {

  public static final int UNITTEST_GRID_SIZE = 32;
  private static final int GRAPH_SEED = 1234;
  private PickupAction sut;
  private Storage storage;
  private ReachableTile playerTile;

  private final ITransactionHandler transactionHandler = mock(ITransactionHandler.class);

  @BeforeEach
  public void setUp() throws Exception {
    var gen = MapGeneratorFactory.getNoise4JMapGenerator(UNITTEST_GRID_SIZE, GameMode.CAPTURE_THE_FLAG, new ItemEntityFactory());
    var graph = gen.generateMap(0, 0, GRAPH_SEED);

    GameState.getInstance().setPlayer(new Player("Unittester"));
    GameState.getInstance().setCurrentWorld(graph);
    GameState.getInstance().setNetwork(transactionHandler);
    GameState.getInstance().getWorldHandler().getChunks().clear();
    GameState.getInstance().getWorldHandler().addChunk(graph);

    var worldTile = graph.findTile(1, 1);
    if (worldTile.isEmpty() || !((worldTile.get()) instanceof ReachableTile)) {
      fail("Could not initialize a correct world");
    }

    playerTile = (ReachableTile) worldTile.get();

    GameState.getInstance().getPlayer().setCurrentPosition(playerTile);
    for (Entity entity : playerTile.getOccupyingEntities()) {
      playerTile.removeEntity(entity);
    }

    var apple = new Strength();
    apple.setName("apple");
    apple.setActionResponses(
        new HashMap<>() {
          {
            put("pick up", "You picked the apple up and put it in your inventory.");
          }
        });
    playerTile.setOccupyingEntities(new ArrayList<Entity>());

    playerTile.addEntity(apple);
    storage = new Storage();
    storage.setName("chest");
    sut = new PickupAction();

    when(transactionHandler.sendAction(any())).thenReturn(true);
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }

  @Test
  void assertPickupActionReturnsFailedMessageWhenTheItemDoesNotExists() {
    var actualResult = sut.execute("sword", GameState.getInstance().getPlayer());

    assertEquals("Your current position does not have a sword available.", actualResult);
  }

  @Test
  void assertPickupActionReturnsPickedUpTargetResponse() {
    var actualResult = sut.execute("apple", GameState.getInstance().getPlayer());

    assertEquals("You picked the apple up and put it in your inventory.", actualResult);
    assertFalse(
        playerTile.getOccupyingEntities().stream().anyMatch(x -> x.getName().equals("apple")));
    assertNotNull(GameState.getInstance().getPlayer().getInventory().searchForItem("apple"));
  }

  @Test
  public void assertPickupActionFromChestReturnsPickedUpTargetResponse() {
    BaseItem testWeapon = new Weapon();
    testWeapon.setName("Broadsword");
    testWeapon.setActionResponses(
        new HashMap<>() {
          {
            put("pick up", "You picked the Broadsword up and put it in your inventory.");
          }
        });
    storage.setCapacity(1);
    storage.addItem(testWeapon);
    playerTile.addEntity(storage);

    var actualResult = sut.execute("Broadsword", GameState.getInstance().getPlayer());

    assertEquals("You picked the Broadsword up and put it in your inventory.", actualResult);
    assertFalse(
        playerTile.getOccupyingEntities().stream().anyMatch(x -> x.getName().equals("Broadsword")));
    assertNotNull(GameState.getInstance().getPlayer().getInventory().searchForItem("Broadsword"));
  }

  @Test
  void assertPickupActionAddsStrengthToPlayer() {
    Armor shield = new Armor();
    shield.setName("testShield");
    shield.setMaxStrengthAddition(100);
    shield.setStrengthAddition(100);
    shield.setActionResponses(
        new HashMap<>() {
          {
            put("pick up", "taken shield.");
          }
        });
    GameState.getInstance().getPlayer().getCurrentPosition().addEntity(shield);

    assertEquals(
        "taken shield. The testShield increased both your strength and max strength with 100. You now have a max strength of 1100.",
        sut.execute("testShield", GameState.getInstance().getPlayer()));
  }
}
