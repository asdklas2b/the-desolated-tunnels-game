package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.Entity;
import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.entities.items.BaseItem;
import nl.aimsites.nzbvuq.game.entities.items.Strength;
import nl.aimsites.nzbvuq.game.entities.items.Weapon;
import nl.aimsites.nzbvuq.game.generators.MapGeneratorFactory;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.state.GameMode;
import nl.aimsites.nzbvuq.game.state.GameState;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class LookActionTest {

  private static final int GRAPH_SEED = 1234;
  public static final int UNITTEST_GRID_SIZE = 32;

  private LookAction sut;
  private ReachableTile playerTile;

  @BeforeEach
  void setUp() throws Exception {
    var gen =
        MapGeneratorFactory.getNoise4JMapGenerator(
            UNITTEST_GRID_SIZE, GameMode.CAPTURE_THE_FLAG, new ItemEntityFactory());
    var graph = gen.generateMap(0, 0, GRAPH_SEED);

    GameState.getInstance().setPlayer(new Player("Unittester"));
    GameState.getInstance().setCurrentWorld(graph);
    GameState.getInstance().getWorldHandler().getChunks().clear();
    GameState.getInstance().getWorldHandler().addChunk(graph);
    var worldTile = graph.findTile(1, 1);
    if (worldTile.isEmpty() || !((worldTile.get()) instanceof ReachableTile)) {
      fail("Could not initialize a correct world");
    }

    playerTile = (ReachableTile) worldTile.get();

    for (Entity entity : playerTile.getOccupyingEntities()) {
      playerTile.removeEntity(entity);
    }

    GameState.getInstance().getPlayer().setCurrentPosition(playerTile);

    var apple = new Strength();
    apple.setName("apple");
    apple.setActionResponses(
        new HashMap<>() {
          {
            put("look", "Unittest response.");
          }
        });

    playerTile.addEntity(apple);

    sut = new LookAction();
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }

  @Test
  void assertLookAtItemInInventoryReturnsCorrectResponse() {
    GameState game = GameState.getInstance();
    BaseItem strengthItem = game.getItemFactory().getItemsFromType(Strength.class).get(0);
    game.getPlayer().getInventory().addItem(strengthItem);

    String actualResponse =
        sut.execute(strengthItem.getName(), GameState.getInstance().getPlayer());

    assertEquals(strengthItem.look(), actualResponse);
  }

  @Test
  void assertLookAtEntityOfTypeBaseItemOnTileReturnsCorrectResponse() {
    String actualResponse = sut.execute("apple", GameState.getInstance().getPlayer());

    assertEquals("Unittest response.", actualResponse);
  }

  @Test
  void assertLookWithoutGivingTargetWithOnlyOneItemReturnsCorrectResponse() {
    String actualResponse = sut.execute(null, GameState.getInstance().getPlayer());

    String expectedResponse =
        new StringBuilder()
            .append("You are currently standing in a room. ")
            .append(System.lineSeparator())
            .append("You see a(n) :")
            .append(System.lineSeparator())
            .append("- Apple")
            .append(System.lineSeparator())
            .append(System.lineSeparator())
            .append("You look around your current position and see the following: ")
            .append(System.lineSeparator())
            .append("- You can reach the tile to the north its a Corridor")
            .append(System.lineSeparator())
            .append("- You can reach the tile to the east its a Field")
            .append(System.lineSeparator())
            .append("- You can reach the tile to the south its a Field")
            .append(System.lineSeparator())
            .append("- You can reach the tile to the west its a Field")
            .append(System.lineSeparator())
            .append(System.lineSeparator())
            .toString();

    assertEquals(expectedResponse, actualResponse);
  }

  @Test
  void assertLookWithoutGivingTargetWithTwoItemsReturnsCorrectResponse() {
    Weapon weapon = new Weapon();
    weapon.setName("testWeapon");
    playerTile.addEntity(weapon);

    String actualResponse = sut.execute(null, GameState.getInstance().getPlayer());

    String expectedResponse =
        new StringBuilder()
            .append("You are currently standing in a room. ")
            .append(System.lineSeparator())
            .append("You see the following:")
            .append(System.lineSeparator())
            .append("- Apple")
            .append(System.lineSeparator())
            .append("- TestWeapon")
            .append(System.lineSeparator())
            .append(System.lineSeparator())
            .append("You look around your current position and see the following: ")
            .append(System.lineSeparator())
            .append("- You can reach the tile to the north its a Corridor")
            .append(System.lineSeparator())
            .append("- You can reach the tile to the east its a Field")
            .append(System.lineSeparator())
            .append("- You can reach the tile to the south its a Field")
            .append(System.lineSeparator())
            .append("- You can reach the tile to the west its a Field")
            .append(System.lineSeparator())
            .append(System.lineSeparator())
            .toString();

    assertEquals(expectedResponse, actualResponse);

    playerTile.removeEntity(weapon);
  }

  @Test
  void assertLookWithoutGivingTargetWithNoItemsReturnsCorrectResponse() {
    playerTile.removeEntity(playerTile.getOccupyingEntities().get(0));

    String actualResponse = sut.execute(null, GameState.getInstance().getPlayer());

    String expectedResponse =
        new StringBuilder()
            .append("You are currently standing in a room. ")
            .append(System.lineSeparator())
            .append("There don't appear to be any items around you.")
            .append(System.lineSeparator())
            .append(System.lineSeparator())
            .append(System.lineSeparator())
            .append("You look around your current position and see the following: ")
            .append(System.lineSeparator())
            .append("- You can reach the tile to the north its a Corridor")
            .append(System.lineSeparator())
            .append("- You can reach the tile to the east its a Field")
            .append(System.lineSeparator())
            .append("- You can reach the tile to the south its a Field")
            .append(System.lineSeparator())
            .append("- You can reach the tile to the west its a Field")
            .append(System.lineSeparator())
            .append(System.lineSeparator())
            .toString();

    assertEquals(expectedResponse, actualResponse);
  }

  @Test
  void assertLookWithTargetButNothingFoundReturnsCorrectResponse() {
    String actualResponse = sut.execute("nonexistentItem", GameState.getInstance().getPlayer());

    assertEquals(
        "You look in your inventory and around you but you cannot seem to find a nonexistentItem.",
        actualResponse);
  }

  @Test
  void assertIsTargetNullableReturnsTrue() {
    assertTrue(sut.isTargetNullable());
  }
}
