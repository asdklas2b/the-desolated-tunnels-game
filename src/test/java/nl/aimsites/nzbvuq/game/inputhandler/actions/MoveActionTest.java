package nl.aimsites.nzbvuq.game.inputhandler.actions;

import nl.aimsites.nzbvuq.game.entities.ItemEntityFactory;
import nl.aimsites.nzbvuq.game.generators.MapGenerator;
import nl.aimsites.nzbvuq.game.generators.MapGeneratorFactory;
import nl.aimsites.nzbvuq.game.graph.tiles.ReachableTile;
import nl.aimsites.nzbvuq.game.state.GameMode;
import nl.aimsites.nzbvuq.game.networking.ITransactionHandler;
import nl.aimsites.nzbvuq.game.state.GameState;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class MoveActionTest {

  private static final int GRAPH_SEED = 1234;
  public static final int UNITTEST_GRID_SIZE = 32;
  private MapGenerator mapGenerator;
  private MoveAction sut;

  private final ITransactionHandler transactionHandler = Mockito.mock(ITransactionHandler.class);

  @BeforeEach
  void setUp() throws Exception {
    GameState.getInstance().setNetwork(transactionHandler);
    sut = new MoveAction();

    mapGenerator =
        MapGeneratorFactory.getNoise4JMapGenerator(
            UNITTEST_GRID_SIZE, GameMode.CAPTURE_THE_FLAG, new ItemEntityFactory());
    var graph = mapGenerator.generateMap(0, 0, GRAPH_SEED);

    GameState.getInstance().setCurrentWorld(graph);
    GameState.getInstance().getWorldHandler().getChunks().clear();
    GameState.getInstance().getWorldHandler().addChunk(graph);
    GameState.getInstance().spawnEntity(GameState.getInstance().getPlayer(), 0, 0);

    when(transactionHandler.sendAction(any())).thenReturn(true);
  }

  @AfterEach
  void tearDown() {
    GameState.clearInstance();
  }

  @Test
  void assertMoveActionNorthReturnsMovementTargetString() {
    String actual = sut.execute("north", GameState.getInstance().getPlayer());

    assertThat(
        actual,
        anyOf(
            equalTo("You enter a corridor. Inside you can hear water dripping onto the ground."),
            equalTo("As you step inside a room, you can feel the warm air flowing over you."),
            equalTo("As you walk through the fields, you hear a bird chirping."),
            equalTo(
                "You step out of the corridor into a field. You feel the fresh air blow through your hair."),
            equalTo("You step up some stairs and enter a room."),
            equalTo("you stay in the corridor."),
            equalTo(
                "As you enter the corridor you hear the cold air rush out of the tunnels into the room."),
            equalTo("You leave the room and go outside. It is a bit colder out here."),
            equalTo("you stepped into another room."),
            equalTo("You did not move, tile Unreachable")));
  }

  @Test
  void assertMoveActionEastReturnsMovementTargetString() {
    String actual = sut.execute("east", GameState.getInstance().getPlayer());

    assertThat(
        actual,
        anyOf(
            equalTo("You enter a corridor. Inside you can hear water dripping onto the ground."),
            equalTo("As you step inside a room, you can feel the warm air flowing over you."),
            equalTo("As you walk through the fields, you hear a bird chirping."),
            equalTo(
                "You step out of the corridor into a field. You feel the fresh air blow through your hair."),
            equalTo("You step up some stairs and enter a room."),
            equalTo("you stay in the corridor."),
            equalTo(
                "As you enter the corridor you hear the cold air rush out of the tunnels into the room."),
            equalTo("You leave the room and go outside. It is a bit colder out here."),
            equalTo("you stepped into another room."),
            equalTo("You did not move, tile Unreachable")));
  }

  @Test
  void assertMoveActionSouthReturnsMovementTargetString() {
    var worldTile = GameState.getInstance().getCurrentWorld().findTile(2, 3);
    if (worldTile.isEmpty() || !(worldTile.get() instanceof ReachableTile)) {
      fail("Could not initialize a correct world");
    }

    GameState.getInstance().getPlayer().setCurrentPosition((ReachableTile) worldTile.get());

    String actual = sut.execute("south", GameState.getInstance().getPlayer());

    assertThat(
        actual,
        anyOf(
            equalTo("You enter a corridor. Inside you can hear water dripping onto the ground."),
            equalTo("As you step inside a room, you can feel the warm air flowing over you."),
            equalTo("As you walk through the fields, you hear a bird chirping."),
            equalTo(
                "You step out of the corridor into a field. You feel the fresh air blow through your hair."),
            equalTo("You step up some stairs and enter a room."),
            equalTo("you stay in the corridor."),
            equalTo(
                "As you enter the corridor you hear the cold air rush out of the tunnels into the room."),
            equalTo("You leave the room and go outside. It is a bit colder out here."),
            equalTo("you stepped into another room."),
            equalTo("You did not move, tile Unreachable")));
  }

  @Test
  void assertMoveActionWestReturnsMovementTargetString() {
    var worldTile = GameState.getInstance().getCurrentWorld().findTile(2, 3);
    if (worldTile.isEmpty() || !(worldTile.get() instanceof ReachableTile)) {
      fail("Could not initialize a correct world");
    }

    GameState.getInstance().getPlayer().setCurrentPosition((ReachableTile) worldTile.get());

    String actual = sut.execute("west", GameState.getInstance().getPlayer());

    assertThat(
        actual,
        anyOf(
            equalTo("You enter a corridor. Inside you can hear water dripping onto the ground."),
            equalTo("As you step inside a room, you can feel the warm air flowing over you."),
            equalTo("As you walk through the fields, you hear a bird chirping."),
            equalTo(
                "You step out of the corridor into a field. You feel the fresh air blow through your hair."),
            equalTo("You step up some stairs and enter a room."),
            equalTo("you stay in the corridor."),
            equalTo(
                "As you enter the corridor you hear the cold air rush out of the tunnels into the room."),
            equalTo("You leave the room and go outside. It is a bit colder out here."),
            equalTo("you stepped into another room."),
            equalTo("You did not move, tile Unreachable")));
  }

  @Test
  void assertInvalidMoveActionReturnsInvalidMovementTarget() {
    assertEquals(
        "Your only move options are: [south, e, forward, north, right, down, n, east, s, left, w, west, backward, up]",
        sut.execute("underground", GameState.getInstance().getPlayer()));
  }

  @Test
  void assertMoveActionDoesNotMoveThroughClosedDoor() {
    var worldTile = GameState.getInstance().getCurrentWorld().findTile(1, 1);
    if (worldTile.isEmpty() || !(worldTile.get() instanceof ReachableTile)) {
      fail("Could not initialize a correct world");
    }

    GameState.getInstance().getPlayer().setCurrentPosition((ReachableTile) worldTile.get());

    when(GameState.getInstance().sendAction(Mockito.any())).thenReturn(true);

    var actualResult = sut.execute("left", GameState.getInstance().getPlayer());
    assertEquals("You cannot move through a closed window", actualResult);
  }

  @Test
  void assertMoveActionMovesThroughOpenedDoor() {
    var worldTile = GameState.getInstance().getCurrentWorld().findTile(3, 6);
    if (worldTile.isEmpty() || !(worldTile.get() instanceof ReachableTile)) {
      fail("Could not initialize a correct world");
    }

    GameState.getInstance().getPlayer().setCurrentPosition((ReachableTile) worldTile.get());

    when(GameState.getInstance().sendAction(Mockito.any())).thenReturn(true);

    new OpenAction().execute("door", GameState.getInstance().getPlayer());

    var actualResult = sut.execute("up", GameState.getInstance().getPlayer());
    assertEquals(
        "You enter a corridor. Inside you can hear water dripping onto the ground.", actualResult);
  }

  @Test
  void assertExecuteReturnsMessageWhenANetworkErrorOccurs() {
    when(transactionHandler.sendAction(any())).thenReturn(false);

    assertEquals(
        "You did not move, network error.", sut.execute("n", GameState.getInstance().getPlayer()));
  }
}
