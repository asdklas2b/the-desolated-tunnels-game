package nl.aimsites.nzbvuq.game.networking;

import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.inputhandler.actions.MoveAction;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import nl.aimsites.nzbvuq.game.networking.transaction.*;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyManagerContract;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyUserContract;
import nl.aimsites.nzbvuq.rpc.RPC;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

class TransactionHandlerTest {
  @Mock
  private TransactionCoordinator transactionCoordinator;

  @Mock
  private TfcTransactionCoordinator tfcTransactionCoordinator;

  @Mock
  private TransactionManager transactionManager;

  @Mock
  private RPC rpc;

  @Mock
  private TfcTransactionManager tfcTransactionManager;

  @Mock
  private IReceiveAction iReceiveAction;

  @Mock
  private LobbyManagerContract managerContract;

  @InjectMocks
  private TransactionHandler sut;

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);
    when(rpc.getIdentifier()).thenReturn("500");
    sut = new TransactionHandler(rpc, managerContract);
    sut.setTransactionManagersAndCoordinators(tfcTransactionManager, transactionManager, tfcTransactionCoordinator, transactionCoordinator);
  }

  @Test
  void replyStartsTfcTransactionManager() {
    String expectedMessage = "STATUS: OK";
    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);

    when(tfcTransactionManager.reply(parameters)).thenReturn(Collections.singletonList("STATUS: OK"));

    assertEquals(expectedMessage, sut.reply(parameters).get(0));
  }

  @Test
  void replyStartsTransactionManager() {
    String expectedMessage = "STATUS: OK";
    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);

    when(transactionManager.reply(parameters)).thenReturn(Collections.singletonList("STATUS: OK"));

    assertEquals(expectedMessage, sut.reply(parameters).get(0));
  }

  @Test
  void replyGivesEmptyList() {
    List<String> parameters = new ArrayList<>();
    parameters.add("SOMETHING_ELSE");

    assertNull(sut.reply(parameters));
  }

  @Test
  void sendActionFromLeaderReturnsTrue() {
    sut.setLeader(sut.getPlayer());
    Action action = new Action(MoveAction.class, "n", sut.getPlayer(), new Point(1, 1));
    assertTrue(sut.sendAction(action));
  }

  @Test
  void sendActionFromOtherPlayerStartsTfcTransactionManager() {
    sut.setLeader(sut.getPlayer());
    Action action = new Action(MoveAction.class, "n", "player", new Point(1, 1));
    assertTrue(sut.sendAction(action));
  }

  @Test
  void sendActionStartsTransactionManager() {
    Action action = new Action(MoveAction.class, "n", "player", new Point(1, 1));
    when(transactionCoordinator.manageTransaction(action)).thenReturn(true);
    assertTrue(sut.sendAction(action));
  }

  @Test
  void StartChooseTheLeaderCallsChooseTheLeader() {
    sut.startLeaderElection();

    verify(transactionCoordinator).chooseTheLeaderAlgorithm(ProtocolMessage.LEADER_ELECTION);
  }

  @Test
  void ChooseTheLeaderCallsChooseTheLeader() {
    sut.chooseTheLeaderAlgorithm(ProtocolMessage.LEADER_ELECTION);

    verify(transactionCoordinator).chooseTheLeaderAlgorithm(ProtocolMessage.LEADER_ELECTION);
  }

  @Test
  void testAddPlayers() {
    List<String> list = new ArrayList<>() {
      {
        add("1");
        add("2");
      }
    };

    sut.addPlayers(getLobbyContractList(list));
    ArrayList<String> players = sut.getPlayers();

    assertEquals("2", players.get(1));
  }

  @Test
  void removeFromPlayersListRemovesPlayer() {
    List<String> list = new ArrayList<>() {
      {
        add("1");
        add("2");
        add("3");
        add("4");
        add("5");
      }
    };

    sut.addPlayers(getLobbyContractList(list));
    sut.removeFromPlayersList("2");
    ArrayList<String> players = sut.getPlayers();
    players.forEach(System.out::println);

    assertNotEquals("2", players.get(1));
  }

  List<LobbyUserContract> getLobbyContractList(List<String> list) {
    List<LobbyUserContract> players = new ArrayList<>();

    list.forEach(string -> {
      LobbyUserContract lobbyUserContract = new LobbyUserContract() {
        @Override
        public String getIdentifier() {
          return string;
        }

        @Override
        public String getName() {
          return "";
        }
      };

      players.add(lobbyUserContract);
    });

    return players;
  }
}
