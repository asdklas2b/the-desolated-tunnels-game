package nl.aimsites.nzbvuq.game.networking.transaction;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.networking.TransactionHandler;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class TransactionManagerTest {
  @Mock
  private TransactionHandler transactionHandlerMock;
  private Gson gson;

  @InjectMocks
  private TransactionManager sut;

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);
    sut = new TransactionManager(transactionHandlerMock);
    gson = new Gson();
  }

  @Test
  void startElectionReturnsStatusOk() {
    String expectedMessage = "STATUS: OK";

    String electionStarter = "500";
    String player = "400";

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);
    parameters.add(ProtocolMessage.START_ELECTION);
    parameters.add(electionStarter);

    when(transactionHandlerMock.getPlayer()).thenReturn(player);
    List<String> response = sut.reply(parameters);

    assertEquals(expectedMessage, response.get(0));
  }

  @Test
  void startElectionReturnsParticipate() {
    String expectedMessage = ProtocolMessage.PARTICIPATE_ELECTION;

    String electionStarter = "400";
    String player = "500";

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);
    parameters.add(ProtocolMessage.START_ELECTION);
    parameters.add(electionStarter);

    when(transactionHandlerMock.getPlayer()).thenReturn(player);
    List<String> response = sut.reply(parameters);

    assertEquals(expectedMessage, response.get(0));
  }

  @Test
  void startElectionCallsSetElectionFlagTrue() {
    String electionStarter = "400";
    String player = "500";

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);
    parameters.add(ProtocolMessage.START_ELECTION);
    parameters.add(electionStarter);

    when(transactionHandlerMock.getPlayer()).thenReturn(player);
    sut.reply(parameters);

    verify(transactionHandlerMock).setElectionFlag(true);
  }

  @Test
  void notifyLeaderReturnsStatusOk() {
    String expectedMessage = "STATUS: OK";

    String electionStarter = "500";

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);
    parameters.add(ProtocolMessage.NOTIFY_LEADER);
    parameters.add(electionStarter);

    List<String> response = sut.reply(parameters);

    assertEquals(expectedMessage, response.get(0));
  }

  @Test
  void newLeaderCallsRemoveFromPlayersList() {
    String leader = "400";
    String oldLeader = "500";
    String player = "300";

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);
    parameters.add(ProtocolMessage.NEW_LEADER);
    parameters.add(leader);

    when(transactionHandlerMock.getPlayer()).thenReturn(player);
    when(transactionHandlerMock.getLeader()).thenReturn(oldLeader);
    sut.reply(parameters);

    verify(transactionHandlerMock).removeFromPlayersList(oldLeader);
  }

  @Test
  void newLeaderNotCallsRemoveFromPlayersList() {
    String leader = "500";
    String player ="300";

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);
    parameters.add(ProtocolMessage.NEW_LEADER);
    parameters.add(leader);

    when(transactionHandlerMock.getPlayer()).thenReturn(player);
    when(transactionHandlerMock.getLeader()).thenReturn(leader);
    sut.reply(parameters);

    verify(transactionHandlerMock, times(0)).removeFromPlayersList(any());
  }

  @Test
  void newLeaderReturnsCorrectInfo() {
    String expectedMessage = "500: received new leader 600";

    String leader = "600";
    String player = "500";

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);
    parameters.add(ProtocolMessage.NEW_LEADER);
    parameters.add(leader);

    when(transactionHandlerMock.getPlayer()).thenReturn(player);
    when(transactionHandlerMock.getLeader()).thenReturn(leader);
    List<String> response = sut.reply(parameters);

    assertEquals(expectedMessage, response.get(0));
  }

  @Test
  void newLeaderVerifyNewLeaderAndEndElection() {
    String leader = "600";
    String player = "500";

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.LEADER_ELECTION);
    parameters.add(ProtocolMessage.NEW_LEADER);
    parameters.add(leader);

    when(transactionHandlerMock.getPlayer()).thenReturn(player);
    when(transactionHandlerMock.getLeader()).thenReturn(leader);
    sut.reply(parameters);

    verify(transactionHandlerMock).setLeader(any());
    verify(transactionHandlerMock).setElectionFlag(false);
  }
}
