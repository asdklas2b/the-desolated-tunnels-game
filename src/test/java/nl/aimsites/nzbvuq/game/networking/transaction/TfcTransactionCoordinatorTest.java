package nl.aimsites.nzbvuq.game.networking.transaction;

import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.inputhandler.actions.MoveAction;
import nl.aimsites.nzbvuq.game.networking.TransactionHandler;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import nl.aimsites.nzbvuq.rpc.RPC;
import nl.aimsites.nzbvuq.rpc.message.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


class TfcTransactionCoordinatorTest {
  @Mock
  private TransactionHandler transactionHandler;

  @Mock
  private RPC rpcMock;

  @InjectMocks
  private TfcTransactionCoordinator sut;

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);
    sut = new TfcTransactionCoordinator(transactionHandler);
  }

  @Test
  void phaseOnePrepareMessage() {
    String expectedMessage = "PHASE_ONE_COMPLETED";
    Action action = new Action(MoveAction.class, "n", "player", new Point(1, 1));

    ArrayList<String> players = new ArrayList<>() {
      {
        add("ae4e0390-e1d9-4206-8c3d-1c19665f4371");
        add("ae4e0390-e1d9-4206-8c3d-1c19665f4384");
      }
    };

    when(transactionHandler.getPlayers()).thenReturn(players);
    when(transactionHandler.getRpc()).thenReturn(rpcMock);
    when(rpcMock.sendRequest(anyString(), any())).thenReturn(CompletableFuture.completedFuture(new Response("5", Collections.singletonList("PREPARE"))));

    assertEquals(expectedMessage, sut.phaseOne(action));
  }

  @Test
  void phaseTwoPrepareToCommitMessage() {
    String expectedMessage = "PHASE_TWO_COMPLETED";
    Action action = new Action(MoveAction.class, "n", "player", new Point(1, 1));

    ArrayList<String> players = new ArrayList<>() {
      {
        add("ae4e0390-e1d9-4206-8c3d-1c19665f4371");
        add("ae4e0390-e1d9-4206-8c3d-1c19665f4384");
      }
    };

    when(transactionHandler.getPlayers()).thenReturn(players);
    when(transactionHandler.getRpc()).thenReturn(rpcMock);
    when(rpcMock.sendRequest(anyString(), any())).thenReturn(CompletableFuture.completedFuture(new Response("5", Collections.singletonList("PREPARE"))));

    assertEquals(expectedMessage, sut.phaseTwo(action));
  }

  @Test
  void phaseThreeCommitMessage() {
    String expectedMessage = "PHASE_THREE_COMPLETED";
    Action action = new Action(MoveAction.class, "n", "player", new Point(1, 1));

    ArrayList<String> players = new ArrayList<>() {
      {
        add("ae4e0390-e1d9-4206-8c3d-1c19665f4371");
        add("ae4e0390-e1d9-4206-8c3d-1c19665f4384");
      }
    };

    when(transactionHandler.getPlayers()).thenReturn(players);
    when(transactionHandler.getRpc()).thenReturn(rpcMock);
    when(rpcMock.sendRequest(anyString(), any())).thenReturn(CompletableFuture.completedFuture(new Response("5", Collections.singletonList("PREPARE"))));

    assertEquals(expectedMessage, sut.phaseThree(action));
  }

}
