package nl.aimsites.nzbvuq.game.networking.transaction;

import nl.aimsites.nzbvuq.game.entities.character.Player;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.inputhandler.actions.MoveAction;
import nl.aimsites.nzbvuq.game.networking.TransactionHandler;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import nl.aimsites.nzbvuq.rpc.RPC;
import nl.aimsites.nzbvuq.rpc.message.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;


import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

class TransactionCoordinatorTest {

  @Mock
  private TransactionHandler transactionHandlerMock;

  @Mock
  private RPC rpcMock;

  @InjectMocks
  private TransactionCoordinator sut;

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);
    sut = new TransactionCoordinator(transactionHandlerMock);

    var player = "1";
    ArrayList<String> players = new ArrayList<>() {{
      add("");
      add("");
    }};
    AtomicInteger i = new AtomicInteger(2);
    players.forEach(x -> x = "" + i.getAndIncrement());

    when(transactionHandlerMock.getPlayer()).thenReturn(player);
    when(transactionHandlerMock.getPlayers()).thenReturn(players);
    when(transactionHandlerMock.getRpc()).thenReturn(rpcMock);

  }

  @Test
  void testActionSendToLeaderReturnTrue() {
    Action action = new Action(MoveAction.class, "n", "player", new Point(1, 1));

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add(ProtocolMessage.ACTION);

    when(transactionHandlerMock.getLeader()).thenReturn("");
    when(rpcMock.sendRequest(anyString(), any())).thenReturn(CompletableFuture.completedFuture(new Response("5", Collections.singletonList("STATUS: OK"))));

    assertTrue(sut.manageTransaction(action));
  }

  @Test
  void testActionSendToLeaderFalse() {
    Action action = new Action(MoveAction.class, "n", "player", new Point(1, 1));

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add(ProtocolMessage.ACTION);

    when(transactionHandlerMock.getLeader()).thenReturn("");
    when(rpcMock.sendRequest(anyString(), any())).thenReturn(CompletableFuture.completedFuture(new Response("5", Collections.singletonList("STATUS: ERR"))));

    assertFalse(sut.manageTransaction(action));
  }

  @Test
  void testChooseTheLeaderLeaderElectionParticipate() {
    when(rpcMock.sendRequest(anyString(), any())).thenReturn(CompletableFuture.completedFuture(new Response("5", Collections.singletonList(ProtocolMessage.PARTICIPATE_ELECTION))));
    when(transactionHandlerMock.getElectionFlag()).thenReturn(false);

    sut.chooseTheLeaderAlgorithm(ProtocolMessage.LEADER_ELECTION);
    verify(transactionHandlerMock, times(3)).getRpc();

  }

  @Test
  void testChooseTheLeaderLeaderElectionNoParticipate() {
    when(rpcMock.sendRequest(anyString(), any())).thenReturn(CompletableFuture.completedFuture(new Response("5", Collections.singletonList("STATUS: OK"))));
    when(transactionHandlerMock.getElectionFlag()).thenReturn(false);
    when(transactionHandlerMock.getLeader()).thenReturn("");


    sut.chooseTheLeaderAlgorithm(ProtocolMessage.LEADER_ELECTION);
    verify(transactionHandlerMock, times(3)).getRpc();

  }

  @Test
  void testChooseTheLeaderLeaderElectionInProgress() {
    when(transactionHandlerMock.getElectionFlag()).thenReturn(true);

    sut.chooseTheLeaderAlgorithm(ProtocolMessage.LEADER_ELECTION);
    verifyNoInteractions(transactionHandlerMock.getRpc());
  }

  @Test
  void testChooseTheLeaderCheckLeaderNoLeader() {
    when(rpcMock.sendRequest(anyString(), any())).thenReturn(CompletableFuture.completedFuture(new Response("5", Collections.singletonList("STATUS: OK"))));
    when(transactionHandlerMock.getElectionFlag()).thenReturn(false);

    sut.chooseTheLeaderAlgorithm(ProtocolMessage.CHECK_LEADER);
    verify(transactionHandlerMock, times(2)).getRpc();

  }

  @Test
  void testChooseTheLeaderCheckLeaderWithLeader() {
    when(rpcMock.sendRequest(anyString(), any())).thenReturn(CompletableFuture.completedFuture(new Response("5", Collections.singletonList("STATUS: OK"))));
    when(transactionHandlerMock.getElectionFlag()).thenReturn(false);
    when(transactionHandlerMock.getLeader()).thenReturn("");

    sut.chooseTheLeaderAlgorithm(ProtocolMessage.CHECK_LEADER);
    verify(transactionHandlerMock, times(1)).getRpc();
  }
}