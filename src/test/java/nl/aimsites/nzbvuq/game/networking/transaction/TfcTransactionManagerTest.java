package nl.aimsites.nzbvuq.game.networking.transaction;

import com.google.gson.Gson;
import nl.aimsites.nzbvuq.game.graph.Point;
import nl.aimsites.nzbvuq.game.inputhandler.actions.MoveAction;
import nl.aimsites.nzbvuq.game.networking.TransactionHandler;
import nl.aimsites.nzbvuq.game.networking.dto.Action;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

class TfcTransactionManagerTest {

  @Mock
  private TransactionHandler transactionHandler;
  private Gson gson;

  @InjectMocks
  private TfcTransactionManager sut;

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);
    sut = new TfcTransactionManager(transactionHandler);
    gson = new Gson();
  }

  @Test
  void threePhaseCommitIsStarted() {
    String expectedMessage = "STATUS: OK";

    Action action = new Action(MoveAction.class, "n", "player", new Point(1, 1));

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add(ProtocolMessage.ACTION);
    parameters.add(gson.toJson(action));

    when(transactionHandler.canExecuteAction(any())).thenReturn(true);
    List<String> response = sut.reply(parameters);

    assertEquals(expectedMessage, response.get(0));
  }

  @Test
  void threePhaseCommitIsAlreadyExecuted() {
    String expectedMessage = "STATUS: ALREADY_EXECUTED";

    Action action = new Action(MoveAction.class, "n", "player", new Point(1, 1));

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add(ProtocolMessage.ACTION);
    parameters.add(gson.toJson(action));

    when(transactionHandler.canExecuteAction(any())).thenReturn(false);
    List<String> response = sut.reply(parameters);

    assertEquals(expectedMessage, response.get(0));
  }

  @Test
  void phaseOneRepliesVoteCommit() {
    String expectedMessage = "VOTE_COMMIT";
    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add(ProtocolMessage.PREPARE);

    List<String> response = sut.reply(parameters);

    assertEquals(expectedMessage, response.get(0));
  }

  @Test
  void phaseTwoRepliesReadyToCommit() {
    String expectedMessage = "READY_TO_COMMIT";

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add(ProtocolMessage.PREPARE_TO_COMMIT);

    List<String> response = sut.reply(parameters);

    assertEquals(expectedMessage, response.get(0));
  }

  @Test
  void phaseThreeRepliesCommitted() {
    Action action = new Action(MoveAction.class, "n", "player", new Point(1, 1));

    String expectedMessage = "COMMITTED";

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add(ProtocolMessage.GLOBAL_COMMIT);
    parameters.add(gson.toJson(action));

    List<String> response = sut.reply(parameters);

    assertEquals(expectedMessage, response.get(0));
  }

  @Test
  void phaseRepliesAborted() {
    Action action = new Action(MoveAction.class, "n", "player", new Point(1, 1));

    String expectedMessage = "ABORTED";

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add(ProtocolMessage.GLOBAL_ABORT);
    parameters.add(gson.toJson(action));

    List<String> response = sut.reply(parameters);

    assertEquals(expectedMessage, response.get(0));
  }

  @Test
  void unknownMessageRepliesVoteAbort() {
    Action action = new Action(MoveAction.class, "n", "player", new Point(1, 1));

    String expectedMessage = "VOTE_ABORT";

    List<String> parameters = new ArrayList<>();
    parameters.add(ProtocolMessage.THREE_PHASE);
    parameters.add("UNKNOWN_MESSAGE");
    parameters.add(gson.toJson(action));

    List<String> response = sut.reply(parameters);

    assertEquals(expectedMessage, response.get(0));
  }
}
