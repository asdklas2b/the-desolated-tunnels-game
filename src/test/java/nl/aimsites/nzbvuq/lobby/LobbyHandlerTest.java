package nl.aimsites.nzbvuq.lobby;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import nl.aimsites.nzbvuq.bootstrapper.Bootstrapper;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
import nl.aimsites.nzbvuq.lobby.exceptions.LobbyException;
import nl.aimsites.nzbvuq.rpc.RPC;
import nl.aimsites.nzbvuq.rpc.RPCListener;
import nl.aimsites.nzbvuq.rpc.message.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static java.lang.reflect.Modifier.TRANSIENT;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

class LobbyHandlerTest {

  private LobbyHandler sut;

  private RPC mRPC;
  private LobbyManager mLobbyManager;
  private Bootstrapper mBootstrapper;
  private LobbyContract mLobby;

  private Gson GSON;
  private List<String> stringList;

  @BeforeEach
  void setup() {
    mRPC = Mockito.mock(RPCListener.class);
    mLobbyManager = Mockito.mock(LobbyManager.class);
    mBootstrapper = Mockito.mock(Bootstrapper.class);
    mLobby = Mockito.mock(Lobby.class);

    sut = new LobbyHandler(mRPC, mLobbyManager, mBootstrapper);

    GSON = new GsonBuilder()
      .excludeFieldsWithoutExposeAnnotation()
      .excludeFieldsWithModifiers(TRANSIENT) // STATIC|TRANSIENT in the default configuration
      .create();
  }

  @Test
  void constructorWorks() {
    var expected = sut;

    var actual = new LobbyHandler(mRPC, mLobbyManager, mBootstrapper);

    Assertions.assertEquals(expected.getClass(), actual.getClass());
  }

  @Test
  void sendDisbandCallsMethods() {
    var nTimesCalled = 1;
    stringList = List.of("Identifier");
    Mockito.when(mBootstrapper.getRelatives()).thenReturn(stringList);
    Mockito.when(mRPC.sendRequest(any(), any())).thenReturn(null);

    sut.sendDisband("LobbyIdentifier");

    Mockito.verify(mBootstrapper, times(nTimesCalled)).getRelatives();
    Mockito.verify(mRPC, times(nTimesCalled)).sendRequest(any(), any());
  }

  @Test
  void receiveDisbandCallsMethod() {
    var nTimesCalled = 1;
    stringList = List.of(GSON.toJson(mLobby));
    Mockito.doNothing().when(mLobbyManager).receiveDisband(any());

    sut.receiveDisband(stringList);

    Mockito.verify(mLobbyManager, times(nTimesCalled)).receiveDisband(any());
  }

  @Test
  void sendNewParticipantCallsMethod() {
    var nTimesCalled = 1;
    Mockito.when(mRPC.sendRequest(any(), any())).thenReturn(null);

    sut.sendNewParticipant("Destination", "Payload");

    Mockito.verify(mRPC, times(nTimesCalled)).sendRequest(any(), any());
  }

  @Test
  void receiveNewParticipantCallsMethod() {
    var nTimesCalled = 1;
    var gson = new Gson();
    var lobbyUser = new LobbyUser("Identifier");
    var arguments = gson.toJson(lobbyUser);
    Mockito.doNothing().when(mLobbyManager).receiveUpdate(any());

    sut.receiveNewParticipant(List.of(arguments));

    Mockito.verify(mLobbyManager, times(nTimesCalled)).receiveNewParticipant(any());
  }

  @Test
  void sendUpdateCallsMethods() {
    var nTimesCalled = 1;
    stringList = List.of("Identifier");
    Mockito.when(mBootstrapper.getRelatives()).thenReturn(stringList);
    Mockito.when(mRPC.sendRequest(any(), any())).thenReturn(null);

    sut.sendUpdate("Update");

    Mockito.verify(mBootstrapper, times(nTimesCalled)).getRelatives();
    Mockito.verify(mRPC, times(nTimesCalled)).sendRequest(any(), any());
  }

  @Test
  void receiveUpdateCallsMethod() {
    var nTimesCalled = 1;
    stringList = List.of(GSON.toJson(mLobby));
    Mockito.doNothing().when(mLobbyManager).receiveUpdate(any());

    sut.receiveUpdate(stringList);

    Mockito.verify(mLobbyManager, times(nTimesCalled)).receiveUpdate(any());
  }

  @Test
  void sendLobbyCallsMethods() {
    var nTimesCalled = 1;
    stringList = List.of("Identifier");
    Mockito.when(mBootstrapper.getRelatives()).thenReturn(stringList);
    Mockito.when(mRPC.sendRequest(any(), any())).thenReturn(null);

    sut.sendLobby("Lobby");

    Mockito.verify(mBootstrapper, times(nTimesCalled)).getRelatives();
    Mockito.verify(mRPC, times(nTimesCalled)).sendRequest(any(), any());
  }

  @Test
  void receiveLobbyCallsMethod() {
    var nTimesCalled = 1;
    stringList = List.of(GSON.toJson(mLobby));
    Mockito.doNothing().when(mLobbyManager).receiveUpdate(any());

    sut.receiveLobby(stringList);

    Mockito.verify(mLobbyManager, times(nTimesCalled)).receiveUpdate(any());
  }

  @Test
  void sendStartGameCallsMethod() {
    var nTimesCalled = 1;
    var lobbyPlayer = new LobbyUser("Player");
    stringList = List.of("Player");
    Mockito.when(mRPC.sendRequest(any(), any())).thenReturn(null);

    Lobby lobby = new Lobby("LobbyIdentifier", "LobbyName", 2);
    lobby.addPlayer(lobbyPlayer);

    sut.sendStartGame(lobby, gameState.getAllPlayersInLobby());

    Mockito.verify(mRPC, times(nTimesCalled)).sendRequest(any(), any());
  }

  @Test
  void receiveStartGameCallsMethod() {
    try {
      var nTimesCalled = 1;
      stringList = List.of(GSON.toJson(mLobby));
      Mockito.doNothing().when(mLobbyManager).receiveStartGame(any());

      sut.receiveStartGame(stringList);

      Mockito.verify(mLobbyManager, times(nTimesCalled)).receiveStartGame(any());
    } catch (LobbyException e) {
      fail();
    }
  }

  @Test
  void sendLobbyRequestCallsMethods() throws ExecutionException, InterruptedException, LobbyException {
    var nTimesCalled = 1;
    var userSelf = new LobbyUser("Player");
    stringList = List.of("Identifier");

    List<LobbyContract> otherLobbies = new ArrayList<>();
    var lobbiesAsResponse = new Response("id",  otherLobbies.stream().map(GSON::toJson).collect(Collectors.toList()));
    var response = Mockito.mock(CompletableFuture.class);

    Mockito.when(mBootstrapper.getRelatives()).thenReturn(stringList);
    Mockito.when(response.get()).thenReturn(lobbiesAsResponse);
    Mockito.when(mRPC.sendRequest(any(), any())).thenReturn(response);

    sut.sendLobbiesRequest(userSelf.getIdentifier());

    Mockito.verify(mBootstrapper, times(nTimesCalled)).getRelatives();
    Mockito.verify(mRPC, times(nTimesCalled)).sendRequest(any(), any());
  }

  @Test
  void receiveLobbiesRequestCallsMethod() {
    var nTimesCalled = 1;
    stringList = List.of("parameters");

    sut.receiveLobbiesRequest(stringList);
    Mockito.verify(mLobbyManager, times(nTimesCalled)).getLobbies();
  }
}