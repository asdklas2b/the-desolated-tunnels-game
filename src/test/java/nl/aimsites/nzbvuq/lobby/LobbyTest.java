package nl.aimsites.nzbvuq.lobby;

import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

class LobbyTest {

  private LobbyContract sut;

  @BeforeEach
  void setup() {
    sut = new Lobby("Identifier", "Name", 2);
  }

  @Test
  void getIdentifier() {
    var expected = "Identifier";

    var actual = sut.getIdentifier();

    Assertions.assertEquals(expected, actual);
  }

  @Test
  void getName() {
    var expected = "Name";

    var actual = sut.getName();

    Assertions.assertEquals(expected, actual);
  }

  @Test
  void getMaxPlayers() {
    var expected = 2;

    var actual = sut.getMaxPlayers();

    Assertions.assertEquals(expected, actual);
  }

  @Test
  void getPlayersCount() {
    var expected = 2;
    var player = new LobbyUser("Player");
    var player2 = new LobbyUser("Player2");
    sut.addPlayer(player);
    sut.addPlayer(player2);

    var actual = sut.getPlayersCount();

    Assertions.assertEquals(expected, actual);
  }

  @Test
  void getPlayers() {
    var player = new LobbyUser("Player");
    var expected = List.of(player);
    sut.addPlayer(player);

    var actual = sut.getPlayers();

    Assertions.assertEquals(expected, actual);
  }

  @Test
  void addPlayer() {
    var player = new LobbyUser("Player");
    var expected = List.of(player);

    sut.addPlayer(player);
    var actual = sut.getPlayers();

    Assertions.assertEquals(expected, actual);
  }

  @Test
  void isStarted() {
    var expected = false;

    var actual = sut.isStarted();

    Assertions.assertEquals(actual, expected);
  }

  @Test
  void setStarted() {
    sut.setStarted(true);

    var actual = sut.isStarted();

    Assertions.assertTrue(actual);
  }
}