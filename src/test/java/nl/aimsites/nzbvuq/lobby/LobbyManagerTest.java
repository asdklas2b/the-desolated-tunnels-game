package nl.aimsites.nzbvuq.lobby;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import nl.aimsites.nzbvuq.bootstrapper.Bootstrapper;
import nl.aimsites.nzbvuq.lobby.contracts.LobbyContract;
import nl.aimsites.nzbvuq.lobby.exceptions.LobbyException;
import nl.aimsites.nzbvuq.rpc.RPC;
import nl.aimsites.nzbvuq.rpc.RPCListener;
import nl.aimsites.nzbvuq.rpc.message.Request;
import nl.aimsites.nzbvuq.rpc.message.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static java.lang.reflect.Modifier.TRANSIENT;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class LobbyManagerTest {

  private LobbyManager sut;

  private RPC mRPC;
  private LobbyContract mLobby, mLobby2;
  private List<String> mRelatives = new ArrayList<>();

  private final ArgumentCaptor<String> destinationArgumentCaptor = ArgumentCaptor.forClass(String.class);
  private final ArgumentCaptor<Request> requestArgumentCaptor = ArgumentCaptor.forClass(Request.class);

  private Gson GSON = new GsonBuilder()
    .excludeFieldsWithoutExposeAnnotation()
    .excludeFieldsWithModifiers(TRANSIENT)
    .create();

  @BeforeEach
  void setup() {
    MockitoAnnotations.openMocks(this);
    Bootstrapper mBootstrapper;
    mRPC = Mockito.mock(RPCListener.class);
    mBootstrapper = Mockito.mock(Bootstrapper.class);
    mLobby = Mockito.mock(Lobby.class);
    mLobby2 = Mockito.mock(Lobby.class);


    mRelatives.add("Relative12");
    mRelatives.add("Relative14");

    when(mBootstrapper.getRelatives()).thenReturn(mRelatives);
    when(mRPC.getIdentifier()).thenReturn("Relative");

    sut = new LobbyManager(mRPC, mBootstrapper);
  }

  @Test
  void getLobbiesReturnsLobbies() {
    var expected = new ArrayList<LobbyContract>();

    var lobby1 = new Lobby("identifier", "name", 10);
    var lobby2 = new Lobby("identifier2", "name2", 4);
    var lobby3 = new Lobby("identifier3", "name3", 6);

    expected.add(lobby1);
    expected.add(lobby2);
    expected.add(lobby3);

    expected.forEach(lobby -> sut.receiveUpdate(lobby));

    var result = sut.getLobbies();

    assertEquals(expected, result);

    assertTrue(result.contains(lobby1));
    assertTrue(result.contains(lobby2));
    assertTrue(result.contains(lobby3));
    assertEquals(3, result.size());
  }

  @Test
  void getLobbiesReturnsNone() {
    var expected = new ArrayList<LobbyContract>();

    var result = sut.getLobbies();

    assertEquals(expected, result);
    assertEquals(0, result.size());
  }

  @Test
  void joinCallsMethods() {
    var lobby = new Lobby("identifier", "name", 10);

    sut.join(lobby);

    verify(mRPC, times(1)).sendRequest(destinationArgumentCaptor.capture(), requestArgumentCaptor.capture());
  }

  @Test
  void joinFullLobby() {
    var lobby = new Lobby("identifier", "name", 0);

    sut.join(lobby);

    verify(mRPC, times(0)).sendRequest(any(), any());
  }

  @Test
  void joinWhileInAnotherLobby() {
    var lobby = new Lobby("identifier", "name", 10);
    var lobby2 = new Lobby("identifier2", "name2", 10);
    lobby.addPlayer(new LobbyUser(mRPC.getIdentifier())); // Puts the current user in lobby
    sut.receiveUpdate(lobby); // Add the lobby with the current user in it to its known list of lobbies

    sut.join(lobby2); // Attempt to join a different lobby

    verify(mRPC, times(0)).sendRequest(any(), any()); // Shouldn't join another lobby
  }

  @Test
  void createCallsMethods() {
    sut.create("LobbyName", 2);

    // Verify it was sent to all relatives
    verify(mRPC, times(mRelatives.size())).sendRequest(destinationArgumentCaptor.capture(), requestArgumentCaptor.capture());
  }

  @Test
  void leaveCallsMethods() {
    var lobby = new Lobby("Relative", "name", 10);
    lobby.addPlayer(new LobbyUser(mRPC.getIdentifier()));
    sut.receiveUpdate(lobby);

    sut.leave();

    // Verify it was sent to all relatives
    verify(mRPC, times(mRelatives.size())).sendRequest(destinationArgumentCaptor.capture(), requestArgumentCaptor.capture());
  }

  @Test
  void leaveLobbyNotJoined() {
    var lobby = new Lobby("SomeoneElse", "name", 10);
    lobby.addPlayer(new LobbyUser("SomeoneElse"));
    sut.receiveUpdate(lobby);

    sut.leave();

    // Verify no requests were sent
    verify(mRPC, times(0)).sendRequest(any(), any());
  }

  @Test
  void disband() {
    var lobby = new Lobby(mRPC.getIdentifier(), "name", 10);
    lobby.addPlayer(new LobbyUser(mRPC.getIdentifier()));
    sut.receiveUpdate(lobby);

    sut.disband();

    // Verify it was sent to all relatives
    verify(mRPC, times(mRelatives.size())).sendRequest(destinationArgumentCaptor.capture(), requestArgumentCaptor.capture());
  }

  @Test
  void disbandLobbyWithoutOwnership() {
    var lobby = new Lobby("SomeoneElse", "name", 10);
    lobby.addPlayer(new LobbyUser(mRPC.getIdentifier()));
    sut.receiveUpdate(lobby);

    sut.disband();

    // Verify no requests were sent
    verify(mRPC, times(0)).sendRequest(any(), any());
  }

  @Test
  void receiveNewParticipant() {
    var lobby = new Lobby(mRPC.getIdentifier(), "name", 10);
    lobby.addPlayer(new LobbyUser(mRPC.getIdentifier()));
    sut.receiveUpdate(lobby);

    var player = new LobbyUser("Player1");

    sut.receiveNewParticipant(player);

    // Verify it was sent to all relatives
    verify(mRPC, times(mRelatives.size())).sendRequest(destinationArgumentCaptor.capture(), requestArgumentCaptor.capture());

    assertTrue(sut.getOwnLobby().getPlayers().contains(player));
    assertTrue(sut.getOwnLobby().getPlayers()
      .stream().anyMatch(p -> p.getIdentifier().equals(player.getIdentifier())));
  }

  @Test
  void receiveNewParticipantWithoutOwnedLobby() {
    var lobby = new Lobby("NotMine", "name", 10);
    lobby.addPlayer(new LobbyUser(mRPC.getIdentifier()));
    sut.receiveUpdate(lobby);

    var player = new LobbyUser("Player1");

    sut.receiveNewParticipant(player);

    // Verify no requests were sent
    verify(mRPC, times(0)).sendRequest(any(), any());

    assertNull(sut.getOwnLobby());
  }

  @Test
  void receiveNewParticipantLobbyIsFull() {
    var lobby = new Lobby(mRPC.getIdentifier(), "name", 1);
    lobby.addPlayer(new LobbyUser(mRPC.getIdentifier()));
    sut.receiveUpdate(lobby);

    var player = new LobbyUser("Player1");
    player.setName("Name");

    sut.receiveNewParticipant(player);

    // Verify no requests were sent
    verify(mRPC, times(0)).sendRequest(any(), any());

    assertFalse(sut.getOwnLobby().getPlayers().contains(player));
  }

  @Test
  void receiveUpdate() {
    var lobby = new Lobby("identifierLobby", "name", 1);
    lobby.addPlayer(new LobbyUser("identifierLobby"));

    assertFalse(sut.getLobbies().contains(lobby));

    sut.receiveUpdate(lobby);

    assertTrue(sut.getLobbies().contains(lobby));
  }

  @Test
  void receiveUpdateOnJoin() {
    var lobby = new Lobby("identifierLobby", "name", 1);
    lobby.addPlayer(new LobbyUser("identifierLobby"));
    lobby.addPlayer(new LobbyUser(mRPC.getIdentifier()));

    assertFalse(sut.getLobbies().contains(lobby));

    sut.receiveUpdate(lobby);

    assertTrue(sut.getLobbies().contains(lobby));
  }

  @Test
  void receiveDisband() {
    var lobby = new Lobby("identifierLobby", "name", 1);
    lobby.addPlayer(new LobbyUser("identifierLobby"));
    lobby.addPlayer(new LobbyUser("identifierLobby2"));
    sut.receiveUpdate(lobby);

    assertTrue(sut.getLobbies().contains(lobby));

    sut.receiveDisband(lobby);

    assertFalse(sut.getLobbies().contains(lobby));
  }

  @Test
  void receiveStartGameCallsMethods() {
    var relative = new LobbyUser("Relative");

    try {
      Mockito.when(mLobby.getIdentifier()).thenReturn("Relative");
      Mockito.when(mLobby.getPlayers()).thenReturn(List.of(relative));
      sut.receiveUpdate(mLobby);

      sut.receiveStartGame(mLobby);

      verify(mLobby, times(1)).setStarted(true);
    } catch (LobbyException e) {
      fail();
    }
  }

  @Test
  void receiveStartGameThrowsLobbyException() {
    var relative = new LobbyUser("Relative");
    Mockito.when(mLobby.getIdentifier()).thenReturn("Relative");
    Mockito.when(mLobby.getPlayers()).thenReturn(List.of(relative));
    sut.receiveUpdate(mLobby);
    Mockito.when(mLobby2.getIdentifier()).thenReturn("NonRelative");

    Assertions.assertThrows(LobbyException.class, () -> sut.receiveStartGame(mLobby2));
  }

  @Test
  void getOwnLobbyReturnsOwnLobby() {
    var expected = mLobby;
    Mockito.when(mLobby.getIdentifier()).thenReturn("Relative");
    sut.receiveUpdate(mLobby);

    var actual = sut.getOwnLobby();

    assertEquals(expected, actual);
  }

  @Test
  void getOwnLobbyReturnsNull() {
    Mockito.when(mLobby.getIdentifier()).thenReturn("NonRelative");
    sut.receiveUpdate(mLobby);

    var actual = sut.getOwnLobby();

    assertNull(actual);
  }

  @Test
  void getJoinedLobbyReturnsJoinedLobby() {
    var relative = new LobbyUser("Relative");
    var expected = mLobby;
    Mockito.when(mLobby.getIdentifier()).thenReturn("Relative");
    Mockito.when(mLobby.getPlayers()).thenReturn(List.of(relative));
    sut.receiveUpdate(mLobby);

    var actual = sut.getJoinedLobby();

    Assertions.assertEquals(expected, actual);
  }

  @Test
  void getJoinedLobbyReturnsJoinedNull() {
    var nonRelative = new LobbyUser("NonRelative");
    Mockito.when(mLobby.getIdentifier()).thenReturn("Relative");
    Mockito.when(mLobby.getPlayers()).thenReturn(List.of(nonRelative));
    sut.receiveUpdate(mLobby);

    var actual = sut.getJoinedLobby();

    Assertions.assertNull(actual);
  }

  /**
   * If there are no lobbies in my list, it should add the already existing lobbies to it.
   *
   * @throws ExecutionException
   * @throws InterruptedException
   * @author Fedor Soffers (FKA.Soffers@student.han.nl)
   */
  @Test
  void getRequestedLobbiesReturnsExistingLobbies() throws ExecutionException, InterruptedException, LobbyException {
    // The already excisting lobbies.
    var lobby1 = new Lobby("identifier", "name", 10);
    var lobby2 = new Lobby("identifier2", "name2", 4);

    List<String> otherLobbies = new ArrayList<>();
    otherLobbies.add(new Gson().toJson(lobby1));
    otherLobbies.add(new Gson().toJson(lobby2));

    // Expected result should contain the 2 already existing lobbies and the new lobby.
    var expected = new ArrayList<LobbyContract>();
    expected.add(lobby1);
    expected.add(lobby2);

    // Response
    var lobbiesAsResponse = new Response(mRelatives.get(0), otherLobbies);

    // Mock
    var response = Mockito.mock(CompletableFuture.class);
    Mockito.when(mRPC.sendRequest(anyString(), any(Request.class))).thenReturn(response);
    Mockito.when(response.get()).thenReturn(lobbiesAsResponse);

    // Results, where resultGet equals 0 since these are the lobbies I have in my list
    var resultGet = sut.getLobbies();
    assertEquals(0, resultGet.size());

    var result = sut.requestLobbies();
    assertEquals(2, result.size());
  }

  /**
   * If I have made a lobby, it should add the already existing lobbies to the my list.
   *
   * @throws ExecutionException
   * @throws InterruptedException
   * @author Fedor Soffers (FKA.Soffers@student.han.nl)
   */
  @Test
  void getRequestedLobbiesAddsExistingLobbies() throws ExecutionException, InterruptedException, LobbyException {
    // The already excisting lobbies.
    var lobby1 = new Lobby("identifier", "name", 10);
    var lobby2 = new Lobby("identifier2", "name2", 10);

    List<String> otherLobbies = new ArrayList<>();
    otherLobbies.add(new Gson().toJson(lobby1));
    otherLobbies.add(new Gson().toJson(lobby2));

    // Expected result should contain the 2 already existing lobbies and the new lobby.
    var expected = new ArrayList<LobbyContract>();
    var lobbyNew = new Lobby("identifierNEW", "lobbyTest", 10);
    expected.add(lobbyNew);
    expected.add(lobby1);
    expected.add(lobby2);

    // Create a new lobby
    sut.create("lobbyTest", 10);

    // Response
    var lobbiesAsResponse = new Response(mRelatives.get(0), otherLobbies);

    // Mock
    var response = Mockito.mock(CompletableFuture.class);
    Mockito.when(mRPC.sendRequest(anyString(), any(Request.class))).thenReturn(response);
    Mockito.when(response.get()).thenReturn(lobbiesAsResponse);

    // Results, where resultGet equals 1 since I have 1 Lobby made and is in my list.
    // The assert needs to happen before requestLobbies, else the lobbies are added to the list and getLobbies() returns the new list
    var resultGet = sut.getLobbies();
    assertEquals(1, resultGet.size());

    var result = sut.requestLobbies();
    assertEquals(3, result.size());

    assertEquals(expected.size(), result.size());
    assertEquals(sut.getLobbies().size(), result.size());
  }
}