package nl.aimsites.nzbvuq.bootstrapper;

import nl.aimsites.nzbvuq.rpc.RPC;
import nl.aimsites.nzbvuq.rpc.message.Request;
import nl.aimsites.nzbvuq.rpc.message.Response;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class RPCBootstrapperTest {
  @Mock RPC rpc;
  @Mock CompletableFuture<Response> responseCompletableFuture;
  RPCBootstrapper rpcBootstrapper;
  BootstrapperHandler bootstrapperHandler;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
    rpcBootstrapper = new RPCBootstrapper(rpc);
    bootstrapperHandler = new BootstrapperHandler(rpcBootstrapper);
  }

  @Test
  void addRelativeAsString() {
    List<String> expectedResult = new ArrayList<>();
    expectedResult.add("relative");

    rpcBootstrapper.addRelative("relative");

    List<String> result = rpcBootstrapper.getRelatives();

    assertArrayEquals(expectedResult.toArray(), result.toArray());
  }

  @Test
  void addRelativeAsNode() {
    List<String> expectedResult = new ArrayList<>();
    expectedResult.add("relative");

    rpcBootstrapper.addRelative(new RPCNode("relative"));

    List<String> result = rpcBootstrapper.getRelatives();

    assertArrayEquals(expectedResult.toArray(), result.toArray());
  }

  @Test
  void removeRelative() {
    List<String> expectedFirstResult = new ArrayList<>();
    expectedFirstResult.add("relative");
    List<String> expectedSecondResult = new ArrayList<>();

    rpcBootstrapper.addRelative(new RPCNode("relative"));

    List<String> firstResult = rpcBootstrapper.getRelatives();

    rpcBootstrapper.removeRelative(new RPCNode("relative"));

    List<String> secondResult = rpcBootstrapper.getRelatives();

    assertArrayEquals(expectedFirstResult.toArray(), firstResult.toArray());
    assertArrayEquals(expectedSecondResult.toArray(), secondResult.toArray());
  }

  @Test
  void start() throws InterruptedException, ExecutionException {
    Response emptyResponse = new Response(null, new ArrayList<>());
    List<String> peers = new ArrayList<>();
    peers.add("relative");
    peers.add("anotherRelative");
    Response response = new Response(null, peers);

    ArgumentCaptor<Request> requestArgumentCaptor = ArgumentCaptor.forClass(Request.class);
    when(rpc.sendRequest(anyString(), requestArgumentCaptor.capture())).thenReturn(responseCompletableFuture);
    when(rpc.getIdentifier()).thenReturn("relative");
    when(responseCompletableFuture.get()).thenReturn(emptyResponse).thenReturn(response);

    rpcBootstrapper.start();

    Thread.sleep(7000);

    verify(rpc, times(6)).sendRequest(anyString(), any());
    List<Request> capturedRequests = requestArgumentCaptor.getAllValues();
    assertEquals(3, capturedRequests.stream().filter(request ->
      request.getClassName().equals("nl.aimsites.nzbvuq.bootstrapper.BootstrapHandler") &&
      request.getMethod().equals("getPeers")).count(), "Invalid amount of requests to the bootstrapper");
    assertEquals(3, capturedRequests.stream().filter(request ->
      request.getClassName().equals(BootstrapperHandler.class.getName()) &&
      request.getMethod().equals("ping")).count(), "Invalid amount of ping request");
    assertArrayEquals(peers.toArray(), rpcBootstrapper.getRelatives().toArray());
  }

  @Test
  void ping() {
    List<String> expectedFirstResult = new ArrayList<>();
    expectedFirstResult.add("relative");
    List<String> expectedSecondResult = new ArrayList<>();
    expectedSecondResult.add("relative");
    expectedSecondResult.add("anotherRelative");

    List<String> firstParameters = new ArrayList<>();
    firstParameters.add("relative");

    List<String> secondParameters = new ArrayList<>();
    secondParameters.add("anotherRelative");

    List<String> firstResult =  bootstrapperHandler.ping(firstParameters);

    List<String> secondResult =  bootstrapperHandler.ping(secondParameters);

    assertArrayEquals(expectedFirstResult.toArray(), firstResult.toArray());
    assertArrayEquals(expectedSecondResult.toArray(), secondResult.toArray());
  }

  @Test
  void pingAddDuplicate() {
    List<String> expectedResult = new ArrayList<>();
    expectedResult.add("relative");

    List<String> parameters = new ArrayList<>();
    parameters.add("relative");

    List<String> firstResult =  bootstrapperHandler.ping(parameters);

    List<String> secondResult =  bootstrapperHandler.ping(parameters);

    assertArrayEquals(expectedResult.toArray(), firstResult.toArray());
    assertArrayEquals(expectedResult.toArray(), secondResult.toArray());
  }
}
